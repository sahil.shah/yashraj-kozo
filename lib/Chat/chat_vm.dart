// import 'dart:convert';
//
// import 'package:yashrajkozo/Login/login.dart';
// import 'package:yashrajkozo/utils/app_models.dart';
// import 'package:yashrajkozo/utils/app_singleton.dart';
// import 'package:yashrajkozo/utils/check_connection.dart';
// import 'package:yashrajkozo/utils/constants.dart';
// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:http/http.dart' as http;
//
// class ChatAPI {
//   ConnectionHelper mCH;
//
//   ChatAPI() {
//     mCH = ConnectionHelper.getInstance();
//   }
//
//   AppSingleton _singleton = AppSingleton();
//
//
//   Future<GetHistoryMsgModel> fetchHistoryData(BuildContext context, int room_id) async {
//     GetHistoryMsgModel res_d;
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String accessToken = prefs.getString('access_token');
//
//     var Headers = {
//       "Content-type" : "application/json",
//       "Accept": "application/json",
//       "authorization": accessToken ?? ""
//     };
//
//     bool con = await mCH.checkConnection();
// //    bool con = true;
//
//     if(con) {
//       try {
//
//         final res = await http.get("http://192.168.1.145:3000/chat/$room_id?organization_id=${prefs.getInt('orgId')}", headers: Headers);
//
//         print("URLs $BASE_URL");
//         switch (res.statusCode) {
//           case 200:
//           case 201:
//             final j_data = json.decode(res.body);
//             print('Res ---> ${res.body}');
//             res_d = GetHistoryMsgModel.fromJson(j_data);
//
// //            res_d.banners.removeAt(0);
// //            print("${res_d.banners[0].banner.ID}");
//           print("Type is: ${res_d.runtimeType}");
//             return res_d;
//             break;
//           case 401 :
//             Navigator.pushReplacement(
//               context,
//               PageRouteBuilder(
//                 settings: RouteSettings(name: '/login'),
//                 pageBuilder: (c, a1, a2) => LoginPage(),
//                 transitionsBuilder: (c, anim, a2, child) =>
//                     FadeTransition(opacity: anim, child: child),
//                 transitionDuration: Duration(milliseconds: 500),
//               ),
//             );
//             break;
//
//           default:
//             return GetHistoryMsgModel.buildErr(res.statusCode);
//         }
//       } catch (err) {
//         return GetHistoryMsgModel.buildErr(
//             0, message: "Somthing went wrong. Please try again later.");
//       }
//     } else {
//       return GetHistoryMsgModel.buildErr(1);
//     }
//   }
//
// }