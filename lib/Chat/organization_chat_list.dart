// import 'package:yashrajkozo/Chat/chat_rooms_list.dart';
// import 'package:yashrajkozo/Chat/chat_ui.dart';
// import 'package:yashrajkozo/utils/app_models.dart';
// import 'package:yashrajkozo/utils/custom_appbar.dart';
// import 'package:yashrajkozo/utils/err.dart';
// import 'package:yashrajkozo/utils/loader.dart';
// import 'package:flutter/material.dart';
// import 'package:socket_io_client/socket_io_client.dart' as IO;
//
// class OrganisationChatList extends StatefulWidget {
//   @override
//   _OrganisationChatListState createState() => _OrganisationChatListState();
// }
//
// class _OrganisationChatListState extends State<OrganisationChatList> {
//   final search = new TextEditingController();
//   IO.Socket socket;
//   Future<GetChatGroups> mF;
//   GetChatGroups chatGroups;
//   ChatRoomListAPI chatRoomListAPI;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     chatRoomListAPI = ChatRoomListAPI();
//     mF = chatRoomListAPI.fetchChatRoomList(context);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//         initialData: null,
//         future: mF,
//         builder: (context, snap) => _checkAPIData(context, snap));
//   }
//
//   Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
//     //ConnectionState.active = snap.connectionState
//     if (snap.hasData) {
//       // API
//       // 404
//       // catch
//       GetChatGroups _d = snap.data;
//
//       if (_d.Status ?? false) {
//         // API true
//         print("Chalo jaate hai");
//         return _buildList(_d);
//       } else if (_d.is_loading ?? false) {
//         return Loader(title: '');
//       } else {
//         return Err(
//             bar_visibility: true,
//             p_title: 'Home',
//             m: _d.Message,
//             mL: () => setState(() {
//                   _d.is_loading = true;
//                   mF = chatRoomListAPI.fetchChatRoomList(context);
//                 }));
//       }
//     } else {
//       // initial loading
//       return Loader(title: '');
//     }
//   }
//
//   Widget _buildList(GetChatGroups d) {
//
//     final h = MediaQuery.of(context).size.height;
//     return Scaffold(
//       appBar: CustomAppBar(
//         title: 'Organisations',
//       ),
//       body: ListView(
//         children: <Widget>[
//           Padding(
//             padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
//             child: Container(
// //                          margin: const EdgeInsets.symmetric(horizontal: 5),
//               decoration: BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     blurRadius: 1.0,
//                   ),
//                 ],
//                 borderRadius: BorderRadius.circular(30),
//                 color: Colors.white,
//               ),
//               padding:
//                   const EdgeInsets.symmetric(horizontal: 12 /*, vertical: 3*/),
//               child: TextField(
//                 controller: search,
//                 decoration: InputDecoration(
//                   hintText: 'Search by Project',
//                   hintStyle: TextStyle(fontSize: 15),
//                   filled: false,
//                   border: InputBorder.none,
//                   counterText: "",
//                 ),
//                 obscureText: false,
//               ),
//             ),
//           ),
//           Container(
//             child: d.rooms?.length > 0
//                 ? ListView.separated(
//                     separatorBuilder: (context, index) {
//                       return Divider(
//                         color: Colors.black,
//                       );
//                     },
//                     padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
//                     shrinkWrap: true,
//                     physics: NeverScrollableScrollPhysics(),
//                     itemCount: d.rooms.length,
//                     itemBuilder: (context, i) {
//                       return InkWell(
//                           onTap: () async {
//                             Navigator.push(
//                               context,
//                               PageRouteBuilder(
//                                 settings: RouteSettings(name: '/chat_page'),
//                                 pageBuilder: (c, a1, a2) => ChatUI(
//                                   chatGroups: d.rooms[i],
//                                 ),
//                                 transitionsBuilder: (c, anim, a2, child) =>
//                                     FadeTransition(opacity: anim, child: child),
//                                 transitionDuration: Duration(milliseconds: 500),
//                               ),
//                             );
//                           },
//                           child: Container(
//                             padding: EdgeInsets.only(top: 4),
//                             child: Row(
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: <Widget>[
//                                 Container(
//                                   child: CircleAvatar(
//                                     child: Container(
//                                         padding: EdgeInsets.all(8.0),
//                                         decoration: BoxDecoration(
//                                           borderRadius:
//                                               BorderRadius.circular(50),
//                                           color: Colors.white,
//                                           image: DecorationImage(
//                                               image: NetworkImage(
//                                                 'https://api.gokozo.com//uploads//394fab7271b796af_org.jpg',
//                                               ),
//                                               fit: BoxFit.cover),
//                                         )),
//                                     backgroundColor: Colors.transparent,
//                                   ),
//                                   height: 42,
//                                   width: 42,
//                                 ),
//                                 SizedBox(
//                                   width: 16,
//                                 ),
//                                 Column(
//                                   mainAxisAlignment:
//                                       MainAxisAlignment.spaceEvenly,
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: <Widget>[
//                                     Text(
//                                       d.rooms[i].display_name ?? "",
//                                       style: TextStyle(
//                                           fontSize: 16,
//                                           fontFamily: 'SanSemiBold'),
//                                     ),
//                                     Text(d.rooms[i].room_description ?? "")
//                                   ],
//                                 )
//                               ],
//                             ),
//                           ));
//                     })
//                 : Container(
//               height: h-100,
//                   child: Center(
//                       child: Text(
//                         "No Chats Available",
//                         style: TextStyle(fontFamily: 'SanRegular', fontSize: 15),
//                       ),
//                     ),
//                 ),
//           )
//         ],
//       ),
//     );
//   }
// }
