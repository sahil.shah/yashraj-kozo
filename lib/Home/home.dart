import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/Notifications/notification_api.dart';
import 'package:yashrajkozo/budget_alert/budget_alert.dart';
import 'package:yashrajkozo/cash_request/cash_request.dart';
import 'package:yashrajkozo/cash_request/tablet_cash_request.dart';
import 'package:yashrajkozo/create_challan/tablet_challan.dart';
import 'package:yashrajkozo/create_expense/create_expense_tablet.dart';
import 'package:yashrajkozo/create_po/tablet_po.dart';
import 'package:yashrajkozo/credit_amount.dart';
import 'package:yashrajkozo/credit_note/credit_note_tablet.dart';
import 'package:yashrajkozo/credit_note/new_cash_note.dart';
import 'package:yashrajkozo/po_review/po_review.dart';
import 'package:yashrajkozo/po_review/po_review_tablet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter_svg/svg.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/MyPO/mypo.dart';
import 'package:yashrajkozo/Notifications/notification.dart';
import 'package:yashrajkozo/Profile/get_profile_api.dart';
import 'package:yashrajkozo/Profile/profile_details.dart';
import 'package:yashrajkozo/Project_Details/project_details.dart';
import 'package:yashrajkozo/UserPO/userpo.dart';
import 'package:yashrajkozo/create_challan/create_challan.dart';
import 'package:yashrajkozo/create_expense/create_expense.dart';
import 'package:yashrajkozo/create_po/create_po.dart';
import 'package:yashrajkozo/search_page/search_page_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:location/location.dart' hide LocationAccuracy;
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


class MyHomePage extends StatefulWidget {

  int isForNotification;


  MyHomePage({Key key, this.isForNotification}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int current_index = 0;
  bool isSelected = false;

  final search = new TextEditingController();
  AppSingleton _singleton = AppSingleton();

  HomeApi homeApiObj;
  GetProfileDataApi pVM;
  Future<GetProjectModel> mF;
  final _homeScaffoldKey = GlobalKey<ScaffoldState>();

  SearchVM sVM;
  Timer _debounce;
  GetProjectModel _d;
  String s_q = "";

  //io.binarynumbers.kozo

  String APP_STORE_URL =
      'https://apps.apple.com/us/app/gokozo/id1487797528?ls=1';
  String PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=io.binarynumbers.kozo';

  NotificationApi notificationApiObj;

  int notiCount = 0;

  //Custom Logic
  List customList = [];
  GetCustomFieldModel customFieldModel;
  bool customFieldDikhao;
  GetCustomKeyModel bottomSheetContents;

  GetUserProfileModel userData;
  ConnectionHelper helper;
  double letfPad, topPad;
  ConnectionHelper mCH;

  double h,w;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // DateTime date = DateTime.parse("2020-10-10");
    // print("Date is:- $date");
    mCH = ConnectionHelper.getInstance();
    helper = ConnectionHelper.getInstance();
    customFieldDikhao = false;
    notificationApiObj = NotificationApi();
    _getLocation();
    _getNotificationCount();
    _getUserProfile();
    homeApiObj = HomeApi();
    pVM = GetProfileDataApi();
    sVM = SearchVM();
    mF = homeApiObj.fetchProjectList(context);
    versionCheck(context);
    //_getCustomExpenseKey();
    search.addListener(_onSearch);
  }

  _getLocation() async {

    double lat;
    double long;

    if(Platform.isIOS){

      final res = await http.get("https://ipapi.co/json", headers: HEADERS);
      switch (res.statusCode) {
        case 200:
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');
          print(j_data);
          lat = j_data["latitude"];
          long = j_data["longitude"];
          break;
        default:
          lat = 0.0;
          long = 0.0;
      }

/*
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      print(position);
      lat = position.latitude;
      long = position.longitude;
*/

    }else{

      Location location = new Location();

      bool _serviceEnabled;
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }

      _locationData = await location.getLocation();

      print("locationData = $_locationData");

      lat = _locationData.latitude ?? 0.0;
      long = _locationData.longitude ?? 0.0;

    }

    final coordinates = new Coordinates(
       lat, long);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;

    if (addresses.first.locality != null) {
      _singleton.currentCity = addresses.first.locality;
    }

    print("#############################################################");

    print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first
        .subAdminArea},${first.addressLine}, ${first.featureName},${first
        .thoroughfare}, ${first.subThoroughfare}');
    return first;
  }


  _getUserProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _singleton.userName = prefs.getString('userName');
    _singleton.access_token = prefs.getString('access_token');


    // get all approver list
    userData = await pVM.fetchUserProfileData(context);

    if (userData.Status ?? false) {
      print("******************getProfile done");
      print("UserId = ${userData.data.user_id}");
      prefs.setInt('userId', userData.data.user_id);
      prefs.setString('userImage', userData.data.user_pic);
      _singleton.userId = userData.data.user_id;
      _singleton.userOrgList.clear();
      _singleton.userLvl1List.clear();
      _singleton.userOrgList.addAll(userData.data.organization_data);

      _singleton.userLvl1List =
          userData.data.dashboard_user.where((p) => p.user_level == 1).toList();

      print(_singleton.userLvl1List);

      for (final e in _singleton.userOrgList.where((k) =>
      k.organization_id == prefs.getInt('orgId'))) {
        e.isSelected = true;
        print(e);
      }

      setState(() {});
    } else {}
  }

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      _getUserProfile();
      _getNotificationCount();
      mF = homeApiObj.fetchProjectList(context);
    });
    return null;
  }

  nav() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  @override
  Widget build(BuildContext context) {
     w = MediaQuery.of(context).size.width;
     h = MediaQuery.of(context).size.height;
     letfPad = 0.00944;
     topPad = 0.00470;
    // TODO: implement build
    return RefreshIndicator(
      onRefresh: refreshList,
      key: refreshKey,
      child: _buildList(),
    );
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch

      _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _bodyWidget(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(
          title: '',
          bar_visibility: false,
        );
      } else {
        return Err(
            bar_visibility: false,
            p_title: 'Home',
            m: _d.Message,
            mL: () =>
                setState(() {
                  _d.is_loading = true;
                  _getUserProfile();
                  mF = homeApiObj.fetchProjectList(context);
                }));
        /* print(
            "error hai idhar pe +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        print(_d.Message);
        if (isFirstApiCall) {
          isFirstApiCall = false;
          SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
                _d.is_loading = true;
                mF = homeApiObj.fetchProjectList(context);
              }));
          return Container();
        } else {
          return Err(
              bar_visibility: false,
              p_title: 'Home',
              m: _d.Message,
              mL: () => setState(() {
                    _d.is_loading = true;
                    mF = homeApiObj.fetchProjectList(context);
                  }));
        }*/
      }
    } else {
      // initial loading
      return Loader(
        title: '',
        bar_visibility: false,
      );
    }
  }

  bool isFirst = true;

  Widget _buildList() {
    if (widget.isForNotification == 1 && isFirst) {
      _firebaseMessage();
      isFirst = false;
    }

    return WillPopScope(
      onWillPop: () {
        return nav();
      },
      child: SafeArea(
        child: Scaffold(
            key: _homeScaffoldKey,
            appBar: AppBar(
              elevation: 5,
              actions: <Widget>[
                /*  IconButton(
                icon: SvgPicture.asset('assets/images/message.svg',
                    fit: BoxFit.cover,
                    height: h*0.023,
                    color: Colors.white,
                    semanticsLabel: 'popup close'),
                onPressed: () {
                  Navigator.push(
                    context,
                    PageRouteBuilder(
                      settings: RouteSettings(name: '/organisation_chat_list'),
                      pageBuilder: (c, a1, a2) => OrganisationChatList(),
                      transitionsBuilder: (c, anim, a2, child) =>
                          FadeTransition(opacity: anim, child: child),
                      transitionDuration: Duration(milliseconds: 500),
                    ),
                  );
                },
              ),*/
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        //color: Colors.redAccent,
                          borderRadius: BorderRadius.all(Radius.circular(h*0.03))),
                      margin: EdgeInsets.only(bottom: topPad*6*h, left: letfPad*4*w),
                      child: notiCount > 0
                          ? Text(
                        "$notiCount",
                        style: TextStyle(fontSize: h*0.016, color: appColor),
                      )
                          : Container(),
                    ),
                    IconButton(
                      icon: SvgPicture.asset('assets/images/notification.svg',
                          height: h*0.023,
                          color: appColor,
                          fit: BoxFit.cover,
                          semanticsLabel: 'popup close'),
                      tooltip: 'Closes application',
                      onPressed: () async {
                        var results = await Navigator.push(
                          context,
                          PageRouteBuilder(
                            settings: RouteSettings(name: '/notification'),
                            pageBuilder: (c, a1, a2) => NotificationList(),
                            transitionsBuilder: (c, anim, a2, child) =>
                                FadeTransition(opacity: anim, child: child),
                            transitionDuration: Duration(milliseconds: 500),
                          ),
                        );

                        if (results != null && results.containsKey('is_done')) {
                          if (results['is_done']) {
                            setState(() {
                              notiCount = 0;
                              _getNotificationCount();
                            });
                          }
                        }
                      },
                    ),
                  ],
                )
              ],
              title: w < 600 ? Image.asset('assets/images/logo.png',
                  height: h*0.06, fit: BoxFit.cover) : Image.asset('assets/images/logo.png',
                  height: h*0.048, fit: BoxFit.cover),
              centerTitle: true,
              backgroundColor: Colors.white,
              leading: Container(),
            ),
            body: FutureBuilder(
                initialData: null,
                future: mF,
                builder: (context, snap) => _checkAPIData(context, snap)),
            bottomNavigationBar: _bottomNavWidget()
        ),
      ),
    );
  }

  Widget _bodyWidget(GetProjectModel d) {
    double w = MediaQuery
        .of(context)
        .size
        .width;
    double h = MediaQuery
        .of(context)
        .size
        .height;

    print("height $h");
    print("width $w");

    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(letfPad*4*w, topPad*4*h,letfPad*4*w, 0),
          child: Container(
//                          margin: const EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                ),
              ],
              borderRadius: BorderRadius.circular(h*0.035),
              color: Colors.white,
            ),
            padding:
            const EdgeInsets.symmetric(horizontal: 12 /*, vertical: 3*/),
            child: TextField(
              controller: search,
              decoration: InputDecoration(
                hintText: 'Search by Project',
                hintStyle: TextStyle(fontSize: h*0.0176),
                filled: false,
                border: InputBorder.none,
                counterText: "",
              ),
              onSubmitted: (String _) {
                _onSearch();
//                _initSearch(context);
              },
              obscureText: false,
            ),
          ),
        ),
        d.data.length > 0
            ? Container(
          child: ListView.builder(
              padding: EdgeInsets.fromLTRB(letfPad*4*w, topPad*h,letfPad*4*w, topPad*4*h),
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemCount: d?.data?.length ?? 0,
              itemBuilder: (context, i) {
                ProjectModelData dataArray = d.data[i];
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      PageRouteBuilder(
                        settings: RouteSettings(name: '/project_detail'),
                        pageBuilder: (c, a1, a2) =>
                            ProjectDetails(
                                imageUrl: dataArray.project_icon,
                                projectID: dataArray.project_id.toString(),
                                projectName: dataArray.project_name,
                                showFourOptions : userData?.data?.isCashMang ?? false,
                               creditNote: userData?.data?.creditNote_isActive ?? 0,
                               // customIds: customList,
                                projectDetails: dataArray.project_details),
                        transitionsBuilder: (c, anim, a2, child) =>
                            FadeTransition(opacity: anim, child: child),
                        transitionDuration: Duration(milliseconds: 500),
                      ),
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: topPad*2*h),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(h*0.014),
                      ),
                      elevation: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                width: w,
                                height: h*0.188,
                                child: ClipRRect(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(h*0.014),
                                        topLeft: Radius.circular(h*0.014)),
                                    child: FadeInImage(
                                      fit: BoxFit.cover,
                                      placeholder: AssetImage(
                                          "assets/images/bg.png"),
                                      image: NetworkImage("${dataArray.project_icon}?token=${_singleton.tempToken}"),
                                    )),
                              ),
                              Positioned(
                                right: letfPad*2*w,
                                top: topPad*8*h,
                                child: GestureDetector(
                                  onTap: (){
                                    getFavourite(dataArray.project_id,
                                        dataArray.favourite);
                                  },
                                  child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 27,
                                    child: SvgPicture.asset(
                                        dataArray.favourite == 1
                                            ? 'assets/images/bookmark-filled.svg'
                                            : 'assets/images/bookmark-empty.svg',
                                        fit: BoxFit.cover,
                                        height: h*0.016,
                                        semanticsLabel: 'popup close'),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                              padding: EdgeInsets.all(w*0.0141),
                              child: Text(
                                dataArray.project_name ?? "",
                                style: TextStyle(fontFamily: 'SanBold'),
                              )),
                          Container(
                              padding: EdgeInsets.only(left: w*0.0141),
                              child: Text(
                                dataArray.company_name ?? "",
                                style: TextStyle(fontFamily: 'SanMedium'),
                              )),
                          Container(
                              padding: EdgeInsets.fromLTRB(w*0.0141, h*0.007, w*0.0141, topPad*h*4),
                              child: Text(
                                dataArray.project_details ?? "",
                                style:
                                TextStyle(fontFamily: 'SanRegular'),
                              )),
                        ],
                      ),
                    ),
                  ),
                );
              }),
        )
            : Container(
          height: h - (h*0.282),
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: h*0.018,
                ),
                SvgPicture.asset('assets/images/noData.svg',
                    fit: BoxFit.cover,
                    width: w * .60,
                    semanticsLabel: 'popup close'),
                SizedBox(
                  height: h*0.018,
                ),
                Text(
                  "No Projects",
                  style: TextStyle(fontFamily: 'SanMedium'),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _bottomNavWidget() {
    return BottomNavigationBar(
        selectedItemColor: appColor,
        currentIndex: 0,
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          setState(() {
            current_index = index;
            print(current_index);
          });

          switch (current_index) {
            case 1:
              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/my_po'),
                  pageBuilder: (c, a1, a2) => MyPOPage(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );

              break;
            case 2:
              settingModalBottomSheet(context, w);
              break;
            case 3:
              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/user_po'),
                  pageBuilder: (c, a1, a2) => UserPOPage(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
              break;
            case 4:
              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/profile'),
                  pageBuilder: (c, a1, a2) => ProfilePage(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
          }
        },
        items: [
          BottomNavigationBarItem(
            title: Text(
              "List",
            ),
            icon: Container(
              height: h*0.0376,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: SvgPicture.asset('assets/images/clipboard.svg',
                    height: h*0.018,
                    color: appColor,
                    fit: BoxFit.cover,
                    semanticsLabel: 'popup close'),
                onPressed: null,
              ),
            ),
          ),
          BottomNavigationBarItem(
            title: Text(
              "My PO",
            ),
            icon: Container(
              height: h*0.0376,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: SvgPicture.asset('assets/images/approved-tick.svg',
                    height: h*0.018,
                    color: Colors.grey,
                    fit: BoxFit.cover,
                    semanticsLabel: 'popup close'),
                onPressed: null,
              ),
            ),
          ),
          BottomNavigationBarItem(
            title: Text("Expense"),
            icon: Container(
              height: h*0.0376,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: SvgPicture.asset(
                    'assets/images/plus-sign-in-circle.svg',
                    height: h*0.018,
                    color: Colors.grey,
                    fit: BoxFit.cover,
                    semanticsLabel: 'popup close'),
                onPressed: null,
              ),
            ),
          ),
          BottomNavigationBarItem(
            title: Text("User PO"),
            icon: Container(
              height: h*0.0376,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: SvgPicture.asset('assets/images/user_po.svg',
                    height: h*0.018,
                    color: Colors.grey,
                    fit: BoxFit.cover,
                    semanticsLabel: 'popup close'),
                onPressed: null,
              ),
            ),
          ),
          BottomNavigationBarItem(
            title: Text("Profile"),
            icon: Container(
              height: h*0.0376,
              child: IconButton(
                padding: EdgeInsets.all(0),
                icon: SvgPicture.asset('assets/images/user.svg',
                    height: h*0.018,
                    color: Colors.grey,
                    fit: BoxFit.cover,
                    semanticsLabel: 'popup close'),
                onPressed: null,
              ),
            ),
          )
        ]);
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void _firebaseMessage() {
    if (Platform.isIOS) {
      iOS_Permission();
      //_firebaseMessaging.subscribeToTopic("iOSUser");
    } else {
      _firebaseMessaging.subscribeToTopic("androidUser");
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage");
        //showNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');

        navigate(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on Launch $message');

        navigate(message);
      },
    );
  }

  navigate(Map<String, dynamic> message) async {
    int pID;
    int mID;
    String approverId;
    int pMapId;
    int isGraph;
    bool isG;

    if (Platform.isIOS) {

      if(message['budgetLogId'] != null && int.parse("${message['budgetLogId']}") != 0) {
        Navigator.push(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/budget_alert'),
            pageBuilder: (c, a1, a2) => BudgetAlert(
              single: true,
              budgetId: int.parse("${message['budgetLogId'] ?? 0}"),
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );
      } else {
        pID = int.parse(message['purchase_id']);
        mID = int.parse(message['moderator_user_id']);
        approverId = message['approver_id'];
        pMapId = int.parse(message['pmap_id']);
        isGraph = int.parse("${message['isGraph'] ?? "0"}");

        if (isGraph == 0) {
          isG = false;
        } else {
          isG = true;
        }

        Navigator.push(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/po_review'),
            pageBuilder: (c, a1, a2) =>
            w < 600 ? PoReview(
              pId: pID,
              mId: mID,
              approverId: approverId,
              pMapId: pMapId,
              isFromUser: isG ?? true,
              isFromUserPending: isG ?? true,
            ) : PoReviewTablet(
              pId: pID,
              mId: mID,
              approverId: approverId,
              pMapId: pMapId,
              isFromUser: isG ?? true,
              isFromUserPending: isG ?? true,
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );
      }

    } else {

      if(message['data']['budgetLogId'] != null && int.parse("${message['data']['budgetLogId']}") != 0) {
        Navigator.push(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/budget_alert'),
            pageBuilder: (c, a1, a2) => BudgetAlert(
              single: true,
              budgetId: int.parse("${message['data']['budgetLogId'] ?? 0}"),
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );
      } else {
        pID = int.parse(message['purchase_id']);
        mID = int.parse(message['moderator_user_id']);
        approverId = message['approver_id'];
        pMapId = int.parse(message['pmap_id']);
        isGraph = int.parse("${message['isGraph'] ?? "0"}");

        if (isGraph == 0) {
          isG = false;
        } else {
          isG = true;
        }

        Navigator.push(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/po_review'),
            pageBuilder: (c, a1, a2) =>
            w < 600 ? PoReview(
              pId: pID,
              mId: mID,
              approverId: approverId,
              pMapId: pMapId,
              isFromUser: isG ?? true,
              isFromUserPending: isG ?? true,
            ) : PoReviewTablet(
              pId: pID,
              mId: mID,
              approverId: approverId,
              pMapId: pMapId,
              isFromUser: isG ?? true,
              isFromUserPending: isG ?? true,
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );
      }

    }


    /*   var results = await Navigator.push(
      context,
      PageRouteBuilder(
        settings: RouteSettings(name: '/notification'),
        pageBuilder: (c, a1, a2) => NotificationList(),
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 500),
      ),
    );

    if (results != null && results.containsKey('is_done')) {
      if (results['is_done']) {
        setState(() {
          notiCount = 0;
          _getNotificationCount();
        });
      }
    }*/
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void settingModalBottomSheet(context,double width) async {
    bool con = await helper.checkConnection();
    if(con) {
      showModalBottomSheet(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(h*0.018),
                  topRight: Radius.circular(h*0.018))),
          context: context,
          builder: (BuildContext bc) {
            return (w < 600) ? Container(
              padding: EdgeInsets.all(h*0.0094),
              child: new Wrap(
                children: <Widget>[
                 new ListTile(
                      leading: SvgPicture.asset('assets/images/po.svg',
                          fit: BoxFit.cover,
                          width: h*0.028,
                          height: h*0.028,
                          semanticsLabel: 'popup close'),
                      title: new Text(
                        'Create Purchase Order',
                        style: TextStyle(fontFamily: 'SanMedium'),
                      ),
                      onTap: () async {
                        //print(customList[0]);

                        //customFieldModel = await getCustomFields(customList[0] ?? 1);

//                      if (customFieldModel.data != null) {
//                        if (customFieldModel.data.length > 0) {
//                          setState(() {
//                            customFieldDikhao = true;
//                          });
//                        } else {
//                          setState(() {
//                            customFieldDikhao = false;
//                          });
//                        }
//                      }

                        prefix0.Navigator.pop(context);
                        Navigator.push(
                          context,
                          PageRouteBuilder(
                            settings: RouteSettings(name: '/create_po'),
                            pageBuilder: (c, a1, a2) =>
                                CreatePo(
                                  //customFieldModel: customFieldModel ?? null,
                                  //customFieldDikha: customFieldDikhao,
                                ),
                            transitionsBuilder: (c, anim, a2, child) =>
                                FadeTransition(opacity: anim, child: child),
                            transitionDuration: Duration(milliseconds: 500),
                          ),
                        );
                      }),
                 Divider(
                    color: Colors.black87,
                  ),
                 new ListTile(
                    leading: SvgPicture.asset('assets/images/invoice.svg',
                        fit: BoxFit.cover,
                        width: h*0.028,
                        height: h*0.028,
                        semanticsLabel: 'popup close'),
                    title: new Text(
                      'Create Expense',
                      style: TextStyle(fontFamily: 'SanMedium'),
                    ),
                    onTap: () async {
                      //print(customList[1]);
                      //customFieldModel = await getCustomFields(customList[1] ?? 2);

//                    if (customFieldModel.data != null) {
//                      if (customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      } else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                      prefix0.Navigator.pop(context);
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/create_expense'),
                          pageBuilder: (c, a1, a2) =>
                              CreateExpense(
                                //  customFieldModel: customFieldModel ?? null,
                                //  customFieldDikha: customFieldDikhao,
                              ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    },
                  ),
                  Divider(
                    color: Colors.black87,
                  ),
                  new ListTile(
                    leading: SvgPicture.asset('assets/images/challan.svg',
                        fit: BoxFit.cover,
                        width: h*0.028,
                        height: h*0.028,
                        semanticsLabel: 'popup close'),
                    title: new Text(
                      'Create Delivery Challan',
                      style: TextStyle(fontFamily: 'SanMedium'),
                    ),
                    onTap: () async {
                      //print(customList[2]);

                      //customFieldModel = await getCustomFields(customList[2] ?? 3);

//                    if (customFieldModel.data != null) {
//                      if (customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      } else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                      prefix0.Navigator.pop(context);
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/create_challan'),
                          pageBuilder: (c, a1, a2) =>
                              CreateChallan(
                                // customFieldModel: customFieldModel ?? null,
                                // customFieldDikha: customFieldDikhao,
                              ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    },
                  ),
                  userData?.data?.isCashMang ?? false ? Divider(
                    color: Colors.black87,
                  ) : Container(),
                  userData?.data?.isCashMang ?? false ? new ListTile(
                    leading: SvgPicture.asset('assets/images/challan.svg',
                        fit: BoxFit.cover,
                        width: h*0.028,
                        height: h*0.028,
                        semanticsLabel: 'popup close'),
                    title: new Text(
                      'Advance Request',
                      style: TextStyle(fontFamily: 'SanMedium'),
                    ),
                    onTap: () async {

                      prefix0.Navigator.pop(context);
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/cash_request'),
                          pageBuilder: (c, a1, a2) =>
                              CashRequestPage(),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    },
                  ) : Container(),
                 userData?.data?.creditNote_isActive == 1 ?? 0 ?  Divider(
                    color: Colors.black87,
                  ) : Container(),
                  userData?.data?.creditNote_isActive == 1 ?? 0 ? new ListTile(
                    leading: SvgPicture.asset('assets/images/invoice.svg',
                        fit: BoxFit.cover,
                        width: h*0.028,
                        height: h*0.028,
                        semanticsLabel: 'popup close'),
                    title: new Text(
                      'Credit Note',
                      style: TextStyle(fontFamily: 'SanMedium'),
                    ),
                    onTap: () async {
                      //print(customList[1]);
                      //customFieldModel = await getCustomFields(customList[1] ?? 2);

//                    if (customFieldModel.data != null) {
//                      if (customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      } else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                      prefix0.Navigator.pop(context);
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/credit_amount'),
                          pageBuilder: (c, a1, a2) =>
                              CreditNotePage(
                                //  customFieldModel: customFieldModel ?? null,
                                //  customFieldDikha: customFieldDikhao,
                              ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    },
                  ) : Container(),
                ],
              ),
            ) : Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 64),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: 60,
                      width: w,
                      alignment: Alignment.centerRight,
                      child: IconButton(
                        onPressed: (){
                          prefix0.Navigator.pop(context);
                        },
                        icon: Icon(Icons.clear, size: 30,),
                      ),
                    ),
                    Card(
                      color: appColor,
                      child: Container(
                        height: 60,
                        width: w,
                        child: FlatButton(
                          onPressed: (){
                            prefix0.Navigator.pop(context);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/create_po'),
                                pageBuilder: (c, a1, a2) =>
                                    PoTabletPage(
                                      //customFieldModel: customFieldModel ?? null,
                                      //customFieldDikha: customFieldDikhao,
                                    ),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset('assets/images/po.svg',
                                    fit: BoxFit.cover,
                                    width: h*0.028,
                                    height: h*0.028,
                                    color: Colors.white,
                                    semanticsLabel: 'popup close'),
                                SizedBox(width: 16.0,),
                                new Text(
                                  'CREATE PURCHASE ORDER',
                                  style: TextStyle(fontFamily: 'SanMedium', fontSize: 17, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: appColor,
                      child: Container(
                        height: 60,
                        width: w,
                        child: FlatButton(
                          onPressed: (){
                            prefix0.Navigator.pop(context);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/create_expense'),
                                pageBuilder: (c, a1, a2) =>
                                    CreateExpenseTablet(
                                      //  customFieldModel: customFieldModel ?? null,
                                      //  customFieldDikha: customFieldDikhao,
                                    ),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset('assets/images/invoice.svg',
                                    fit: BoxFit.cover,
                                    width: h*0.028,
                                    height: h*0.028,
                                    color: Colors.white,
                                    semanticsLabel: 'popup close'),
                                SizedBox(width: 16.0,),
                                new Text(
                                  'CREATE EXPENSE',
                                  style: TextStyle(fontFamily: 'SanMedium', fontSize: 17, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: appColor,
                      child: Container(
                        height: 60,
                        width: w,
                        child: FlatButton(
                          onPressed: (){
                            prefix0.Navigator.pop(context);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/create_challan'),
                                pageBuilder: (c, a1, a2) =>
                                    ChallanTabletPage(
                                      // customFieldModel: customFieldModel ?? null,
                                      // customFieldDikha: customFieldDikhao,
                                    ),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset('assets/images/challan.svg',
                                    fit: BoxFit.cover,
                                    width: h*0.028,
                                    height: h*0.028,
                                    color: Colors.white,
                                    semanticsLabel: 'popup close'),
                                SizedBox(width: 16.0,),
                                new Text(
                                  'CREATE DELIVERY CHALLAN',
                                  style: TextStyle(fontFamily: 'SanMedium', fontSize: 17, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: appColor,
                      child: Container(
                        height: 60,
                        width: w,
                        child: FlatButton(
                          onPressed: (){
                            prefix0.Navigator.pop(context);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/cash_request'),
                                pageBuilder: (c, a1, a2) =>
                                    CashRequestTabletPage(),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset('assets/images/challan.svg',
                                    fit: BoxFit.cover,
                                    width: h*0.028,
                                    height: h*0.028,
                                    color: Colors.white,
                                    semanticsLabel: 'popup close'),
                                SizedBox(width: 16.0,),
                                new Text(
                                  'ADVANCE REQUEST',
                                  style: TextStyle(fontFamily: 'SanMedium', fontSize: 17, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Card(
                      color: appColor,
                      child: Container(
                        height: 60,
                        width: w,
                        child: FlatButton(
                          onPressed: (){
                            prefix0.Navigator.pop(context);
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/credit_amount'),
                                pageBuilder: (c, a1, a2) =>
                                    CreditNoteTabletPage(
                                      //  customFieldModel: customFieldModel ?? null,
                                      //  customFieldDikha: customFieldDikhao,
                                    ),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: FittedBox(
                            fit: BoxFit.fitWidth,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset('assets/images/invoice.svg',
                                    fit: BoxFit.cover,
                                    width: h*0.028,
                                    height: h*0.028,
                                    color: Colors.white,
                                    semanticsLabel: 'popup close'),
                                SizedBox(width: 16.0,),
                                new Text(
                                  'CREATE CREDIT NOTE',
                                  style: TextStyle(fontFamily: 'SanMedium', fontSize: 17, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
    } else {
      final snackBar = SnackBar(content: Text("Please check your internet connection"));
      _homeScaffoldKey.currentState.showSnackBar(snackBar);
    }

  }

  _getCustomExpenseKey() async {
    //Get Expense Types : https://dev-api.gokozo.com/v1/custom_key/expenseTypes/get

    customList = [];

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    GetCustomKeyModel res_d;

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    try {
      final res = await http.get(
          '${BASE_URL}custom_key/expenseTypes/get?organization_id=${prefs
              .getInt('orgId')}',
          headers: Headers);

      print(
          'res at CustomTabKeys +++++++++++++++++++++++++++++++++++++++++++++++++++ = $res');

      print("res.body = ${res.body}");
      print("res.statusCode = ${res.statusCode}");

      print("URLs ${BASE_URL}custom_key/expenseTypes/get");
      switch (res.statusCode) {
        case 200:
        case 201:
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');
          res_d = GetCustomKeyModel.fromJson(j_data);

          if (res_d.data != null) {
            for (int i = 0; i < res_d.data?.length; i++) {
              customList.add(res_d.data[i].expense_id);
            }
          }

          print(customList);

          break;
        case 401:
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
              settings: RouteSettings(name: '/login'),
              pageBuilder: (c, a1, a2) => LoginPage(),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 500),
            ),
          );
          break;

        default:
        //return GetCustomKeyModel.buildErr(res.statusCode);
          print(res.statusCode);
      }
    } catch (err) {
      return GetCustomKeyModel.buildErr(0,
          message: "Somthing went wrong. Please try again later.");
    }
  }

  _getNotificationCount() async {
    GetNotificationModel Notidata;
    int counter = 0;

    Notidata = await notificationApiObj.fetchNotificationList(context);

    if(Notidata.data != null) {
      for (int i = 0; i < Notidata?.data?.length; i++) {
        if (Notidata.data[i].notification_active == 1) {
          counter = counter + 1;
        }
      }
    }

    print("Count is $notiCount");

    setState(() {
      notiCount = counter;
    });
  }

  // force update
  versionCheck(context) async {
    //Get Current installed version of app
    final PackageInfo info = await PackageInfo.fromPlatform();
    String currentVersion = info.buildNumber;
    print(currentVersion);

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      remoteConfig.getString('force_update_current_version');

      String newVersion =
      remoteConfig.getString('force_update_current_version');

      if (int.parse(newVersion) > int.parse(currentVersion)) {
        _showVersionDialog(context);
      }
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

  _showVersionDialog(context) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "New Update Available !!!";
        String message =
            "Please download the latest update to continue using App.";
        String btnLabel = "Update";

        return WillPopScope(
          onWillPop: () {
            return Future.value(false);
          },
          child: Platform.isIOS
              ? new CupertinoAlertDialog(
            title: Text(title),
//                  content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text(btnLabel),
                onPressed: () => _launchURL(APP_STORE_URL),
              ),
            ],
          )
              : new AlertDialog(
            title: Text(title),
            //content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text(btnLabel),
                onPressed: () => _launchURL(PLAY_STORE_URL),
              ),
            ],
          ),
        );
      },
    );
  }

  _onSearch() {
    print("on search start ****************");
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      // do something with _searchQuery.text
      if (/*search.text.isNotEmpty &&*/ s_q != search.text) {
        s_q = search.text;
        setState(() {
          _d.Status = false;
          _d.is_loading = true;
          mF = sVM.initSearch(search.text ?? "", context);
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        });
      }
    });
  }


//  bool isFirstApiCall = true;


  SetFavouriteModel res_d;

  Future<SetFavouriteModel> getFavourite(int pId, int fav) async {
    int isFav;
    if (fav == 1) {
      isFav = 2;
    } else {
      isFav = 1;
    }

    //bool con = true;
    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if (con) {
      try {
        final res = await http.get(
            "${BASE_URL}project/favourite/$pId/$isFav?organization_id=${prefs
                .getInt('orgId')}",
            headers: Headers);

        if (res.statusCode == "200" || res.statusCode == 200 || res.statusCode == 201) {
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');

          res_d = SetFavouriteModel.fromJson(j_data);

          print("******************favourite done");

          print(res_d);

          setState(() {
            mF = homeApiObj.fetchProjectList(context);
          });
        }
      } catch (err) {
        return SetFavouriteModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  getCustomFields(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    GetCustomFieldModel res_d;

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    try {
      final res =
      await http.get(
          "${BASE_URL}custom_key/Expense/$id?organization_id=${prefs.getInt(
              'orgId')}", headers: Headers);

      print("URLs $BASE_URL");
      switch (res.statusCode) {
        case 200:
        case 201:
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');
          res_d = GetCustomFieldModel.fromJson(j_data);

//            res_d.banners.removeAt(0);
//            print("${res_d.banners[0].banner.ID}");
          return res_d;

          break;
        case 401:
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
              settings: RouteSettings(name: '/login'),
              pageBuilder: (c, a1, a2) => LoginPage(),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 500),
            ),
          );
          break;

        default:
        //return GetCustomKeyModel.buildErr(res.statusCode);
          print(res.statusCode);
      }
    } catch (err) {
      return GetCustomFieldModel.buildErr(0,
          message: "Somthing went wrong. Please try again later.");
    }
  }
}
