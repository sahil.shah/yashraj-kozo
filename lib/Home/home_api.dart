import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeApi {
  ConnectionHelper mCH;

  HomeApi() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  bool isFirst = true;

  Future<GetProjectModel> fetchProjectList(BuildContext context) async {
    print("home data start ****************");
    GetProjectModel res_d;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String access_token = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": _singleton.access_token ?? access_token
    };

    print("**********************access_token at home api = $access_token");

    print("headers = $Headers");
    print("URLs ${BASE_URL}project");

    //bool con = true;
    bool con = await mCH.checkConnection();

    if (con) {
      try {
        final res = await http.get(
            '${BASE_URL}project?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}',
            headers: Headers);

        print('res at Home APi ++++++++++++++++++++++++++++++++++++= $res');

        print("res.body = ${res.body}");
        print("res.statusCode = ${res.statusCode}");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            res_d = GetProjectModel.fromJson(j_data);
            print("******************homedata done");

            return res_d;
          case 401:
            print("Mai idhar hu");
            if (isFirst) {
              isFirst = false;
              fetchProjectList(context);
            } else {
              Navigator.pushReplacement(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/login'),
                  pageBuilder: (c, a1, a2) => LoginPage(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
            }
            break;
          default:
            return GetProjectModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetProjectModel.buildErr(0,
            message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return GetProjectModel.buildErr(1);
    }
  }
}
