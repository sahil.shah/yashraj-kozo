import 'dart:convert';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yashrajkozo/Login/verify_otp.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/validation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {

  final email = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AppSingleton _singleton = AppSingleton();
  double h,w;

  @override
  void dispose() {
    // TODO: implement dispose
    email.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    return SafeArea(
      bottom: true,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("FORGOT PASSWORD", style: TextStyle(fontFamily: 'SanSemiBold'),),
          centerTitle: true,
          backgroundColor: appColor,
        ),
        body: Padding(
          padding: EdgeInsets.only(top: h*0.0188),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: TextField(
                  controller: email,
                  decoration: InputDecoration(
                      hintText: 'Email',
                      hintStyle: TextStyle(fontFamily: 'SanRegular'),
                      prefixIcon: Container(
                        height: h*0.02,
                        child: IconButton(
                          padding: EdgeInsets.all(0),
                          icon: SvgPicture.asset('assets/images/user.svg',
                              fit: BoxFit.cover,
                              width: h*0.02117,
                              height: h*0.02,
                              color: Colors.black54,
                              semanticsLabel: 'popup close'),
                          onPressed: null,
                        ),
                      ),
                  ),
                ),
              ),

              CustomBtn(
                title: "Proceed",
                onTap: () {
                  forgotPassword();
                },
              )
            ],
          ),
        )
      ),
    );
  }

  forgotPassword() {
    if(checkErr()) {
      forgetPassword();
    }
  }

  bool checkErr() {
    if(email.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter email address'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if(!validator.email(email.text)) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter proper email address'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

  void forgetPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();


    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    ForgetPasswordModel res_d;

    if(con) {
      try {
        var req = json.encode({
          "user_name": "${email.text}"
        });

        final res = await http.post(
            '${BASE_URL}forgotpassword?organization_id=${prefs.getInt('orgId')}', headers: Headers, body: req);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ForgetPasswordModel.fromJson(j_data);

            if (res_d.Status) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/verify_otp'),
                  pageBuilder: (c, a1, a2) =>
                      VerifyOTP(
                          username: email.text
                      ),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ForgetPasswordModel.fromJson(j_data);

            if (res_d.Status) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/verify_otp'),
                  pageBuilder: (c, a1, a2) =>
                      VerifyOTP(
                          username: email.text
                      ),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          default:
        }
      } catch (err) {
        print(err.toString());
      }
    }else {
      final snackBar = SnackBar(
          content: Text('Please check your internet connection'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

}