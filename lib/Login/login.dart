import 'package:yashrajkozo/Profile/get_profile_api.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yashrajkozo/Home/home.dart';
import 'package:yashrajkozo/Login/forgot_password.dart';
import 'package:yashrajkozo/Login/login_api.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController emailCtrl;
  TextEditingController passwordCtrl;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  AppSingleton _singleton = AppSingleton();
  GetUserProfileModel userData;
  GetProfileDataApi pVM;
  bool show = false;
  LoginAPI lVM;
  LoginModel res;
  double letfPad, topPad;
  double h,w;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pVM = GetProfileDataApi();
    lVM = LoginAPI();
    _firebaseToken();
    emailCtrl = new TextEditingController();
    passwordCtrl = new TextEditingController();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    emailCtrl.dispose();
    passwordCtrl.dispose();
    super.dispose();
  }

  void _firebaseToken() async {

    _firebaseMessaging.getToken().then((token) async {
      _firebaseMessaging.onTokenRefresh;
      print(token);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('firebase_token', token);
    });
    _firebaseMessaging.onTokenRefresh;
  }

  nav() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  @override
  Widget build(BuildContext context) {

    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    print(h);

    letfPad = 0.00944;
    topPad = 0.00470;

    return WillPopScope(
      onWillPop: () {
        return nav();
      },
      child: SafeArea(
        bottom: true,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            leading: Container(),
            title: w < 600 ? Image.asset('assets/images/logo.png',
                height: h*0.06, fit: BoxFit.cover) : Image.asset('assets/images/logo.png',
              height: h*0.048, fit: BoxFit.cover),
            centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 85,
                child: ListView(
                  children: <Widget>[
                    Container(
                      width: w,
                      color: Colors.transparent,
                      child: Image.asset("assets/images/login_new.png", fit: BoxFit.cover,),
                    ),
                    Container(
                      child: Padding(
                        padding: EdgeInsets.only(left: (letfPad*4*w), top: (topPad*4*h), right: (letfPad*4*w), bottom: (topPad*4*h)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text(
                              'Login',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: h*0.02587,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'SanRegular'),
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text(
                              'Please provide your email address and password to sign into your application.',
                              style: TextStyle(
                                  fontSize: h*0.018, fontFamily: 'SanRegular'),
                            ),
                            SizedBox(
                              height: h*0.0141,
                            ),
                            TextField(
                              keyboardType: TextInputType.emailAddress,
                              controller: emailCtrl,
                              decoration: InputDecoration(
                                  prefixIcon: Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset(
                                          'assets/images/user.svg',
                                          fit: BoxFit.cover,
                                          width: h*0.0211,
                                          height: h*0.02,
                                          color: Colors.black54,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  hintText: 'Email',
                                  hintStyle:
                                      TextStyle(fontFamily: 'SanRegular')),
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            TextField(
                              controller: passwordCtrl,
                              obscureText: show ? false : true,
                              decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        show = !show;
                                      });
                                    },
                                    icon: show
                                        ? Icon(FontAwesomeIcons.eye)
                                        : Icon(FontAwesomeIcons.eyeSlash),
                                  ),
                                  prefixIcon: Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset(
                                          'assets/images/password.svg',
                                          fit: BoxFit.cover,
                                          color: Colors.black54,
                                          width: h*0.0211,
                                          height: h*0.02,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  hintText: 'Password',
                                  hintStyle:
                                      TextStyle(fontFamily: 'SanRegular')),
                            ),
                            SizedBox(
                              height: h*0.018,
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: Container(
                                child: FlatButton(
                                  padding: EdgeInsets.only(left: (letfPad*4*w), top: (topPad*4*h), right: (letfPad*4*w), bottom: (topPad*4*h)),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                        settings: RouteSettings(
                                            name: '/forgot_password'),
                                        pageBuilder: (c, a1, a2) =>
                                            ForgotPassword(),
                                        transitionsBuilder:
                                            (c, anim, a2, child) =>
                                                FadeTransition(
                                                    opacity: anim,
                                                    child: child),
                                        transitionDuration:
                                            Duration(milliseconds: 500),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    'Forgot Password ?',
                                    style: TextStyle(
                                      color: appColor,
                                      fontFamily: 'SanRegular',
                                      fontSize: h*0.015
                                    ),
                                    textAlign: TextAlign.end,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: h*0.018,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 15,
                child: CustomBtn(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);

                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                    verifyUser(context);
                  },
                  title: "Sign In",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void verifyUser(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("access_token", null);

    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();

    if (con) {
      if (checkErr()) {
        _showLoading();

        res = await lVM.getLoginDetails(emailCtrl.text, passwordCtrl.text);

        if (res?.Status ?? false) {

          print("******************login done");
          print("access_token on login = ${res.data.access_token}");
          await prefs.setString('access_token', (res.data.access_token));
          await prefs.setString('userName', res.data.user_name);
          await prefs.setString('temp_token', res.data.temp_token);
          _singleton.access_token = res.data.access_token;

          _getUserProfile(context);

       /*   Navigator.pushReplacement(
            context,
            PageRouteBuilder(
              settings: RouteSettings(name: '/home'),
              pageBuilder: (c, a1, a2) => MyHomePage(),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 500),
            ),
          );*/

        } else {
          _dismissLoader(context);
          final snackBar = SnackBar(
              duration: const Duration(milliseconds: 500),
              content: Text('${res?.Message ?? "Something went wrong"}',
                style: TextStyle(fontFamily: 'SanRegular'),
              ));
          _scaffoldKey.currentState.showSnackBar(snackBar);
          //_dismissLoader(context);
        }
      }
    } else {
      final snackBar =
          SnackBar(content: Text('Please check your internet connection'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _dismissLoader(BuildContext context) {
    Navigator.of(context).pop();
  }

  void _showLoading() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            //contentPadding: const EdgeInsets.only(left : 16, right: 0, top: 10, bottom: 10),
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                  ),
                  Text(
                    'Loading...',
                    style: TextStyle(fontFamily: 'SanRegular'),
                  ),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  _getUserProfile(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    _singleton.userName = prefs.getString('userName');
    _singleton.access_token = prefs.getString('access_token');

    // get all approver list
    userData = await pVM.fetchUserProfileData(context);

    if (userData.Status ?? false) {
      print("******************getProfile done");
      print("UserId = ${userData.data.user_id}");
      prefs.setInt('userId', userData.data.user_id);
      prefs.setString('userImage', userData.data.user_pic);
      _singleton.userId = userData.data.user_id;
      _singleton.userOrgList.clear();
      _singleton.userOrgList.addAll(userData.data.organization_data);

      _dismissLoader(context);

      if(userData.data.organization_data.length == 1) {

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setInt('orgId', userData.data.organization_data[0].organization_id);
        _singleton.orgId = userData.data.organization_data[0].organization_id;
        _singleton.userOrgList[0].isSelected = true;
        print("Id is :- ${_singleton.userOrgList[0].organization_id}");
        setState(() {});

        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/home'),
            pageBuilder: (c, a1, a2) => MyHomePage(),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );

      } else {
        selectOrganization();
      }

//      prefs.setStringList('userOrganizationList', userData.data.organization_data);

      setState(() {});
    } else {}
  }

  void selectOrganization() {
    showModalBottomSheet(
      isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(h*0.0188), topRight: Radius.circular(h*0.0188))),
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, setState) =>
                  Container(
                      padding: EdgeInsets.only(left: (letfPad*2*w), top: (topPad*2*h), right: (letfPad*2*w), bottom: (topPad*2*h)),
                      child: Wrap(
                        children: <Widget>[
                          Column(
                            children: <Widget>[

                              Container(
                                height: h*0.0047,
                                width: w*0.10,
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.all(Radius.circular(h*0.01))
                                ),
                              ),

                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: _singleton?.userOrgList?.length ?? 0,
                                  itemBuilder: (context,i){
                                    return Column(
                                      children: <Widget>[
                                        ListTile(
                                          onTap: (){
                                            navFun(context, i);
                                          },

                                          title: new Text(_singleton.userOrgList[i].organization_name ?? "", style: TextStyle(fontFamily: 'SanMedium'),),
                                          trailing: (_singleton.userOrgList[i].isSelected ==true)
                                              ?
                                          Radio(value: true, groupValue: true, onChanged: (i){}, activeColor: appColor,)
                                          /* Icon(
                                          FontAwesomeIcons.check,
                                          color: Colors.green,
                                          size: 14,
                                        )*/
                                              : null,
                                        ),
                                      ],
                                    );
                                  }),
                            ],
                          ),
                        ],
                      )
                  )
          );
        });
  }


  void navFun(BuildContext context, int i)async{

    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setInt('orgId', _singleton.userOrgList[i].organization_id);
    _singleton.orgId = _singleton.userOrgList[i].organization_id;

    _singleton.userOrgList[i].isSelected = true;

    print("Id is :- ${_singleton.userOrgList[i].organization_id}");

    setState(() {

    });

    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        settings: RouteSettings(name: '/home'),
        pageBuilder: (c, a1, a2) => MyHomePage(),
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 500),
      ),
    );

  }

  bool checkErr() {
    if (emailCtrl.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter email address'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (passwordCtrl.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter password'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

}
