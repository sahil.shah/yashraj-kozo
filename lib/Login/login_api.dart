import 'dart:convert';

import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginAPI {
  ConnectionHelper mCH;

  LoginAPI() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  Future<LoginModel> getLoginDetails(String email, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String firebaseToken = prefs.getString('firebase_token');
    LoginModel res_d;

    //bool con = true;

    bool con = await mCH.checkConnection();

    if (con) {
      try {
        var req = json.encode({
          "user_name": email ?? '',
          "password": password ?? '',
          "firebase_token": firebaseToken ?? "",
          "device_type": "device_type",
          "device_model": "device_model"
        });

        final res = await http.post("${BASE_URL}login?organization_id=${prefs.getInt('orgId')}", body: req, headers: HEADERS);

        switch (int.parse("${res.statusCode}")) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = LoginModel.fromJson(j_data);
            return res_d;

          default:
            return res_d;
        }
      } catch (err) {
        print(err);
      }
    }
  }
}
