import 'dart:convert';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewPassword extends StatefulWidget {

  String username;
  String otp;
  String token;

  NewPassword({
    Key key,
    this.username,
    this.otp,
    this.token,
}) : super(key : key);

  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {

  final new_password = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  double h,w;
  AppSingleton _singleton = AppSingleton();
  @override
  void dispose() {
    // TODO: implement dispose
    new_password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    return SafeArea(
      bottom: true,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("NEW PASSWORD", style: TextStyle(fontFamily: 'SanSemiBold'),),
          centerTitle: true,
          backgroundColor: appColor,
        ),

        body: Padding(
          padding: EdgeInsets.only(top: h*0.0188),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: TextField(
                  obscureText: true,
                  controller: new_password,
                  decoration: InputDecoration(
                    hintText: 'Enter your new password',
                    hintStyle: TextStyle(fontFamily: 'SanRegular'),
                    prefixIcon: Container(
                      height: h*0.02,
                      child: IconButton(
                        padding: EdgeInsets.all(0),
                        icon: SvgPicture.asset('assets/images/user.svg',
                            fit: BoxFit.cover,
                            width: h*0.02117,
                            height: h*0.02,
                            color: Colors.black54,
                            semanticsLabel: 'popup close'),
                        onPressed: null,
                      ),
                    ),
                  ),
                ),
              ),

              CustomBtn(
                title: "Proceed",
                onTap: () {
                  if(checkErr()) {
                    resetPassword();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  resetPassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _HEADERS;

    ResetPasswordModel res_d;
    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();

    if(con) {
      try {
        var req = json.encode({
          "user_name": "${widget.username}",
          "otp": widget.otp,
          "reset_token": widget.token,
          "password": new_password.text,
        });


        print(req);

        final res = await http.post(
            '${BASE_URL}resetpassword?organization_id=${prefs.getInt('orgId')}', headers: HEADERS, body: req);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ResetPasswordModel.fromJson(j_data);

            if (res_d.Status ?? false) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.pop(context);
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ResetPasswordModel.fromJson(j_data);

            if (res_d.Status ?? false) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.pop(context);
              Navigator.pop(context);
              Navigator.pop(context);
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          default:
        }
      } catch (err) {

      }
    }else {
      final snackBar = SnackBar(

          content: Text('Please checkyour internet connection.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  bool checkErr() {
    if(new_password.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter new password'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }
}
