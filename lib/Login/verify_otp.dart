import 'dart:convert';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yashrajkozo/Login/new_password.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/validation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerifyOTP extends StatefulWidget {

  String username;

  VerifyOTP({
    Key key,
    this.username}) : super(key : key);

  @override
  _VerifyOTPState createState() => _VerifyOTPState();
}

class _VerifyOTPState extends State<VerifyOTP> {

  AppSingleton _singleton = AppSingleton();

  final otp = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  double h,w;
  @override
  void dispose() {
    // TODO: implement dispose
    otp.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    return SafeArea(
      bottom: true,
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text("VERIFY OTP", style: TextStyle(fontFamily: 'SanSemiBold'),),
            centerTitle: true,
            backgroundColor: appColor,
          ),
          body: Padding(
            padding: EdgeInsets.only(top: h*0.0188),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: TextField(
                    keyboardType: TextInputType.numberWithOptions(),
                    controller: otp,
                    decoration: InputDecoration(
                      hintText: 'Enter OTP',
                      hintStyle: TextStyle(fontFamily: 'SanRegular'),
                      prefixIcon: Container(
                        height: h*0.02,
                        child: IconButton(
                          padding: EdgeInsets.all(0),
                          icon: SvgPicture.asset('assets/images/user.svg',
                              fit: BoxFit.cover,
                              width: h*0.02117,
                              height: h*0.02,
                              color: Colors.black54,
                              semanticsLabel: 'popup close'),
                          onPressed: null,
                        ),
                      ),
                    ),
                  ),
                ),

                CustomBtn(
                  title: "Reset Password",
                  onTap: () {
                    if(checkErr()) {
                      verifyOTP();
                    }
                  },
                )
              ],
            ),
          )
      ),
    );
  }

  verifyOTP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var _HEADERS;

    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();

    VerifyOTPModel res_d;

    if(con) {
      try {
        var req = json.encode({
          "user_name": "${widget.username}",
          "otp": otp.text,
        });

        final res = await http.post(
            '${BASE_URL}verifyotp?organization_id=${prefs.getInt('orgId')}', headers: HEADERS, body: req);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = VerifyOTPModel.fromJson(j_data);

            if (res_d.Status ?? false) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/reset_password'),
                  pageBuilder: (c, a1, a2) =>
                      NewPassword(
                        username: widget.username,
                        otp: otp.text,
                        token: res_d.data.token,
                      ),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = VerifyOTPModel.fromJson(j_data);

            if (res_d.Status ?? false) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );

              Navigator.push(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/reset_password'),
                  pageBuilder: (c, a1, a2) =>
                      NewPassword(
                        username: widget.username,
                        otp: otp.text,
                        token: res_d.data.token,
                      ),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018
              );
            }

            break;

          default:
        }
      } catch (err) {

      }
    }else {
      final snackBar = SnackBar(
          content: Text('Please check your internet connection'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  bool checkErr() {
    if(otp.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter OTP to verify'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

}