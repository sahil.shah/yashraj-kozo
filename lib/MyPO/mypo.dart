import 'dart:async';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/MyPO/my_approved.dart';
import 'package:yashrajkozo/MyPO/my_pending.dart';
import 'package:yashrajkozo/MyPO/my_rejected.dart';
import 'package:yashrajkozo/MyPO/mypo_vm.dart';
import 'package:yashrajkozo/search_page/search_page_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';

class MyPOPage extends StatefulWidget {
  @override
  _MyPOPageState createState() => _MyPOPageState();
}

class _MyPOPageState extends State<MyPOPage>
    with SingleTickerProviderStateMixin {
  GlobalKey _keyRed = GlobalKey();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _s_con = ScrollController();
  TabController _tab_controller;
  TextEditingController search;
  MyPoApi myPoApiObj;
  Future<GetMyPoModel> mF;
  double h,w;
  double letfPad, topPad;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    search = new TextEditingController();
    _tab_controller = TabController(vsync: this, initialIndex: 0, length: 3);
    myPoApiObj = MyPoApi();
    sVM = SearchVM();
    mF = myPoApiObj.fetchMyPoDataList(context);
    search.addListener(_onSearch);
  }

  SearchVM sVM;
  Timer _debounce;
  GetMyPoModel _d;
  String s_q = "";

  _onSearch() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      // do something with _searchQuery.text
      if (/*search.text.isNotEmpty &&*/ s_q != search.text) {
        s_q = search.text;
        setState(() {
          _d.Status = false;
          _d.is_loading = true;
          mF = sVM.initMyPOSearch(search.text ?? "");
          FocusScope.of(context).requestFocus(FocusNode());
        });
      }
    });
  }

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    setState(() {
      mF = myPoApiObj.fetchMyPoDataList(context);
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    letfPad = 0.00944;
    topPad = 0.00470;
    return RefreshIndicator(
      onRefresh: refreshList,
      key: refreshKey,
      child: FutureBuilder(
          initialData: null,
          future: mF,
          builder: (context, snap) => _checkAPIData(context, snap)),
    );
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch

      _d = snap.data;
      if (_d.Status ?? false) {
        // API true
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
                  _d.is_loading = true;
                  mF = myPoApiObj.fetchMyPoDataList(context);
                }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  List<GetMyPoModelData> pList = [];
  List<GetMyPoModelData> aList = [];
  List<GetMyPoModelData> rList = [];

  Widget _buildList(GetMyPoModel d) {
    pList = d.data.where((s) => s.purchase_status == 0).toList();
    aList = d.data.where((s) => s.purchase_status == 1).toList();
    rList = d.data.where((s) => s.purchase_status == 2).toList();

    print(pList);
    print(aList);
    print(rList);

    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          title: "My Expenses",
        ),
        body: NestedScrollView(
          key: _keyRed,
          controller: _s_con,
          physics: BouncingScrollPhysics(),
          headerSliverBuilder: (BuildContext context, bool isSwiped) {
            return <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.white,
                expandedHeight: h*0.13,
                snap: false,
                centerTitle: false,
                forceElevated: false,
                pinned: true,
                floating: true,
                //automaticallyImplyLeading: true,
                primary: false,
                //titleSpacing: 20,
                bottom: PreferredSize(
                  preferredSize: Size(double.infinity, 0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: h*0.06351,
                        child: Padding(
                          padding: EdgeInsets.only(left: letfPad*3*w, right: letfPad*3*w, top: topPad*2*h, bottom: topPad*2*h),
                          child:
                              StreamBuilder<Object>(builder: (context, snap) {
                            return Container(
//                          margin: const EdgeInsets.symmetric(horizontal: 5),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 1.0,
                                  ),
                                ],
                                borderRadius: BorderRadius.circular(h*0.035),
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12 /*, vertical: 3*/),
                              child: TextField(
                                controller: search,
                                decoration: InputDecoration(
                                  hintText: 'Search',
                                  hintStyle: TextStyle(
                                    fontSize: h*0.018,
                                  ),
                                  filled: false,
                                  border: InputBorder.none,
                                  counterText: "",
                                ),
                                onSubmitted: (String _) {
                                  _onSearch();
                                },
                                obscureText: false,
                              ),
                            );
                          }),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        color: Colors.white,
                        height: h*0.056,
                        child: TabBar(
                            isScrollable: false,
                            labelColor: Colors.white,
                            labelPadding: EdgeInsets.only(top: 2),
                            labelStyle: TextStyle(
                                fontSize: h*0.014, fontFamily: "SanMedium"),
                            unselectedLabelColor: Color(0xff333545),
                            unselectedLabelStyle:
                                TextStyle(fontFamily: 'SanMedium'),
                            //labelPadding: const EdgeInsets.all(40.0),
                            indicator: BubbleTabIndicator(
                              indicatorHeight: h*0.0376,
                              indicatorColor: appColor,
                              tabBarIndicatorSize: TabBarIndicatorSize.tab,
                            ),
                            controller: _tab_controller,
                            tabs: <Widget>[
                              Tab(
                                text: "Pending",
                              ),
                              Tab(text: "Approved"),
                              Tab(text: "Rejected"),
                            ]),
                      ),
                    ],
                  ),
                ),
              )
            ];
          },
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 2),
            child: TabBarView(
              physics: ClampingScrollPhysics(),
              controller: _tab_controller,
              children: <Widget>[
                MyPending(
                  pList: pList,
                ),
                MyApproved(
                  aList: aList,
                ),
                MyRejected(
                  rList: rList,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
