import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyPoApi {

  ConnectionHelper mCH;

  MyPoApi() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  Future<GetMyPoModel> fetchMyPoDataList(BuildContext context) async {
    GetMyPoModel res_d;
    /* List<GetMyPoModelData> pending = List();
    List<GetMyPoModelData> rejected = List();
    List<GetMyPoModelData> approved = List();*/

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    bool con = await mCH.checkConnection();

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    print(prefs.getString('temp_token'));
    print(accessToken);

    if (con) {
      try {
        final res = await http.get('${BASE_URL}purchase?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}', headers: Headers);

        print("URLs ${BASE_URL}purchase?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = GetMyPoModel.fromJson(j_data);

            print(res_d);

            return res_d;
            break;
          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return GetMyPoModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetMyPoModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    } else {
      return GetMyPoModel.buildErr(1);
    }
  }
}