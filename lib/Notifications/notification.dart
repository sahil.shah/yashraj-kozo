import 'dart:convert';
import 'package:yashrajkozo/budget_alert/budget_alert.dart';
import 'package:yashrajkozo/po_review/po_review.dart';
import 'package:yashrajkozo/po_review/po_review_tablet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Notifications/notification_api.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class NotificationList extends StatefulWidget {
  @override
  _NotificationListState createState() => _NotificationListState();
}

class _NotificationListState extends State<NotificationList> {
  NotificationApi notificationApiObj;
  Future<GetNotificationModel> mF;
  int unseen;

  AppSingleton _singleton = AppSingleton();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    notificationApiObj = NotificationApi();
    mF = notificationApiObj.fetchNotificationList(context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      GetNotificationModel _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        makeSeen(_d);
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
              _d.is_loading = true;
              mF = notificationApiObj.fetchNotificationList(context);
            }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }


  bool isG= false;
  bool isTablet = false;

  Widget _buildList(GetNotificationModel d) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: CustomAppBar(
        title: "Notification",
      ),
      body: d?.data?.length == 0
          ? Center(
        child: Text(
          "No data found",
          style: TextStyle(fontSize: h*0.02117, color: Colors.black),
        ),
      )
          : w < 600 ? ListView.separated(
          padding: EdgeInsets.only(top: h*0.0188),
          physics: BouncingScrollPhysics(),
          separatorBuilder: (BuildContext context, int index) => Divider(
            color: Colors.black38,
          ),
          shrinkWrap: true,
          itemCount: d?.data?.length,
          itemBuilder: (context, i) {
            GetNotificationModelData notiData = d?.data[i];

            return Column(
              children: [
                Container(
                  child: ListTile(
                      leading: Container(
                        child: CircleAvatar(
                          child: Container(
                              padding: EdgeInsets.all(h*0.0094),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(h*0.058),
                                color: Colors.white,
                                image: DecorationImage(
                                    image: NetworkImage(
                                      '${notiData.notification_icon ?? ''}?token=${_singleton.tempToken}',
                                    ),
                                    fit: BoxFit.cover),
                              )),
                          backgroundColor: Colors.transparent,
                        ),
                        height: h*0.05,
                        width: h*0.05,
                      ),

                      title: Text("${notiData.project_name ?? ''}"),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("${notiData.notification_msg ?? ''}"),
                          Align(
                              alignment: Alignment.centerRight, child: Text(notiData.created_date ?? "")),
                        ],
                      ),
                  onTap: (){

                    if(notiData.isBudget == 1) {
                      print(notiData.budgetLog_id);
                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/po_review'),

                          pageBuilder: (c, a1, a2) => BudgetAlert(
                            single : true,
                            budgetId : notiData.budgetLog_id,
                          ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    } else {
                      if(notiData.isGraph == 0) {
                        isG = false;
                      } else{
                        isG = true;
                      }

                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/po_review'),

                          pageBuilder: (c, a1, a2) => w < 600 ? PoReview(
                            pId: notiData.purchase_id,
                            mId: notiData.moderator_user_id,
                            approverId: notiData.approver_id.toString(),
                            pMapId: notiData.pmap_id,
                            isFromUser: isG ?? true,
                            isFromUserPending: isG ?? true,
                          ) : PoReviewTablet(
                            pId: notiData.purchase_id,
                            mId: notiData.moderator_user_id,
                            approverId: notiData.approver_id.toString(),
                            pMapId: notiData.pmap_id,
                            isFromUser: isG ?? true,
                            isFromUserPending: isG ?? true,
                          ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    }


                  },
                  )

                ),

                i == d?.data?.length-1 ? Divider(color: Colors.grey,) : Container(),
              ],
            );
          }) :
      ListView.builder(
          padding: EdgeInsets.only(top: h*0.0188, left: 16.0, right: 16.0),
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          itemCount: d?.data?.length,
          itemBuilder: (context, i) {
            GetNotificationModelData notiData = d?.data[i];
            return Column(
              children: [
                Container(
                    child: GestureDetector(
                      onTap: (){

                        if(notiData.isBudget == 1) {
                          print(notiData.budgetLog_id);
                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/budget_alert'),

                              pageBuilder: (c, a1, a2) => BudgetAlert(
                                single : true,
                                budgetId : notiData.budgetLog_id,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        } else {
                          if(notiData.isGraph == 0) {
                            isG = false;
                          } else{
                            isG = true;
                          }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/po_review'),

                              pageBuilder: (c, a1, a2) => w < 600 ? PoReview(
                                pId: notiData.purchase_id,
                                mId: notiData.moderator_user_id,
                                approverId: notiData.approver_id.toString(),
                                pMapId: notiData.pmap_id,
                                isFromUser: isG ?? true,
                                isFromUserPending: isG ?? true,
                              ) : PoReviewTablet(
                                pId: notiData.purchase_id,
                                mId: notiData.moderator_user_id,
                                approverId: notiData.approver_id.toString(),
                                pMapId: notiData.pmap_id,
                                isFromUser: isG ?? true,
                                isFromUserPending: isG ?? true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        }
                      },
                      child: Card(
                        color: lightGrey,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 90,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("${notiData.project_name ?? ''}", style: TextStyle(color: Colors.black54, fontSize: 17),),
                                    SizedBox(height: 6.0,),
                                    Text("${notiData.notification_msg ?? ''}",style: TextStyle(color: Colors.black, fontSize: 17),),
                                    SizedBox(height: 6.0,),
                                    Text("${notiData.created_date ?? ''}",style: TextStyle(color: Colors.black54, fontSize: 17),),
                                    SizedBox(height: 6.0,),
                                  ],
                                ),
                              ),
                              Expanded(
                                  flex: 10,
                                  child: Icon(Icons.arrow_forward_ios_outlined),)
                            ],
                          ),
                        ),
                      ),
                    ),

                ),

                SizedBox(height: 8,)
              ],
            );
          }),
    );
  }

  void makeSeen(GetNotificationModel d) {
    if(d.data.length > 0) {
      for(int i = 0;i<d.data.length;i++) {
        if(d.data[i].notification_active == 1) {
          updateNotification(d.data[i]);
        }
      }
    }
  }

  void updateNotification(GetNotificationModelData data) async {

    var req = json.encode({
      "notification_id": data.notification_id,
      "project_id": data.project_id,
      "project_name": data.project_name,
      "purchase_id": data.purchase_id,
      "user_id": data.user_id,
      "user_name": data.user_name,
      "notification_icon": data.notification_icon,
      "notification_title": data.notification_title,
      "notification_msg": data.notification_msg,
      "notification_active": 2,
    });

    try {

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String accessToken = prefs.getString('access_token');

      var Headers = {
        "Content-type" : "application/json",
        "Accept": "application/json",
        "authorization": accessToken ?? ""
      };

      final res = await http.put('${BASE_URL}notification/${data.notification_id}?organization_id=${prefs.getInt('orgId')}', headers: Headers, body: req);

      print("URLs $BASE_URL");

      if(res.statusCode == 201 || res.statusCode == 200) {
        print("Done....");
      }

    } catch (err) {
      print(err);
    }

  }
}
