import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:intl/intl.dart';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yashrajkozo/Profile/profile_details.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class EditUserData extends StatefulWidget {
  String user_email;
  String user_name;
  String user_mobile;
  String user_gender;
  String user_address;
  String user_dob;
  int role_id;
  String user_image_pic;
  String user_status;

  EditUserData(
      {Key key,
      this.role_id,
      this.user_mobile,
      this.user_name,
      this.user_gender,
      this.user_dob,
      this.user_address,
      this.user_email,
      this.user_image_pic,
      this.user_status})
      : super(key: key);

  @override
  _EditUserDataState createState() => _EditUserDataState();
}

class _EditUserDataState extends State<EditUserData> {
  final name = new TextEditingController();
  final phone = new TextEditingController();
  final address = new TextEditingController();
  final status = new TextEditingController();
  final dob = new TextEditingController();
  final email = new TextEditingController();

  String userProfileImage;

  AppSingleton _singleton = AppSingleton();

  String date = "";
  File multiImg;
  String filePath = "";
  String fileName = "";
  String imageUpload;
  int groupVal;
  double h,w;
  GlobalKey _containerKey = new GlobalKey();
  Size size;
  double letfPad, topPad;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    imageUpload = widget.user_image_pic;
    name.text = widget.user_name;
    phone.text = widget.user_mobile;
    address.text = widget.user_address;
    dob.text = widget.user_dob;
    email.text = widget.user_email;
    status.text = widget.user_status;
    _getRadioIndex();
  }

  _getRadioIndex() {
    if (widget.user_gender == "Male") {
      groupVal = 0;
    } else {
      groupVal = 1;
    }
  }


  @override
  void dispose() {
    // TODO: implement dispose
    name.dispose();
    phone.dispose();
    address.dispose();
    status.dispose();
    dob.dispose();
    email.dispose();
    super.dispose();
  }

  void removePhoto() {
    setState(() {
      imageUpload = null;
      widget.user_image_pic = null;
    });
  }

  void onGallary(
    BuildContext context,
  ) async {
    multiImg = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    print(multiImg);

    if (multiImg != null) {
      setState(() {});

      uploadImage(context);
    }
  }

  ImageUploadRes imgres;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  uploadImage(BuildContext context) async {
    _showLoading(context);
    FormData formData = new FormData();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // j=1 => Document, j=2 => Camera, j=3 => Gallery
    filePath = multiImg.path;
    fileName = multiImg.path;

    formData = new FormData.fromMap({
      'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
    });
//    formData.add("file[]", UploadFileInfo(File(filePath), fileName));

    Dio dio = new Dio()..options.baseUrl = BASE_URL;
    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        setState(() {
          imageUpload = imgres.data.image_path;
        });
      } else {
        final snackBar = SnackBar(content: Text(imgres.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(content: Text("Cannot load media."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    Navigator.pop(context);
  }

//"https://img.gokozo.com/2a92d266080c611b_org.png"

  @override
  Widget build(BuildContext context) {
     w = MediaQuery.of(context).size.width;
     h = MediaQuery.of(context).size.height;
     letfPad = 0.00944;
     topPad = 0.00470;

    return SafeArea(
      bottom: true,
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        appBar: CustomAppBar(
          title: "Edit Profile",
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              flex: 90,
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.only(top: topPad*h*4),
                children: <Widget>[
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                           w < 600 ? Container(
                              child: GestureDetector(
                                onTap: () {
                                  onGallary(context);
                                },
                                child: CircleAvatar(
                                  child: Container(
                                          padding: EdgeInsets.only(left: letfPad*2*w, right: letfPad*2*w, top: topPad*2*h, bottom: topPad*2*h),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(h*0.058),
                                            color: Colors.white,
                                            image: DecorationImage(
                                                image: NetworkImage(
                                                  '${imageUpload ?? widget.user_image_pic ?? 'https://api.capshun.co/assets/icons/comm_icons/user.png'}?token=${_singleton.tempToken}',
                                                ),
                                                fit: BoxFit.cover),
                                          )),
                                  backgroundColor: Colors.transparent,
                                ),
                              ),
                              height: h*0.094,
                              width: h*0.094,
                            ) : Padding(
                             padding: EdgeInsets.only(left: letfPad*2*w, right: letfPad*2*w, top: topPad*2*h),
                              child: GestureDetector(
                                onTap: () {
                                  onGallary(context);
                                },
                                child: Container(
                                 height: h*0.157,
                                 width: h*0.157,
                                 decoration: BoxDecoration(
                                   color: lightGrey,
                                   shape: BoxShape.circle,
                                   image: DecorationImage(
                                       image: NetworkImage(
                                         '${imageUpload ?? widget.user_image_pic ?? 'https://api.capshun.co/assets/icons/comm_icons/user.png'}?token=${_singleton.tempToken}',
                                       ),
                                       fit: BoxFit.cover),
                                 ),
                           ),
                              ),
                            ),
                            SizedBox(
                              height: h*0.018,
                            ),
                            Container(
                                height: h*0.028,
                                padding: EdgeInsets.only(left: letfPad*2*w),
                                child: FlatButton(
                                  color: Colors.red,
                                  onPressed: () {
                                    (imageUpload == null &&
                                            widget.user_image_pic == null)
                                        ? onGallary(context)
                                        : removePhoto();
                                  },
                                  child: Text(
                                    (imageUpload == null &&
                                            widget.user_image_pic == null)
                                        ? "Add"
                                        : "Remove",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: h*0.016,
                                        fontFamily: 'SanRegular'),
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(
                          width: w*0.0472,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                child: Text(
                              "Name",
                              style: TextStyle(
                                  fontSize: 18, fontFamily: 'SanMedium',color: Colors.black54),
                            )),
                            Container(
                              width: w*0.42,
                              height: h*0.042,
                              child: TextField(
                                readOnly: true,
                                controller: name,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize: h*0.018, color: Colors.black),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Enter name',
                                    hintStyle:
                                        TextStyle(fontFamily: 'SanRegular')),
                              ),
                            ),
                            Text(
                              "Designation",
                              style: TextStyle(
                                fontFamily: 'SanMedium',
                                fontSize: 18,
                                color: Colors.black54
                              ),
                            ),
                            Container(
                              width: w*0.425,
                              height: h*0.0423,
                              child: TextField(
                                readOnly: true,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize: h*0.018, color: Colors.black),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'User',
                                    hintStyle:
                                        TextStyle(fontFamily: 'SanMedium', fontSize: h*0.018, color: Colors.black)),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                 w > 600 ? SizedBox(height: 16,) : Container(),
                  Divider(
                    color: w < 600 ? Colors.black : Colors.black12,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: letfPad*4*w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Your status",
                            style: TextStyle(
                                fontFamily: 'SanMedium', fontSize: h*0.018)),
                        TextField(
                          controller: status,
                          textInputAction: TextInputAction.go,
                          maxLines: 2,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Set your status',
                              hintStyle: TextStyle(
                                  fontFamily: 'SanRegular', fontSize: h*0.016)),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    color: w < 600 ? Colors.black : Colors.black12,
                  ),
                w < 600 ? Column(
                    children: [
                      Container(
                        height: h*0.058,
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          controller: email,
                          decoration: InputDecoration(
                            hintText: 'Email',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            prefixIcon: Container(
                              height: h*0.02,
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                icon: SvgPicture.asset('assets/images/email.svg',
                                    fit: BoxFit.cover, semanticsLabel: 'popup close'),
                                onPressed: null,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: h*0.058,
                        child: TextField(
                          controller: phone,
                          keyboardType: TextInputType.numberWithOptions(),
                          decoration: InputDecoration(
                            hintText: 'PHONE',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            prefixIcon: Container(
                              height: h*0.02,
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                icon: SvgPicture.asset('assets/images/phone.svg',
                                    fit: BoxFit.cover,
                                    width:  h*0.0211,
                                    height:  h*0.0211,
                                    semanticsLabel: 'popup close'),
                                onPressed: null,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: h*0.058,
                        child: TextField(
                          controller: address,
                          decoration: InputDecoration(
                            hintText: 'ADDRESS',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            prefixIcon: Container(
                              height: h*0.02,
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                icon: SvgPicture.asset('assets/images/location.svg',
                                    fit: BoxFit.cover,
                                    height: h*0.02,
                                    width:  h*0.0211,
                                    semanticsLabel: 'popup close'),
                                onPressed: null,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: h*0.058,
                        child: TextField(
                          onTap: () {
                            _selectDate();
                          },
                          readOnly: true,
                          controller: dob,
                          decoration: InputDecoration(
                            hintText: 'DOB',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            prefixIcon: Container(
                              height: h*0.02,
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                icon: SvgPicture.asset('assets/images/birthday.svg',
                                    fit: BoxFit.cover,
                                    width:  h*0.0211,
                                    height: h*0.02,
                                    semanticsLabel: 'popup close'),
                                onPressed: null,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                          height: h*0.058,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: w*0.0118,
                              ),
                              Radio(
                                value: 0,
                                groupValue: groupVal,
                                onChanged: (i) {
                                  setState(() {
                                    groupVal = i;
                                    print(groupVal);
                                  });
                                },
                              ),
                              Text(
                                "Male",
                                style:
                                TextStyle(fontFamily: 'SanRegular', fontSize: h*0.01764),
                              ),
                              Radio(
                                value: 1,
                                groupValue: groupVal,
                                onChanged: (i) {
                                  setState(() {
                                    groupVal = i;
                                    print(groupVal);
                                  });
                                },
                              ),
                              Text(
                                "Female",
                                style:
                                TextStyle(fontFamily: 'SanRegular', fontSize: h*0.01764),
                              )
                            ],
                          )),
                    ],
                  ) : Padding(
                  padding: EdgeInsets.only(left: letfPad*4*w),
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Email", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                      SizedBox(
                        height: h*0.0094/2,
                      ),
                      Container(
                        height: h*0.045,
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          controller: email,
                          style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Email',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),

                          ),
                        ),
                      ),

                      Divider(
                        height: 0.0,
                        color: Colors.black12,
                      ),
                      SizedBox(
                        height: h*0.0094,
                      ),
                      Text("Phone No.", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                      SizedBox(
                        height: h*0.0094/2,
                      ),
                      Container(
                        height: h*0.045,
                        child: TextField(
                          controller: phone,
                          keyboardType: TextInputType.numberWithOptions(),
                          style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'PHONE',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                          ),
                        ),
                      ),
                      Divider(
                        height: 0.0,
                        color: Colors.black12,
                      ),
                      SizedBox(
                        height: h*0.0094,
                      ),
                      Text("Location", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                      SizedBox(
                        height: h*0.0094/2,
                      ),
                      Container(
                        height: h*0.045,
                        child: TextField(
                          controller: address,
                          style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular', decoration: TextDecoration.underline),
                          decoration: InputDecoration(
                            hintText: 'ADDRESS',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      Divider(
                        height: 0.0,
                        color: Colors.black12,
                      ),
                      SizedBox(
                        height: h*0.0094,
                      ),
                      Text("DOB", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                      SizedBox(
                        height: h*0.0094/2,
                      ),
                      Container(
                        height: h*0.045,
                        child: TextField(
                          onTap: () {
                            _selectDate();
                          },
                          readOnly: true,
                          controller: dob,
                          style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular', decoration: TextDecoration.underline),
                          decoration: InputDecoration(
                            hintText: 'DOB',
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      Divider(
                        height: 0.0,
                        color: Colors.black12,
                      ),
                      SizedBox(
                        height: h*0.0094,
                      ),
                      Text("Gender", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                      SizedBox(
                        height: h*0.0094/2,
                      ),
                      Container(
                          height: h*0.045,
                          child: Row(
                            children: <Widget>[

                              Radio(
                                value: 0,
                                groupValue: groupVal,
                                onChanged: (i) {
                                  setState(() {
                                    groupVal = i;
                                    print(groupVal);
                                  });
                                },
                              ),
                              Text(
                                "Male",
                                style:
                                TextStyle(fontFamily: 'SanRegular', fontSize: h*0.01764),
                              ),
                              Radio(
                                value: 1,
                                groupValue: groupVal,
                                onChanged: (i) {
                                  setState(() {
                                    groupVal = i;
                                    print(groupVal);
                                  });
                                },
                              ),
                              Text(
                                "Female",
                                style:
                                TextStyle(fontFamily: 'SanRegular', fontSize: h*0.01764),
                              )
                            ],
                          )),
                    ],
                ),
                  ),
                  SizedBox(
                    height: h*0.018,
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 10,
              child: CustomBtn(
                title: "Update Details",
                onTap: () {
                  UpdateUserDetails();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(1900),
      lastDate: new DateTime.now(),
    );


    String formattedDate = DateFormat('dd-MM-yyyy – kk:mm').format(picked);

    print(formattedDate);

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        dob.text = date;
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }

  void UpdateUserDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    EditProfileModel res_d;

    if (con) {
      try {
        var req = json.encode({
          "user_mobile":
              phone.text.isEmpty ?? '' ? '${widget.user_mobile}' : phone.text,
          "user_email": '${widget.user_email}',
          "user_dob": dob.text.isEmpty ?? '' ? '${widget.user_dob}' : dob.text,
          "user_name":
              name.text.isEmpty ?? '' ? '${widget.user_name}' : name.text,
          "user_gender": groupVal == 0 ? 'Male' : 'Female',
          "user_pic": imageUpload ?? widget.user_image_pic,
          "user_status": status.text,
          "user_address": address.text.isEmpty ?? ''
              ? '${widget.user_address}'
              : address.text,
        });

        print(req);

        showLoading(context);

        final res = await http.post(
            '${BASE_URL}updateprofileinfo?organization_id=${prefs.getInt('orgId')}',
            body: req,
            headers: Headers);

        print(res.body);

        if (res.statusCode == 200 || res.statusCode == 201) {
          dismissLoader();
          final j_data = json.decode(res.body);
          res_d = EditProfileModel.fromJson(j_data);
          if (res_d.Status ?? false) {
            Fluttertoast.showToast(
                msg: res_d.Message,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black,
                textColor: Colors.white,
                fontSize: h*0.018);

            Navigator.of(context).pop({'is_done': true});
          } else if (res.statusCode == 401) {
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
          } else {
            Fluttertoast.showToast(
                msg: res_d.Message,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black,
                textColor: Colors.white,
                fontSize: h*0.018);
          }
        }
      } catch (err) {
        dismissLoader();
      }
    } else {
      final snackBar =
          SnackBar(content: Text("Please check your internet connection"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            //contentPadding: const EdgeInsets.only(left : 16, right: 0, top: 10, bottom: 10),
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), (topPad*4*h)),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), (topPad*4*h)),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  void dismissLoader() {
    Navigator.pop(context);
  }
}
