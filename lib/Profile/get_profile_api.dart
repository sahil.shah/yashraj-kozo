import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class GetProfileDataApi {

  ConnectionHelper mCH;
  GetProfileDataApi() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  bool isFirst = true;

  Future<GetUserProfileModel> fetchUserProfileData(BuildContext context) async {
    GetUserProfileModel res_d;

    bool con = await mCH.checkConnection();
//    bool con = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');
    _singleton.tempToken = prefs.getString('temp_token');

    print("id is:- ${prefs.getInt('orgId')}");
    print("token is $accessToken");

    if(prefs.getString("signColor") != null) {
      String temp = prefs.getString("signColor");
      int val = int.parse("$temp");
      _singleton.signColor = Color(val);
    } else {
      _singleton.signColor = Colors.black;
    }

    if(prefs.getDouble("signStroke") != null) {
      _singleton.signStrokeWidth = prefs.getDouble("signStroke");
    } else {
      _singleton.signStrokeWidth = 2.0;
    }

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization":   _singleton.access_token ?? accessToken
    };

    print("temptoken is ${prefs.getString('temp_token')}");

    if(con) {
      try {

        final res = await http.get('${BASE_URL}getprofileinfo?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}', headers: Headers);

        print("URLs ${BASE_URL}getprofileinfo");

        print('res at GetUserDetail +++++++++++++++++++++++++++++++++++++++++++++++++++ = $res');

        print("res.body = ${res.body}");
        print("res.statusCode = ${res.statusCode}");

      switch (res.statusCode) {
        case 200:
        case 201:
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');
          res_d = GetUserProfileModel.fromJson(j_data);

          _singleton.showCashRequest = res_d.data.isCashMang;
          print("is cahmng ${res_d.data.isCashMang}");
          print("is credit note ${res_d.data.creditNote_isActive}");
          _singleton.isAgainstRequest = res_d.data.isAgainstRequest ?? false;
          _singleton.againstRequestText = res_d.data.againstRequestText ?? "Against Request";

//            res_d.banners.removeAt(0);
//            print("${res_d.banners[0].banner.ID}");
          if(res_d.data.showBudgetAlert == null) {
            res_d.data.showBudgetAlert = false;
          }

            return res_d;
            break;

//        case 401:
//          if(isFirst) {
//            isFirst = false;
//            print("401 hai");
//            fetchUserProfileData();
//          }else {
//            return GetUserProfileModel.buildErr(res.statusCode);
//          }
//          break;
          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return GetUserProfileModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetUserProfileModel.buildErr(0, message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return GetUserProfileModel.buildErr(1);
    }

  }


  Future<SetFavouriteModel> logOutApi() async {
    SetFavouriteModel res_d;

    bool con = await mCH.checkConnection();
//    bool con = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization":   _singleton.access_token ?? accessToken
    };

    if(con) {
      try {

        final res = await http.post('${BASE_URL}logout?organization_id=${prefs.getInt('orgId')}', headers: Headers);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

//            res_d.banners.removeAt(0);
//            print("${res_d.banners[0].banner.ID}");


            return res_d;
          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SetFavouriteModel.buildErr(0, message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return SetFavouriteModel.buildErr(1);
    }

  }
}