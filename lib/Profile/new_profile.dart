import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/constants.dart';

class NewProfilePage extends StatefulWidget {
  @override
  _NewProfilePageState createState() => _NewProfilePageState();
}

class _NewProfilePageState extends State<NewProfilePage> {
  double w, h;
  @override
  Widget build(BuildContext context) {
    w = MediaQuery.of(context).size.width;
    h = MediaQuery.of(context).size.height;
    print(w);
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 200,
            width: w,

            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 180,
                  width: 180,
                  decoration: BoxDecoration(
                      color: lightGrey,
                      shape: BoxShape.circle
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "My PO/Expense",
                        style: TextStyle(
                            fontSize: h*0.0211, fontFamily: 'SanMedium'),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: h*0.02,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("0",
                                  style: TextStyle(
                                      fontSize: h*0.018,
                                      fontFamily: 'SanMedium')),
                              Text(
                                "Pending",
                                style: TextStyle(
                                    fontSize: h*0.016,
                                    fontFamily: 'SanRegular'),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: w*0.028,
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("0",
                                  style: TextStyle(
                                      fontSize: h*0.018,
                                      fontFamily: 'SanMedium')),
                              Text(
                                "Approved",
                                style: TextStyle(
                                    fontSize: h*0.016,
                                    fontFamily: 'SanRegular'),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: w*0.028,
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("0",
                                  style: TextStyle(
                                      fontSize: h*0.018,
                                      fontFamily: 'SanMedium')),
                              Text(
                                "Rejected",
                                style: TextStyle(
                                    fontSize: h*0.016,
                                    fontFamily: 'SanRegular'),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
