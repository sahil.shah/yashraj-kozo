import 'dart:convert';

import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController old;
  TextEditingController newPassword;
  TextEditingController confirm;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AppSingleton _singleton = AppSingleton();
  double h,w;
  double letfPad, topPad;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    confirm = new TextEditingController();
    newPassword = new TextEditingController();
    old = new TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    confirm.dispose();
    newPassword.dispose();
    old.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    letfPad = 0.00944;
    topPad = 0.00470;
    return SafeArea(
      bottom: true,
      child: Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomInset: false,
          appBar: CustomAppBar(
            title: "Change Password",
          ),
          body: w < 600 ? Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: topPad*4*h, bottom: topPad*25*h),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          TextField(
                            controller: old,
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset(
                                        'assets/images/password.svg',
                                        fit: BoxFit.cover,
                                        width: h*0.02117,
                                        height: h*0.02,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                hintText: 'Old Password',
                                hintStyle: TextStyle(fontFamily: 'SanRegular')),
                          ),
                          TextField(
                            controller: newPassword,
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset(
                                        'assets/images/password.svg',
                                        fit: BoxFit.cover,
                                        width: h*0.02117,
                                        height: h*0.02,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                hintText: 'New Password',
                                hintStyle: TextStyle(fontFamily: 'SanRegular')),
                          ),
                          TextField(
                            obscureText: true,
                            controller: confirm,
                            decoration: InputDecoration(
                                prefixIcon: Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset(
                                        'assets/images/password.svg',
                                        fit: BoxFit.cover,
                                        width: h*0.02117,
                                        height: h*0.02,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                hintText: 'Confirm Password',
                                hintStyle: TextStyle(fontFamily: 'SanRegular')),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomBtn(
                    title: "Update Password",
                    onTap: () {
                      updatePassword();
                    },
                  ))
            ],
          ) :
          Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: topPad*4*h, bottom: topPad*25*h),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Card(
                              color: lightGrey,
                              child: TextField(
                                controller: old,
                                obscureText: true,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(left: 16.0),
                                    hintText: 'Enter Old Password',
                                    hintStyle: TextStyle(fontFamily: 'SanRegular')),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Card(
                              color: lightGrey,
                              child: TextField(
                                controller: newPassword,
                                obscureText: true,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(left: 16.0),
                                    hintText: 'Enter New Password',
                                    hintStyle: TextStyle(fontFamily: 'SanRegular')),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Card(
                              color: lightGrey,
                              child: TextField(
                                obscureText: true,
                                controller: confirm,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: EdgeInsets.only(left: 16.0),
                                    hintText: 'Enter Confirm Password',
                                    hintStyle: TextStyle(fontFamily: 'SanRegular')),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomBtn(
                    title: "Update Password",
                    onTap: () {
                      updatePassword();
                    },
                  ))
            ],
          )),
    );
  }

  updatePassword() {
    if (checkErr()) {
      changePassword();
    }
  }

  bool checkErr() {
    if (old.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter password'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (newPassword.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter new password'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (confirm.text.isEmpty) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please enter password to confirm'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (!(newPassword.text == confirm.text)) {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Your passwords are not matching'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

  void changePassword() async {
    ChangePasswordModel res_d;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();

    bool con = await mCH.checkConnection();

    if (con) {
      try {
        var req = json.encode({
          "user_old_password": "${old.text}",
          "user_password": "${confirm.text}"
        });

        final res = await http.post(
            '${BASE_URL}changepassword?organization_id=${prefs.getInt('orgId')}',
            headers: Headers,
            body: req);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ChangePasswordModel.fromJson(j_data);

            if (res_d.Status) {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018);

              Navigator.pop(context);
            } else {
              Fluttertoast.showToast(
                  msg: res_d.Message,
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.black,
                  textColor: Colors.white,
                  fontSize: h*0.018);
            }

            break;

          case 401:
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
        }
      } catch (err) {}
    } else {
      final snackBar = SnackBar(
          duration: const Duration(milliseconds: 500),
          content: Text('Please check your internet connection'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
