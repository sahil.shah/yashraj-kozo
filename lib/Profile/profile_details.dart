import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yashrajkozo/Home/home.dart';
import 'package:yashrajkozo/budget_alert/budget_alert.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/Profile/edit_user_data.dart';
import 'package:yashrajkozo/Profile/get_profile_api.dart';
import 'package:yashrajkozo/Profile/profile_change_password.dart';
import 'package:yashrajkozo/Profile/profile_feedback.dart';
import 'package:yashrajkozo/Profile/profile_help.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String userProfileImage;
  GetProfileDataApi getProfileDataApiObj;
  Future<GetUserProfileModel> mF;

  AppSingleton _singleton = AppSingleton();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey _containerKey ;
  Size size;
  double letfPad, topPad;
  double h, w;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _containerKey = new GlobalKey();
    getProfileDataApiObj = GetProfileDataApi();
    mF = getProfileDataApiObj.fetchUserProfileData(context);
    _getOrgId();
  }

  getSize() {
    RenderBox containerBox = _containerKey.currentContext.findRenderObject();
    size = containerBox.size;
    print("size is $size");
  }

  int selectedOrg = -1;

  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      GetUserProfileModel _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
                  _d.is_loading = true;
                  mF = getProfileDataApiObj.fetchUserProfileData(context);
                }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  logoutUser() async {
    _showLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    SetFavouriteModel res_d = await getProfileDataApiObj.logOutApi();
    if (res_d.Status ?? false) {
      _dismissLoader(context);
      await prefs.setString("access_token", null);
      await prefs.setInt('userId', null);
      await prefs.setInt('orgId', null);
      await prefs.setString('temp_token', null);
      await prefs.setString('firebase_token', null);
      await prefs.setString('userImage', null);
      await prefs.setString('userName', null);

     // await prefs.clear();

      _singleton.userId = 0;
      _singleton.userName = "";
      _singleton.access_token = "";
      _singleton.orgId = 0;
      _singleton.userOrgList = [];

      Navigator.pushReplacement(
        context,
        PageRouteBuilder(
          settings: RouteSettings(name: '/login'),
          pageBuilder: (c, a1, a2) => LoginPage(),
          transitionsBuilder: (c, anim, a2, child) =>
              FadeTransition(opacity: anim, child: child),
          transitionDuration: Duration(milliseconds: 500),
        ),
      );
    } else {
      _dismissLoader(context);
      final snackBar = SnackBar(content: Text(res_d.Message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Widget _buildList(GetUserProfileModel d) {
    w = MediaQuery.of(context).size.width;
    h = MediaQuery.of(context).size.height;

    letfPad = 0.00944;
    topPad = 0.00470;

    GetUserProfileModelData dataObj = d?.data;

    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 5,
          leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back, color: appColor,),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                  onPressed: () {
                    selectOrganization(dataObj);
                  },
                  //color: appColor,
                  child: Row(
                    children: <Widget>[
                      Text(
                        "${dataObj.user_name ?? "My Profile"}",
                        style: TextStyle(
                            color: appColor,
                            fontFamily: 'SanSemiBold',
                            fontSize: h*0.02),
                      ),
//                    SizedBox(
//                      width: 8.0,
//                    ),

                      Icon(
                        Icons.keyboard_arrow_down,
                        color: appColor,
                      )
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0)))),
              SizedBox(
                width: w*0.085,
              )
            ],
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        body: ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(top: topPad*4*w, bottom:  topPad*8*w),
          children: <Widget>[
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Container(
                          width: w,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                             w < 600 ? Container(
                                child: CircleAvatar(
                                  child: Container(
                                      padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(h*0.058),
                                        color: Colors.white,
                                        image: DecorationImage(
                                            image: NetworkImage(
                                              '${dataObj.user_pic ?? 'https://api.capshun.co/assets/icons/comm_icons/user.png'}?token=${_singleton.tempToken}',
                                            ),
                                            fit: BoxFit.cover),
                                      )),
                                  backgroundColor: Colors.transparent,
                                ),
                                height: h*0.094,
                                width: w*0.188,
                              ) : Container(
                               height: h*0.162,
                               width: h*0.162,
                               decoration: BoxDecoration(
                                   color: lightGrey,
                                   shape: BoxShape.circle,
                                 image: DecorationImage(
                                     image: NetworkImage(
                                       '${dataObj.user_pic ?? 'https://api.capshun.co/assets/icons/comm_icons/user.png'}?token=${_singleton.tempToken}',
                                     ),
                                     fit: BoxFit.cover),
                               ),
                             ),
                              SizedBox(
                                width: w*0.018,
                              ),
                              Column(
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      "My PO/Expense",
                                      style: TextStyle(
                                          fontSize: h*0.0211, fontFamily: 'SanMedium'),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(
                                    height: h*0.0094,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("${dataObj.pending ?? 0}",
                                                style: TextStyle(
                                                    fontSize: h*0.018,
                                                    fontFamily: 'SanMedium')),
                                            Text(
                                              "Pending",
                                              style: TextStyle(
                                                  fontSize: h*0.016,
                                                  color: Colors.black54,
                                                  fontFamily: 'SanRegular'),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: w*0.028,
                                      ),
                                      Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("${dataObj.approved ?? 0}",
                                                style: TextStyle(
                                                    fontSize: h*0.018,
                                                    fontFamily: 'SanMedium')),
                                            Text(
                                              "Approved",
                                              style: TextStyle(
                                                  fontSize: h*0.016,
                                                  color: Colors.black54,
                                                  fontFamily: 'SanRegular'),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: w*0.028,
                                      ),
                                      Container(
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("${dataObj.rejected ?? 0}",
                                                style: TextStyle(
                                                    fontSize: h*0.018,
                                                    fontFamily: 'SanMedium')),
                                            Text(
                                              "Rejected",
                                              style: TextStyle(
                                                  fontSize: h*0.016,
                                                  color: Colors.black54,
                                                  fontFamily: 'SanRegular'),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                          height: h*0.0023
                      ),
                      Container(
                        padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                        child: Row(
                          children: <Widget>[
                           w < 600 ? Text(
                              "${dataObj.user_name ?? ''}",
                              style: TextStyle(fontFamily: 'SanRegular'),
                            ) : Text(
                             "${dataObj.user_name ?? ''}",
                             style: TextStyle(fontFamily: 'SanRegular', fontSize: 17, fontWeight: FontWeight.bold),
                           ),
                            SizedBox(
                              width: w*0.018,
                            ),
                            FlatButton(
                                onPressed: () async {
                                  var results = await Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      settings: RouteSettings(name: '/edit_profile'),
                                      pageBuilder: (c, a1, a2) => EditUserData(
                                        user_name: dataObj.user_name ?? '',
                                        user_mobile:
                                            dataObj.user_mobile.toString() ?? '',
                                        user_gender: dataObj.user_gender ?? '',
                                        user_address: dataObj.user_address ?? '',
                                        user_dob: dataObj.user_dob ?? '',
                                        user_email: dataObj.user_email ?? '',
                                        role_id: dataObj.role_id ?? 0,
                                        user_image_pic: dataObj.user_pic ?? '',
                                        user_status: dataObj.user_status ?? '',
                                      ),
                                      transitionsBuilder: (c, anim, a2, child) =>
                                          FadeTransition(opacity: anim, child: child),
                                      transitionDuration: Duration(milliseconds: 500),
                                    ),
                                  );

                                  if (results != null &&
                                      results.containsKey('is_done')) {
                                    if (results['is_done']) {
                                      GetUserProfileModel res = await mF;
                                      res.is_loading = true;
                                      res.Status = false;
                                      mF = getProfileDataApiObj
                                          .fetchUserProfileData(context);
                                      setState(() {});
                                    }
                                  }
                                },
                                child: w < 600 ? Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.edit,
                                      size: h*0.01881,
                                    ),
                                    Text(
                                      "Edit",
                                      style: TextStyle(fontFamily: 'SanRegular'),
                                    )
                                  ],
                                ) : Icon(FontAwesomeIcons.edit))
                          ],
                        ),
                      ),
                      Divider(
                        color: Colors.black12,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: letfPad*4*w),
                          child: Text(
                            "Your status",
                            style: TextStyle(fontSize: h*0.018, fontFamily: 'SanMedium'),
                          )),
                      Container(
                          padding: EdgeInsets.only(left: letfPad*4*w),
                          child: Text(
                            "${dataObj.user_status ?? '-'}",
                            style: TextStyle(fontSize: h*0.01764, fontFamily: 'SanRegular'),
                          )),
                      Divider(
                        color: Colors.black12,
                      ),
                      w < 600 ?Column(
                        children: <Widget>[
                          Container(
                            height: h*0.0247,
                            child: Row(
                              key: _containerKey,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset('assets/images/email.svg',
                                        fit: BoxFit.cover,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Container(
                                  child: Text(
                                    "${dataObj.user_email ?? ''}",
                                    style: TextStyle(fontFamily: 'SanRegular'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Container(
                            height: h*0.0247,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset('assets/images/phone.svg',
                                        fit: BoxFit.cover,
                                        width: w*0.04,
                                        height: h*0.02,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Text(
                                  "${dataObj.user_mobile ?? ''}",
                                  style: TextStyle(fontFamily: 'SanRegular'),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Container(
                            height: h*0.0247,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset('assets/images/location.svg',
                                        fit: BoxFit.cover,
                                        height: h*0.02,
                                        width: w*0.04,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Text("${dataObj.user_address ?? ''}",
                                    style: TextStyle(fontFamily: 'SanRegular')),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Container(
                            height: h*0.0247,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset('assets/images/birthday.svg',
                                        fit: BoxFit.cover,
                                        width: w*0.04,
                                        height: h*0.02,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Text("${dataObj.user_dob ?? ''}",
                                    style: TextStyle(fontFamily: 'SanRegular')),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Container(
                            height: h*0.0247,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  height: h*0.02,
                                  child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: SvgPicture.asset('assets/images/gender.svg',
                                        fit: BoxFit.cover,
                                        height: h*0.02,
                                        width: w*0.04,
                                        semanticsLabel: 'popup close'),
                                    onPressed: null,
                                  ),
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Text("${dataObj.user_gender ?? ''}",
                                    style: TextStyle(fontFamily: 'SanRegular')),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/help'),
                                  pageBuilder: (c, a1, a2) => Help(),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Container(
                              height: h*0.0247,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset(
                                          'assets/images/question-mark.svg',
                                          fit: BoxFit.cover,
                                          height: h*0.02,
                                          width: w*0.04,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  SizedBox(
                                    width: w*0.018,
                                  ),
                                  Text(
                                    "Help",
                                    style: TextStyle(
                                        color: appColor, fontFamily: 'SanRegular'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/feedback'),
                                  pageBuilder: (c, a1, a2) => FeedbackPage(),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Container(
                              height: h*0.0247,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset(
                                          'assets/images/question-mark.svg',
                                          fit: BoxFit.cover,
                                          width: w*0.04,
                                          height: h*0.02,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  SizedBox(
                                    width: w*0.018,
                                  ),
                                  Text(
                                    "Feedback",
                                    style: TextStyle(
                                        color: appColor, fontFamily: 'SanRegular'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/change_password'),
                                  pageBuilder: (c, a1, a2) => ChangePassword(),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Container(
                              height: h*0.0247,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset(
                                          'assets/images/question-mark.svg',
                                          fit: BoxFit.cover,
                                          width: w*0.04,
                                          height: h*0.02,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  SizedBox(
                                    width: w*0.018,
                                  ),
                                  Text(
                                    "Change Password",
                                    style: TextStyle(
                                        color: appColor, fontFamily: 'SanRegular'),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          SizedBox(
                            height: h*0.0094,
                          ),
                          Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ),
                        d.data.showBudgetAlert ? SizedBox(
                            height: h*0.0094,
                          ) : Container(),
                         d.data.showBudgetAlert ? InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/po_review'),
                                  pageBuilder: (c, a1, a2) => BudgetAlert(),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Container(
                              height: h*0.0247,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: h*0.02117,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset('assets/images/exit.svg',
                                          fit: BoxFit.cover,
                                          width: w*0.0425,
                                          height: h*0.02117,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  SizedBox(
                                    width: w*0.01888,
                                  ),
                                  Text(
                                    "Budget Alert",
                                    style: TextStyle(
                                        color: Colors.green, fontFamily: 'SanMeduim'),
                                  ),
                                ],
                              ),
                            ),
                          ) : Container(),

//                    Divider(
//                      height: 0.0,
//                      color: Colors.black87,
//                    ),
//                    SizedBox(height: h*0.0094,),
//                    InkWell(
//                      onTap: () {
//                        Navigator.push(
//                          context,
//                          PageRouteBuilder(
//                            settings: RouteSettings(name: '/user_chart'),
//                            pageBuilder: (c, a1, a2) => UserChart(),
//                            transitionsBuilder: (c, anim, a2, child) =>
//                                FadeTransition(opacity: anim, child: child),
//                            transitionDuration: Duration(milliseconds: 500),
//                          ),
//                        );
//
//                      },
//                      child: Row(
//                        children: <Widget>[
//                          Container(
//                            height: 18,
//                            child: IconButton(
//                              padding: EdgeInsets.all(0),
//                              icon: Icon(FontAwesomeIcons.chartBar),
//                              onPressed: null,
//                            ),
//                          ),
//                          SizedBox(width: 8,),
//                          Text("Chart", style: TextStyle(fontFamily: 'SanRegular', color: appColor)),
//                        ],
//                      ),
//                    ),

                        d.data.showBudgetAlert ? SizedBox(
                            height: h*0.0094,
                          ) : Container(),
                         d.data.showBudgetAlert ? Divider(
                            height: 0.0,
                            color: Colors.black87,
                          ) : Container(),
                          SizedBox(
                            height: h*0.0094,
                          ),
                          InkWell(
                            onTap: () {
                              logoutUser();
                            },
                            child: Container(
                              height: h*0.0247,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: h*0.02,
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      icon: SvgPicture.asset('assets/images/exit.svg',
                                          fit: BoxFit.cover,
                                          width: w*0.04,
                                          height: h*0.02,
                                          semanticsLabel: 'popup close'),
                                      onPressed: null,
                                    ),
                                  ),
                                  SizedBox(
                                    width: w*0.018,
                                  ),
                                  Text(
                                    "Logout",
                                    style: TextStyle(
                                        color: appColor, fontFamily: 'SanMeduim'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ) :
                      Padding(
                        padding: EdgeInsets.only(left: letfPad*4*w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            Text("Email", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("${dataObj.user_email}", style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("Phone No.", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("${dataObj.user_mobile}", style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),

                            Text("Location", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("${dataObj.user_address}", style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular', decoration: TextDecoration.underline),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("DOB", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("${dataObj.user_dob ?? ""}", style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("Gender", style: TextStyle(color: Colors.black54, fontSize: 18, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Text("${dataObj.user_gender}", style: TextStyle(color: Colors.black, fontSize: h*0.018, fontFamily: 'SanRegular'),),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            
                           Wrap(
                              runSpacing: 32,
                              spacing: 32,
                              direction: Axis.horizontal,
                              children: <Widget>[

                                d.data.showBudgetAlert ? Container(
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)
                                    ),
                                    onPressed: (){
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          settings: RouteSettings(name: '/budget_alert'),
                                          pageBuilder: (c, a1, a2) => BudgetAlert(),
                                          transitionsBuilder: (c, anim, a2, child) =>
                                              FadeTransition(opacity: anim, child: child),
                                          transitionDuration: Duration(milliseconds: 500),
                                        ),
                                      );
                                    },
                                    color: lightGrey,
                                    child: Text("BUDGET ALERT", style: TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'SanRegular', fontWeight: FontWeight.w500),),
                                  ),
                                ) : Container(),

                                Container(
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8)
                                    ),
                                    onPressed: (){
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          settings: RouteSettings(name: '/change_password'),
                                          pageBuilder: (c, a1, a2) => ChangePassword(),
                                          transitionsBuilder: (c, anim, a2, child) =>
                                              FadeTransition(opacity: anim, child: child),
                                          transitionDuration: Duration(milliseconds: 500),
                                        ),
                                      );
                                    },
                                    color: lightGrey,
                                    child: Text("CHANGE PASSWORD", style: TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'SanRegular', fontWeight: FontWeight.w500),),
                                  ),
                                ),

                                Container(
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)
                                    ),
                                    onPressed: (){
                                      logoutUser();
                                    },
                                    color: lightGrey,
                                    child: Text("LOGOUT", style: TextStyle(color: Colors.black, fontSize: 18, fontFamily: 'SanRegular', fontWeight: FontWeight.w500),),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Divider(
                              height: 0.0,
                              color: Colors.black12,
                            ),
                            SizedBox(
                              height: h*0.0094,
                            ),
                            Container(
                              height: h*0.049,
                              width: w*0.375,
                              decoration: BoxDecoration(
                                color: appColor,
                                borderRadius: BorderRadius.circular(50)
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  FlatButton(
                                    onPressed: (){
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          settings: RouteSettings(name: '/help'),
                                          pageBuilder: (c, a1, a2) => Help(),
                                          transitionsBuilder: (c, anim, a2, child) =>
                                              FadeTransition(opacity: anim, child: child),
                                          transitionDuration: Duration(milliseconds: 500),
                                        ),
                                      );
                                    },
                                    child: Text("HELP", style: TextStyle(color: Colors.white, fontSize: 18, fontFamily: 'SanRegular',),),
                                  ),
                                  VerticalDivider(color: Colors.white, thickness: 2,),
                                  FlatButton(
                                    onPressed: (){
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          settings: RouteSettings(name: '/feedback'),
                                          pageBuilder: (c, a1, a2) => FeedbackPage(),
                                          transitionsBuilder: (c, anim, a2, child) =>
                                              FadeTransition(opacity: anim, child: child),
                                          transitionDuration: Duration(milliseconds: 500),
                                        ),
                                      );
                                    },
                                    child: Text("FEEDBACK", style: TextStyle(color: Colors.white, fontSize: 18, fontFamily: 'SanRegular',),),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: (letfPad*4*w), right: (letfPad*4*w), top: (topPad*4*h), bottom: (topPad*4*h)),
                    alignment:Alignment.centerRight,
                      width: w,
                      child: Text("version 10.0", style: TextStyle(fontSize: h*0.01),),)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showLoading() async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            //contentPadding: const EdgeInsets.only(left : 16, right: 0, top: 10, bottom: 10),
            content: Container(
              width: w*0.4772,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.only(left: (letfPad*4*w), right: (letfPad*4*w), top: (topPad*4*h), bottom: (topPad*4*h)),
                  ),
                  Text(
                    'Loading...',
                    style: TextStyle(fontFamily: 'SanRegular'),
                  ),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  void _dismissLoader(BuildContext context) {
    Navigator.of(context).pop();
  }

  void selectOrganization(GetUserProfileModelData data) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(h*0.01881),
                topRight: Radius.circular(h*0.01881))),
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, setState) => Container(
                  padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                  child: Wrap(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            height: h*0.00470,
                            width: w*0.108,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(h*0.01))),
                          ),
                          ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _singleton?.userOrgList?.length ?? 0,
                              itemBuilder: (context, i) {
                                return Column(
                                  children: <Widget>[
                                    ListTile(
                                      onTap: () {
                                        navFun(context, i);
                                      },
/*  leading: CircleAvatar(
                              child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    color: Colors.white,
                                    image: DecorationImage(
                                        image: NetworkImage(
                                          '${data.user_pic ?? ''}',
                                        ),
                                        fit: BoxFit.cover),
                                  )),
                              backgroundColor: Colors.transparent,
                            ),*/
                                      title: new Text(
                                        _singleton.userOrgList[i]
                                                .organization_name ??
                                            "",
                                        style:
                                            TextStyle(fontFamily: 'SanMedium'),
                                      ),
                                      trailing: (selectedOrg == i ||
                                              _singleton.userOrgList[i]
                                                      .isSelected ==
                                                  true)
                                          ? Radio(
                                              value: true,
                                              groupValue: true,
                                              onChanged: (i) {},
                                              activeColor: appColor,
                                            )
                                          /* Icon(
                                          FontAwesomeIcons.check,
                                          color: Colors.green,
                                          size: h*0.0164614,
                                        )*/
                                          : null,
                                    ),
                                  ],
                                );
                              }),
                        ],
                      ),
                    ],
                  )));
        });
  }

  void navFun(BuildContext context, int i) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setInt('orgId', _singleton.userOrgList[i].organization_id);
    selectedIndex = i;
    _singleton.orgId = _singleton.userOrgList[i].organization_id;

    for (int k = 0; k < _singleton.userOrgList.length; k++) {
      _singleton.userOrgList[k].isSelected = false;
    }

    _singleton.userOrgList[i].isSelected = true;

    setState(() {});

    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        settings: RouteSettings(name: '/home'),
        pageBuilder: (c, a1, a2) => MyHomePage(),
        transitionsBuilder: (c, anim, a2, child) =>
            FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 500),
      ),
    );
  }

  void _getOrgId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    selectedOrg = prefs.getInt('orgId');
  }
}
