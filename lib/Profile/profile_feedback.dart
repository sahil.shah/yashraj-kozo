import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FeedbackPage extends StatefulWidget {
  @override
  _FeedbackState createState() => _FeedbackState();
}

class _FeedbackState extends State<FeedbackPage> {
  final msg = TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ConnectionHelper mCH;

  AppSingleton _singleton = AppSingleton();
  double h,w;
  double letfPad, topPad;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mCH = ConnectionHelper.getInstance();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    msg.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    letfPad = 0.00944;
    topPad = 0.00470;
    return SafeArea(
      bottom: true,
      child: Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomInset: false,
          appBar: CustomAppBar(
            title: "Feedback",
          ),
          body: w < 600 ?
          Stack(children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    left: letfPad*4*w, right: letfPad*4*w, top: topPad*4*h, bottom: topPad*25*h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "FeedBack",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: h*0.0258,
                          fontFamily: 'SanMedium'),
                    ),
                    SizedBox(
                      height: h*0.0047,
                    ),
                    Text(
                      "We value your feedback the most. Write us a line.",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: h*0.018,
                          fontFamily: 'SanRegular'),
                    ),
                    SizedBox(
                      height: h*0.0235,
                    ),
                    Text(
                      "Message",
                      style: TextStyle(color: appColor, fontFamily: 'SanMedium'),
                    ),
                    SizedBox(height: h*0.0094,),
                    Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(h*0.01)),
                      child: new Container(
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                          height: h*0.1176,
                          child: TextField(
                            textInputAction: TextInputAction.go,
                            maxLines: 4,
                            controller: msg,
                            decoration: InputDecoration(
                              filled: false,
                                counterText: "",
                                border: InputBorder.none,
                                hintText: 'Your Message...',
                                hintStyle: TextStyle(fontFamily: 'SanRegular')),
                          )),
                    ),
                    SizedBox(height: h*0.0094),

                  ],
                )),
            Align(
              alignment: Alignment.bottomCenter,
              child: CustomBtn(
                title: "Send your Feedback",
                onTap: (){
                  sendFeedBack();
                },
              )
            )
          ]) : Stack(children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    left: letfPad*4*w, right: letfPad*4*w, top: topPad*4*h, bottom: topPad*25*h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Feedback",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: h*0.0258,
                          fontFamily: 'SanMedium'),
                    ),
                    SizedBox(
                      height: h*0.0047,
                    ),
                    Text(
                      "We value your feedback immensely, write us a line and we will get back to you as early as possible.",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: h*0.018,
                          fontFamily: 'SanRegular'),
                    ),
                    SizedBox(
                      height: h*0.0235,
                    ),

                    Card(
                      color: lightGrey,
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(h*0.01)),
                      child: new Container(
                          padding: EdgeInsets.all(0),
                          margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                          height: h*0.1176,
                          child: TextField(
                            textInputAction: TextInputAction.go,
                            maxLines: 4,
                            controller: msg,
                            decoration: InputDecoration(
                                filled: false,
                                counterText: "",
                                border: InputBorder.none,
                                hintText: 'Your Message...',
                                hintStyle: TextStyle(fontFamily: 'SanRegular')),
                          )),
                    ),
                    SizedBox(height: h*0.0094),

                  ],
                )),
            Align(
                alignment: Alignment.bottomCenter,
                child: CustomBtn(
                  title: "SEND FEEDBACK",
                  onTap: (){
                    sendFeedBack();
                  },
                )
            )
          ])),
    );
  }

  sendFeedBack() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    FeedbackModel res_d;
    ConnectionHelper mCH;
    mCH = ConnectionHelper.getInstance();
    bool con = await mCH.checkConnection();

    if (checkErr()) {
      if (con) {
        try {
          showLoading(context);
          var req = json.encode({
            "feedback_msg": msg.text,
          });

          final res = await http.post(
              '${BASE_URL}feedback?organization_id=${prefs.getInt('orgId')}', body: req, headers: Headers);

          switch (res.statusCode) {
            case 200:
            case 201:
              final j_data = json.decode(res.body);
              print('Res ---> ${res.body}');
              res_d = FeedbackModel.fromJson(j_data);
              print(res_d);
              if (res_d.Status ?? false) {
                dismissLoader();
                Fluttertoast.showToast(
                    msg: res_d.Message,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black,
                    textColor: Colors.white,
                    fontSize: h*0.018
                );

                Navigator.pop(context);
              } else {
                dismissLoader();
                Navigator.pop(context);
                Fluttertoast.showToast(
                    msg: res_d.Message,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black,
                    textColor: Colors.white,
                    fontSize: h*0.018
                );
              }
              break;

            case 401 :
              Navigator.pushReplacement(
                context,
                PageRouteBuilder(
                  settings: RouteSettings(name: '/login'),
                  pageBuilder: (c, a1, a2) => LoginPage(),
                  transitionsBuilder: (c, anim, a2, child) =>
                      FadeTransition(opacity: anim, child: child),
                  transitionDuration: Duration(milliseconds: 500),
                ),
              );
              break;

            default:
              return FeedbackModel.buildErr(res.statusCode);
          }
        } catch (err) {

        }
      } else {
        final snackBar =
        SnackBar(
            duration: const Duration(milliseconds: 500),
            content: Text('Please check your internet Connection'));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }

  }

  bool checkErr() {
    if (msg.text.isEmpty) {
      final snackBar =
          SnackBar(
              duration: const Duration(milliseconds: 500),
              content: Text('Please enter some message for feedback'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

  void showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            //contentPadding: const EdgeInsets.only(left : 16, right: 0, top: 10, bottom: 10),
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  void dismissLoader() {
    Navigator.pop(context);
  }
}
