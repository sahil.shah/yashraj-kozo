import 'package:yashrajkozo/Profile/profile_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_expansion_tile.dart';

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  IconData iconData;

  ProfileVM pVM;
  Future<HelpModel> mF;

  double h,w;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pVM = ProfileVM();
    mF = pVM.getHelpData();
    iconData = Icons.add;
  }

  @override
  Widget build(BuildContext context) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      HelpModel _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
                  _d.is_loading = true;
                  mF = pVM.getHelpData();
                }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  Widget _buildList(HelpModel d) {
    w = MediaQuery.of(context).size.width;
    print(w);
    return w < 600 ? Scaffold(
        appBar: CustomAppBar(
          title: "Help",
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 16),
          child: d.data.length > 0
              ? ListView.separated(
                  itemBuilder: (context, i) {
                    return Column(
                      children: <Widget>[
                        CustomExpansionTile(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 16, right: 16),
                                child: Text(d.data[i].body ?? "",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: h*0.016,
                                        fontFamily: 'SanRegular')),
                              ),
                            )
                          ],

//                            trailing: (isTap && lIndex == i) ? Icon(Icons.remove , color: Colors.black,size: 20): Icon(Icons.add , color: Colors.black,size: 20),
                          title: Text(
                            d.data[i].title ?? "",
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: h*0.018,
                                fontFamily: 'SanMedium'),
                          ),
                        ),
                      ],
                    );
                  },
                  separatorBuilder: (context, i) => Divider(
                        color: Colors.black,
                      ),
                  itemCount: d.data.length)
              : dummyData(),
        )) : Scaffold(
        appBar: CustomAppBar(
          title: "Help",
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 16),
          child: d.data.length > 0
              ? ListView.separated(
              padding: const EdgeInsets.only(left: 16, right: 16),
              itemBuilder: (context, i) {
                return Column(
                  children: <Widget>[
                    CustomExpansionTile(
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(top: 12.0),
                          child: Text(d.data[i].body ?? "",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: h*0.016,
                                  fontFamily: 'SanRegular')),
                        )
                      ],

//                            trailing: (isTap && lIndex == i) ? Icon(Icons.remove , color: Colors.black,size: 20): Icon(Icons.add , color: Colors.black,size: 20),
                      title: Text(
                        d.data[i].title ?? "",
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: h*0.018,
                            fontFamily: 'SanMedium'),
                      ),
                    ),
                    SizedBox(height: 8.0,)
                  ],
                );
              },
              separatorBuilder: (context, i) => Divider(
                color: Colors.black12,
              ),
              itemCount: d.data.length)
              : dummyData(),
        ));
  }

  Widget dummyData() {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        CustomExpansionTile(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Text(
                  "No, unfortunately this will not be allowed however you can view your submitted expenses & purchase orders.",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: h*0.016,
                      fontFamily: 'SanRegular')),
            ),
          ],
          title: Text(
            "Can I edit an expense or purchase order after submitting it?",
            style: TextStyle(
                color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanMedium'),
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        CustomExpansionTile(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Text(
                  "The sole reason for this is that there are no expenses or purchase orders for you to approve at the moment.",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: h*0.016,
                      fontFamily: 'SanRegular')),
            ),
          ],
          title: Text(
            "User PO section has no data",
            style: TextStyle(
                color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanMedium'),
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        CustomExpansionTile(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: Text("Unfortunately, not.",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: h*0.016,
                        fontFamily: 'SanRegular')),
              ),
            )
          ],
          title: Text(
            "Once I have approved or rejected an expense or purchase order, can I change my mind?",
            style: TextStyle(
                color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanMedium'),
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        CustomExpansionTile(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Text(
                  "Kindly go to your phone settings and check if the notification is enabled for KOZO application",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: h*0.016,
                      fontFamily: 'SanRegular')),
            ),
          ],
          title: Text(
            "I am not receiving notifications.",
            style: TextStyle(
                color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanMedium'),
          ),
        ),
      ],
    );
  }
}
