import 'dart:convert';

import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileVM {
  ConnectionHelper mCH;

  ProfileVM() {
    mCH = ConnectionHelper.getInstance();
  }

  var data = [
    {
      "title": "Can I edit an expense or purchase order after submitting it?",
      "body": "No, unfortunately this will not be allowed however you can view your submitted expenses & purchase orders."
    },
    {
      "title": "User PO section has no data.",
      "body": "The sole reason for this is that there are no expenses or purchase orders for you to approve at the moment."
    },
    {
      "title": "Once I have approved or rejected an expense or purchase order, can I change my mind?",
      "body": "Unfortunately, not."
    },
    {
      "title": "I am not receiving notifications.",
      "body": "Kindly go to your phone settings and check if the notification is enabled for KOZO application"
    }
  ];

  Future<HelpModel> getHelpData() async {
    HelpModel res_d;

    // bool con = true;

    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    if (con) {
      try {
        final res = await http.get(
            "${BASE_URL}help?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = HelpModel.fromJson(j_data);

            print(res_d);

            return res_d;

          default:
            return HelpModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return HelpModel.buildErr(0,
            message: res_d.Message ?? "Something went wrong. Please try again later");
      }
    } else {
      return HelpModel.buildErr(1);
    }
  }

}
