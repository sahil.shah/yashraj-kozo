import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class UserChart extends StatefulWidget {
  @override
  _UserChartState createState() => _UserChartState();
}

class _UserChartState extends State<UserChart> {

  @override
  Widget build(BuildContext context) {

    var data = [
      new ClicksPerYear('2018', 12, appColor),
      new ClicksPerYear('2017', 42,appColor),
      new ClicksPerYear('2016',52, appColor),
      new ClicksPerYear('2015', 12, appColor),
      new ClicksPerYear('2014', 42, appColor),
      new ClicksPerYear('2013',52 ,appColor),
    ];

    var series = [
      new charts.Series(
        id: 'Clicks',
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => charts.MaterialPalette.purple.shadeDefault,
        data: data,
      ),
    ];

    var chart = new charts.BarChart(series,animate: true,);


    var chartWidget = new Padding(
      padding: new EdgeInsets.all(32.0),
      child: new SizedBox(
        height: MediaQuery.of(context).size.height/2,
        child: chart,
      ),
    );

    return Scaffold(
      appBar: CustomAppBar(
        title: "Chart Analysis",
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            chartWidget,
          ],
        ),
      )
    );
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final Color color;

  ClicksPerYear(this.year, this.clicks, this.color);

}