import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProjectDetailApi {

  ConnectionHelper mCH;

  ProjectDetailApi(){
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  Future<ProjectDetailTreeModel> fetchProjectDetailTreeUser(String projectID, BuildContext context) async {
    ProjectDetailTreeModel res_d;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    bool con = await mCH.checkConnection();
//    bool con = true;

    if (con) {
      try {


        final res = await http.get(
            '${BASE_URL}projectmap/project/$projectID?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}', headers: Headers);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = ProjectDetailTreeModel.fromJson(j_data);

//            res_d.banners.removeAt(0);
//            print("${res_d.banners[0].banner.ID}");


            return res_d;
          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return ProjectDetailTreeModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return ProjectDetailTreeModel.buildErr(
            0, message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return ProjectDetailTreeModel.buildErr(1);
    }
  }
}