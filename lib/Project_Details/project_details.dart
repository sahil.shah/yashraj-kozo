import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/cash_request/cash_request.dart';
import 'package:yashrajkozo/cash_request/tablet_cash_request.dart';
import 'package:yashrajkozo/create_challan/tablet_challan.dart';
import 'package:yashrajkozo/create_expense/create_expense_tablet.dart';
import 'package:yashrajkozo/create_po/tablet_po.dart';
import 'package:yashrajkozo/credit_note/credit_note_tablet.dart';
import 'package:yashrajkozo/credit_note/new_cash_note.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Project_Details/project_detail_api.dart';
import 'package:yashrajkozo/create_challan/create_challan.dart';
import 'package:yashrajkozo/create_expense/create_expense.dart';
import 'package:yashrajkozo/create_po/create_po.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProjectDetails extends StatefulWidget {
  final String imageUrl;
  final String projectID;
  final String projectName;
  final String projectDetails;
  bool showFourOptions;
  int creditNote;
  //List customIds;

  ProjectDetails(
      {Key key,
      this.projectID,
      this.imageUrl,
      this.showFourOptions,
      this.projectDetails,
      this.creditNote,
   //   this.customIds,
      this.projectName})
      : super(key: key);

  @override
  _ProjectDetailsState createState() => _ProjectDetailsState();
}

class _ProjectDetailsState extends State<ProjectDetails> {
  ProjectDetailApi projectDetailApiObj;
  Future<ProjectDetailTreeModel> mF;

//  GetCustomFieldModel customFieldModel;
//  bool customFieldDikhao;

  AppSingleton _singleton = AppSingleton();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.showFourOptions);
    print(widget.creditNote);
   // customFieldDikhao = false;
    projectDetailApiObj = ProjectDetailApi();
    mF = projectDetailApiObj.fetchProjectDetailTreeUser(widget.projectID, context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      ProjectDetailTreeModel _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
                  _d.is_loading = true;
                  mF = projectDetailApiObj
                      .fetchProjectDetailTreeUser(widget.projectID, context);
                }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  Widget _buildList(ProjectDetailTreeModel d) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          title: "Project Detail",
        ),
        body: ListView(
          physics: AlwaysScrollableScrollPhysics(),
          padding: EdgeInsets.all(h*0.0188),
          children: <Widget>[
            Container(
              height: h*0.27,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(h*0.0141),
                ),
                elevation: 3,
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(h*0.0141)),
                    child: FadeInImage(
                      fit: BoxFit.cover,
                      placeholder: AssetImage("assets/images/bg.png"),
                      image: NetworkImage("${widget.imageUrl ?? ''}?token=${_singleton.tempToken}"),
                    )),
              ),
            ),
            SizedBox(
              height : h*0.014,
            ),
            Container(
              child: Text(
                "${widget.projectName ?? ''}",
                style: TextStyle(fontSize: h*0.02117, fontFamily: 'SanSemiBold'),
              ),
            ),
            SizedBox(
              height: h*0.0094,
            ),
           widget.projectDetails != "" && widget.projectDetails != null ? Container(
              child: Text(
                "${widget.projectDetails ?? ''}",
                style: TextStyle(fontSize : h*0.014, fontFamily: 'SanRegular'),
                maxLines: 4,
              ),
            ) : Container(),
            SizedBox(
              height: h*0.0094,
            ),
            widget.showFourOptions ?

            (w < 600 ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[0] ?? 1);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_po'),
                                  pageBuilder: (c, a1, a2) => CreatePo(
                                    projectName: widget.projectName ?? '',
                                    projectID: widget.projectID ?? '',
                                    // customFieldModel: customFieldModel ?? null,
                                    // customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Purchase ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Order",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),

                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async{

//                    customFieldModel = await getCustomFields(widget.customIds[1] ?? 2);
//
//                   if(customFieldModel.data !=null){
//                     if(customFieldModel.data.length > 0) {
//                       setState(() {
//                         customFieldDikhao = true;
//                       });
//                     }else {
//                       setState(() {
//                         customFieldDikhao = false;
//                       });
//                     }
//                   }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_expense'),
                                  pageBuilder: (c, a1, a2) => CreateExpense(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    // customFieldModel: customFieldModel ?? null,
                                    // customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Expense",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                    SizedBox(height: h*0.00994,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_challan'),
                                  pageBuilder: (c, a1, a2) => CreateChallan(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    //  customFieldModel: customFieldModel ?? null,
                                    //  customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Delivery ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Challan",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),

                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/cash_request'),
                                  pageBuilder: (c, a1, a2) => CashRequestPage(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    //  customFieldModel: customFieldModel ?? null,
                                    //  customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Advance ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Request",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ) : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[0] ?? 1);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_po'),
                                  pageBuilder: (c, a1, a2) => PoTabletPage(
                                    projectName: widget.projectName ?? '',
                                    projectID: widget.projectID ?? '',
                                    // customFieldModel: customFieldModel ?? null,
                                    // customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Purchase ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Order",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),

                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async{

//                    customFieldModel = await getCustomFields(widget.customIds[1] ?? 2);
//
//                   if(customFieldModel.data !=null){
//                     if(customFieldModel.data.length > 0) {
//                       setState(() {
//                         customFieldDikhao = true;
//                       });
//                     }else {
//                       setState(() {
//                         customFieldDikhao = false;
//                       });
//                     }
//                   }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_expense'),
                                  pageBuilder: (c, a1, a2) => CreateExpenseTablet(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    // customFieldModel: customFieldModel ?? null,
                                    // customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Expense",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                    SizedBox(height: h*0.00994,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/create_challan'),
                                  pageBuilder: (c, a1, a2) => ChallanTabletPage(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    //  customFieldModel: customFieldModel ?? null,
                                    //  customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Create ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Delivery ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Challan",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),

                        Container(
                          height: h*0.058,
                          width: w*0.45,
                          child: RaisedButton(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                            onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/cash_request'),
                                  pageBuilder: (c, a1, a2) => CashRequestTabletPage(
                                    projectName : widget.projectName ?? '',
                                    projectID : widget.projectID ?? '',
                                    //  customFieldModel: customFieldModel ?? null,
                                    //  customFieldDikha: customFieldDikhao,
                                    isFromDetail: true,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Wrap(
                              alignment: WrapAlignment.center,
                              children: [
                                Text("Advance ",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                                Text("Request",
                                    textAlign: TextAlign.center,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ))
              : w < 600 ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[0] ?? 1);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_po'),
                              pageBuilder: (c, a1, a2) => CreatePo(
                                projectName: widget.projectName ?? '',
                                projectID: widget.projectID ?? '',
                                // customFieldModel: customFieldModel ?? null,
                                // customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Purchase",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Order",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: w*0.0188/2,
                    ),
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async{

//                    customFieldModel = await getCustomFields(widget.customIds[1] ?? 2);
//
//                   if(customFieldModel.data !=null){
//                     if(customFieldModel.data.length > 0) {
//                       setState(() {
//                         customFieldDikhao = true;
//                       });
//                     }else {
//                       setState(() {
//                         customFieldDikhao = false;
//                       });
//                     }
//                   }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_expense'),
                              pageBuilder: (c, a1, a2) => CreateExpense(
                                projectName : widget.projectName ?? '',
                                projectID : widget.projectID ?? '',
                                // customFieldModel: customFieldModel ?? null,
                                // customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Expense",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: w*0.0188/2,
                    ),
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_challan'),
                              pageBuilder: (c, a1, a2) => CreateChallan(
                                projectName : widget.projectName ?? '',
                                projectID : widget.projectID ?? '',
                                //  customFieldModel: customFieldModel ?? null,
                                //  customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Delivery",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Challan",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),

                  ],
                ),
              ],
            ) : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[0] ?? 1);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_po'),
                              pageBuilder: (c, a1, a2) => PoTabletPage(
                                projectName: widget.projectName ?? '',
                                projectID: widget.projectID ?? '',
                                // customFieldModel: customFieldModel ?? null,
                                // customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Purchase",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Order",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: w*0.0188/2,
                    ),
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async{

//                    customFieldModel = await getCustomFields(widget.customIds[1] ?? 2);
//
//                   if(customFieldModel.data !=null){
//                     if(customFieldModel.data.length > 0) {
//                       setState(() {
//                         customFieldDikhao = true;
//                       });
//                     }else {
//                       setState(() {
//                         customFieldDikhao = false;
//                       });
//                     }
//                   }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_expense'),
                              pageBuilder: (c, a1, a2) => CreateExpenseTablet(
                                projectName : widget.projectName ?? '',
                                projectID : widget.projectID ?? '',
                                // customFieldModel: customFieldModel ?? null,
                                // customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Expense",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: w*0.0188/2,
                    ),
                    Container(
                      height : h*0.0941,
                      width: w / 3 - 28,
                      child: RaisedButton(
                        color: Colors.grey.shade200,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                        onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                          Navigator.push(
                            context,
                            PageRouteBuilder(
                              settings: RouteSettings(name: '/create_challan'),
                              pageBuilder: (c, a1, a2) => ChallanTabletPage(
                                projectName : widget.projectName ?? '',
                                projectID : widget.projectID ?? '',
                                //  customFieldModel: customFieldModel ?? null,
                                //  customFieldDikha: customFieldDikhao,
                                isFromDetail: true,
                              ),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration: Duration(milliseconds: 500),
                            ),
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Create",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Delivery",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                            Text("Challan",
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                          ],
                        ),
                      ),
                    ),

                  ],
                ),
              ],
            ),
            widget.creditNote == 1 ? SizedBox(height: h*0.018/2,) : Container(),
            widget.creditNote == 1 ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: h*0.058,
                  width: w*0.45,
                  child: RaisedButton(
                    color: Colors.grey.shade200,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(h*0.01))),
                    onPressed: () async {

//                    customFieldModel = await getCustomFields(widget.customIds[2] ?? 3);
//
//                    if(customFieldModel.data !=null){
//                      if(customFieldModel.data.length > 0) {
//                        setState(() {
//                          customFieldDikhao = true;
//                        });
//                      }else {
//                        setState(() {
//                          customFieldDikhao = false;
//                        });
//                      }
//                    }

                      Navigator.push(
                        context,
                        PageRouteBuilder(
                          settings: RouteSettings(name: '/create_credit_note'),
                          pageBuilder: (c, a1, a2) => w < 600 ? CreditNotePage(
                            projectName : widget.projectName ?? '',
                            projectID : widget.projectID ?? '',
                            //  customFieldModel: customFieldModel ?? null,
                            //  customFieldDikha: customFieldDikhao,
                            isFromDetail: true,
                          ) : CreditNoteTabletPage(
                            projectName : widget.projectName ?? '',
                            projectID : widget.projectID ?? '',
                            //  customFieldModel: customFieldModel ?? null,
                            //  customFieldDikha: customFieldDikhao,
                            isFromDetail: true,
                          ),
                          transitionsBuilder: (c, anim, a2, child) =>
                              FadeTransition(opacity: anim, child: child),
                          transitionDuration: Duration(milliseconds: 500),
                        ),
                      );
                    },
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        Text("Credit ",
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),
                        Text("Note",
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontFamily: 'SanMedium', fontSize : h*0.014)),

                      ],
                    ),
                  ),
                ),
              ],
            ) : Container(),
            SizedBox(
              height: h*0.018,
            ),
            Text("Members", style: TextStyle(fontFamily: 'SanBold')),
            Divider(
              color: Colors.black,
            ),
            MediaQuery.removePadding(
              context: context,
              removeLeft: true,
              removeBottom: true,
              removeRight: true,
              removeTop: true,
              child: d.data.length > 0 ? ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: d?.data?.length ?? 0,
                  itemBuilder: (BuildContext context, int index) {
                  ProjectDetailTreeModelData dataArray = d?.data[index] ?? [];


                    return ListTile(
                      title: Text(
                        "${dataArray.user_name ?? ''}",
                        style: TextStyle(fontFamily: 'SanMeduim'),
                      ),
                      subtitle: Text("${dataArray.user_status ?? ''}",
                          style: TextStyle(fontFamily: 'SanRegular')),
                      leading: Container(
                        child: CircleAvatar(
                          child: Container(
                              padding: EdgeInsets.all(h*0.0094),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(h*0.058),
                                color: Colors.white,
                                image: DecorationImage(
                                    image: NetworkImage(
                                      '${dataArray.user_pic ?? 'https://api.capshun.co/assets/icons/comm_icons/user.png'}?token=${_singleton.tempToken}',
                                    ),
                                    fit: BoxFit.cover),
                              )),
                          backgroundColor: Colors.transparent,
                        ),
                        height: h*0.05,
                        width: h*0.05,
                      ),

                      /* CircleAvatar(
                        radius: 25,
                        child: FadeInImage(
                          fit: BoxFit.cover,
                          placeholder: AssetImage("assets/images/bg.png"),
                          image: NetworkImage("${dataArray.user_pic}"),
                        )
                      ),*/
                    );
                  }) :
              Center(
                child: Text(
                  "No Members Available!",
                  style: TextStyle(fontFamily: 'SanMedium', fontSize: h*0.018),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  getCustomFields(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    GetCustomFieldModel res_d;

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    try {
      final res =
      await http.get("${BASE_URL}custom_key/Expense/$id?organization_id=${prefs.getInt('orgId')}", headers: Headers);

      print("URLs $BASE_URL");
      switch (res.statusCode) {
        case 200:
        case 201:
          final j_data = json.decode(res.body);
          print('Res ---> ${res.body}');
          res_d = GetCustomFieldModel.fromJson(j_data);

//            res_d.banners.removeAt(0);
//            print("${res_d.banners[0].banner.ID}");
          return res_d;

          break;
        case 401:
          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
              settings: RouteSettings(name: '/login'),
              pageBuilder: (c, a1, a2) => LoginPage(),
              transitionsBuilder: (c, anim, a2, child) =>
                  FadeTransition(opacity: anim, child: child),
              transitionDuration: Duration(milliseconds: 500),
            ),
          );
          break;

        default:
        //return GetCustomKeyModel.buildErr(res.statusCode);
          print(res.statusCode);
      }
    } catch (err) {
      return GetCustomFieldModel.buildErr(0,
          message: "Somthing went wrong. Please try again later.");
    }
  }
}

/*

 */
