import 'package:flutter/material.dart';
import 'package:yashrajkozo/Tablet/form_page.dart';


class AppFormButton extends StatelessWidget {
  final String hintText;
  final void Function() onTap;

  AppFormButton({Key key, this.hintText, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);

    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        margin: const EdgeInsets.symmetric(vertical: 32.0),
        decoration: BoxDecoration(
          color: _theme.canvasColor,
          borderRadius: appBorderRadius,
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0).add(
            EdgeInsets.symmetric(vertical: 8, horizontal: 24),
          ),
          child: Text(hintText),
        ),
      ),
    );
  }
}
