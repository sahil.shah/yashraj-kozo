import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Tablet/form_page.dart';

class AppFormDropDownButton extends StatelessWidget {
  final String hintText;
  final String value;
  final bool emphasizeHint;
  final List<DropdownMenuItem<dynamic>> items;
  final void Function(dynamic) onChanged;

  AppFormDropDownButton(
      {Key key,
      this.hintText,
      this.items,
      this.onChanged,
      this.value,
      bool emphasizeHint})
      : emphasizeHint = emphasizeHint ?? false,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 24),
      margin: const EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        color: _theme.canvasColor,
        borderRadius: appBorderRadius,
      ),
      child: DropdownButtonFormField(
        elevation: 0,
        hint: Text(
          value.isEmpty ? hintText : value,
          style: emphasizeHint ? TextStyle(fontWeight: FontWeight.w600) : null,
        ),
        // hint: Text(hintText),
        items: items,
        onChanged: onChanged,
        icon: Icon(
          Icons.keyboard_arrow_down,
          color: Colors.black,
        ),
        decoration: InputDecoration(hintText: '', border: InputBorder.none),
      ),
    );
  }
}
