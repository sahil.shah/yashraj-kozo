import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Tablet/strings.dart';

final appBorderRadius = BorderRadius.circular(6);
final appColorRed = Colors.red[400];
final _scaffoldBodyPadding = const EdgeInsets.all(16);

class AppFormActionButton extends StatelessWidget {
  final IconData icon;
  final String title;
  final void Function() onTap;

  AppFormActionButton(this.title, this.icon, {this.onTap});

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    return Padding(
      padding: EdgeInsets.only(left: 12, bottom: 8, top: 8),
      child: Material(
        color: _theme.accentColor,
        borderRadius: appBorderRadius,
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  icon,
                  color: _theme.iconTheme.color,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(
                    title,
                    style: _theme.textTheme.button,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AppFormPage extends StatelessWidget {
  final String title;
  final List<AppFormActionButton> formActionButtons;
  final List<Widget> formBody;
  final void Function() onSubmit;
  final bool hideSubmitButton;

  const AppFormPage(
      {Key key,
      this.title,
      List<AppFormActionButton> formActionButtons,
      List<Widget> formBody,
      this.onSubmit,
      this.hideSubmitButton = false})
      : this.formActionButtons = formActionButtons ?? const [],
        this.formBody = formBody ?? const [],
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      // Adding this in MaterialApp would be better
      data: ThemeData(
        scaffoldBackgroundColor: Colors.grey[300],
        iconTheme: IconThemeData(color: Colors.white),
        // Used by [AppFormActionButton]
        accentColor: appColorRed,
        buttonColor: appColorRed,
        canvasColor: Colors.grey[350],
        cursorColor: appColorRed,
        fontFamily: 'Poppins',
        textTheme: TextTheme(
          headline1: TextStyle(
              fontSize: 93, fontWeight: FontWeight.w300, letterSpacing: -1.5),
          headline2: TextStyle(
              fontSize: 58, fontWeight: FontWeight.w300, letterSpacing: -0.5),
          headline3: TextStyle(fontSize: 46, fontWeight: FontWeight.w400),
          headline4: TextStyle(
              fontSize: 33, fontWeight: FontWeight.w400, letterSpacing: 0.25),
          headline5: TextStyle(fontSize: 23, fontWeight: FontWeight.w400),
          // Used in appbar titles
          headline6: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.15,
            color: appColorRed,
          ),
          subtitle1: TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, letterSpacing: 0.15),
          subtitle2: TextStyle(
              fontSize: 13, fontWeight: FontWeight.w500, letterSpacing: 0.1),
          bodyText1: TextStyle(
              fontSize: 15, fontWeight: FontWeight.w400, letterSpacing: 0.5),
          bodyText2: TextStyle(
              fontSize: 13, fontWeight: FontWeight.w400, letterSpacing: 0.25),
          // Used in buttons
          button: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w500,
            letterSpacing: 1.25,
            color: Colors.white,
          ),
          caption: TextStyle(
              fontSize: 12, fontWeight: FontWeight.w400, letterSpacing: 0.4),
          overline: TextStyle(
              fontSize: 10, fontWeight: FontWeight.w400, letterSpacing: 1.5),
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: appColorRed,
        ),
        appBarTheme: AppBarTheme(
          centerTitle: true,
          elevation: 0,
          color: Colors.white,
          shadowColor: Colors.grey[200],
          // Used by icons of [AppFormActionButton]
          iconTheme: IconThemeData(color: appColorRed),
          actionsIconTheme: IconThemeData(color: appColorRed),
        ),
      ),
      child: _AppFormPage(
        title: title,
        formActionButtons: formActionButtons,
        formBody: formBody,
        onSubmit: onSubmit,
        hideSubmitButton: hideSubmitButton,
      ),
    );
  }
}

class _AppFormPage extends StatelessWidget {
  final String title;
  final List<AppFormActionButton> formActionButtons;
  final List<Widget> formBody;
  final void Function() onSubmit;
  final bool hideSubmitButton;

  const _AppFormPage({
    Key key,
    this.title,
    List<AppFormActionButton> formActionButtons,
    List<Widget> formBody,
    this.onSubmit,
    this.hideSubmitButton = false,
  })  : this.formActionButtons = formActionButtons ?? const [],
        this.formBody = formBody ?? const [],
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    final _headHeight = formActionButtons.length * 60.0;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: CupertinoNavigationBarBackButton(
            color: _theme.appBarTheme.iconTheme.color,
          ),
        ),
        title: Text(title, style: _theme.textTheme.headline6),
      ),
      body: ListView(
        children: [
          Padding(
            padding: _scaffoldBodyPadding,
            child: Column(
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints.tightFor(height: _headHeight),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.only(right: 50),
                          decoration: BoxDecoration(
                            color: _theme.canvasColor,
                            borderRadius: appBorderRadius,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            children: formActionButtons,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ...formBody,
              ],
            ),
          ),
          hideSubmitButton
              ? SizedBox()
              : ConstrainedBox(
                  constraints: const BoxConstraints(
                      minWidth: double.infinity, maxHeight: 55),
                  child: Material(
                    color: Theme.of(context).buttonColor,
                    child: InkWell(
                      onTap: onSubmit,
                      child: Center(
                        child: Text(
                          FormStrings.submit,
                          style: _theme.textTheme.button,
                        ),
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
