import 'package:flutter/material.dart';
import 'package:yashrajkozo/Tablet/form_page.dart';

class AppFormField extends StatelessWidget {
  final String hintText;
  final String infoText;
  final int maxLines;
  final TextEditingController controller;

  AppFormField({Key key, this.hintText, this.controller, this.infoText, this.maxLines})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: TextFormField(
        controller: controller,
        maxLines: maxLines ?? 1,
        decoration: InputDecoration(
          fillColor: _theme.canvasColor,
          filled: true,
          enabledBorder: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: appBorderRadius,
          ),
          border: new OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: appBorderRadius,
          ),
          contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 24),
          hintText: hintText,
          focusColor: _theme.accentColor,
          helperText: infoText,
        ),
      ),
    );
  }
}
