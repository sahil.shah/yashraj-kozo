class FormStrings {
  FormStrings._();
  static const uploadFromGallery = 'UPLOAD FROM GALLERY';
  static const useTheCamera = 'USE THE CAMERA';
  static const enterYourChallanName = 'Enter Your Challan Name';
  static const selectProject = 'Select Project';
  static const selectPO = 'Select PO';
  static const enterExpenseAmount = 'Enter Expense Amount';
  static const taxesIncluded = 'Taxes excluded';
  static const challanDescription = 'Challan Description';
  static const submit = 'SUBMIT';
  static const selectExpense = 'Select Expense';
  static const enterInvoiceNumber = 'Enter Invoice Number';
  static const enterInvoiceDate = 'Enter Invoice Date';
  static const enterTaxValue = 'Enter Tax Value';
  static const totalAmount = 'Total Amount';
  static const expenseDescription = 'Expense Description';
  static const enterYourVendorName = 'Enter Your Vendor Name';
  static const selectCategory = 'Select Category';
  static const selectSubCategory = 'Select Sub Category';
  static const selectApprover = 'Select Approver';
  static const modeOfPayment = 'Mode of Payment';
  static const enterVendorName = 'Enter Vendor Name';
  static const enterPaymentDueDate = 'Enter Payment Due Date';
  static const alreadyApproved = 'Already Approved';
}
