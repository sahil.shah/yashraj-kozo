import 'package:yashrajkozo/po_review/po_review_tablet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/po_review/po_review.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class UserApproved extends StatefulWidget {
  List<GetUserPoModelData> aData;

  UserApproved({this.aData});

  @override
  _UserApprovedState createState() => _UserApprovedState();
}

class _UserApprovedState extends State<UserApproved> {
  AppSingleton _singleton = AppSingleton();
  double letfPad, topPad;
  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    letfPad = 0.00944;
    topPad = 0.00470;
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: widget.aData.length > 0
              ? ListView.separated(
                  physics: BouncingScrollPhysics(),
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(color: Colors.black54,),
                  shrinkWrap: true,
                  padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), topPad*4*h),
                  itemCount: widget.aData.length ?? 0,
                  itemBuilder: (context, i) {
                    GetUserPoModelData fData = widget.aData[i] ?? [];
                    List<String> p = [];

                    if (fData.purchase_image != null) {
                      p = fData.purchase_image.split(',');
                      print(p);
                    }
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                settings: RouteSettings(name: '/po_review'),
                                pageBuilder: (c, a1, a2) => w < 600 ? PoReview(
                                  isFromUser: true,
                                  isFromUserPending: false,
                                  pId: fData.purchase_id,
                                ) : PoReviewTablet(
                                  isFromUser: true,
                                  isFromUserPending: false,
                                  pId: fData.purchase_id,
                                ),
                                transitionsBuilder: (c, anim, a2, child) =>
                                    FadeTransition(opacity: anim, child: child),
                                transitionDuration: Duration(milliseconds: 500),
                              ),
                            );
                          },
                          child: Container(
                            color: Colors.white,
                            width: w,
                            margin: EdgeInsets.only(top: topPad*h*2, bottom: topPad*h*2),
                            child: Row(
                              children: <Widget>[
                                Stack(
                                  alignment: Alignment.topRight,
                                  children: <Widget>[
                                    Card(
                                        elevation: 2,
                                        child: (fData.approver_id == "0" || (p.length > 0 && p[0] != '-'))
                                            ? Container(
                                                constraints: BoxConstraints(
                                                    minHeight: h*0.13,
                                                  maxHeight: h*0.16,
                                                  maxWidth: w*0.28),
                                                width: w*0.28,
                                                child: p.length > 0
                                                    ? Image(
                                                        image: NetworkImage(((p[
                                                                            0]
                                                                        .contains(
                                                                            'png') ||
                                                                    p[0].contains(
                                                                        'jpg') ||
                                                                    p[0].contains(
                                                                        'jpeg') ||
                                                                    p[0].contains(
                                                                        'tif'))
                                                                ? "${p[0]}?token=${_singleton.tempToken}"
                                                                : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW") ??
                                                            "https://developers.google.com/maps/documentation/streetview/images/error-image-generic.png"),
                                                        fit: BoxFit.cover,
                                                      )
                                                    : Container(),
                                              )
                                            : Container(
                                                width: w*0.28,
                                                constraints: BoxConstraints(
                                                    minHeight: h*0.13,
                                                  maxHeight: h*0.16,
                                                    minWidth: w*0.28),
                                                child: Center(
                                                  child: Text(
                                                    fData.po_name
                                                            .substring(0, 2)
                                                            .toUpperCase() ??
                                                        "",
                                                    style: TextStyle(
                                                        fontSize: h*0.035,
                                                        fontFamily: 'SanBold'),
                                                  ),
                                                ),
                                              )),
                                    Icon(
                                      FontAwesomeIcons.solidCheckCircle,
                                      color: Colors.green,
                                    )
                                  ],
                                ),
                                SizedBox(
                                  width: w*0.018,
                                ),
                                Container(
                                  width: w - (w*0.4061),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        fData.po_name ?? '',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(fontFamily: 'SanBold'),
                                      ),
                                      SizedBox(
                                        height: h*0.0094,
                                      ),
                                      Text(
                                        fData.purchase_description ?? '',
                                        maxLines: 3,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontFamily: 'SanMedium',
                                            fontSize: h*0.016,
                                            color: Colors.black54),
                                      ),
                                      SizedBox(
                                        height: h*0.0047,
                                      ),
                                      RichText(
                                          textAlign: TextAlign.start,
                                          text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: 'Project: ',
                                                  style: TextStyle(
                                                      fontFamily: 'SanSemiBold',
                                                      color: Colors.black54,
                                                      fontSize: h*0.016)),
                                              TextSpan(
                                                  text:
                                                      fData.project_name ?? '',
                                                  style: TextStyle(
                                                      fontFamily: 'SanMedium',
                                                      color: Colors.black54,
                                                      fontSize: h*0.016)),
                                            ],
                                          )),
                                      SizedBox(
                                        height: h*0.0047,
                                      ),
                                      RichText(
                                          textAlign: TextAlign.start,
                                          text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: 'Created by: ',
                                                  style: TextStyle(
                                                      fontFamily: 'SanSemiBold',
                                                      color: Colors.black54,
                                                      fontSize: h*0.016)),
                                              TextSpan(
                                                  text: fData.user_name ?? '',
                                                  style: TextStyle(
                                                      fontFamily: 'SanMedium',
                                                      color: Colors.black54,
                                                      fontSize: h*0.016)),
                                            ],
                                          )),

                                      SizedBox(
                                        height: h*0.0047,
                                      ),
                                      RichText(
                                          textAlign: TextAlign.start,
                                          text: TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: 'Created On: ',
                                                  style: TextStyle(
                                                      fontFamily: 'SanSemiBold',
                                                      color: Colors.black54,
                                                      fontSize: h*0.016)),
                                              TextSpan(
                                                  text: fData.created_date ?? "",
                                                  style: TextStyle(
                                                      fontFamily: 'SanMedium',
                                                      color: Colors.black54,
                                                      fontSize: h*0.014)),
                                            ],
                                          )),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  })
              : Center(
                  child: Text(
                    "No Data Found!",
                    style: TextStyle(fontFamily: 'SanMedium', fontSize: h*0.018),
                  ),
                ),
        ));
  }
}
