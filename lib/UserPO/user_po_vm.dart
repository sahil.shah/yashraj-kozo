import 'dart:convert';
import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPoApi {

  ConnectionHelper mCH;

  UserPoApi() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  Future<GetUserPoModel> fetchUserPoDataList(BuildContext context) async {
    GetUserPoModel res_d;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    print(accessToken);
    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    print("Id is :- ${prefs.getInt('orgId')}");

    bool con = await mCH.checkConnection();

//    bool con = true;

    if(con) {
      try {

        final res = await http.get(
            '${BASE_URL}purchase/other?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}', headers: Headers);

        print("URLs $BASE_URL");

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = GetUserPoModel.fromJson(j_data);


            return res_d;

          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return GetUserPoModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetUserPoModel.buildErr(0, message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return GetUserPoModel.buildErr(1);
    }
  }
}