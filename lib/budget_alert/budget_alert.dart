import 'package:flutter/material.dart';
import 'package:yashrajkozo/budget_alert/budget_API.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';

class BudgetAlert extends StatefulWidget {
  
  bool single;
  int budgetId;


  BudgetAlert({this.single = false, this.budgetId = 9999});

  @override
  _BudgetAlertState createState() => _BudgetAlertState();
}

class _BudgetAlertState extends State<BudgetAlert> {
  double h,w;

  Future<BudgetAlertModel> mF;
  BudgetApi budgetApi;
  bool seeMore = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    budgetApi = BudgetApi();
    mF = budgetApi.fetchBudgetAlertData(context, widget.single, widget.budgetId);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      BudgetAlertModel _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _buildList(_d);
      } else if (_d.is_loading ?? false) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
              _d.is_loading = true;
              mF = budgetApi.fetchBudgetAlertData(context, widget.single, widget.budgetId);
            }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  @override
  Widget _buildList(BudgetAlertModel d) {
    h = MediaQuery.of(context).size.height;
    w = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: CustomAppBar(
        title: "Budget Alert",
      ),
      body: d.data.length == 0 ? Center(
        child: Text("You have no notifications!", style: TextStyle(color: Colors.black),),
      ) : SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(h*0.014),
          child: Column(
            children: d.data.map((e) {
              return Container(
                width: w,
                padding: EdgeInsets.only(top: h*0.014),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(h*0.0188),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(h*0.0188),
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: h*0.014, vertical: h*0.014),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Username", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.user_name}", style: TextStyle(fontSize: h*0.017)),
                          SizedBox(height: h*0.014,),

                          // Text("Initial Amount", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          // SizedBox(height: 4,),
                          // Text("${e.amount}", style: TextStyle(fontSize: h*0.017)),
                          // SizedBox(height: h*0.014,),

                          Text("Edited Budget Amount", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.amount}", style: TextStyle(fontSize: h*0.017)),
                          SizedBox(height: h*0.014,),

                          Text("Category", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.category_name}", style: TextStyle(fontSize: h*0.017)),
                          SizedBox(height: h*0.014,),

                          Text("Sub Category", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.sub_category_name}", style: TextStyle(fontSize: h*0.017)),
                          SizedBox(height: h*0.014,),

                          Text("Reason", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.reason}", style: TextStyle(fontSize: 15), maxLines: seeMore ? 100 : 2, overflow: TextOverflow.ellipsis,),
                          SizedBox(height: h*0.0047,),
                          GestureDetector(
                              onTap: (){
                                seeMore = !seeMore;
                                setState(() {});
                              },
                              child: Text(seeMore ? "See Less" : "See More", style: TextStyle(color: appColor),)),
                          SizedBox(height: h*0.014,),

                          Text("Date", style: TextStyle(color: Colors.black54, fontSize: h*0.014)),
                          SizedBox(height: h*0.0047,),
                          Text("${e.budgetLog_created_date}", style: TextStyle(fontSize: h*0.017)),
                          SizedBox(height: h*0.014,),

                        ],
                      ),
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
