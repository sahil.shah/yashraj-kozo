import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:yashrajkozo/create_po/category_sheet.dart';
import 'package:yashrajkozo/create_po/create_po_vm.dart';
import 'package:yashrajkozo/utils/autoselect.dart';

import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/walk.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/createAllVM/create_all_vm.dart';
import 'package:yashrajkozo/create_po/category_sheet.dart';
import 'package:yashrajkozo/create_po/create_po_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/custom_popup.dart';
import 'package:yashrajkozo/utils/text_style.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_singleton.dart';

class CashRequestPage extends StatefulWidget {

  String projectName;
  String projectID;
  bool isFromDetail;
  // GetCustomFieldModel customFieldModel;
  // bool customFieldDikha;

  CashRequestPage({
    Key key,
    this.projectName,
    this.projectID,
    this.isFromDetail = false,
    //  this.customFieldDikha = false,
    //  this.customFieldModel
  }) : super(key: key);

  @override
  _CashRequestPageState createState() => _CashRequestPageState();
}

class _CashRequestPageState extends State<CashRequestPage> {
  final _titleC = TextEditingController();
  final _projectC = TextEditingController();
  final _shootLocation = TextEditingController();
  final _cateC = TextEditingController();
  final _subCatC = TextEditingController();
  final _amountC = TextEditingController();
  final _approverC = TextEditingController();
  final _poDescriptionController = TextEditingController();

  AppSingleton _singleton = AppSingleton();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  CreateAllVM cVM;
  HomeApi homeApiObj;
  CreatePoVM sVM;

  //for all project
  GetProjectModel projectData;
  List<ProjectModelData> projectList = [];
  List<String> pNameList = [];

  // for all category
  SelectCategoryModel categoryData;
  List<SelectCategoryModelData> categoryList = [];
  List<String> cNameList = [];


  //for all approver
  SelectApproverModel approverData;
  List<SelectApproverModelData> approverList = [];
  List<SelectApproverModelData> selectedApproverList = [];
  List<String> approNameList = [];

  //List<String> selectedApproverList = [];

  //shoot location
  SelectShootlocationModelData shootLocationData;
  List<ShootLocationData> shootLocationList = [];

  //for all currency
  SelectCurrencyModel currencyData;
  List<SelectCurrencyModelData> currencyList = [];
  List<String> currNameList = [];

  int _filter1 = 0;
  int _filter2 = 0;
  int _filter5 = 0;
  int _filter6 = 0;
  int _filter7 = 0;
  int _filter10 = 0;

  String multiSelectApprove;

  //Vendor
  int vendorId = 0;
  String vendorName = "";
  List<VendorData> data = [];
  double letfPad, topPad;

  callProjectAPI() async {
    projectData = await homeApiObj.fetchProjectList(context);
    if (projectData.Status ?? false) {
      projectList = projectData.data ?? [];

      for (int j = 0; j < projectList.length; j++) {
        pNameList.add(projectList[j].project_name);
      }
      _filter1 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    if (widget.isFromDetail == true) {
      _getCategoryList();
    }
  }
  callShootLocationAPI() async {
    shootLocationData = await cVM.getAllShootlocationData();
    if(shootLocationData.Status ?? false) {
      _filter10 = 1;
      shootLocationList = shootLocationData.data;
      print("Data is");
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("${shootLocationData.Message}"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
  callCurrencyAPI() async {
    currencyData = await cVM.getAllCurrency();
    if (currencyData.Status ?? false) {
      currencyList = currencyData.data ?? [];

      for (int j = 0; j < currencyList.length; j++) {
        currNameList.add(currencyList[j].currency_code);
      }
      _filter6 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _getAllApiData() async {
    callProjectAPI();
    callShootLocationAPI();
    callCurrencyAPI();
  }


  _getCategoryList() async {
    // get all category list
    categoryData = await cVM.getAllCategory(projectId);
    if (categoryData.Status ?? false) {
      categoryList = categoryData.data ?? [];

      for (int j = 0; j < categoryList.length; j++) {
        cNameList.add(categoryList[j].category_name);
      }
      _filter2 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }


  _getApproverList() async {
    print(sProjectIndex);
    // get all approver list
    approverData = await cVM.getAllApprover(projectId);

    if (approverData.Status ?? false) {
      approverList = approverData.data ?? [];
      for (int j = 0; j < approverList.length; j++) {
        approNameList.add(approverList[j].user_name);
      }
      _filter5 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = [];
    homeApiObj = HomeApi();
    //getCustomData();
    cVM = CreateAllVM();
    sVM = CreatePoVM();
    setInitialData();
    _getAllApiData();
  }

//  getCustomData() {
//    if (widget.customFieldDikha) {
//      cftec = [];
//      if (widget.customFieldModel != null) {
//        for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//          TextEditingController tec = new TextEditingController();
//          cftec.add(tec);
//        }
//      }
//    }
//  }

  setInitialData() {
    if (widget.isFromDetail) {
      _projectC.text = "${widget.projectName ?? ''}";
      projectId = int.parse("${widget.projectID}");
      _getApproverList();
    }
  }


  ScrollController _scrollController;
  int catId = 0;
  int location_id = 0;
  int subCatId = 0;
  int projectId = 0;
  int countryId = 0;
  int cityId = 0;
  int selectedIndex = 0;
  int tabIndex = 0;
  String currencyS = "INR";
  String paymentS = "Bank";
  String priorityS = "Normal";
  int sProjectIndex;
  int sCateIndex;
  int sCountIndex;
  int sCityIndex;
  int sApproveIndex;

  List<String> finalList = [];

  // For Custom Field
  List<TextEditingController> cftec;

  @override
  void dispose() {
    // TODO: implement dispose
    _titleC.dispose();
//    _scrollController.dispose();
    _projectC.dispose();
    _cateC.dispose();
    _subCatC.dispose();
    _amountC.dispose();
    _approverC.dispose();
    _poDescriptionController.dispose();
    // disposeCustomField();
    super.dispose();
  }

//  disposeCustomField() {
//    if (widget.customFieldDikha) {
//      for (int i = 0; i < cftec.length; i++) {
//        cftec[i].dispose();
//      }
//    }
//  }

  Widget SelectedIcon() {
    return Icon(Icons.add);
  }

  List<File> multiImg;
  File imgCamera;
  int sImageLength = 0;
  ImageUploadRes imgres;
  String filePath = "";
  String fileName = "";
  bool firstTime = false;
  List<String> imgRes;

  File tempFirebaseFile;

  void onCamera(BuildContext context, int i) async {
    if (i == 1) {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
    } else {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.camera,
      );
    }


    if (imgCamera != null) {
      sImageLength = 1;

      //sImageLength = 1;

      setState(() {});
      uploadImage(context, 1);
    }
  }

  void onGallary(BuildContext context, int i) async {
    if (i == 2) {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'xlsx', 'xls'],
      );
    } else {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.any,
      );
    }

    print(multiImg);

    if (multiImg != null) {
      sImageLength = 1;

      //sImageLength = 1;
      setState(() {});
      print(multiImg.length);
      uploadImage(context, 2);
    }
  }


  uploadImage(BuildContext context, int j) async {
    _showLoading(context);
    var formData = new FormData();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // j=1 => Document, j=2 => Camera, j=3 => Gallery

    if (j == 2) {
      for (int i = 0; i < multiImg.length; i++) {
        filePath = multiImg[i].path ?? '';
        fileName = multiImg[i].path ?? '';

        List<String> res = fileName.split('.');

        if (res.last == 'mp4') {
          print(res);
          Navigator.pop(context);

          final snackBar = SnackBar(
              duration: Duration(seconds: 1),
              content: Text("Videos are not allowed."));
          _scaffoldKey.currentState.showSnackBar(snackBar);

          return false;
        }
        else {
          formData.files.addAll([
            MapEntry(
              "file[]",
              MultipartFile.fromFileSync(filePath ?? "",
                  filename: fileName ?? ""),
            ),
          ]);
//          formData = new FormData.fromMap({
//            'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
//          });
        }
//
      }
    } else {
      filePath = imgCamera.path ?? '';
      fileName = imgCamera.path ?? '';
      formData = new FormData.fromMap({
        'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
      });
//      formData.add("file[]", UploadFileInfo(File(filePath), fileName));
    }

    Dio dio = new Dio()
      ..options.baseUrl = BASE_URL;

    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        // imgRes = imgres.data.image_path.split(',');

        if (!firstTime) {
          imgRes = imgres.data.image_path.split(',');
          firstTime = !firstTime;
        } else {
          List<String> temp = [];
          temp = imgres.data.image_path.split(',');
          setState(() {
            for (int i = 0; i < temp.length; i++) {
              imgRes.add(temp[i]);
            }
          });
        }

        print(imgRes);

        setState(() {});
      } else {
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(imgres.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Cannot load media."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery
        .of(context)
        .size
        .width;
    final h = MediaQuery
        .of(context)
        .size
        .height;

    letfPad = 0.00944;
    topPad = 0.00470;

    return SafeArea(
      bottom: true,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: CustomAppBar(
          title: "Advance Request",
        ),
        body: Container(
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), 0),
                child: Row(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        if (imgRes != null && imgRes.length != 0) {
                          navWalk();
                        } else {
                          _showmodalsheet(
                            w,
                            h,
                            context,
                          );
                        }
                      },
                      child: Card(
                        color: Color(0xffeeeeee),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                            side: BorderSide(color: borderColor)),
                        elevation: 2,
                        child: Container(
                            width: w*0.28,
                            height: h*0.11,
                            child: ClipRRect(
                              borderRadius:
                              BorderRadius.all(Radius.circular( h*0.0094)),
                              child: (imgRes != null && imgRes.length > 0)
                                  ? Stack(
                                alignment: Alignment.topRight,
                                fit: StackFit.expand,
                                children: <Widget>[
                                  FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/loading.gif'),
                                    image: NetworkImage(
                                        (imgRes[0].contains('pdf') || imgRes[0].contains('xlsx') || imgRes[0].contains("xls")
                                            ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
                                            : imgRes[0].contains('png') || imgRes[0].contains('jpeg') || imgRes[0].contains("jpg") ? "${imgRes[0]}?token=${_singleton.tempToken}" : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW" ) ),
                                    fit: BoxFit.cover,
                                  ),
                                  /*  Image(
                                          image: NetworkImage(imgRes[0]),
                                          fit: BoxFit.cover,
                                        ),*/

                                  imgRes.length > 1
                                      ? Container(
                                    padding: EdgeInsets.only(left: (letfPad*8*w), right: (letfPad*8*w), top: (topPad*8*h), bottom: (topPad*8*h)),
                                    child: SvgPicture.asset(
                                        'assets/images/multiple-uploads.svg',
                                        color: Color(0xffd0d3d8),
                                        semanticsLabel:
                                        'document logo'),
                                  )
                                      : Container()
                                ],
                              )
                                  : Padding(
                                padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                                child: SvgPicture.asset(
                                    'assets/images/file.svg',
                                    color: Color(0xffd0d3d8),
                                    semanticsLabel: 'document logo'),
                              ),
                            )),
                      ),
                    ),
                    Container(
                      height: h*0.1270,
                      width: w * .60 - w*0.0377,
                      child: Column(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.only(left: (letfPad*2*w), top: (topPad*h), right: (letfPad*2*w), bottom: (topPad*h*2)),
                              width: w * .60 - w*0.0377,
                              height: h*0.06351,
                              child: RaisedButton(
                                color: Color(0xffeeeeee),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular( h*0.0094)),
                                    side: BorderSide(color: borderColor)),
                                onPressed: () {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  _showmodalsheet(
                                    w,
                                    h,
                                    context,
                                  );
                                },
                                child: FittedBox(
                                  fit: BoxFit.fitWidth,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        FontAwesomeIcons.images,
                                        size: h*0.01881,
                                      ),
                                      SizedBox(
                                        width: w*0.01416,
                                      ),
                                      Text(
                                        "Upload from Gallery",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: h*0.014,
                                            fontFamily: 'SanMedium'),
                                      )
                                    ],
                                  ),
                                ),
                              )),
                          Container(
                              padding: EdgeInsets.only(left: letfPad*2*w, bottom: topPad*2*h, right: letfPad*2*w, top: topPad*h),
                              width: w * .60 - w*0.0377,
                              height: h*0.06,
                              child: RaisedButton(
                                color: Color(0xffeeeeee),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular( h*0.0094)),
                                    side: BorderSide(color: borderColor)),
                                onPressed: () {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  onCamera(context, 2);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.camera,
                                      size: h*0.01881,
                                    ),
                                    SizedBox(
                                      width: w*0.01416,
                                    ),
                                    Text(
                                      "Use the Camera",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: h*0.014,
                                          fontFamily: 'SanMedium'),
                                    )
                                  ],
                                ),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                child: Column(
                  children: <Widget>[
                    TypeAheadField<VendorData>(

                      textFieldConfiguration: TextFieldConfiguration(
                          autofocus: false,
                          controller: _titleC,
                          onTap: (){
                            setState(() {
                              vendorId = 0;
                            });
                          },
                          decoration: InputDecoration(
                              suffixIcon: Icon(Icons.add),
                              hintText: 'Enter vendor name'
                          )
                      ),

                      suggestionsCallback: (pattern) async {
                        print(pattern);
                        await getVendorData(pattern);
                        return data.where((element) => element.vendor_name.toLowerCase().startsWith(pattern.toLowerCase())).toList();
                      },

                      itemBuilder: (context, suggestion) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Padding(
                                padding: EdgeInsets.all(h*0.0188),
                                child: Text(suggestion.vendor_name, style: TextStyle(fontSize: h*0.02117),),
                              ),
                            ),
                            Divider()
                          ],
                        );
                      },
                      onSuggestionSelected: (suggestion) {

                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        setState(() {
                          _titleC.text = suggestion.vendor_name;
                          vendorId = suggestion.vendor_id;
                        });
                      },
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          selectedIndex = 0;
                          tabIndex = 1;
                          _filter1 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                          if (_filter1 != 1) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please wait loading data...'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          }
                        },
                        child: TextField(

                          readOnly: true,
                          decoration: new InputDecoration(
                              hintText: "Select Project",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              enabled: false,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.01881,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.0282,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _projectC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          if (_projectC.text.isEmpty) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please select Project first'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                            FocusScope.of(context).requestFocus(FocusNode());
                            tabIndex = 2;
                            selectedIndex = 0;
                            _filter2 == 1
                                ? _openBottomSheetCategory(context)
                                : null;

                            if (_filter2 != 1) {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text('Please wait loading data...'));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Category",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.01881,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.0282,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _cateC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (_cateC.text.isEmpty) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please select Category first'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                            tabIndex = 7;
                            selectedIndex = 0;
                            _filter7 == 1
                                ? _openBottomSheetCategory(context)
                                : null;
                            if (_filter7 != 1) {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text('Please wait loading data...'));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Sub Category",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.01881,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.0282,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _subCatC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {

                          FocusScope.of(context).requestFocus(FocusNode());
                          selectedIndex = 0;
                          tabIndex = 10;
                          _filter10 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                          if (_filter10 != 1) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please wait loading data...'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          }

                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Set Name (Optional)",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _shootLocation,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),

                    // Container(
                    //   height: h*0.058,
                    //   child: RaisedButton(
                    //     color: Color(0xffeeeeee),
                    //     padding: EdgeInsets.all(0.0),
                    //     onPressed: () {
                    //       if (_projectC.text.isEmpty) {
                    //         final snackBar = SnackBar(
                    //             duration: Duration(seconds: 1),
                    //             content: Text('Please select Project first'));
                    //         _scaffoldKey.currentState.showSnackBar(snackBar);
                    //       } else {
                    //         FocusScope.of(context).requestFocus(FocusNode());
                    //         tabIndex = 5;
                    //         selectedIndex = 0;
                    //         _filter5 == 1
                    //             ? _openBottomSheetApprover(context)
                    //             : null;
                    //         if (_filter5 != 1) {
                    //           final snackBar = SnackBar(
                    //               duration: Duration(seconds: 1),
                    //               content: Text('Please wait loading data...'));
                    //           _scaffoldKey.currentState.showSnackBar(snackBar);
                    //         }
                    //       }
                    //     },
                    //     child: TextField(
                    //       readOnly: true,
                    //       enabled: false,
                    //       decoration: new InputDecoration(
                    //           hintText: "Select Approver",
                    //           hintStyle: TextStyle(fontFamily: 'SanRegular'),
                    //           border: InputBorder.none,
                    //           prefixIcon: Container(
                    //             margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                    //             child: Icon(
                    //               FontAwesomeIcons.fileInvoice,
                    //               size: h*0.01881,
                    //             ),
                    //           ),
                    //           suffixIcon: Icon(
                    //             Icons.keyboard_arrow_down,
                    //             color: appColor,
                    //             size: h*0.0282,
                    //           )),
                    //       style: Styles.formInputTextStyle,
                    //       controller: _approverC,
                    //       obscureText: false,
                    //     ),
                    //     shape: RoundedRectangleBorder(
                    //         borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                    //         side: BorderSide(color: borderColor)),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: h*0.0282,
                    // ),

//                    widget.customFieldDikha ? ListView.builder(
//                        shrinkWrap: true,
//                        physics: NeverScrollableScrollPhysics(),
//                        itemCount: widget.customFieldModel.data.length,
//                        itemBuilder: (BuildContext context, int i) {
//                          return Container(
//                            height: 80,
//                            width: w * .60 - w*0.0377,
//                            //color: Colors.blue,
//                            child: Column(
//                              children: <Widget>[
//                                TextField(
//                                  keyboardType: widget.customFieldModel.data[i]
//                                      .field_type_id == 1
//                                      ? TextInputType.text
//                                      : TextInputType.number,
//                                  decoration: new InputDecoration(
//                                      labelText: "${widget.customFieldModel
//                                          .data[i].key_name}",
//                                      labelStyle: TextStyle(
//                                          fontFamily: 'SanRegular'),
//                                      prefixIcon: Container(
//                                        margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
//                                        child: Icon(
//                                          FontAwesomeIcons.fileInvoice,
//                                          size: h*0.01881,
//                                        ),
//                                      )),
//                                  inputFormatters: [
//                                    new BlacklistingTextInputFormatter(
//                                        widget.customFieldModel.data[i]
//                                            .field_type_id == 1
//                                            ? new RegExp('')
//                                            : new RegExp('[\\,|\\-|\\ ]')),
//                                  ],
//                                  style: Styles.formInputTextStyle,
//                                  controller: cftec[i],
//                                  obscureText: false,
//                                ),
//
//                              ],
//                            ),
//                          );
//                        }) : Container(),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        PopupMenuButton<String>(
                          itemBuilder: (BuildContext context) {
                            return currNameList.map((String s) {
                              return PopupMenuItem<String>(
                                value: s,
                                child: Text(s),
                              );
                            }).toList();
                          },
                          onSelected: (j) {
                            FocusScope.of(context).requestFocus(FocusNode());
                            currencyS = j;
                            setState(() {});
                          },
                          child: Container(
                              padding: EdgeInsets.only(left: letfPad*1*w, ),
                              width: (w) / 3 - letfPad*2*w,
                              height: h*0.05,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular( h*0.0094))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  RichText(
                                      textAlign: TextAlign.start,
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                              text: 'Currency: ',
                                              style: TextStyle(
                                                  fontFamily: 'SanRegular',
                                                  color: Colors.grey,
                                                  fontSize: h*0.016)),
                                          TextSpan(
                                              text: currencyS ?? '',
                                              style: TextStyle(
                                                  fontFamily: 'SanSemiBold',
                                                  color: Colors.black,
                                                  fontSize: h*0.016)),
                                        ],
                                      )),
                                  Icon(Icons.arrow_drop_down)
                                ],
                              )),
                        ),
                        SizedBox(
                          width: w*0.037,
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: (w) / 2 - w*0.04722,
                                child: TextField(
                                  decoration: new InputDecoration(
                                    labelText: "Enter Expense Amount",
                                    labelStyle: TextStyle(
                                        fontFamily: 'SanRegular'),
                                  ),
                                  inputFormatters: [
                                    new BlacklistingTextInputFormatter(
                                        new RegExp('[\\,|\\-|\\.|\\ ]')),
                                  ],
                                  style: Styles.formInputTextStyle,
                                  controller: _amountC ?? 0,
                                  keyboardType: TextInputType.number,
                                  obscureText: false,
                                ),
                              ),
                              Text("Taxes Excluded", style: TextStyle(
                                  fontSize: h*0.01, fontFamily: 'SanRegular'),)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: h*0.03763,
              ),
              Padding(
                padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                child: new Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(h*0.01)),
                  child: new Container(
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(left: (w*letfPad*4), right: (w*letfPad*4)),
                      height: h*0.1176,
                      child: TextField(
                        textInputAction: TextInputAction.go,
                        maxLines: 4,
                        controller: _poDescriptionController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Comments',
                            hintStyle: TextStyle(fontFamily: 'SanRegular')),
                      )),
                ),
              ),
              SizedBox(
                height: h*0.0282,
              ),

              CustomBtn(
                title: "Submit",
                onTap: () async {
                  ConnectionHelper mCH;
                  mCH = ConnectionHelper.getInstance();

                  bool con = await mCH.checkConnection();

                  if (con) {
                    if (_check_for_err()) {
                      _submitForm(context);
                    }
                  } else {
                    final snackBar = SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text("Please check your internet connection"));
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  getVendorData(String keyword) async {
    data = await getData(keyword ?? "");
    print("gaya");
    if(data == null) {
      data = [];
    }
    print(data);
    setState(() {});
  }

  getData(String queryString) async {
    VendorAutoCompleteData res_d;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    final HEADERS = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "Authorization": accessToken ?? ""
    };

    print("Token is :- ${accessToken}");
    print("queryString is $queryString");

    var res = await http.get('${BASE_URL}vendor?search_txt=${queryString ?? ""}', headers: HEADERS);
    if(res.statusCode == 200) {
      final jData = json.decode(res.body);
      res_d = VendorAutoCompleteData.fromJson(jData);
      print(res_d);
      return res_d.data;
    }
  }

  void _submitForm(BuildContext context) async {

    String fImage;
    if (imgRes != null) {
      fImage = imgRes.join(',');
    } else {
      fImage = "-";
    }


    int sCatId;

    if (subCatId == 0) {
      sCatId = catId;
    } else {
      sCatId = subCatId;
    }


    _showLoading(context);
    // cash = 1, card = 2 , Bank = 3

    var body = json.encode({
      "project_id": projectId,
      "po_name": _titleC.text,
      "item_name": "-",
      "company_name": "-",
      "category_id": sCatId,
      // "customFieldValues": widget.customFieldDikha ? customFieldValues : [],
      "customFieldValues": [],
      "company_address": "-",
      "price": _amountC.text,
      "currency": currencyS,
      "purchase_image": fImage,
      "sgst": "0",
      "cgst": "0",
      "total": _amountC.text,
      "purchase_description": _poDescriptionController.text,
      "purchase_status": "0",
      "approved_percentage": "0",
      "purchase_active": "1",
      "payment_mode_description": "-",
      "purchase_type": "purchase_order",
      //"approver_id": multiSelectApprove,
      "approver_id": "0",
      "vendorName" : _titleC.text,
      "vendor_id" : vendorId,
      "payment_due_date" : "",
      "invoice_number" : "",
      "invoice_date" : "",
      "gst_amount" : "",
      "gst_id" : 0,
      "location_id" : location_id ?? 0
    });

    print(body);

    SetFavouriteModel res = await sVM.setCashRequest(body, context);

    if (res.Status ?? false) {
      Navigator.pop(context);

      _bookEventDialog(context, "Advance request created successfully!");
    } else {
      Navigator.pop(context);
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text(res.Message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _openBottomSheetApprover(BuildContext context) {
    getBottomSheetApprover(context);
  }

  void _openBottomSheetCategory(BuildContext context) async {
    await getBottomSheetCategory(context);
    Autoselect.subCategory(
      tabIndex,
      categoryList,
      catId,
      _cateC,
      _subCatC,
      (value) {
        subCatId = value;
      },
      setState,
    );
  }

  int groupVal = 0;

  void getBottomSheetApprover(BuildContext context) async {
    if (tabIndex == 1) {
      finalList = pNameList ?? [];
    } else if (tabIndex == 2) {
      finalList = cNameList ?? [];
    } else if (tabIndex == 5) {
      finalList = approNameList ?? [];
    }

    var results = await showModalBottomSheet(
        context: context,
        isDismissible: true,
        builder: (context) {
          return ApprovedListWidge(
            approverList: approverList ?? [],
            //temp: selectedApproverList,
          );
        });

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);
        setState(() {
          _approverC.text = '';
          multiSelectApprove = '';
          selectedApproverList = results['approver_list'];
          if (selectedApproverList.length != 0) {
            List<SelectApproverModelData> sL =
            selectedApproverList.where((k) => k.isSelected = true).toList();
            for (int i = 0; i < sL.length; i++) {
              if (i == sL.length - 1) {
                _approverC.text = _approverC.text + sL[i].user_name;
                multiSelectApprove = multiSelectApprove +
                    selectedApproverList[i].user_id.toString();
              } else {
                _approverC.text = _approverC.text + sL[i].user_name + ",";
                multiSelectApprove = multiSelectApprove +
                    selectedApproverList[i].user_id.toString() +
                    ",";
              }
              sL[i].isSelected = false;
            }
          }
        });
      }
    }
  }

  Future<void> getBottomSheetCategory(BuildContext context) async {
    var results;

    if (tabIndex == 1) {
      results = await showModalBottomSheet(
          context: context,
          isDismissible: true,
          builder: (context) {
            return DynamicSheet(
              projectList: projectList,
              tabIndex: 1,
            );
          });
    } else if (tabIndex == 2) {
      results = await showModalBottomSheet(
          context: context,
          isDismissible: true,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 2,
              projectId: projectId,
            );
          });
    }  else if (tabIndex == 7) {
      results = await showModalBottomSheet(
          context: context,
          isDismissible: true,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 7,
              catId: catId,
              projectId: projectId,
            );
          });
    } else if (tabIndex == 10) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              shootDataList: shootLocationList,
              tabIndex: 10,
            );
          });
    }

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);

        if (tabIndex == 1) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _projectC.text = results['approver_list'][j].project_name;
              projectId = results['approver_list'][j].project_id;
              results['approver_list'][j].isSelected = false;
              approverList.clear();
              approNameList.clear();
              categoryList.clear();
              _subCatC.clear();
              _cateC.clear();
              _filter5 = 0;
              _filter2 = 0;
              _approverC.clear();
              _getCategoryList();
              _getApproverList();
            }
          }
        } else if (tabIndex == 2) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _cateC.text = results['approver_list'][j].category_name;
              catId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
              _filter7 = 1;
              _subCatC.clear();
              subCatId = 0;
            }
          }
          /*  _cateC.text = results['approver_list'].category_name;
          catId = results['approver_list'].category_id;

          results['approver_list'].isSelected = false;
          results['approver_list'].selectedIndex = -1;*/
        } else if (tabIndex == 7) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _subCatC.text = results['approver_list'][j].category_name;
              subCatId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 10) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _shootLocation.text = results['approver_list'][j].location_name;
              location_id = results['approver_list'][j].location_id;
              results['approver_list'][j].isSelected = false;
              print(location_id);
            }
          }
        }

        setState(() {});
      }
    }
  }

  void _bookEventDialog(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CustomPopUp(
          msg: message,
        );
      },
    );
  }

  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }


  void navWalk() async {
    var results = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) =>
                CarouselDemo(
                  imgUrl: imgRes,
                  fromPage: 4,
                )));

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);
        imgRes = results['approver_list'];
        setState(() {});
      }
    }
  }


  bool _check_for_err() {
    if (_titleC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Vendor name.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_projectC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a Project.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_cateC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a Category.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    }
    // else if (_approverC.text.isEmpty) {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text('Please select an Approver.'));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    //   return false;
    // }
    else if (_amountC.text.isEmpty) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please write Total Expense amount.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poDescriptionController.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please write Expense Description.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    }

    else {
      return true;
    }
  }

  void _showmodalsheet(double w, double h, BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(h*0.03), topRight: Radius.circular(h*0.03)),
        ),
        context: context,
        builder: (buider) {
          return getBottomSheet(w, h, context);
        });
  }

  Widget getBottomSheet(double w, double h, BuildContext context) {
    return Container(
        height: h*0.1411,
        width: double.infinity,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 2);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/document.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'document logo'),
                  SizedBox(
                    height: h*0.0047,
                  ),
                  Text("Document")
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 1);
                //onCamera(context, 1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/gallery.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'gallery logo'),
                  SizedBox(
                    height: h*0.0047,
                  ),
                  Text("Image")
                ],
              ),
            ),

          ],
        ));
  }
}

class ApprovedListWidge extends StatefulWidget {
  List<SelectApproverModelData> approverList;

  ApprovedListWidge({
    Key key,
    this.approverList,
  }) : super(key: key);

  @override
  _ApprovedListWidgeState createState() => _ApprovedListWidgeState();
}

class _ApprovedListWidgeState extends State<ApprovedListWidge> {
  List<String> approveKarneValonkiList = [];
  double letfPad, topPad;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  bool isFirst = true;

  List<SelectApproverModelData> finalList = [];

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    letfPad = 0.00944;
    topPad = 0.00470;
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Container(
        height: h * .70,
        width: double.infinity,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
                color: appColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: letfPad*4*w),
                      child: Text(
                        "Select the Approvers",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'SanMedium',
                            fontSize: h*0.018),
                      ),
                    ),
                    FlatButton(
                      child: Text(
                        "Done",
                        style: TextStyle(
                            color: Colors.white, fontFamily: 'SanSemiBold'),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop({'approver_list': finalList});
                      },
                    )
                  ],
                )),
            Container(
              height: h * .70 - h*0.153,
              child: widget.approverList.length > 0
                  ? ListView.builder(
                  shrinkWrap: true,
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.fromLTRB((letfPad*2*w), (topPad*2*h), (letfPad*2*w), (topPad*8*h)),

                  itemCount: widget?.approverList?.length ?? 0,
                  itemBuilder: (context, i) {
                    SelectApproverModelData aData = widget.approverList[i] ??
                        [];
                    if (isFirst ||
                        widget.approverList[i].isSelected == null) {
                      widget.approverList[i].isSelected = false;
                      isFirst = false;
                    }

                    return Column(
                      children: <Widget>[
                        ListTile(
                          onTap: () {
                            if (!widget.approverList[i].isSelected) {
                              finalList.add(widget.approverList[i]);
                              widget.approverList[i].isSelected = true;
                              setState(() {});
                            } else {
                              finalList.remove(widget.approverList[i]);
                              widget.approverList[i].isSelected = false;
                              setState(() {});
                            }
                          },
                          title: Text(widget.approverList[i].user_name ?? ''),
                          trailing: widget.approverList[i].isSelected
                              ? Icon(
                            FontAwesomeIcons.check,
                            color: Colors.green,
                            size: h*0.01646,
                          )
                              : null,
                        )
                      ],
                    );
                  })
                  : Center(
                child: Text("No Data available.",
                    style: TextStyle(
                        fontSize: h*0.02117, fontFamily: 'SanRegular')),
              ),
            )
          ],
        ),
      ),
    );
  }

}

