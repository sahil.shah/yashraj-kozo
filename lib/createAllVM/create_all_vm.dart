import 'dart:convert';

import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CreateAllVM {

  ConnectionHelper mCH;

  PoReviewVM() {
    mCH = ConnectionHelper.getInstance();
  }


  AppSingleton _singleton = AppSingleton();

  Future<SelectCategoryModel> getAllCategory(int pId) async {
    SelectCategoryModel res_d;

    bool con = true;

//    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {


        final res = await http.get("${BASE_URL}category/project/$pId?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectCategoryModel.fromJson(j_data);

            return res_d;
          default:
            return SelectCategoryModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectCategoryModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }
  }

  Future<SelectShootlocationModelData> getAllShootlocationData() async {
    SelectShootlocationModelData res_d;

    bool con = true;

//    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {


        final res = await http.get("${BASE_URL}shootLocation?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectShootlocationModelData.fromJson(j_data);

            return res_d;
          default:
            return SelectShootlocationModelData.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectShootlocationModelData.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }
  }

  Future<BehalfPeopleModel> getAllBehalfPeople(int pId) async {
    BehalfPeopleModel res_d;

    bool con = true;

//    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {


        final res = await http.get("${BASE_URL}purchase/getMapList/$pId?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = BehalfPeopleModel.fromJson(j_data);

            return res_d;
          default:
            return BehalfPeopleModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return BehalfPeopleModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }
  }

  Future<SelectCountryModel> getAllCountry() async {
    SelectCountryModel res_d;

    bool con = true;

    //bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {



        final res = await http.get("${BASE_URL}country?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectCountryModel.fromJson(j_data);

            return res_d;
          default:
            return SelectCountryModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectCountryModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }


  Future<SelectCityModel> getAllCity() async {
    SelectCityModel res_d;

    bool con = true;

    //bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {



        final res = await http.get("${BASE_URL}city?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectCityModel.fromJson(j_data);

            return res_d;
          default:
            return SelectCityModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectCityModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }


  Future<SelectApproverModel> getAllApprover(int pId) async {
    SelectApproverModel res_d;

    bool con = true;

    //bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {



        final res = await http.get("${BASE_URL}purchase/approver/$pId?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectApproverModel.fromJson(j_data);

            return res_d;
          default:
            return SelectApproverModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectApproverModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }



  Future<SelectCurrencyModel> getAllCurrency() async {
    SelectCurrencyModel res_d;

    bool con = true;

    //bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {


        final res = await http.get("${BASE_URL}currency?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectCurrencyModel.fromJson(j_data);

            return res_d;
          default:
            return SelectCurrencyModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SelectCurrencyModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }


  Future<GetPoListChallanModel> getChallanPO(int pId) async {
    GetPoListChallanModel res_d;

    bool con = true;

    //bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {


        final res = await http.get("${BASE_URL}purchase/challan/$pId?organization_id=${prefs.getInt('orgId')}", headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = GetPoListChallanModel.fromJson(j_data);

            return res_d;
          default:
            return GetPoListChallanModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return GetPoListChallanModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }
}