import 'dart:convert';
import 'dart:io';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/createAllVM/create_all_vm.dart';
import 'package:yashrajkozo/create_po/create_po_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/custom_popup.dart';
import 'package:yashrajkozo/utils/text_style.dart';
import 'package:yashrajkozo/utils/walk.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'challan_sheet.dart';

class CreateChallan extends StatefulWidget {
  String projectName;
  String projectID;
  bool isFromDetail;
  //GetCustomFieldModel customFieldModel;
  //bool customFieldDikha;

  CreateChallan(
      {Key key,
      this.projectName,
      this.projectID,
      this.isFromDetail = false,
     // this.customFieldModel,
    //  this.customFieldDikha = false
      })
      : super(key: key);

  @override
  _CreateChallanState createState() => _CreateChallanState();
}

class _CreateChallanState extends State<CreateChallan> {
  AppSingleton _singleton = AppSingleton();
  final _titleC = TextEditingController();
  final _projectC = TextEditingController();
  final _poC = TextEditingController();
  final _amountC = TextEditingController();
  final _poDescriptionController = TextEditingController();

  List<File> multiImg;
  File imgCamera;

  ScrollController _scrollController;
  List<String> finalList = [];
  int selectedIndex = 0;
  int tabIndex = 0;

  String currencyS = "INR";
  String payS = "";
  String priS = "Normal";
  int sImageLength = 0;
  List<String> imgRes;

//  int _filterI = 0;
  int _filter1 = 0;
  int _filter2 = 0;
  int _filter3 = 0;
  int sProjectIndex;
  int sPOIndex;

  int projectId = 0;
  int poId = 0;

  List<String> paymentS = [
    "Cash",
    "Bank",
    "Card",
  ];

  List<String> priorityS = ["Low", "Normal", "High"];

  int prioI = -1;

  CreatePoVM cPM;
  int pIndex = -1;

  int count = 0;
  double letfPad, topPad;

  //For Custom Field
  List<TextEditingController> cftec;

  void onGallary(BuildContext context, int i) async {
    if (i == 2) {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'xlsx', 'xls'],
      );
    } else {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.image,
      );
    }

    print(multiImg);

    if (multiImg != null) {
      sImageLength = 1;

      //sImageLength = 1;
      setState(() {});
      print(multiImg.length);
      uploadImage(context, 2);
    }
  }

/*  void onMultiGallary(BuildContext context) async {
     this.filesPaths = await FilePicker.getMultiFilePath(type: FileType.IMAGE);
    multiImage = await PhotoPicker.pickAsset(
      context: context,
      maxSelected: 100,
      provider: I18nProvider.english,
      checkBoxBuilderDelegate: DefaultCheckBoxBuilderDelegate(
        activeColor: Colors.white,
        unselectedColor: Colors.white,

      ),
      pickType: PickType.all,
    );
    if (multiImage != null) {

      uploadImage(context, 3);
    }
  }*/

  bool firstTime = false;

  void onCamera(BuildContext context, int i) async {
    if (i == 1) {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
    } else {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.camera,
      );
    }
    if (imgCamera != null) {
      sImageLength = 1;
      setState(() {});
      uploadImage(context, 1);
    }
  }

  final _scaffoldKeyCreateChallan = GlobalKey<ScaffoldState>();

  ImageUploadRes imgres;
  String filePath = "";
  String fileName = "";
  int i = 0;

  uploadImage(BuildContext context, int j) async {
    _showLoading(context);

    var formData = new FormData();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // j=1 => Document, j=2 => Camera, j=3 => Gallery

    if (j == 2) {
      for (int i = 0; i < multiImg.length; i++) {
        filePath = multiImg[i].path;
        fileName = multiImg[i].path;

        List<String> res = fileName.split('.');

        if (res.last == 'mp4') {
          print(res);
          Navigator.pop(context);

          final snackBar = SnackBar(
              duration: Duration(seconds: 1),
              content: Text("Videos are not allowed."));
          _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);

          return false;
        } else {
//          formData = new FormData.fromMap({
//            'file[$i]': await MultipartFile.fromFile(filePath, filename: fileName)
//          });

          formData.files.addAll([
            MapEntry(
              "file[]",
              MultipartFile.fromFileSync(filePath ?? "",
                  filename: fileName ?? ""),
            ),
          ]);

//          formData.fields("file[$i]", MultipartFile.fromFile(filePath, filename: fileName));
//          formData.add("file[$i]", UploadFileInfo(File(filePath), fileName));
        }
      }
    } else {
      filePath = imgCamera.path;
      fileName = imgCamera.path;
      formData = new FormData.fromMap({
        'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
      });
//      formData.add("file[]", UploadFileInfo(File(filePath), fileName));
    }

    Dio dio = new Dio()..options.baseUrl = BASE_URL;
    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        if (!firstTime) {
          imgRes = imgres.data.image_path.split(',');
          firstTime = !firstTime;
        } else {
          List<String> temp = [];
          temp = imgres.data.image_path.split(',');
          setState(() {
            for (int i = 0; i < temp.length; i++) {
              imgRes.add(temp[i]);
            }
          });
        }
        print(imgRes);

        setState(() {});
      } else {
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(imgres.Message));
        _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Cannot load media."));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
    Navigator.pop(context);
  }

  CreateAllVM cVM;
  HomeApi homeApiObj;

  //for all project
  GetProjectModel projectData;
  List<ProjectModelData> projectList = [];
  List<String> pNameList = [];

  // for all category
  GetPoListChallanModel poData;
  List<POListChallanData> poList = [];
  List<String> poNameList = [];

  //for all currency
  SelectCurrencyModel currencyData;
  List<SelectCurrencyModelData> currencyList = [];
  List<String> currNameList = [];

  void _getAllApiData() async {
    // get all project list
    projectData = await homeApiObj.fetchProjectList(context);
    if (projectData.Status ?? false) {
      projectList = projectData.data;

      for (int j = 0; j < projectList.length; j++) {
        pNameList.add(projectList[j].project_name);
      }

      if (projectList.length == 1) {
        _projectC.text = projectList[0].project_name;
        projectId = projectList[0].project_id;
        _getPoListChallan();
      }

      _filter1 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }

    // get all currency list
    currencyData = await cVM.getAllCurrency();
    if (currencyData.Status ?? false) {
      currencyList = currencyData.data;

      for (int j = 0; j < currencyList.length; j++) {
        currNameList.add(currencyList[j].currency_code);
      }
      _filter3 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  _getPoListChallan() async {
    // get all PO list
    poData = await cVM.getChallanPO(projectId);
    if (poData.Status ?? false) {
      poList = poData.data;
      for (int j = 0; j < poList.length; j++) {
        poNameList.add(poList[j].po_name);
      }
      _filter2 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  int userId;

  _getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getInt('userId');

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   // getCustomData();
    homeApiObj = HomeApi();
    cVM = CreateAllVM();
    cPM = CreatePoVM();
    setInitialData();
    _getUserData();
    _getAllApiData();
  }

//  getCustomData() {
//    if (widget.customFieldDikha) {
//      cftec = [];
//      if (widget.customFieldModel != null) {
//        for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//          TextEditingController tec = new TextEditingController();
//          cftec.add(tec);
//        }
//      }
//    }
//  }

  setInitialData() {
    if (widget.isFromDetail) {
      _projectC.text = "${widget.projectName ?? ''}";
      projectId = int.parse("${widget.projectID}");
      _getPoListChallan();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _titleC.dispose();
//    _scrollController.dispose();
    _projectC.dispose();
    _amountC.dispose();
    _poDescriptionController.dispose();
  //  disposeCustomField();
    super.dispose();
  }

//  disposeCustomField() {
//    if (widget.customFieldDikha) {
//      for (int i = 0; i < cftec.length; i++) {
//        cftec[i].dispose();
//      }
//    }
//  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;

    letfPad = 0.00944;
    topPad = 0.00470;

    return SafeArea(
      bottom: true,
      child: Scaffold(
          key: _scaffoldKeyCreateChallan,
          backgroundColor: Colors.white,
          appBar: CustomAppBar(
            title: "Create Challan",
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 90,
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      child: Column(
                        //physics: BouncingScrollPhysics(),
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), 0),
                            child: Row(
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    FocusScopeNode currentFocus = FocusScope.of(context);

                                    if (!currentFocus.hasPrimaryFocus) {
                                      currentFocus.unfocus();
                                    }
                                    if (imgRes != null && imgRes.length != 0) {
                                      navWalk();
                                    } else {
                                      _showmodalsheet(
                                        w,
                                        h,
                                        context,
                                      );
                                    }
                                  },
                                  child: Card(
                                    color: Color(0xffeeeeee),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(h*0.0094)),
                                        side: BorderSide(
                                            color: borderColor)),
                                    elevation: 2,
                                    child: Container(
                                        width: w*0.283,
                                        height: h*0.11,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(h*0.0094)),
                                          child: (imgRes != null &&
                                                  imgRes.length > 0)
                                              ? Stack(
                                                  alignment: Alignment.topRight,
                                                  fit: StackFit.expand,
                                                  children: <Widget>[
                                                    FadeInImage(
                                                      placeholder: AssetImage(
                                                          'assets/images/loading.gif'),
                                                      image: NetworkImage(
                                                          (imgRes[0].contains('pdf') || imgRes[0].contains('xlsx') || imgRes[0].contains("xls")
                                                              ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
                                                              : imgRes[0].contains('png') || imgRes[0].contains('jpeg') || imgRes[0].contains("jpg") ? "${imgRes[0]}?token=${_singleton.tempToken}" : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW" ) ),
                                                      fit: BoxFit.cover,
                                                    ),

                                                    imgRes.length > 1
                                                        ? Container(
                                                      padding: EdgeInsets.only(left: (letfPad*8*w), right: (letfPad*8*w), top: (topPad*8*h), bottom: (topPad*8*h)),
                                                            child: SvgPicture.asset(
                                                                'assets/images/multiple-uploads.svg',
                                                                color: Color(
                                                                    0xffd0d3d8),
                                                                semanticsLabel:
                                                                    'document logo'),
                                                          )
                                                        : Container()
                                                  ],
                                                )
                                              : Padding(
                                            padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                                                  child: SvgPicture.asset(
                                                      'assets/images/file.svg',
                                                      color: Color(0xffd0d3d8),
                                                      semanticsLabel:
                                                          'document logo'),
                                                ),
                                        )),
                                  ),
                                ),

                                /* Container(
                            padding: EdgeInsets.only(right: 8, top: 8, bottom: 8),
                            width: w * .40 - 16,
                            height: 108,
                            child: RaisedButton(
                              padding: EdgeInsets.all(0),
                              color: Color(0xffeeeeee),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                                  side: BorderSide(color: borderColor)),
                              onPressed: () {
                                for(int i = 0; i < aImg.length; i++){
                                  Navigator.push(context, new MaterialPageRoute(builder: (context) =>  CarouselDemo()));
                                }

                              },
                              child:imgRes != null ? Image(image: NetworkImage(imgRes[0]), fit: BoxFit.cover,): Icon(FontAwesomeIcons.image),
                            ),
                          ),*/
                                Container(
                                  height: h*0.127,
                                  width: w * .60 - (w*0.0377),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                          padding: EdgeInsets.fromLTRB((letfPad*2*w), (topPad*h), (letfPad*2*w), (topPad*2*w)),
                                          width: w * .60 - w*0.037,
                                          height: h*0.06,
                                          child: RaisedButton(
                                            color: Color(0xffeeeeee),
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(h*0.0094)),
                                                side: BorderSide(
                                                    color: borderColor)),
                                            onPressed: () {
                                              FocusScopeNode currentFocus = FocusScope.of(context);

                                              if (!currentFocus.hasPrimaryFocus) {
                                                currentFocus.unfocus();
                                              }
                                              _showmodalsheet(
                                                w,
                                                h,
                                                context,
                                              );
                                            },
                                            child: FittedBox(
                                              fit: BoxFit.fitWidth,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Icon(
                                                    FontAwesomeIcons.images,
                                                    size: h*0.01881,
                                                  ),
                                                  SizedBox(
                                                    width: w*0.01416,
                                                  ),
                                                  Text(
                                                    "Upload from Gallery",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: h*0.014,
                                                        fontFamily:
                                                            'SanMedium'),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )),
                                      Container(
                                          padding: EdgeInsets.fromLTRB((letfPad*2*w), (topPad*h), (letfPad*2*w), (topPad*2*w)),
                                          width: w * .60 - w*0.037,
                                          height: h*0.06,
                                          child: RaisedButton(
                                            color: Color(0xffeeeeee),
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(h*0.0094)),
                                                side: BorderSide(
                                                    color: borderColor)),
                                            onPressed: () {
                                              FocusScopeNode currentFocus = FocusScope.of(context);

                                              if (!currentFocus.hasPrimaryFocus) {
                                                currentFocus.unfocus();
                                              }
                                              onCamera(context, 2);
                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  FontAwesomeIcons.camera,
                                                  size: h*0.01881,
                                                ),
                                                SizedBox(
                                                  width: w*0.01416,
                                                ),
                                                Text(
                                                  "Use the Camera",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: h*0.014,
                                                      fontFamily: 'SanMedium'),
                                                )
                                              ],
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.028,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                            child: Column(
                              children: <Widget>[
                                TextField(
                                  decoration: new InputDecoration(
                                      labelText: "Enter Your Challan Name",
                                      labelStyle:
                                          TextStyle(fontFamily: 'SanRegular'),
                                      prefixIcon: Container(
                                        margin: EdgeInsets.only(right: 16),
                                        child: Icon(
                                          FontAwesomeIcons.fileInvoice,
                                          size: h*0.0188,
                                        ),
                                      )),
                                  style: Styles.formInputTextStyle,
                                  controller: _titleC,
                                  obscureText: false,
                                ),
                                SizedBox(
                                  height: h*0.018,
                                ),
                                Container(
                                  height: h*0.058,
                                  child: RaisedButton(
                                    color: Color(0xffeeeeee),
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      FocusScopeNode currentFocus = FocusScope.of(context);

                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }
                                      selectedIndex = 0;
                                      tabIndex = 1;

                                      _filter1 == 1
                                          ? _openBottomSheetCategory(context)
                                          : null;

                                      if (_filter1 != 1) {
                                        final snackBar = SnackBar(
                                            duration: Duration(seconds: 1),
                                            content: Text(
                                                'Please wait loading data...'));
                                        _scaffoldKeyCreateChallan.currentState
                                            .showSnackBar(snackBar);
                                      }
                                    },
                                    child: TextField(
                                      readOnly: true,
                                      enabled: false,
                                      decoration: new InputDecoration(
                                          hintText: "Select Project",
                                          hintStyle:
                                              TextStyle(fontFamily: 'SanRegular'),
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                            child: Icon(
                                              FontAwesomeIcons.fileInvoice,
                                              size: h*0.01881,
                                            ),
                                          ),
                                          suffixIcon: Icon(
                                            Icons.keyboard_arrow_down,
                                            color: appColor,
                                            size: h*0.028,
                                          )),
                                      style: Styles.formInputTextStyle,
                                      controller: _projectC,
                                      obscureText: false,
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(h*0.0094)),
                                        side:
                                            BorderSide(color: borderColor)),
                                  ),
                                ),
                                SizedBox(
                                  height: h*0.0182,
                                ),
                                Container(
                                  height: h*0.0580,
                                  child: RaisedButton(
                                    color: Color(0xffeeeeee),
                                    padding: EdgeInsets.all(0.0),
                                    onPressed: () {
                                      FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
                                      if (_projectC.text.isEmpty) {
                                        final snackBar = SnackBar(
                                            duration: Duration(seconds: 1),
                                            content: Text(
                                                'Please select Project first'));
                                        _scaffoldKeyCreateChallan.currentState
                                            .showSnackBar(snackBar);
                                      } else {
                                        selectedIndex = 0;
                                        tabIndex = 2;

                                        _filter2 == 1
                                            ? _openBottomSheetCategory(context)
                                            : null;

                                        if (_filter2 != 1) {
                                          final snackBar = SnackBar(
                                              duration: Duration(seconds: 1),
                                              content: Text(
                                                  'Please wait loading data...'));
                                          _scaffoldKeyCreateChallan.currentState
                                              .showSnackBar(snackBar);
                                        }
                                      }
                                    },
                                    child: TextField(
                                      readOnly: true,
                                      enabled: false,
                                      decoration: new InputDecoration(
                                          hintText: "Select PO",
                                          hintStyle:
                                              TextStyle(fontFamily: 'SanRegular'),
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                            child: Icon(
                                              FontAwesomeIcons.fileInvoice,
                                              size: h*0.0188,
                                            ),
                                          ),
                                          suffixIcon: Icon(
                                            Icons.keyboard_arrow_down,
                                            color: appColor,
                                            size: h*0.028,
                                          )),
                                      style: Styles.formInputTextStyle,
                                      controller: _poC,
                                      obscureText: false,
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(h*0.0094)),
                                        side:
                                            BorderSide(color: borderColor)),
                                  ),
                                ),
                                SizedBox(
                                  height: h*0.028,
                                ),
//                                widget.customFieldDikha
//                                    ? ListView.builder(
//                                        shrinkWrap: true,
//                                        physics: NeverScrollableScrollPhysics(),
//                                        itemCount:
//                                            widget.customFieldModel.data.length,
//                                        itemBuilder:
//                                            (BuildContext context, int i) {
//                                          return Container(
//                                            height: 80,
//                                            width: w * .60 - w*0.037,
//                                            //color: Colors.blue,
//                                            child: Column(
//                                              children: <Widget>[
//                                                TextField(
//                                                  keyboardType: widget
//                                                              .customFieldModel
//                                                              .data[i]
//                                                              .field_type_id ==
//                                                          1
//                                                      ? TextInputType.text
//                                                      : TextInputType.number,
//                                                  decoration:
//                                                      new InputDecoration(
//                                                          labelText:
//                                                              "${widget.customFieldModel.data[i].key_name}",
//                                                          labelStyle: TextStyle(
//                                                              fontFamily:
//                                                                  'SanRegular'),
//                                                          prefixIcon: Container(
//                                                            margin:
//                                                                EdgeInsets.only(
//                                                                    right: 16),
//                                                            child: Icon(
//                                                              FontAwesomeIcons
//                                                                  .fileInvoice,
//                                                              size: h*0.01881,
//                                                            ),
//                                                          )),
//                                                  inputFormatters: [
//                                                    new BlacklistingTextInputFormatter(widget
//                                                                .customFieldModel
//                                                                .data[i]
//                                                                .field_type_id ==
//                                                            1
//                                                        ? RegExp('')
//                                                        : new RegExp(
//                                                            '[\\,|\\-|\\ ]')),
//                                                  ],
//                                                  style:
//                                                      Styles.formInputTextStyle,
//                                                  controller: cftec[i],
//                                                  obscureText: false,
//                                                ),
//                                              ],
//                                            ),
//                                          );
//                                        })
//                                    : Container(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    PopupMenuButton(
                                      padding: EdgeInsets.fromLTRB((letfPad*8*w), (topPad*8*h), (letfPad*8*w), (topPad*8*w)),
                                      itemBuilder: (BuildContext context) {
                                        return currNameList.map((String s) {
                                          return PopupMenuItem<String>(
                                            value: s,
                                            child: Text(s),
                                          );
                                        }).toList();
                                      },
                                      onSelected: (j) {
                                        FocusScopeNode currentFocus = FocusScope.of(context);

                                        if (!currentFocus.hasPrimaryFocus) {
                                          currentFocus.unfocus();
                                        }
                                        currencyS = j;
                                        setState(() {});
                                      },
                                      child: Container(
                                          padding: EdgeInsets.only(left: letfPad*1*w, ),
                                          width: (w) / 3 - letfPad*2*w,
                                          height: h*0.05,
                                          decoration: BoxDecoration(
                                              border:
                                                  Border.all(color: Colors.grey),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(h*0.0094))),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceBetween,
                                            children: <Widget>[
                                              RichText(
                                                  textAlign: TextAlign.start,
                                                  text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text: 'Currency: ',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'SanRegular',
                                                              color:
                                                                  Colors.grey,
                                                              fontSize: h*0.016)),
                                                      TextSpan(
                                                          text:
                                                              currencyS ?? '',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'SanSemiBold',
                                                              color: Colors
                                                                  .black,
                                                              fontSize: h*0.016)),
                                                    ],
                                                  )),
                                              Icon(Icons.arrow_drop_down)
                                            ],
                                          )),
                                    ),
                                    SizedBox(
                                      width: w*0.0377,
                                    ),
                                    Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: (w) / 2 -  w*0.04722,
                                            child: TextField(
                                              decoration: new InputDecoration(
                                                labelText:
                                                    "Enter Expense Amount",
                                                labelStyle: TextStyle(
                                                    fontFamily: 'SanRegular'),
                                              ),
                                              inputFormatters: [
                                                new BlacklistingTextInputFormatter(
                                                    new RegExp(
                                                        '[\\,|\\-|\\.|\\ ]')),
                                              ],
                                              style: Styles.formInputTextStyle,
                                              controller: _amountC,
                                              keyboardType:
                                                  TextInputType.number,
                                              obscureText: false,
                                            ),
                                          ),
                                          Text(
                                            "Taxes Excluded",
                                            style: TextStyle(
                                                fontSize: h*0.01,
                                                fontFamily: 'SanRegular'),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.028,
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                            child: new Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(h*0.01)),
                              child: new Container(
                                  padding: EdgeInsets.all(0),
                                  margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                                  height: h*0.1176,
                                  child: TextField(
                                    textInputAction: TextInputAction.go,
                                    maxLines: 4,
                                    controller: _poDescriptionController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Challan description...',
                                        hintStyle: TextStyle(
                                            fontFamily: 'SanRegular')),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: h*0.0423,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 10,
                child: CustomBtn(
                  title: "Submit",
                  onTap: () async {
                    ConnectionHelper mCH;
                    mCH = ConnectionHelper.getInstance();

                    bool con = await mCH.checkConnection();

                    if (con) {
                      if (_check_for_err()) {
                        submitExpense();
                      }
                    } else {
                      final snackBar = SnackBar(
                          duration: Duration(seconds: 1),
                          content:
                              Text("Please check your internet connection"));
                      _scaffoldKeyCreateChallan.currentState
                          .showSnackBar(snackBar);
                    }
                  },
                ),
              ),
            ],
          )),
    );
  }

  void _showmodalsheet(double w, double h, BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(h*0.03), topRight: Radius.circular(h*0.03)),
        ),
        context: context,
        builder: (buider) {
          return getBottomSheet(w, h, context);
        });
  }

  Widget getBottomSheet(double w, double h, BuildContext context) {
    return Container(
        height: h*0.1411,
        width: double.infinity,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 2);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/document.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'document logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Document")
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 1);
                //onCamera(context, 1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/gallery.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'gallery logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Image")
                ],
              ),
            ),
          ],
        ));
  }

  void _openBottomSheetCategory(BuildContext context) {
    getBottomSheetCategory(context);
  }

  void getBottomSheetCategory(BuildContext context) async {
    var results;

    if (tabIndex == 1) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return ChallanSheet(
              projectList: projectList,
              tabIndex: 1,
            );
          });
    } else if (tabIndex == 2) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return ChallanSheet(
              poList: poList,
              tabIndex: 2,
            );
          });
    }

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);

        if (tabIndex == 1) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _projectC.text = results['approver_list'][j].project_name;
              projectId = results['approver_list'][j].project_id;
              results['approver_list'][j].isSelected = false;
              poList = [];
              poNameList.clear();
              _filter2 = 0;
              _getPoListChallan();
            }
          }

          _poC.clear();

          setState(() {});
        } else if (tabIndex == 2) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _poC.text = results['approver_list'][j].po_name;
              poId = results['approver_list'][j].purchase_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        }
        setState(() {});
      }
    }
  }

  void navWalk() async {
    var results = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => CarouselDemo(
                  imgUrl: imgRes,
                  fromPage: 3,
                )));

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);
        imgRes = results['approver_list'];
        setState(() {});
      }
    }
  }

  void submitExpense() async {
    List customFieldValues = [];

    String fImage = imgRes.join(',');
    print(fImage);

    int payId;

    if (payS == "Cash") {
      payId = 1;
    } else if (payS == "Bank") {
      payId = 3;
    } else if (payS == "Card") {
      payId = 2;
    } else {
      payId = 1;
    }

//    if (widget.customFieldDikha) {
//      for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//        var obj = {};
//        obj['key_id'] = "${widget.customFieldModel.data[i].key_id}";
//        obj['value'] = "${cftec[i].text}";
//        // print(obj);
//
//        customFieldValues.add(obj);
//        // obj = {};
//
//      }
//
//      print("Array : $customFieldValues");
//    }

    _showLoading(context);
    // cash = 1, card = 2 , Bank = 3

    var body = json.encode({
      "project_id": projectId,
      "user_id": userId,
      "challan_no": _titleC.text,
      "company_name": "-",
      "currency": currencyS,
      "challan_amount": _amountC.text,
      "challan_description": _poDescriptionController.text,
      "challan_image": fImage ?? '',
      "challan_active": 1,
      "purchase_id": poId,
      //"customFieldValues": widget.customFieldDikha ? customFieldValues : [],
      "customFieldValues": [],
    });
    print(body);

    SetFavouriteModel res = await cPM.setChallan(body, context);

    if (res.Status ?? false) {
      Navigator.pop(context);

      _bookEventDialog(context, "Challan created successfully!");
    } else {
      Navigator.pop(context);
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text(res.Message));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  void _bookEventDialog(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CustomPopUp(
          msg: message,
        );
      },
    );
  }

  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  bool _check_for_err() {
    if (imgRes == null) {
      final snackBar =
          SnackBar(
              duration: Duration(seconds: 1),
              content: Text('Please select a Challan image.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_titleC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Challan title.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_projectC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a Project.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a PO.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_amountC.text.isEmpty) {
      final snackBar =
          SnackBar(
              duration: Duration(seconds: 1),
              content: Text('Please write Total Challan amount.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poDescriptionController.text.isEmpty) {
      final snackBar =
          SnackBar(
              duration: Duration(seconds: 1),
              content: Text('Please write Challan Description.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    }
//    else if (widget.customFieldDikha) {
//      for(int i=0; i<cftec.length;i++) {
//        if(cftec[i].text.isEmpty) {
//          final snackBar = SnackBar(content: Text('Please fill all the fields.'));
//          _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
//          return false;
//        }
//      }
//      return true;
//    }
    else {
      return true;
    }
  }
}
