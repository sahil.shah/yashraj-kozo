import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:yashrajkozo/Profile/get_profile_api.dart';
import 'package:yashrajkozo/create_po/category_sheet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/autoselect.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/createAllVM/create_all_vm.dart';
import 'package:yashrajkozo/create_po/create_po_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/custom_popup.dart';
import 'package:yashrajkozo/utils/text_style.dart';
import 'package:yashrajkozo/utils/walk.dart';
import 'package:photo_view/photo_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zoom_widget/zoom_widget.dart';
import 'package:http/http.dart' as http;

class CreateExpense extends StatefulWidget {
  String projectName;
  String projectID;
  bool isFromDetail;
  // GetCustomFieldModel customFieldModel;
  // bool customFieldDikha;

  CreateExpense(
      {Key key,
        this.projectName,
        this.projectID,
        //    this.customFieldModel,
        //    this.customFieldDikha = false,
        this.isFromDetail = false})
      : super(key: key);

  @override
  _CreateExpenseState createState() => _CreateExpenseState();
}

class _CreateExpenseState extends State<CreateExpense> {
  final _titleC = TextEditingController();
  final _projectC = TextEditingController();
  final _cateC = TextEditingController();
  final _shootLocation = TextEditingController();
  final _countryC = TextEditingController();
  final _cityC = TextEditingController();
  final _subCatC = TextEditingController();
  final _amountC = TextEditingController();
  final _behalfC = TextEditingController();
  final _poDescriptionController = TextEditingController();
  final invoiceDate = TextEditingController();
  final paymentDueDate = TextEditingController();
  final invoiceNumber = TextEditingController();
  final gstAmount = TextEditingController();

  AppSingleton _singleton = AppSingleton();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<File> multiImg;
  File imgCamera;

  ScrollController _scrollController;
  List<String> finalList = [];
  int selectedIndex = 0;
  int tabIndex = 0;

  String currencyS = "INR";
  String payS = "Bank";
  String priS = "Normal";
  int sImageLength = 0;
  List<String> imgRes;
  int _filter1 = 0;
  int _filter2 = 0;
  int _filter3 = 0;
  int _filter4 = 0;
  int _filter5 = 0;
  int _filter7 = 0;
  int _filter8 = 0;
  int _filter9 = 0;
  int _filter10 = 0;

  int sProjectIndex;
  int sCateIndex;
  int sCountIndex;
  int sCityIndex;
  int sApproveIndex;

  bool firstTime;

  int projectId = 0;
  int catId = 0;
  int countryId = 0;
  int cityId = 0;
  int behalfId = 0;
  int subCatId = 0;
  int location_id = 0;

  CreatePoVM cPM;

  List<String> paymentS = [
    "Cash",
    "Bank",
    "Card",
  ];

  List<String> priorityS = ["Low", "Normal", "High"];

  int prioI = 1;

  int pIndex = 1;

  List<IconData> payI = [
    FontAwesomeIcons.moneyBillWave,
    FontAwesomeIcons.moneyCheckAlt,
    FontAwesomeIcons.solidCreditCard,
  ];

  bool isAlreadyApproved = false;
  bool isEstimatedCost = false;

  //Vendor
  int vendorId = 0;
  String vendorName = "";
  List<VendorData> data = [];

  double letfPad, topPad;
  String gstValueName = "";
  double totalExpenseValue = 0.0;
  bool manualChange = true;
  int mainGstId = 0;

  bool isAgainstRequest = false;

  void onGallary(BuildContext context, int i) async {
    if (i == 2) {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'xlsx', 'xls'],
        allowCompression: true,
      );
    } else {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.image,
        allowCompression: true,
      );
    }

    print(multiImg);

    if (multiImg != null) {
      sImageLength = 1;

      //sImageLength = 1;
      setState(() {});
      print(multiImg.length);
      uploadImage(context, 2);
    }
  }

/*  void onMultiGallary(BuildContext context) async {
     this.filesPaths = await FilePicker.getMultiFilePath(type: FileType.IMAGE);
    multiImage = await PhotoPicker.pickAsset(
      context: context,
      maxSelected: 100,
      provider: I18nProvider.english,
      checkBoxBuilderDelegate: DefaultCheckBoxBuilderDelegate(
        activeColor: Colors.white,
        unselectedColor: Colors.white,

      ),
      pickType: PickType.all,
    );
    if (multiImage != null) {

      uploadImage(context, 3);
    }
  }*/

  void onCamera(BuildContext context, int i) async {
    if (i == 1) {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 80,
      );
    } else {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.camera,
        imageQuality: 80,
      );
    }

    if (imgCamera != null) {
      sImageLength = 1;

      //sImageLength = 1;

      setState(() {});
      uploadImage(context, 1);
    }
  }

  ImageUploadRes imgres;
  String filePath = "";
  String fileName = "";
  DateTime mainDateTime;

  uploadImage(BuildContext context, int j) async {
    _showLoading(context);
    var formData = new FormData();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // j=1 => Document, j=2 => Camera, j=3 => Gallery

    if (j == 2) {
      for (int i = 0; i < multiImg.length; i++) {
        filePath = multiImg[i].path ?? '';
        fileName = multiImg[i].path ?? '';

        List<String> res = fileName.split('.');

        if (res.last == 'mp4') {
          print(res);
          Navigator.pop(context);

          final snackBar = SnackBar(content: Text("Videos are not allowed."));
          _scaffoldKey.currentState.showSnackBar(snackBar);

          return false;
        } else {
//          formData = new FormData.fromMap({
//            'file[$i]': await MultipartFile.fromFile(filePath, filename: fileName)
//          });
          formData.files.addAll([
            MapEntry(
              "file[]",
              MultipartFile.fromFileSync(filePath ?? "",
                  filename: fileName ?? ""),
            ),
          ]);
//          formData.add("file[$i]", UploadFileInfo(File(filePath), fileName));
        }
      }
    } else {
      filePath = imgCamera.path ?? '';
      fileName = imgCamera.path ?? '';
      formData = new FormData.fromMap({
        'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
      });
//      formData.add("file[]", UploadFileInfo(File(filePath), fileName));
    }

    Dio dio = new Dio()..options.baseUrl = BASE_URL;
    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        // imgRes = imgres.data.image_path.split(',');

        if (!firstTime) {
          imgRes = imgres.data.image_path.split(',');
          firstTime = !firstTime;
        } else {
          List<String> temp = [];
          temp = imgres.data.image_path.split(',');
          setState(() {
            for (int i = 0; i < temp.length; i++) {
              imgRes.add(temp[i]);
            }
          });
        }

        print(imgRes);

        setState(() {});
      } else {
        final snackBar = SnackBar(content: Text(imgres.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(content: Text("Cannot load media."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    Navigator.pop(context);
  }

  CreateAllVM cVM;
  HomeApi homeApiObj;

  //for all project
  GetProjectModel projectData;
  List<ProjectModelData> projectList = [];
  List<String> pNameList = [];

  // for all category
  SelectCategoryModel categoryData;
  List<SelectCategoryModelData> categoryList = [];
  List<String> cNameList = [];

  //shoot location
  SelectShootlocationModelData shootLocationData;
  List<ShootLocationData> shootLocationList = [];
  // for all behalfPeople
  BehalfPeopleModel behalfData;
  List<BehalfPeopleList> behalfList = [];
  List<String> behalfNameList = [];

  //for all country
  SelectCountryModel countryData;
  List<SelectCountryModelData> countryList = [];
  List<String> countNameList = [];
  SelectCountryModelData initCountry;

  //for all city
  SelectCityModel cityData;
  List<SelectCityModelData> cityList = [];
  List<String> cityNameList = [];
  SelectCityModelData initCity;

  //for all currency
  SelectCurrencyModel currencyData;

  List<SelectCurrencyModelData> currencyList = [];
  List<String> currNameList = [];
  List<String> gstNameList = [];
  List<double> gstValueList = [];

  // For Custom Field
  List<TextEditingController> cftec;

  GetUserProfileModel userData;
  GetProfileDataApi pVM;

  bool isModerator = false;
  var gstObj = [];
  var payModeObj = [];
  var prioObj = [];

  var gstData;
  List<double> gstPercentage = [];
  List<String> gstName = [];
  List<int> gstId = [];

  _getUserProfile() async {
    // get all approver list
    userData = await pVM.fetchUserProfileData(context);

    if (userData.Status ?? false) {
      _filter8 = 1;
      print("******************getProfile done");
      print("UserId = ${userData.data.user_id}");

      setState(() {});
    } else {}
  }


  callProjectAPI() async {
    projectData = await homeApiObj.fetchProjectList(context);
    if (projectData.Status ?? false) {
      projectList = projectData.data ?? [];

      for (int j = 0; j < projectList.length; j++) {
        pNameList.add(projectList[j].project_name);
      }

      if (projectList.length == 1) {
        _projectC.text = projectList[0].project_name;
        projectId = projectList[0].project_id;
        _getCategoryList();
      }

      _filter1 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("${projectData.Message}"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    if (widget.isFromDetail == true) {
      _getCategoryList();
    }
  }

  callShootLocationAPI() async {
    shootLocationData = await cVM.getAllShootlocationData();
    if(shootLocationData.Status ?? false) {
      _filter10 = 1;
      shootLocationList = shootLocationData.data;
      print("Data is");
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("${shootLocationData.Message}"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callCurrencyAPI() async {
    currencyData = await cVM.getAllCurrency();
    if (currencyData.Status ?? false) {
      currencyList = currencyData.data ?? [];

      for (int j = 0; j < currencyList.length; j++) {
        currNameList.add(currencyList[j].currency_code);
      }
      _filter5 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("${currencyData.Message}"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callCountryAPI() async {
    countryData = await cVM.getAllCountry();
    if (countryData.Status ?? false) {
      countryList = countryData.data ?? [];
      for (int j = 0; j < countryList.length; j++) {
        countNameList.add(countryList[j].country_name);
      }
      initCountry = countryList.singleWhere((p) => p.country_name == "India");
      _countryC.text = initCountry.country_name;
      countryId = initCountry.country_id;
      _filter3 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callGSTAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');
    print(accessToken);
    print(prefs.getInt('orgId'));


    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    final res = await http.get("${BASE_URL}master?organization_id=${prefs.getInt('orgId')}", headers: Headers);
    if (res.statusCode == 200 || res.statusCode == 201) {
      final j_data = json.decode(res.body);
      gstData = j_data;
      print(gstData.runtimeType);
      for(int i=0; i<gstData['data']['gst'].length; i++) {
        gstName.add(gstData['data']['gst'][i]['gst_name']);

        gstObj.add({
          "name" : gstData['data']['gst'][i]['gst_name'],
          "id" : gstData['data']['gst'][i]['gst_id'],
          "percent" : double.parse("${gstData['data']['gst'][i]['gst_percentage']}")
        });
      }
      for(int j = 0; j<gstData['data']['payment_mode'].length; j++) {
        payModeObj.add({
          "payId" : gstData['data']['payment_mode'][j]['payment_mode_id'],
          "payName" : gstData['data']['payment_mode'][j]['payment_mode_name']
        });
      }
      for(int k = 0; k<gstData['data']['priority'].length; k++) {
        prioObj.add({
          "prioId" : gstData['data']['priority'][k]['priority_id'],
          "prioName" : gstData['data']['priority'][k]['priority_name'],
        });
      }

      print(payModeObj);
      print(prioObj);
      _filter9 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callCityAPI() async {
    cityData = await cVM.getAllCity();
    if (cityData.Status ?? false) {
      cityList = cityData.data ?? [];
      for (int j = 0; j < cityList.length; j++) {
        cityNameList.add(cityList[j].city_name);
      }
      if(initCity != null) {
        initCity = cityList.singleWhere((p) => p.city_name == "Mumbai");
        _cityC.text = initCity.city_name;
        cityId = initCity.city_id;
      }
      _filter4 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _getAllApiData() async {
    callProjectAPI();
    callShootLocationAPI();
    callCountryAPI();
    callCurrencyAPI();
    callGSTAPI();
    callCityAPI();
  }

  _getCategoryList() async {
    // get all category list
    categoryData = await cVM.getAllCategory(projectId);
    if (categoryData.Status ?? false) {
      categoryList = categoryData.data ?? [];
      _filter2 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    // get all behalf people list
    behalfData = await cVM.getAllBehalfPeople(projectId);
    if (behalfData.Status ?? false) {
      behalfList = behalfData.data ?? [];

      for(final e in userData.data.project_role.where((k)=> k.project_id == projectId)){

        if(e.project_role.toLowerCase() == "moderator"){
          isModerator = true;
        }else{
          isModerator = false;
        }
        setState(() {
        });
      }

      for (int j = 0; j < behalfList.length; j++) {
        behalfNameList.add(behalfList[j].user_name);

        /* if (userData.data.project_role.length > 0) {
          if (userData.data.project_role[j].project_id == projectId) {
            if (userData.data.project_role[j].project_role.toLowerCase() ==
                "moderator") {
              isModerator = true;
              break;
            }
          } else {
            isModerator = false;
          }
        }*/
      }

      /* _filter2 = 1;*/
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getCustomData();
    gstAmount.text = "0";
    setInitialData();

    firstTime = false;
    pVM = GetProfileDataApi();
    homeApiObj = HomeApi();
    cPM = CreatePoVM();
    cVM = CreateAllVM();
    _getAllApiData();
    _getUserProfile();
  }

//  getCustomData() {
//    if (widget.customFieldDikha) {
//      cftec = [];
//      if (widget.customFieldModel != null) {
//        for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//          TextEditingController tec = new TextEditingController();
//          cftec.add(tec);
//        }
//      }
//    }
//  }

  setInitialData() {
    if (widget.isFromDetail) {
      _projectC.text = "${widget.projectName ?? ''}";
      projectId = int.parse("${widget.projectID ?? 0}");
    }
  }

  void isAlreadyApproveChange(bool value) {
    if (isAlreadyApproved) {
      print("AP: $isAlreadyApproved");
      print("EC: $isEstimatedCost");
    } else {
      setState(() {
        isAlreadyApproved = value;
        isEstimatedCost = !value;
      });
      print("AP: $isAlreadyApproved");
      print("EC: $isEstimatedCost");
    }
  }

  void isEstimatedChange(bool value) {
    if (isEstimatedCost) {
      print("AP: $isAlreadyApproved");
      print("EC: $isEstimatedCost");
    } else {
      setState(() {
        isAlreadyApproved = !value;
        isEstimatedCost = value;
      });
      print("AP: $isAlreadyApproved");
      print("EC: $isEstimatedCost");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _titleC.dispose();
    _behalfC.dispose();
//    _scrollController.dispose();
    _projectC.dispose();
    _cateC.dispose();
    _subCatC.dispose();
    _countryC.dispose();
    _cityC.dispose();
    _amountC.dispose();
    _poDescriptionController.dispose();
    invoiceDate.dispose();
    paymentDueDate.dispose();
    invoiceNumber.dispose();
    gstAmount.dispose();
    //  disposeCustomField();
    super.dispose();
  }

//  disposeCustomField() {
//    if (widget.customFieldDikha) {
//      for (int i = 0; i < cftec.length; i++) {
//        cftec[i].dispose();
//      }
//    }
//  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;
    letfPad = 0.00944;
    topPad = 0.00470;
    return SafeArea(
      bottom: true,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: CustomAppBar(
          title: "Create Expense",

        ),
        body: Container(
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), 0),
                child: Row(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        if (imgRes != null && imgRes.length != 0) {
                          navWalk();
                        } else {
                          _showmodalsheet(
                            w,
                            h,
                            context,
                          );
                        }
                      },
                      child: Card(
                        color: Color(0xffeeeeee),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                        elevation: 2,
                        child: Container(
                            width: w*0.28,
                            height: h*0.11,
                            child: ClipRRect(
                              borderRadius:
                              BorderRadius.all(Radius.circular(h*0.0094)),
                              child: (imgRes != null && imgRes.length > 0)
                                  ? Stack(
                                alignment: Alignment.topRight,
                                fit: StackFit.expand,
                                children: <Widget>[
                                  FadeInImage(
                                    placeholder: AssetImage(
                                        'assets/images/loading.gif'),
                                    image: NetworkImage(
                                        (imgRes[0].contains('pdf') || imgRes[0].contains('xlsx') || imgRes[0].contains("xls")
                                            ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
                                            : imgRes[0].contains('png') || imgRes[0].contains('jpeg') || imgRes[0].contains("jpg") ? "${imgRes[0]}?token=${_singleton.tempToken}" : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW" ) ),
                                    fit: BoxFit.cover,
                                  ),
                                  /*  Image(
                                          image: NetworkImage(imgRes[0]),
                                          fit: BoxFit.cover,
                                        ),*/

                                  imgRes.length > 1
                                      ? Container(
                                    padding: EdgeInsets.only(left: (letfPad*8*w), right: (letfPad*8*w), top: (topPad*8*h), bottom: (topPad*8*h)),
                                    child: SvgPicture.asset(
                                        'assets/images/multiple-uploads.svg',
                                        color: Color(0xffd0d3d8),
                                        semanticsLabel:
                                        'document logo'),
                                  )
                                      : Container()
                                ],
                              )
                                  : Padding(
                                padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                                child: SvgPicture.asset(
                                    'assets/images/file.svg',
                                    color: Color(0xffd0d3d8),
                                    semanticsLabel: 'document logo'),
                              ),
                            )),
                      ),
                    ),
                    Container(
                      height: h*0.127,
                      width: w * .60 - (w*0.037),
                      child: Column(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.only(left: (letfPad*2*w), top: (topPad*2*h), right: (letfPad*2*w), bottom: (topPad*h)),
                              width: w * .60 - letfPad*4*w,
                              height: h*0.06,
                              child: RaisedButton(
                                color: Color(0xffeeeeee),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(h*0.0094)),
                                    side: BorderSide(color: borderColor)),
                                onPressed: () {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  _showmodalsheet(
                                    w,
                                    h,
                                    context,
                                  );
                                },
                                child: FittedBox(
                                  fit: BoxFit.fitWidth,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        FontAwesomeIcons.images,
                                        size: topPad*h*4,
                                      ),
                                      SizedBox(
                                        width: w*0.0141,
                                      ),
                                      Text(
                                        "Upload from Gallery",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: h*0.014,
                                            fontFamily: 'SanMedium'),
                                      )
                                    ],
                                  ),
                                ),
                              )),
                          Container(
                              padding: EdgeInsets.only(left: letfPad*2*w, bottom: topPad*2*h, right: letfPad*2*w, top: topPad*h),
                              width: w * .60 - letfPad*4*w,
                              height: h*0.06,
                              child: RaisedButton(
                                color: Color(0xffeeeeee),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(h*0.0094)),
                                    side: BorderSide(color: borderColor)),
                                onPressed: () {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  onCamera(context, 2);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.camera,
                                      size: topPad*4*h,
                                    ),
                                    SizedBox(
                                      width: w*0.0141,
                                    ),
                                    Text(
                                      "Use the Camera",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: h*0.014,
                                          fontFamily: 'SanMedium'),
                                    )
                                  ],
                                ),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: h*0.028,
              ),
              Container(
                padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                child: Column(
                  children: <Widget>[
                    TypeAheadField<VendorData>(
                      textFieldConfiguration: TextFieldConfiguration(
                          autofocus: false,
                          controller: _titleC,

                          onTap: (){
                            setState(() {
                              vendorId = 0;
                            });
                          },
                          decoration: InputDecoration(
                              suffixIcon: Icon(Icons.add),
                              hintText: 'Enter vendor name'
                          )
                      ),

                      suggestionsCallback: (pattern) async {
                        print(pattern);
                        await getVendorData(pattern);
                        return data.where((element) => element.vendor_name.toLowerCase().startsWith(pattern.toLowerCase())).toList();
                      },

                      itemBuilder: (context, suggestion) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Padding(
                                padding: EdgeInsets.only(left: letfPad*2*w, top: topPad*2*h, right: letfPad*2*w, bottom: topPad*2*h),
                                child: Text(suggestion.vendor_name, style: TextStyle(fontSize: h*0.018),),
                              ),
                            ),
                            Divider()
                          ],
                        );
                      },
                      onSuggestionSelected: (suggestion) {

                        FocusScopeNode currentFocus = FocusScope.of(context);

                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        setState(() {
                          _titleC.text = suggestion.vendor_name;
                          vendorId = suggestion.vendor_id;
                        });
                      },
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),

                    Container(
                      height: h*0.058,
                      child: TextField(
                        controller: invoiceNumber,
                        decoration: InputDecoration(
                          hintText: 'Enter Invoice Number (Optional)',
                          hintStyle: TextStyle(fontFamily: 'SanRegular'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),


                    Container(
                      height: h*0.058,
                      child: TextField(
                        onTap: () {
                          _selectDateInvoiceDate();
                        },
                        readOnly: true,
                        controller: invoiceDate,
                        decoration: InputDecoration(
                          hintText: 'Enter Invoice Date (Optional)',
                          hintStyle: TextStyle(fontFamily: 'SanRegular'),
                          prefixIcon: Container(
                            height: h*0.02,
                            child: IconButton(
                              padding: EdgeInsets.all(0),
                              icon: SvgPicture.asset('assets/images/birthday.svg',
                                  fit: BoxFit.cover,
                                  width:  h*0.0211,
                                  height: h*0.02,
                                  semanticsLabel: 'popup close'),
                              onPressed: null,
                            ),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),

                    Container(
                      height: h*0.058,
                      child: TextField(
                        onTap: () {
                          if(invoiceDate.text.isEmpty) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please select Invoice Date'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                            _selectDatePaymentDueDate();
                          }

                        },
                        readOnly: true,
                        controller: paymentDueDate,
                        decoration: InputDecoration(
                          hintText: 'Enter Payment Due Date (Optional)',
                          hintStyle: TextStyle(fontFamily: 'SanRegular'),
                          prefixIcon: Container(
                            height: h*0.02,
                            child: IconButton(
                              padding: EdgeInsets.all(0),
                              icon: SvgPicture.asset('assets/images/birthday.svg',
                                  fit: BoxFit.cover,
                                  width:  h*0.0211,
                                  height: h*0.02,
                                  semanticsLabel: 'popup close'),
                              onPressed: null,
                            ),
                          ),
                        ),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          selectedIndex = 0;
                          tabIndex = 1;

                          _filter1 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                          if (_filter1 != 1) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please wait loading data...'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Project",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _projectC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          if (_projectC.text.isEmpty) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please select Project first'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                            FocusScope.of(context).requestFocus(FocusNode());
                            selectedIndex = 0;
                            tabIndex = 2;

                            _filter2 == 1
                                ? _openBottomSheetCategory(context)
                                : null;

                            if (_filter2 != 1) {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text('Please wait loading data...'));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Category",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _cateC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (_cateC.text.isEmpty) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please select Category first'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                            tabIndex = 7;
                            selectedIndex = 0;
                            _filter7 == 1
                                ? _openBottomSheetCategory(context)
                                : null;
                            if (_filter7 != 1) {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text('Please wait loading data...'));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Sub Category",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _subCatC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {

                            FocusScope.of(context).requestFocus(FocusNode());
                            selectedIndex = 0;
                            tabIndex = 10;
                            _filter10 == 1
                                ? _openBottomSheetCategory(context)
                                : null;

                            if (_filter10 != 1) {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text('Please wait loading data...'));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }

                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Set Name (Optional)",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _shootLocation,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),

                    SizedBox(
                      height: h*0.018,
                    ),
                    Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          selectedIndex = 0;
                          tabIndex = 3;

                          _filter3 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                          if (_filter3 != 1) {
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text('Please wait loading data...'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Select Country",
                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.0188,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _countryC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    ),
                    /*   SizedBox(
                      height: 16,
                    ),
                    RaisedButton(
                      color: Color(0xffeeeeee),
                      padding: EdgeInsets.all(0.0),
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        tabIndex = 4;
                        selectedIndex = 0;
                        _filter4 == 1
                            ? _openBottomSheetCategory(context)
                            : null;

                        if (_filter4 != 1) {
                          final snackBar = SnackBar(
                              content: Text('Please wait loading data...'));
                          _scaffoldKey.currentState.showSnackBar(snackBar);
                        }
                      },
                      child: TextField(
                        readOnly: true,
                        enabled: false,
                        decoration: new InputDecoration(
                            hintText: "Select City",
                            hintStyle: TextStyle(fontFamily: 'SanRegular'),
                            border: InputBorder.none,
                            prefixIcon: Container(
                              margin: EdgeInsets.only(right: 16),
                              child: Icon(
                                FontAwesomeIcons.fileInvoice,
                                size: 16,
                              ),
                            ),
                            suffixIcon: Icon(
                              Icons.keyboard_arrow_down,
                              color: appColor,
                              size: 24,
                            )),
                        style: Styles.formInputTextStyle,
                        controller: _cityC,
                        obscureText: false,
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                          side: BorderSide(color: borderColor)),
                    ),*/

                    SizedBox(
                      height: h*0.018,
                    ),
                    isModerator
                        ? Container(
                      height: h*0.058,
                      child: RaisedButton(
                        color: Color(0xffeeeeee),
                        padding: EdgeInsets.all(0.0),
                        onPressed: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                          selectedIndex = 0;
                          tabIndex = 5;

                          _filter8 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                          if (_filter8 != 1) {
                            final snackBar = SnackBar(
                                content:
                                Text('Please wait loading data...'));
                            _scaffoldKey.currentState
                                .showSnackBar(snackBar);
                          }
                        },
                        child: TextField(
                          readOnly: true,
                          enabled: false,
                          decoration: new InputDecoration(
                              hintText: "Creating on Behalf of (OPTIONAL)",
                              hintStyle:
                              TextStyle(fontFamily: 'SanRegular'),
                              border: InputBorder.none,
                              prefixIcon: Container(
                                margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
                                child: Icon(
                                  FontAwesomeIcons.fileInvoice,
                                  size: h*0.01881,
                                ),
                              ),
                              suffixIcon: Icon(
                                Icons.keyboard_arrow_down,
                                color: appColor,
                                size: h*0.028,
                              )),
                          style: Styles.formInputTextStyle,
                          controller: _behalfC,
                          obscureText: false,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(h*0.0094)),
                            side: BorderSide(color: borderColor)),
                      ),
                    )
                        : Container(),
                    SizedBox(
                      height: h*0.02,
                    ),
//                    widget.customFieldDikha
//                        ? ListView.builder(
//                            shrinkWrap: true,
//                            physics: NeverScrollableScrollPhysics(),
//                            itemCount: widget.customFieldModel.data.length,
//                            itemBuilder: (BuildContext context, int i) {
//                              return Container(
//                                height: 80,
//                                width: w * .60 - 16,
//                                //color: Colors.blue,
//                                child: Column(
//                                  children: <Widget>[
//                                    TextField(
//                                      keyboardType: widget.customFieldModel
//                                                  .data[i].field_type_id ==
//                                              1
//                                          ? TextInputType.text
//                                          : TextInputType.number,
//                                      decoration: new InputDecoration(
//                                          labelText:
//                                              "${widget.customFieldModel.data[i].key_name}",
//                                          labelStyle: TextStyle(
//                                              fontFamily: 'SanRegular'),
//                                          prefixIcon: Container(
//                                            margin: EdgeInsets.only(right: 16),
//                                            child: Icon(
//                                              FontAwesomeIcons.fileInvoice,
//                                              size: 16,
//                                            ),
//                                          )),
//                                      inputFormatters: [
//                                        new BlacklistingTextInputFormatter(
//                                            widget.customFieldModel.data[i]
//                                                        .field_type_id ==
//                                                    1
//                                                ? new RegExp('')
//                                                : new RegExp('[\\,|\\-|\\ ]')),
//                                      ],
//                                      style: Styles.formInputTextStyle,
//                                      controller: cftec[i],
//                                      obscureText: false,
//                                    ),
//                                  ],
//                                ),
//                              );
//                            }) : Container(),
                    GestureDetector(
                      onTap: () {
                        isAlreadyApproved = !isAlreadyApproved;
                        setState(() {});
                      },
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Checkbox(
                              value: isAlreadyApproved,
                              onChanged: (p) {
                                isAlreadyApproved = !isAlreadyApproved;
                                setState(() {});
                              },
                              activeColor: appColor,
                            ),
                          ),
                          Flexible(
                            flex: 5,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text(
                                  'Already Approved',
                                )),
                          ),
                        ],
                      ),
                    ),

                    _singleton.isAgainstRequest ? SizedBox(height: 16) : Container(),

                    _singleton.isAgainstRequest ? GestureDetector(
                      onTap: () {
                        isAgainstRequest = !isAgainstRequest;
                        print(isAgainstRequest);
                        setState(() {});
                      },
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Checkbox(
                              value: isAgainstRequest,
                              onChanged: (p) {
                                isAgainstRequest = !isAgainstRequest;
                                print(isAgainstRequest);
                                setState(() {});
                              },
                              activeColor: appColor,
                            ),
                          ),
                          Flexible(
                            flex: 5,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text(
                                  _singleton.againstRequestText ?? 'Against Request',
                                )),
                          ),
                        ],
                      ),
                    ) : Container(),
                    /* Row(
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Checkbox(
                            value: isAlreadyApproved,
                            onChanged: isAlreadyApproveChange,
                            activeColor: appColor,
                          ),
                        ),

                      Flexible(
                        flex: 5,
                        child: Material(
                            type: MaterialType.transparency,
                            child: Text(
                              'Already Approved',
                            )),
                      ),
                    ],
                    ),*/

                    /*  Row(
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          child: Checkbox(
                            value: isEstimatedCost,
                            onChanged: isEstimatedChange,
                            activeColor: appColor,
                          ),
                        ),

                        Flexible(
                          flex: 5,
                          child: Material(
                              type: MaterialType.transparency,
                              child: Text(
                                'Estimated Cost',
                              )),
                        ),
                      ],
                    ),*/

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Center(
                          child: PopupMenuButton(

                            itemBuilder: (BuildContext context) {
                              return currNameList.map((String s) {
                                return PopupMenuItem<String>(
                                  value: s,
                                  child: Text(s),
                                );
                              }).toList();
                            },
                            onSelected: (j) {
                              FocusScope.of(context).requestFocus(FocusNode());
                              currencyS = j;
                              setState(() {});
                            },
                            child: Container(
                                padding: EdgeInsets.only(left: letfPad*1*w, ),
                                width: (w) / 3 - letfPad*2*w,
                                height: h*0.05,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(h*0.0094))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    RichText(
                                        textAlign: TextAlign.start,
                                        text: TextSpan(
                                          children: [
                                            TextSpan(
                                                text: 'Currency: ',
                                                style: TextStyle(
                                                    fontFamily: 'SanRegular',
                                                    color: Colors.grey,
                                                    fontSize: h*0.016)),
                                            TextSpan(
                                                text: currencyS,
                                                style: TextStyle(
                                                    fontFamily: 'SanSemiBold',
                                                    color: Colors.black,
                                                    fontSize: h*0.016)),
                                          ],
                                        )),
                                    Icon(Icons.arrow_drop_down)
                                  ],
                                )),
                          ),
                        ),
                        SizedBox(
                          width: w*0.037,
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: (w) / 2 - (w*0.04722),
                                child: TextField(
                                  onChanged: (text){
                                    totalExpenseValue = double.parse(text);
                                    setState(() {});
                                  },
                                  decoration: new InputDecoration(
                                    labelText: "Enter Expense Amount",
                                    labelStyle:
                                    TextStyle(fontFamily: 'SanRegular'),
                                  ),
                                  inputFormatters: [
                                    new BlacklistingTextInputFormatter(
                                        new RegExp('[\\,|\\-|\\ ]')),
                                  ],
                                  style: Styles.formInputTextStyle,
                                  controller: _amountC,
                                  keyboardType: TextInputType.number,
                                  obscureText: false,
                                ),
                              ),
                              Text(
                                "Taxes Excluded",
                                style: TextStyle(
                                    fontSize: h*0.01, fontFamily: 'SanRegular'),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: h*0.018,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        PopupMenuButton(
                          itemBuilder: (BuildContext context) {
                            return gstName.map((String s) {
                              return PopupMenuItem<String>(
                                value: s,
                                child: Text("$s"),
                              );
                            }).toList();
                          },

                          onSelected: (j) {
                            FocusScope.of(context).requestFocus(FocusNode());
                            gstAmount.text="";

                            if(_amountC.text == "") {
                              final snackBar = SnackBar(
                                  duration: Duration(seconds: 1),
                                  content: Text("Please enter amount"));
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            } else {
                              totalExpenseValue = double.parse(_amountC.text.isEmpty ? 0.0 : _amountC.text);
                              gstValueName = j;
                              double val = double.parse(_amountC.text);

                              for(int i=0;i<gstObj.length; i++) {
                                if(gstValueName == gstObj[i]['name']) {
                                  print(gstObj[i]['percent']);
                                  mainGstId =  gstObj[i]['id'];
                                  if (gstObj[i]['percent'] == 0.0 || gstObj[i]['percent']==0) {
                                    manualChange = false;
                                    break;
                                  } else {
                                    totalExpenseValue = double.parse(_amountC.text) + (gstObj[i]['percent'] / 100 * val);
                                    double gstValue = (gstObj[i]['percent'] / 100 * val);
                                    gstAmount.text = gstValue.toStringAsFixed(2).toString();
                                    totalExpenseValue = double.parse(totalExpenseValue.toStringAsFixed(2));
                                    manualChange = false;
                                    break;
                                  }
                                }
                              }
                              setState(() {});
                            }
                          },
                          child: Container(
                              padding: EdgeInsets.only(right: letfPad*w, left: letfPad*w),
                              height: h*0.05,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.all(Radius.circular(h*0.0094))),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  RichText(
                                      textAlign: TextAlign.start,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                              text: 'Tax Type : ',
                                              style: TextStyle(
                                                  fontFamily: 'SanRegular',
                                                  color: Colors.grey,
                                                  fontSize: h*0.012)),
                                          TextSpan(
                                              text: gstValueName,
                                              style: TextStyle(
                                                  fontFamily: 'SanSemiBold',
                                                  color: Colors.black,
                                                  fontSize: h*0.012)),
                                        ],
                                      )),
                                  Icon(Icons.arrow_drop_down)
                                ],
                              )),
                        ),

                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: gstValueName == "" ? (w) / 2 - (w*0.04722) : w/3 + w*6*letfPad,
                                child: TextField(
                                  readOnly: manualChange,
                                  decoration: new InputDecoration(
                                    labelText: "Tax value (Optional)",
                                    labelStyle: TextStyle(fontFamily: 'SanRegular'),
                                  ),
                                  inputFormatters: [
                                    new BlacklistingTextInputFormatter(
                                        new RegExp('[\\,|\\-|\\ ]')),
                                  ],
                                  style: Styles.formInputTextStyle,
                                  controller: gstAmount,
                                  keyboardType: TextInputType.number,
                                  onChanged: (text) {
                                    totalExpenseValue = double.parse(_amountC.text) + double.parse(gstAmount.text);
                                    setState(() {});
                                  },
                                  obscureText: false,
                                  onSubmitted: (text) {
                                    totalExpenseValue = double.parse(_amountC.text) + double.parse(gstAmount.text);
                                  },
                                ),
                              ),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: h*0.018,),
              Container(
                padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Total Amount", style: TextStyle(fontSize: h*0.021),),
                    Text("${totalExpenseValue.toStringAsFixed(2) ?? 0}", style: TextStyle(fontSize: h*0.021)),
                  ],
                ),
              ),

              SizedBox(
                height: h*0.037,
              ),

              Padding(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                child: Text(
                  "Mode of Payment",
                  style: TextStyle(fontFamily: 'SanSemiBold'),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                width: w,
                height: h*0.0705,
                child: ListView.builder(
                    padding: EdgeInsets.all(0),
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: payModeObj?.length ?? 0,
                    itemBuilder: (context, i) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                            color: pIndex == i ? appColor : Color(0xffeeeeee),
                            onPressed: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              pIndex = i;
                              payS = payModeObj[i]['payName'];
                              setState(() {});
                              print(payS);
                              print(payModeObj[pIndex]['payId']);
                            },
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  payI[0],
                                  size: h*0.016,
                                  color:
                                  pIndex == i ? Colors.white : Colors.black,
                                ),
                                SizedBox(
                                  width: w*0.014,
                                ),
                                Text(
                                  payModeObj[i]['payName'] ?? '',
                                  style: TextStyle(
                                      fontFamily: 'SanMedium',
                                      color: pIndex == i
                                          ? Colors.white
                                          : Colors.black),
                                )
                              ],
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(h*0.0094)),
                                side: BorderSide(color: borderColor)),
                          ),
                          SizedBox(
                            width: w*0.018,
                          )
                        ],
                      );
                    }),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                child: Text(
                  "Select Priority",
                  style: TextStyle(fontFamily: 'SanSemiBold'),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                width: w,
                height: h*0.0705,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: prioObj?.length ?? 0,
                    itemBuilder: (context, i) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                            color: prioI == i ? appColor : Color(0xffeeeeee),
                            onPressed: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              prioI = i;
                              priS = prioObj[i]['prioName'];
                              setState(() {});
                              print(prioObj[prioI]['prioId']);
                              print(prioObj[prioI]['prioName']);
                            },
                            child: Text(
                              prioObj[i]['prioName'] ?? '',
                              style: TextStyle(
                                  fontFamily: 'SanMedium',
                                  color:
                                  prioI == i ? Colors.white : Colors.black),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(h*0.0094)),
                                side: BorderSide(color: borderColor)),
                          ),
                          SizedBox(
                            width: w*0.0188,
                          )
                        ],
                      );
                    }),
              ),
              SizedBox(
                height: h*0.018,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                child: new Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(h*0.01)),
                  child: new Container(
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                      height: h*0.1176,
                      child: TextField(
                        textInputAction: TextInputAction.go,
                        maxLines: 4,
                        controller: _poDescriptionController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Expense description...',
                            hintStyle: TextStyle(fontFamily: 'SanRegular')),
                      )),
                ),
              ),
              SizedBox(
                height: h*0.02,
              ),
              CustomBtn(
                title: "Submit",
                onTap: () async {
                  print(vendorId);
                  print(_titleC.text);
                  ConnectionHelper mCH;
                  mCH = ConnectionHelper.getInstance();

                  bool con = await mCH.checkConnection();

                  if (con) {
                    if (_check_for_err()) {
                      submitExpense(context);
                    }
                  } else {
                    final snackBar = SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text("Please check your internet connection"));
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  getVendorData(String keyword) async {
    data = await getData(keyword ?? "");
    if(data == null) {
      data = [];
    }
    setState(() {});
    print(data);
  }

  getData(String queryString) async {
    VendorAutoCompleteData res_d;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    final HEADERS = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "Authorization": accessToken ?? ""
    };

    print("Token is :- ${accessToken}");

    var res = await http.get('${BASE_URL}vendor?search_txt=${queryString ?? ""}', headers: HEADERS);
    if(res.statusCode == 200) {
      final jData = json.decode(res.body);
      res_d = VendorAutoCompleteData.fromJson(jData);
      print(res_d);
      return res_d.data;
    }
  }


  void _showmodalsheet(double w, double h, BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(h*0.03), topRight: Radius.circular(h*0.03)),
        ),
        context: context,
        builder: (buider) {
          return getBottomSheet(w, h, context);
        });
  }

  Widget getBottomSheet(double w, double h, BuildContext context) {
    return Container(
        height: h*0.1411,
        width: double.infinity,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 2);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/document.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'document logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Document")
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 1);
                //onCamera(context, 1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/gallery.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'gallery logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Image")
                ],
              ),
            ),
          ],
        ));
  }

  Future<void> _openBottomSheetCategory(BuildContext context) async {
    await getBottomSheetCategory(context);
    Autoselect.subCategory(
      tabIndex,
      categoryList,
      catId,
      _cateC,
      _subCatC,
      (value) {
        subCatId = value;
      },
      setState,
    );
  }

  Future<void> getBottomSheetCategory(BuildContext context) async {
    var results;

    if (tabIndex == 1) {
      results = await showModalBottomSheet(
          context: context,
          isDismissible: true,
          builder: (context) {
            return DynamicSheet(
              projectList: projectList,
              tabIndex: 1,
            );
          });
    } else if (tabIndex == 2) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 2,
              projectId: projectId,
            );
          });
    } else if (tabIndex == 3) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              countryList: countryList,
              tabIndex: 3,
            );
          });
    } else if (tabIndex == 4) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              cityList: cityList,
              tabIndex: 4,
            );
          });
    } else if (tabIndex == 5) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              behalfList: behalfList,
              tabIndex: 5,
            );
          });
    } else if (tabIndex == 7) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 7,
              catId: catId,
              projectId: projectId,
            );
          });
    } else if (tabIndex == 10) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              shootDataList: shootLocationList,
              tabIndex: 10,
            );
          });
    }

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);

        if (tabIndex == 1) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _projectC.text = results['approver_list'][j].project_name;
              projectId = results['approver_list'][j].project_id;
              results['approver_list'][j].isSelected = false;
              categoryList.clear();
              _cateC.clear();
              _subCatC.clear();
              behalfList.clear();
              _behalfC.clear();
              _filter2 = 0;
              _getCategoryList();

              /*   if(userData.data.project_role.length > 0 ){
                for(int j =0; j < userData.data.project_role.length; j++){

                  if(userData.data.project_role[j].project_id == projectId){
                    isModerator = true;
                    break;
                  }else{
                    isModerator = false;
                  }
                }
              }*/
            }
          }
        } else if (tabIndex == 2) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _cateC.text = results['approver_list'][j].category_name;
              catId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
              _filter7 = 1;
              _subCatC.clear();
              subCatId = 0;
            }
          }
          /*  _cateC.text = results['approver_list'].category_name;
          catId = results['approver_list'].category_id;

          results['approver_list'].isSelected = false;
          results['approver_list'].selectedIndex = -1;*/
        } else if (tabIndex == 3) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _countryC.text = results['approver_list'][j].country_name;
              countryId = results['approver_list'][j].country_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 4) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _cityC.text = results['approver_list'][j].city_name;
              cityId = results['approver_list'][j].city_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 5) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _behalfC.text = results['approver_list'][j].user_name;
              behalfId = results['approver_list'][j].user_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 7) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _subCatC.text = results['approver_list'][j].category_name;
              subCatId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 10) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _shootLocation.text = results['approver_list'][j].location_name;
              location_id = results['approver_list'][j].location_id;
              results['approver_list'][j].isSelected = false;
              print(location_id);
            }
          }
        }

        setState(() {});
      }
    }
  }

  void submitExpense(BuildContext context) async {
    List customFieldValues = [];

    String fImage = imgRes.join(',');
    print(fImage);

    int payId = int.parse("${payModeObj[pIndex]['payId']}");
    int priorityID = int.parse("${prioObj[prioI]['prioId']}");

    int sCatId;

    if (subCatId == 0) {
      sCatId = catId;
    } else {
      sCatId = subCatId;
    }

    // if (payS == "Cash") {
    //   payId = 1;
    // } else if (payS == "Bank") {
    //   payId = 3;
    // } else if (payS == "Card") {
    //   payId = 2;
    // } else {
    //   payId = 1;
    // }

//    if (widget.customFieldDikha) {
//      for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//        var obj = {};
//        obj['key_id'] = "${widget.customFieldModel.data[i].key_id}";
//        obj['value'] = "${cftec[i].text}";
//        // print(obj);
//
//        customFieldValues.add(obj);
//        // obj = {};
//      }
//
//      print("Array : $customFieldValues");
//    }

    _showLoading(context);
    // cash = 1, card = 2 , Bank = 3

    var body = json.encode({
      "project_id": projectId,
      "po_name": _titleC.text,
      "item_name": "-",
      "company_name": "-",
      //"customFieldValues": widget.customFieldDikha ? customFieldValues : [],
      "customFieldValues": [],
      "category_id": sCatId,
      "company_address": "-",
      "price": _amountC.text,
      "currency": currencyS,
      "sgst": "0",
      "cgst": "0",
      "total": _amountC.text,
      "purchase_image": fImage,
      "country_id": countryId,
      "purchase_description": _poDescriptionController.text,
      "purchase_status": "0",
      "approved_percentage": "0",
      "purchase_active": "1",
      "payment_mode": payId,
      "payment_mode_description": "-",
      "priority": "$priorityID",
      "namePriority": priS,
      "purchase_type": "expense",
      "approver_id": "0",
      "city_id": _singleton.currentCity,
      "onBehalfId": behalfId,
      "onBehalfName": "",
      "vendorName" : _titleC.text,
      "vendor_id" : vendorId,
      "isAlreadyPaid": isAlreadyApproved ? 1 : 0,
      "payment_due_date" : paymentDueDate.text,
      "invoice_number" : invoiceNumber.text,
      "invoice_date" : invoiceDate.text,
      "total_amount" : totalExpenseValue.toString(),
      "gst_amount" : gstAmount.text,
      "gst_id" : mainGstId,
      "isAgainstRequest" : isAgainstRequest ? 1 : 0,
      "location_id" : location_id ?? 0
    });
    print(body);

    SetFavouriteModel res = await cPM.setPO(body, context);

    if (res.Status ?? false) {
      Navigator.pop(context);

      _bookEventDialog(context, "Expense created successfully!");
    } else {
      Navigator.pop(context);
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text(res.Message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void navWalk() async {
    var results = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => CarouselDemo(
              imgUrl: imgRes,
              fromPage: 2,
            )));

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);
        imgRes = results['approver_list'];
        setState(() {});
      }
    }
  }

  void _bookEventDialog(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CustomPopUp(
          msg: message,
        );
      },
    );
  }

  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  bool _check_for_err() {
    if (imgRes == null) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select an Expense image.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_titleC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Expense title.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    }
    // else if (invoiceNumber.text.isEmpty) {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text('Please enter Invoice Number.'));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    //   return false;
    // }
    // else if (invoiceDate.text.isEmpty) {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text('Please enter Invoice Date.'));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    //   return false;
    // }
    // else if (paymentDueDate.text.isEmpty) {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text('Please enter Payment Due Date.'));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    //   return false;
    // }
    else if (_cateC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a Category.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_countryC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select a Country.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    }
    // else if (_cityC.text.isEmpty) {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text('Please select a City.'));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    //   return false;
    // }
    else if (_amountC.text.isEmpty) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please write Total Expense amount.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poDescriptionController.text.isEmpty) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please write Expense Description.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (payS == "") {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select Payment Mode.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else if (priS == "") {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select Priority.'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    }
//    else if (widget.customFieldDikha) {
//      for(int i=0; i<cftec.length;i++) {
//        if(cftec[i].text.isEmpty) {
//          final snackBar = SnackBar(content: Text('Please fill all the fields.'));
//          _scaffoldKey.currentState.showSnackBar(snackBar);
//          return false;
//        }
//      }
//      return true;
//    }
    else {
      return true;
    }
  }

  Future _selectDateInvoiceDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(DateTime.now().year - 2),
      lastDate: new DateTime(DateTime.now().year + 2),
    );

    mainDateTime = picked;
    print(mainDateTime);

    String formattedDate = DateFormat('dd-MM-yyyy – kk:mm').format(picked);

    print(formattedDate);
    String date;

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        invoiceDate.text = date;
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }

  Future _selectDatePaymentDueDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: mainDateTime,
      firstDate: mainDateTime,
      lastDate: new DateTime(2050),
    );


    String formattedDate = DateFormat('dd-MM-yyyy – kk:mm').format(picked);

    print(formattedDate);
    String date;

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        paymentDueDate.text = date;
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }

  _launchURL(String d) async {
    if (await canLaunch(d)) {
      await launch(d);
    } else {
      throw 'Could not launch $d';
    }
  }
}