import 'dart:async';
import 'dart:convert';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/search_page/search_page_vm.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CategorySheet extends StatefulWidget {

  List<SelectCategoryModelData> categoryList;
  int tabIndex;
  int catId;
  int projectId;
  CategorySheet({
    Key key,
    this.tabIndex,
    this.catId,
    this.projectId,
    this.categoryList,
  }) : super(key: key);

  @override
  _CategorySheetState createState() => _CategorySheetState();
}

class _CategorySheetState extends State<CategorySheet> {

  SelectCategoryModel mF;
  HomeApi homeApiObj;

  List<SelectCategoryModelData> dummyParentList =[] ;
  List<SelectCategoryModelData> dummyChildList =[] ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    search.addListener(_onSearch);
    sVM = SearchVM();


  }
  final _scaffoldKey1 = GlobalKey<ScaffoldState>();

  List<SelectCategoryModelData> childList = [];

  final search = new TextEditingController();

  SearchVM sVM;
  Timer _debounce;
  GetProjectModel _d;
  String s_q = "";

//  List<SelectCategoryModelData> selectedCat = [] ;
  SelectCategoryModelData selectedCat =  SelectCategoryModelData() ;
//  bool isTap = false;

  int currentIndex = -2;

  int listIndex = -1;


  _onSearch() async{
    print("on search start ****************");
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      // do something with _searchQuery.text
      if (/*search.text.isNotEmpty &&*/ s_q != search.text) {
        s_q = search.text;
        _searchFun();
      }
    });
  }

  int searchId;

  _searchFun()async{
    FocusScope.of(context).requestFocus(FocusNode());
    mF = await sVM.initCategorySearch(search.text ?? "", searchId, widget.projectId );
    if(mF.Status ?? false){
//      FocusScope.of(context).requestFocus(FocusNode());
      parentList = mF.data;
      selectedIndex = -1;
    }else{
      selectedIndex = -1;
      final snackBar = SnackBar(content: Text(mF.Message));
      _scaffoldKey1.currentState.showSnackBar(snackBar);
    }
    setState(() {
      /* _d.Status = false;
          _d.is_loading = true;*/
    });
  }

/*  Future<SelectCategoryModel> getSubCategory(int parentId) async {
    SelectCategoryModel res_d;
    bool con = true;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if (con) {
      try {
        final res = await http.get(
            "${BASE_URL}category/singleDataByParent/$parentId",
            headers: Headers);
        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SelectCategoryModel.fromJson(j_data);

            if (res_d.data.length > 0) {
              childList.addAll(res_d.data);
              print(childList);
            }

            return res_d;
          default:
            return SelectCategoryModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SelectCategoryModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }*/

  int selectedIndex = -1;
  List<SelectCategoryModelData> parentList;

  bool isFirst = true;

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;

    if(widget.tabIndex == 2 && isFirst){
      searchId = 0;
      parentList = widget.categoryList.where((k) => k.parent_id == 0).toList();
      isFirst = false;
    }else if(widget.tabIndex == 7 && isFirst){
      searchId = widget.catId;
      parentList = widget.categoryList.where((k) => k.parent_id == widget.catId).toList();
      isFirst = false;
    }

    print(parentList);

    return Container(
      height: h*.70,
      width: double.infinity,
      child: ListView(

        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
            color: appColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Container(
                      margin: EdgeInsets.only(top: 8, bottom: 8),
                      width: w * .70,
                      height: 40,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 1.0,
                          ),
                        ],
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.white,
                      ),
                      padding: const EdgeInsets.only(left: 8, bottom: 1),
                      child: TextField(
                        controller: search,
                        decoration: InputDecoration(
                          hintText: 'Search by Category',
                          hintStyle: TextStyle(fontSize: 16),
                          filled: false,
                          border: InputBorder.none,
                          counterText: "",
                        ),
                        onSubmitted: (String _) {
                          _onSearch();
//                _initSearch(context);
                        },
                        obscureText: false,
                      ),
                    ),
                  ),
                  Container(
                    width: 50,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: SvgPicture.asset("assets/images/pop_close.svg",
                          height: 14,
                          fit: BoxFit.cover,
                          color: Colors.white,
                          semanticsLabel: 'popup close'),
                    ),
                  )
                 /* FlatButton(
                    child: Text("Done", style: TextStyle(color: Colors.white, fontFamily: 'SanSemiBold'),),
                    onPressed: () {
                      Navigator.of(context)
                          .pop({'approver_list': parentList});
                    },
                  )*/
                ],
              )),

          Container(
            height: h*.70 - 130,
            width: w,
            child: parentList.length > 0
                ? ListView.builder(
              padding: EdgeInsets.only(bottom: 24),
                  shrinkWrap: true,
                  itemCount: parentList?.length,
                  itemBuilder: (context, i) {
                    if(parentList[i].isSelected == null) {
                      parentList[i].isSelected = false;
                    }
                    return Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(parentList[i].category_name, style: TextStyle(fontFamily: 'SanMedium'),),
                          onTap: () {
                            selectedIndex = i;
                            if(!parentList[i].isSelected) {
                              for(int k=0; k < parentList.length; k++){
                                // for clearing other selection
                                parentList[k].isSelected = false;
                              }
                              parentList[i].isSelected = true;
                              setState(() {});
                            } else {
                              parentList[i].isSelected = false;
                              setState(() {});
                            }

                            Navigator.of(context)
                                .pop({'approver_list': parentList});
                          },
                          trailing: selectedIndex == i
                              ? Icon(
                            FontAwesomeIcons.check,
                            color: Colors.green,
                            size: 14,
                          )
                              : null,
                        ),
                      ],
                    );
                  },
                )
                : Center(
                    child: Text(widget.tabIndex == 7 ? "No Sub-Category Found" : "No Category Found.",
                        style: TextStyle(
                            fontSize: 18, fontFamily: 'SanRegular')),
                  ),
          ),
        ],
      ),
    );
  }
}




class DynamicSheet extends StatefulWidget {
  int tabIndex;

  List<ProjectModelData> projectList;
  List<SelectCountryModelData> countryList;
  List<SelectCityModelData> cityList;
  List<SelectCategoryModelData> categoryList;
  List<BehalfPeopleList> behalfList;
  List<ShootLocationData> shootDataList;

  DynamicSheet({
    Key key, this.cityList, this.projectList, this.countryList, this.tabIndex, this.categoryList, this.behalfList, this.shootDataList
  }) : super(key: key);

  @override
  _DynamicSheetState createState() => _DynamicSheetState();
}

class _DynamicSheetState extends State<DynamicSheet> {


  var proD;

  int selectedIndex = -1;

  String titleText(){
    if(widget.tabIndex==1){
      return "Select a Project";
    }else if(widget.tabIndex==2){
      return "Select a Category";
    }else if(widget.tabIndex==3){
      return "Select a Country";
    }else if(widget.tabIndex==4){
      return "Select a City";
    }else if(widget.tabIndex==5){
      return "Select the user";
    }else if(widget.tabIndex==10){
      return "Select location";
    }
  }

  int listLength = 0;
  bool isSelected = false;
  bool isFirst = true;

  List m = [];

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;

 if(isFirst){
   if(widget.tabIndex==1){
     listLength = widget.projectList.length;
     m = widget.projectList;
     proD = ProjectModelData();

   }else if(widget.tabIndex==2){
     listLength = widget.categoryList.length;
     m = widget.categoryList;
     proD = SelectCategoryModelData();
   }else if(widget.tabIndex==3){
     listLength = widget.countryList.length;
     m = widget.countryList;
     proD = SelectCountryModelData();
   }else if(widget.tabIndex==4){
     listLength = widget.cityList.length;
     m = widget.cityList;
     proD = SelectCityModelData();
   }else if(widget.tabIndex==5){
     listLength = widget.behalfList.length;
     m = widget.behalfList;
     proD = BehalfPeopleList();
   }else if(widget.tabIndex==10){
     listLength = widget.shootDataList.length;
     m = widget.shootDataList;
     proD = ShootLocationData();
   }
 }

    return SafeArea(
      bottom: true,
      child: Container(
        height: h*.70,
        width: double.infinity,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Container(
                color: appColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
                      child: Text(titleText(), style: TextStyle(color: Colors.white, fontFamily: 'SanMedium', fontSize: 16),),
                    ),
                    Container(
                      width: 50,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset("assets/images/pop_close.svg",
                            height: 14,
                            fit: BoxFit.cover,
                            color: Colors.white,
                            semanticsLabel: 'popup close'),
                      ),
                    )
                   /* FlatButton(
                      child: Text("Done", style: TextStyle(color: Colors.white, fontFamily: 'SanSemiBold'),),
                      onPressed: () {
                      },
                    )*/
                  ],
                )),

            Container(
              height: h*.70 - 130,
              width: w,
              child: m.length > 0
                  ? ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(bottom: 32),
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount: m?.length ?? 0,
                  itemBuilder: (context, i) {

                    if(m[i].isSelected == null) {
                      m[i].isSelected = false;
                    }

                    proD = m[i];

                    return Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(bodyTitle(proD), style: TextStyle(fontFamily: 'SanMedium'),),
                          onTap: () {
                            selectedIndex = i;
                            if(!m[i].isSelected) {
                              for(int k=0; k < m.length; k++){
                                // for clearing other selection
                                m[k].isSelected = false;
                              }
                              m[i].isSelected = true;
                              setState(() {});
                            } else {
                              m[i].isSelected = false;
                              setState(() {});
                            }

                            if(widget.tabIndex == 1){
                              Navigator.of(context).pop({'approver_list': m});
                            }else if(widget.tabIndex ==2){
                              Navigator.of(context).pop({'approver_list': m});
                            }else if(widget.tabIndex ==3){
                              Navigator.of(context).pop({'approver_list': m});
                            }else if(widget.tabIndex == 4){
                              Navigator.of(context).pop({'approver_list': m});
                            }else if(widget.tabIndex == 5){
                              Navigator.of(context).pop({'approver_list': m});
                            }else if(widget.tabIndex == 10){
                              Navigator.of(context).pop({'approver_list': m});
                            }
                          },
                          trailing: selectedIndex == i
                              ? Icon(
                            FontAwesomeIcons.check,
                            color: Colors.green,
                            size: 14,
                          )
                              : null,
                        ),
                      ],
                    );
                  })
                  : Center(
                child: Text("No Data available.",
                    style: TextStyle(
                        fontSize: 18, fontFamily: 'SanRegular')),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String bodyTitle(proD){

    if(widget.tabIndex==1){
      return proD.project_name;
    }else if(widget.tabIndex==2){
      return proD.category_name;
    }else if(widget.tabIndex==3){
      return proD.country_name;
    }else if(widget.tabIndex==4){
      return proD.city_name;
    }else if(widget.tabIndex==5){
      return proD.user_name;
    }else if(widget.tabIndex==10){
      return proD.location_name;
    }

  }
}