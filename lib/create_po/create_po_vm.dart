import 'dart:convert';

import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CreatePoVM {

  ConnectionHelper mCH;

  CreatePoVM() {
    mCH = ConnectionHelper.getInstance();
  }


  AppSingleton _singleton = AppSingleton();

  Future<SetFavouriteModel> setPO(String body, BuildContext context) async {
    SetFavouriteModel res_d;

   // bool con = true;

    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {

        final res = await http.post("${BASE_URL}purchase?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}", body:body, headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;

          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SetFavouriteModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }

  Future<SetFavouriteModel> setChallan(String body, BuildContext context) async {
    SetFavouriteModel res_d;


    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    if(con) {
      try {


        final res = await http.post("${BASE_URL}challan?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}", body: body, headers: Headers);
        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;

          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SetFavouriteModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }


  Future<SetFavouriteModel> setCashRequest(String body, BuildContext context) async {
    SetFavouriteModel res_d;

    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if(con) {
      try {

        final res = await http.post("${BASE_URL}cashmang?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}", body:body, headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;

          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      }catch(err) {
        return SetFavouriteModel.buildErr(0, message: "Something went wrong. Please try again later.");
      }
    }

  }

}