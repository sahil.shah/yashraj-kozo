// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:yashrajkozo/create_po/create_po_vm.dart';
// import 'package:yashrajkozo/credit_note/credit_amount_sheet.dart';
// import 'package:yashrajkozo/po_review/po_review_vm.dart';
// import 'package:yashrajkozo/utils/app_models.dart';
// import 'package:yashrajkozo/utils/check_connection.dart';
// import 'package:yashrajkozo/utils/constants.dart';
// import 'package:yashrajkozo/utils/custom_appbar.dart';
// import 'package:yashrajkozo/utils/custom_button.dart';
// import 'package:yashrajkozo/utils/custom_popup.dart';
// import 'package:yashrajkozo/utils/text_style.dart';
//
// import 'Home/home_api.dart';
// import 'createAllVM/create_all_vm.dart';
// import 'package:http/http.dart' as http;
//
// class CreditAmount extends StatefulWidget {
//   @override
//   _CreditAmountState createState() => _CreditAmountState();
// }
//
// class _CreditAmountState extends State<CreditAmount> {
//   final _scaffoldKeyCreateChallan = GlobalKey<ScaffoldState>();
//   double letfPad, topPad;
//   int selectedIndex = 0;
//   int tabIndex = 0;
//   int _filter1 = 0;
//   int _filter2 = 0;
//   int _filter3 = 0;
//   int sProjectIndex;
//   int sPOIndex;
//
//   int projectId = 0;
//   int poId = 0;
//   List<ProjectModelData> projectList = [];
//   final _projectC = TextEditingController();
//   final _poC = TextEditingController();
//   final vendorName = TextEditingController();
//   final invoiceNumber = TextEditingController();
//   final invoiceDate = TextEditingController();
//   final _paymentDueDate = TextEditingController();
//   final category = TextEditingController();
//   final subCategory = TextEditingController();
//   final country = TextEditingController();
//   final onbehalfof = TextEditingController();
//   final amount = TextEditingController();
//   final modeofpay = TextEditingController();
//   final priority = TextEditingController();
//   final gst = TextEditingController();
//   final totalAmount = TextEditingController();
//   final creditAmount = TextEditingController();
//   final _poDescriptionController = TextEditingController();
//
//   GetPoListChallanModel poData;
//   List<POListChallanData> poList = [];
//   List<String> poNameList = [];
//   CreateAllVM cVM;
//   CreatePoVM cPM;
//   List<String> pNameList = [];
//   GetProjectModel projectData;
//   HomeApi homeApiObj;
//   var obj = [];
//   GetReviewModel poReviewData;
//   PoReviewVM rVm;
//   ConnectionHelper mCH;
//   final _scaffoldKey = GlobalKey<ScaffoldState>();
//   final _scaffoldKey1 = GlobalKey<ScaffoldState>();
//   var mainPurchaseId;
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     rVm = PoReviewVM();
//     mCH = ConnectionHelper.getInstance();
//     homeApiObj = HomeApi();
//     // getCustomData();
//     cVM = CreateAllVM();
//     cPM = CreatePoVM();
//     _getAllApiData();
//   }
//
//   void _getAllApiData() async {
//     // get all project list
//     projectData = await homeApiObj.fetchProjectList(context);
//     if (projectData.Status ?? false) {
//       projectList = projectData.data;
//
//       for (int j = 0; j < projectList.length; j++) {
//         pNameList.add(projectList[j].project_name);
//       }
//
//       if (projectList.length == 1) {
//         _projectC.text = projectList[0].project_name;
//         projectId = projectList[0].project_id;
//        // _getPoListChallan();
//         getExpenseData();
//       }
//
//       _filter1 = 1;
//       setState(() {});
//     } else {
//       final snackBar = SnackBar(content: Text("Something went wrong!"));
//       _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     final w = MediaQuery.of(context).size.width;
//     final h = MediaQuery.of(context).size.height;
//
//     letfPad = 0.00944;
//     topPad = 0.00470;
//     return SafeArea(
//       bottom: true,
//       child: Scaffold(
//           key: _scaffoldKey,
//           backgroundColor: Colors.white,
//           appBar: CustomAppBar(
//             title: "Credit Note",
//             leading: InkWell(
//               onTap: () {
//                 Navigator.of(context).pop();
//               },
//               child: Icon(Icons.arrow_back, color: appColor,),
//             ),
//           ),
//           body: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               Flexible(
//                 flex: 90,
//                 child: ListView(
//                   physics: BouncingScrollPhysics(),
//                   shrinkWrap: true,
//                   children: <Widget>[
//                     Container(
//                       child: Column(
//                         //physics: BouncingScrollPhysics(),
//                         children: <Widget>[
//
//                           Container(
//                             padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
//                             child: Column(
//                               children: <Widget>[
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.058,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//                                       selectedIndex = 0;
//                                       tabIndex = 1;
//
//                                       _filter1 == 1
//                                           ? _openBottomSheetCategory(context)
//                                           : null;
//
//                                       if (_filter1 != 1) {
//                                         final snackBar = SnackBar(
//                                             content: Text(
//                                                 'Please wait loading data...'));
//                                         _scaffoldKeyCreateChallan.currentState
//                                             .showSnackBar(snackBar);
//                                       }
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Select Project",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           prefixIcon: Container(
//                                             margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
//                                             child: Icon(
//                                               FontAwesomeIcons.fileInvoice,
//                                               size: h*0.01881,
//                                             ),
//                                           ),
//                                           suffixIcon: Icon(
//                                             Icons.keyboard_arrow_down,
//                                             color: appColor,
//                                             size: h*0.028,
//                                           )),
//                                       style: Styles.formInputTextStyle,
//                                       controller: _projectC,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//                                       if (_projectC.text.isEmpty) {
//                                         final snackBar = SnackBar(
//                                             content: Text(
//                                                 'Please select Project first'));
//                                         _scaffoldKeyCreateChallan.currentState
//                                             .showSnackBar(snackBar);
//                                       } else {
//                                         selectedIndex = 0;
//                                         tabIndex = 2;
//
//                                         _filter2 == 1
//                                             ? _openBottomSheetCategory(context)
//                                             : null;
//
//                                         if (_filter2 != 1) {
//                                           final snackBar = SnackBar(
//                                               content: Text(
//                                                   'Please wait loading data...'));
//                                           _scaffoldKeyCreateChallan.currentState
//                                               .showSnackBar(snackBar);
//                                         }
//                                       }
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Select Expense",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           prefixIcon: Container(
//                                             margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
//                                             child: Icon(
//                                               FontAwesomeIcons.fileInvoice,
//                                               size: h*0.0188,
//                                             ),
//                                           ),
//                                           suffixIcon: Icon(
//                                             Icons.keyboard_arrow_down,
//                                             color: appColor,
//                                             size: h*0.028,
//                                           )),
//                                       style: Styles.formInputTextStyle,
//                                       controller: _poC,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Vendor",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: vendorName,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Invoice Number",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: invoiceNumber,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Invoice Date",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: invoiceDate,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Payment Due Date",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: _paymentDueDate,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Category",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: category,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Sub Category",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: subCategory,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Country",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: country,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "On Behalf Of",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: onbehalfof,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Amount",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: amount,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Mode of Payment",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: modeofpay,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Priority",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: priority,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "GST",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: gst,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: RaisedButton(
//                                     color: Color(0xffeeeeee),
//                                     padding: EdgeInsets.all(0.0),
//                                     onPressed: () {
//                                       FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//
//                                     },
//                                     child: TextField(
//                                       readOnly: true,
//                                       enabled: false,
//                                       decoration: new InputDecoration(
//                                           hintText: "Total Amount",
//                                           hintStyle:
//                                           TextStyle(fontFamily: 'SanRegular'),
//                                           border: InputBorder.none,
//                                           contentPadding: EdgeInsets.only(left: (w*letfPad*2))
//                                           ),
//                                       style: Styles.formInputTextStyle,
//                                       controller: totalAmount,
//                                       obscureText: false,
//                                     ),
//                                     shape: RoundedRectangleBorder(
//                                         borderRadius:
//                                         BorderRadius.all(Radius.circular(h*0.0094)),
//                                         side:
//                                         BorderSide(color: borderColor)),
//                                   ),
//                                 ),
//                                 SizedBox(
//                                   height: h*0.0182,
//                                 ),
//                                 Container(
//                                   height: h*0.0580,
//                                   child: TextField(
//                                     keyboardType: TextInputType.number,
//                                     decoration: new InputDecoration(
//                                         hintText: "Credit Amount",
//                                         hintStyle:
//                                         TextStyle(fontFamily: 'SanRegular'),
//
//                                         prefixIcon: Container(
//                                           margin: EdgeInsets.only(left: (w*letfPad*2), right: (w*letfPad*2)),
//                                           child: Icon(
//                                             FontAwesomeIcons.fileInvoice,
//                                             size: h*0.0188,
//                                           ),
//                                         ),
//                                         ),
//                                     style: Styles.formInputTextStyle,
//                                     controller: creditAmount,
//                                     obscureText: false,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                           SizedBox(height: h*0.0188,),
//                           CustomBtn(
//                             title: "submit",
//                             onTap: () async {
//
//                               bool con  = await mCH.checkConnection();
//
//                               if(con) {
//                                 if(_check_for_err()) {
//                                   _showVersionDialog(context, h, w);
//                                 }
//                               } else {
//                                 Navigator.pop(context);
//                                 final snackBar = SnackBar(
//                                     duration: Duration(seconds: 1),
//                                     content: Text("Please check your internet connection"));
//                                 _scaffoldKey.currentState.showSnackBar(snackBar);
//                               }
//                             },
//                           )
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//
//             ],
//           )),
//     );
//   }
//
//   _showVersionDialog(cotext, double h, double w) async {
//     await showDialog<String>(
//
//       context: context,
//       barrierDismissible: false,
//       builder: (BuildContext context) {
//         return StatefulBuilder(
//           key: _scaffoldKey1,
//           builder: (context, setState) {
//             return UpdatePopUp(context, setState, h, w);
//           },
//         );
//       },
//     );
//   }
//
//   bool _check_for_err() {
//     if (_projectC.text.isEmpty) {
//       final snackBar = SnackBar(content: Text('Please select a Project.'));
//       _scaffoldKey.currentState.showSnackBar(snackBar);
//       return false;
//     } else if (_poC.text.isEmpty) {
//       final snackBar = SnackBar(content: Text('Please select an Expense.'));
//       _scaffoldKey.currentState.showSnackBar(snackBar);
//       return false;
//     } else if (creditAmount.text.isEmpty) {
//       final snackBar = SnackBar(content: Text('Please enter some Credit Amount.'));
//       _scaffoldKey.currentState.showSnackBar(snackBar);
//       return false;
//     } else if (double.parse(creditAmount.text) > double.parse(amount.text)) {
//       final snackBar = SnackBar(content: Text('Credit Amount should be less than Total Amount'));
//       _scaffoldKey.currentState.showSnackBar(snackBar);
//       return false;
//     }
//
//     else {
//       return true;
//     }
//   }
//
//   Widget UpdatePopUp(BuildContext context, setState, double h, double w) {
//     final w = MediaQuery.of(context).size.width;
//     return Scaffold(
//       backgroundColor: Colors.transparent,
//       body: Center(
//           child: SingleChildScrollView(
//             scrollDirection: Axis.vertical,
//             child: Material(
//               color: Colors.white,
//               elevation: 5.0,
//               child: Container(
//                 width: w * .80,
// //            padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
// //            margin: EdgeInsets.only(left: 32,right: 32),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[
//                     Stack(
//                       alignment: Alignment.centerRight,
//                       children: <Widget>[
//                         Container(
//                           width: double.infinity,
//                           color: appColor,
//                           height: h*0.058,
//                           child: Center(
//                               child: Text(
//                                 "Accept Credit",
//                                 style: TextStyle(
//                                     fontSize: h*0.02117,
//                                     fontFamily: 'SanSemiBold',
//                                     color: Colors.white),
//                                 textAlign: TextAlign.center,
//                               )),
//                         ),
//                         Container(
//                           width: w*0.1180,
//                           child: FlatButton(
//                             onPressed: () {
//                               Navigator.pop(context);
//                             },
//                             child: SvgPicture.asset('assets/images/pop_close.svg',
//                                 height: h*0.016,
//                                 fit: BoxFit.cover,
//                                 color: Colors.white,
//                                 semanticsLabel: 'popup close'),
//                           ),
//                         )
//                       ],
//                     ),
//                     Container(
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         children: <Widget>[
//
//                           SizedBox(height: h*0.0094,),
//                          Padding(
//                             padding: EdgeInsets.only(left: letfPad*3*w, right: letfPad*3*w),
//                             child: new Card(
//                               elevation: 4,
//                               shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(h*0.01)),
//                               child: new Container(
//                                   padding: EdgeInsets.all(0),
//                                   margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
//                                   height: h*0.1176,
//                                   child: new Form(
// //                                  key: _formKeyScreen1,
//                                     child: TextField(
//                                       textInputAction: TextInputAction.go,
//                                       maxLines: 4,
//                                       controller: _poDescriptionController,
//                                       onEditingComplete: () {
//                                         FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//                                       },
//                                       onSubmitted: (l) {
//                                         FocusScopeNode currentFocus = FocusScope.of(context);
//
//             if (!currentFocus.hasPrimaryFocus) {
//               currentFocus.unfocus();
//             }
//                                       },
//                                       decoration: InputDecoration(
//                                         border: InputBorder.none,
//                                         hintText: 'Reason for accepting...',
//                                       ),
//                                     ),
//                                   )),
//                             ),
//                           ),
//
//                           SizedBox(
//                             height: h*0.0094,
//                           ),
//
//                           CustomBtn(
//                             title: "submit",
//                             onTap: () async {
//
//                               bool con  = await mCH.checkConnection();
//
//                               if(con) {
//                                 if(_poDescriptionController.text.isEmpty) {
//                                   Navigator.pop(context);
//                                   final snackBar = SnackBar(
//                                       duration: Duration(seconds: 1),
//                                       content: Text("Please enter reason"));
//                                   _scaffoldKey.currentState.showSnackBar(snackBar);
//                                 } else {
//                                   SharedPreferences prefs = await SharedPreferences.getInstance();
//                                   String accessToken = prefs.getString('access_token');
//
//                                   var Headers = {
//                                     "Content-type" : "application/json",
//                                     "Accept": "application/json",
//                                     "authorization": accessToken ?? ""
//                                   };
//
//                                   var body = json.encode({
//                                     "credit_reason": _poDescriptionController.text ?? "-",
//                                     "credit_amount" : creditAmount.text,
//                                     "purchase_id" :  mainPurchaseId
//                                   });
//
//                                   final res = await http.post("${BASE_URL}purchase/creditNote/$projectId?organization_id=${prefs.getInt('orgId')}", headers: Headers, body: body);
//
//                                   if(res.statusCode == 200 || res.statusCode == 201) {
//                                     Navigator.pop(context);
//                                     _bookEventDialog(context, "You have Accepted!");
//                                   } else {
//                                     Navigator.pop(context);
//                                     Navigator.pop(context);
//                                     final snackBar = SnackBar(content: Text("Something went wrong"));
//                                     _scaffoldKey.currentState.showSnackBar(snackBar);
//                                   }
//
//                                 }
//                               } else {
//                                 Navigator.pop(context);
//                                 final snackBar = SnackBar(
//                                     duration: Duration(seconds: 1),
//                                     content: Text("Please check your internet connection"));
//                                 _scaffoldKey.currentState.showSnackBar(snackBar);
//                               }
//                             },
//                           )
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           )),
//     );
//   }
//
//   void _bookEventDialog(BuildContext context, String message) {
//     showDialog<void>(
//       context: context,
//       barrierDismissible: true, // user must tap button!
//       builder: (BuildContext context) {
//         return CustomPopUp(
//           msg: message,
//         );
//       },
//     );
//   }
//
//   void _openBottomSheetCategory(BuildContext context) {
//     getBottomSheetCategory(context);
//   }
//
//   void getBottomSheetCategory(BuildContext context) async {
//     var results;
//
//     if (tabIndex == 1) {
//       results = await showModalBottomSheet(
//           context: context,
//           builder: (context) {
//             return CreditSheet(
//               projectList: projectList,
//               tabIndex: 1,
//             );
//           });
//     } else if (tabIndex == 2) {
//       results = await showModalBottomSheet(
//           context: context,
//           builder: (context) {
//             return CreditSheet(
//               dynamicList: obj,
//               tabIndex: 2,
//             );
//           });
//     }
//
//     if (results != null) {
//       if (results.containsKey('approver_list')) {
//         print(results['approver_list']);
//
//         if (tabIndex == 1) {
//           for (int j = 0; j < results['approver_list'].length; j++) {
//             if (results['approver_list'][j].isSelected) {
//               _projectC.text = results['approver_list'][j].project_name;
//               projectId = results['approver_list'][j].project_id;
//               results['approver_list'][j].isSelected = false;
//               poList = [];
//               poNameList.clear();
//               _filter2 = 0;
//               //_getPoListChallan();
//               getExpenseData();
//             }
//           }
//
//           _poC.clear();
//
//           setState(() {});
//         } else if (tabIndex == 2) {
//           for(int i=0; i<obj.length; i++) {
//             if(obj[i]['id'] == results['approver_list']) {
//               _poC.text = obj[i]['name'];
//               break;
//             }
//           }
//           mainPurchaseId = results['approver_list'];
//           setState(() {});
//           poReviewData = await rVm.getReviewData(results['approver_list'], context);
//           print(poReviewData);
//           category.text = poReviewData.data.parent_name;
//           subCategory.text = poReviewData.data.category_name;
//           vendorName.text = poReviewData.data.po_name;
//           _paymentDueDate.text = poReviewData.data.payment_due_date ?? "Payment Due Date";
//           invoiceDate.text = poReviewData.data.invoice_date ?? "Invoice Date";
//           invoiceNumber.text = poReviewData.data.invoice_number ?? "Invoice Number";
//           country.text = poReviewData.data.country_name;
//           onbehalfof.text = poReviewData.data.user_name;
//           priority.text = poReviewData.data.priority_name;
//           amount.text = "${poReviewData.data.total}";
//           modeofpay.text = poReviewData.data.payment_mode_name;
//           totalAmount.text = "${poReviewData.data.total_amount}";
//           gst.text = "${poReviewData.data.gst_amount}";
//           setState(() {});
//         }
//         setState(() {});
//       }
//     }
//   }
//
//   _getPoListChallan() async {
//     // get all PO list
//     poData = await cVM.getChallanPO(projectId);
//     if (poData.Status ?? false) {
//       poList = poData.data;
//       for (int j = 0; j < poList.length; j++) {
//         poNameList.add(poList[j].po_name);
//       }
//       _filter2 = 1;
//       setState(() {});
//     } else {
//       final snackBar = SnackBar(content: Text("Something went wrong!"));
//       _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
//     }
//   }
//
//   getExpenseData() async {
//     // get all Expense data
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String accessToken = prefs.getString('access_token');
//
//     var Headers = {
//       "Content-type" : "application/json",
//       "Accept": "application/json",
//       "authorization": accessToken ?? ""
//     };
//
//     final res = await http.get("${BASE_URL}purchase/creditNote/$projectId?organization_id=${prefs.getInt('orgId')}", headers: Headers);
//     if (res.statusCode == 200 || res.statusCode == 201) {
//       final j_data = json.decode(res.body);
//       for(int i=0; i<j_data['data'].length; i++) {
//         obj.add({
//             "name" : j_data['data'][i]['expense_name'],
//             "id" : j_data['data'][i]['purchase_id']
//           });
//       }
//       _filter2 = 1;
//       setState(() {});
//     } else {
//       final snackBar = SnackBar(content: Text("Something went wrong!"));
//       _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
//     }
//   }
// }
//
//
