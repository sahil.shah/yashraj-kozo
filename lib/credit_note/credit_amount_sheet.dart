import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yashrajkozo/po_review/po_review.dart';
import 'package:yashrajkozo/po_review/po_review_tablet.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';

class CreditSheet extends StatefulWidget {
  int tabIndex;

  List<ProjectModelData> projectList;
  List<POListChallanData> poList;
  var dynamicList = [];

  CreditSheet({Key key, this.poList, this.projectList, this.tabIndex, this.dynamicList})
      : super(key: key);
  @override
  _CreditSheetState createState() => _CreditSheetState();
}

class _CreditSheetState extends State<CreditSheet> {
  var proD;
  int selectedIndex = -1;

  String titleText() {
    if (widget.tabIndex == 1) {
      return "Select a project";
    } else if (widget.tabIndex == 2) {
      return "Select an expense";
    }
  }

  bool isCheck = false;

  bool isSelected = false;
  bool isFirst = true;

  List m = [];

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;

    print(m.length);

    if (isFirst) {
      if (widget.tabIndex == 1) {
        m = widget.projectList;
        proD = ProjectModelData();
      }
    }

    return Container(
      height: h * .70,
      width: double.infinity,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
              color: appColor,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(
                      titleText(),
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'SanMedium',
                          fontSize: 16),
                    ),
                  ),

                  widget.tabIndex == 1 ? Container(
                    width: 50,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: SvgPicture.asset("assets/images/pop_close.svg",
                          height: 14,
                          fit: BoxFit.cover,
                          color: Colors.white,
                          semanticsLabel: 'popup close'),
                    ),
                  ) : Container(
                    width: 50,
                    child: FlatButton(
                      onPressed: () {
                        //Navigator.pop(context);
                      },
                      child: Container()
                    ),
                  )

                  // FlatButton(
                  //   child: Text(
                  //     "Done",
                  //     style: TextStyle(
                  //         color: Colors.white, fontFamily: 'SanSemiBold'),
                  //   ),
                  //   onPressed: () {
                  //     Navigator.of(context).pop({'approver_list': m});
                  //     /* if (widget.tabIndex == 1) {
                  //
                  //     } else if (widget.tabIndex == 2) {
                  //
                  //     }*/
                  //   },
                  // )
                ],
              )),

          widget.tabIndex == 1 ? Container(
            height: h * .70 - 130,
            width: w,
            child: m.length > 0
                ? ListView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.fromLTRB(8,8,8,32),
                physics: AlwaysScrollableScrollPhysics(),

                itemCount: m?.length ?? 0,
                itemBuilder: (context, i) {
                  if (m[i].isSelected == null) {
                    m[i].isSelected = false;
                  }

                  proD = m[i];

                  return Column(
                    children: <Widget>[
                      ListTile(
                        title: Text(bodyTitle(proD) ?? '', style: TextStyle(fontFamily: 'SanMedium')),
                        onTap: () {
                          selectedIndex = i;
                          if (!m[i].isSelected) {
                            for (int k = 0; k < m.length; k++) {
                              // for clearing other selection
                              m[k].isSelected = false;
                            }
                            m[i].isSelected = true;
                            setState(() {});
                          } else {
                            m[i].isSelected = false;
                            setState(() {});
                          }

                          if(widget.tabIndex == 1){
                            Navigator.of(context).pop({'approver_list': m});
                          }
                          /*  if(widget.tabIndex == 1){
                            selectedProject.add(m[i]);
                          }else if(widget.tabIndex ==3){
                            selectedCountry.add(m[i]);
                          }else if(widget.tabIndex == 4){
                            selectedCity.add(m[i]);
                          }
                          setState(() {
                            selectedIndex = i;
                          });*/
                        },
                        trailing: widget.tabIndex == 2 ? (
                            IconButton(icon: Icon(FontAwesomeIcons.arrowAltCircleRight, color: appColor,), onPressed: (){
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  settings: RouteSettings(name: '/po_review'),
                                  pageBuilder: (c, a1, a2) => w < 600 ? PoReview(
                                    pId: m[i].purchase_id,
                                    isFromUserPending: false,
                                  ) : PoReviewTablet(
                                    pId: m[i].purchase_id,
                                    isFromUserPending: false,
                                  ),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(opacity: anim, child: child),
                                  transitionDuration: Duration(milliseconds: 500),
                                ),
                              );
                            })
//                              RaisedButton(child: Text("View"), onPressed: (){},)
                        ):
                        (selectedIndex == i
                            ? Icon(
                          FontAwesomeIcons.check,
                          color: Colors.green,
                          size: 14,
                        )
                            : null),
                        leading: widget.tabIndex == 2
                            ? Checkbox(
                            value:  selectedIndex == i ? true : false,
                            onChanged: (c) {
                              selectedIndex = i;
                              if (!m[i].isSelected) {
                                for (int k = 0; k < m.length; k++) {
                                  // for clearing other selection
                                  m[k].isSelected = false;
                                }
                                m[i].isSelected = true;
                              } else {
                                m[i].isSelected = false;
                              }
                              setState(() {
                                isCheck = !isCheck;
                              });
                            })
                            : null,
                        contentPadding: EdgeInsets.all(0.0),
                      ),
                    ],
                  );
                }) : Center(
              child: Text("No Data available.",
                  style: TextStyle(
                      fontSize: 18, fontFamily: 'SanRegular')),
            ),
          ) : widget.dynamicList.length > 0 ? ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.fromLTRB(8,8,8,32),
              physics: AlwaysScrollableScrollPhysics(),
              itemCount: widget.dynamicList?.length ?? 0,
              itemBuilder: (BuildContext con, int index){
                return Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(widget.dynamicList[index]['name'] ?? '', style: TextStyle(fontFamily: 'SanMedium')),
                      onTap: () {
                        selectedIndex = index;
                        setState(() {});
                        Navigator.of(context).pop({'approver_list': widget.dynamicList[index]['id']});
                        /*  if(widget.tabIndex == 1){
                            selectedProject.add(m[i]);
                          }else if(widget.tabIndex ==3){
                            selectedCountry.add(m[i]);
                          }else if(widget.tabIndex == 4){
                            selectedCity.add(m[i]);
                          }
                          setState(() {
                            selectedIndex = i;
                          });*/
                      },
                      trailing:
                      selectedIndex == index
                          ? Icon(
                        FontAwesomeIcons.check,
                        color: Colors.green,
                        size: 14,
                      ) : null,

                      contentPadding: EdgeInsets.all(0.0),
                    ),
                  ],
                );
              }) : Padding(
                padding: const EdgeInsets.only(top: 24),
                child: Center(
            child: Text("No Data available.",
                  style: TextStyle(
                      fontSize: 18, fontFamily: 'SanRegular')),
          ),
              ),
        ],
      ),
    );
  }

  String bodyTitle(proD) {
    if (widget.tabIndex == 1) {
      return proD.project_name;
    } else if (widget.tabIndex == 2) {
      return "Select an Expense";
    }
  }
}
