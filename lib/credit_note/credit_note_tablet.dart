import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:yashrajkozo/create_challan/challan_sheet.dart';
import 'package:yashrajkozo/credit_note/credit_amount_sheet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/createAllVM/create_all_vm.dart';
import 'package:yashrajkozo/create_po/create_po_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/custom_popup.dart';
import 'package:yashrajkozo/utils/text_style.dart';
import 'package:yashrajkozo/utils/walk.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class CreditNoteTabletPage extends StatefulWidget {
  String projectName;
  String projectID;
  bool isFromDetail;
  //GetCustomFieldModel customFieldModel;
  //bool customFieldDikha;

  CreditNoteTabletPage(
      {Key key,
        this.projectName,
        this.projectID,
        this.isFromDetail = false,
        // this.customFieldModel,
        //  this.customFieldDikha = false
      })
      : super(key: key);
  @override
  _CreditNoteTabletPageState createState() => _CreditNoteTabletPageState();
}

class _CreditNoteTabletPageState extends State<CreditNoteTabletPage> {
  AppSingleton _singleton = AppSingleton();
  final _titleC = TextEditingController();
  final _projectC = TextEditingController();
  final _poC = TextEditingController();
  final _amountC = TextEditingController();
  final _poDescriptionController = TextEditingController();
  final invoiceDate = TextEditingController();
  final gstAmount = TextEditingController();
  final invoiceNumber = TextEditingController();
  List<String> gstName = [];
  var gstObj = [];
  var gstData;

  List<File> multiImg;
  File imgCamera;

  ScrollController _scrollController;
  List<String> finalList = [];
  int selectedIndex = 0;
  int tabIndex = 0;

  String currencyS = "INR";
  String payS = "";
  String priS = "Normal";
  int sImageLength = 0;
  List<String> imgRes;

//  int _filterI = 0;
  int _filter1 = 0;
  int _filter2 = 0;
  int _filter3 = 0;
  int _filter9 = 0;
  int sProjectIndex;
  int sPOIndex;

  int projectId = 0;
  int poId = 0;

  List<String> paymentS = [
    "Cash",
    "Bank",
    "Card",
  ];

  List<String> priorityS = ["Low", "Normal", "High"];

  int prioI = -1;

  CreatePoVM cPM;
  int pIndex = -1;

  int count = 0;
  double letfPad, topPad;
  int mainGstId = 0;
  //For Custom Field
  List<TextEditingController> cftec;
  var obj = [];
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String gstValueName = "";
  double totalExpenseValue = 0.0;
  bool manualChange = true;

  void onGallary(BuildContext context, int i) async {
    if (i == 2) {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.custom,
        allowedExtensions: ['pdf', 'xlsx', 'xls'],
      );
    } else {
      multiImg = await FilePicker.getMultiFile(
        type: FileType.image,
      );
    }

    print(multiImg);

    if (multiImg != null) {
      sImageLength = 1;

      //sImageLength = 1;
      setState(() {});
      print(multiImg.length);
      uploadImage(context, 2);
    }
  }

/*  void onMultiGallary(BuildContext context) async {
     this.filesPaths = await FilePicker.getMultiFilePath(type: FileType.IMAGE);
    multiImage = await PhotoPicker.pickAsset(
      context: context,
      maxSelected: 100,
      provider: I18nProvider.english,
      checkBoxBuilderDelegate: DefaultCheckBoxBuilderDelegate(
        activeColor: Colors.white,
        unselectedColor: Colors.white,

      ),
      pickType: PickType.all,
    );
    if (multiImage != null) {

      uploadImage(context, 3);
    }
  }*/

  bool firstTime = false;

  void onCamera(BuildContext context, int i) async {
    if (i == 1) {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
    } else {
      imgCamera = await ImagePicker.pickImage(
        source: ImageSource.camera,
      );
    }
    if (imgCamera != null) {
      sImageLength = 1;
      setState(() {});
      uploadImage(context, 1);
    }
  }

  final _scaffoldKeyCreateChallan = GlobalKey<ScaffoldState>();

  ImageUploadRes imgres;
  String filePath = "";
  String fileName = "";
  int i = 0;

  uploadImage(BuildContext context, int j) async {
    _showLoading(context);

    var formData = new FormData();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // j=1 => Document, j=2 => Camera, j=3 => Gallery

    if (j == 2) {
      for (int i = 0; i < multiImg.length; i++) {
        filePath = multiImg[i].path;
        fileName = multiImg[i].path;

        List<String> res = fileName.split('.');

        if (res.last == 'mp4') {
          print(res);
          Navigator.pop(context);

          final snackBar = SnackBar(
              duration: Duration(seconds: 1),
              content: Text("Videos are not allowed."));
          _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);

          return false;
        } else {
//          formData = new FormData.fromMap({
//            'file[$i]': await MultipartFile.fromFile(filePath, filename: fileName)
//          });

          formData.files.addAll([
            MapEntry(
              "file[]",
              MultipartFile.fromFileSync(filePath ?? "",
                  filename: fileName ?? ""),
            ),
          ]);

//          formData.fields("file[$i]", MultipartFile.fromFile(filePath, filename: fileName));
//          formData.add("file[$i]", UploadFileInfo(File(filePath), fileName));
        }
      }
    } else {
      filePath = imgCamera.path;
      fileName = imgCamera.path;
      formData = new FormData.fromMap({
        'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
      });
//      formData.add("file[]", UploadFileInfo(File(filePath), fileName));
    }

    Dio dio = new Dio()..options.baseUrl = BASE_URL;
    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        if (!firstTime) {
          imgRes = imgres.data.image_path.split(',');
          firstTime = !firstTime;
        } else {
          List<String> temp = [];
          temp = imgres.data.image_path.split(',');
          setState(() {
            for (int i = 0; i < temp.length; i++) {
              imgRes.add(temp[i]);
            }
          });
        }
        print(imgRes);

        setState(() {});
      } else {
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(imgres.Message));
        _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Cannot load media."));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
    Navigator.pop(context);
  }

  CreateAllVM cVM;
  HomeApi homeApiObj;

  //for all project
  GetProjectModel projectData;
  List<ProjectModelData> projectList = [];
  List<String> pNameList = [];

  // for all category
  GetPoListChallanModel poData;
  List<POListChallanData> poList = [];
  List<String> poNameList = [];

  //for all currency
  SelectCurrencyModel currencyData;
  List<SelectCurrencyModelData> currencyList = [];
  List<String> currNameList = [];
  var mainPurchaseId;

  callProjectAPI() async {
    projectData = await homeApiObj.fetchProjectList(context);
    if (projectData.Status ?? false) {
      projectList = projectData.data;

      for (int j = 0; j < projectList.length; j++) {
        pNameList.add(projectList[j].project_name);
      }

      if (projectList.length == 1) {
        _projectC.text = projectList[0].project_name;
        projectId = projectList[0].project_id;
        //_getPoListChallan();
        getExpenseData();
      }

      _filter1 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  callCurrencyAPI() async {
    currencyData = await cVM.getAllCurrency();
    if (currencyData.Status ?? false) {
      currencyList = currencyData.data;

      for (int j = 0; j < currencyList.length; j++) {
        currNameList.add(currencyList[j].currency_code);
      }
      _filter3 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  callGSTAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    final res = await http.get("${BASE_URL}master?organization_id=${prefs.getInt('orgId')}", headers: Headers);
    if (res.statusCode == 200 || res.statusCode == 201) {
      final j_data = json.decode(res.body);
      gstData = j_data;
      print(gstData.runtimeType);
      for(int i=0; i<gstData['data']['gst'].length; i++) {
        gstName.add(gstData['data']['gst'][i]['gst_name']);

        gstObj.add({
          "name" : gstData['data']['gst'][i]['gst_name'],
          "id" : gstData['data']['gst'][i]['gst_id'],
          "percent" : double.parse("${gstData['data']['gst'][i]['gst_percentage']}")
        });

      }
      _filter9 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  void _getAllApiData() async {
    // get all project list
    callProjectAPI();
    callCurrencyAPI();
    callGSTAPI();

  }

  _getPoListChallan() async {
    // get all PO list
    poData = await cVM.getChallanPO(projectId);
    if (poData.Status ?? false) {
      poList = poData.data;
      for (int j = 0; j < poList.length; j++) {
        poNameList.add(poList[j].po_name);
      }
      _filter2 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  getExpenseData() async {
    // get all Expense data
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    final res = await http.get("${BASE_URL}purchase/creditNote/$projectId?organization_id=${prefs.getInt('orgId')}", headers: Headers);
    if (res.statusCode == 200 || res.statusCode == 201) {
      final j_data = json.decode(res.body);
      for(int i=0; i<j_data['data'].length; i++) {
        obj.add({
          "name" : j_data['data'][i]['expense_name'],
          "id" : j_data['data'][i]['purchase_id']
        });
      }
      _filter2 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
    }
  }

  int userId;

  _getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getInt('userId');

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getCustomData();
    gstAmount.text = "0";
    homeApiObj = HomeApi();
    cVM = CreateAllVM();
    cPM = CreatePoVM();
    setInitialData();
    _getUserData();
    _getAllApiData();
  }

//  getCustomData() {
//    if (widget.customFieldDikha) {
//      cftec = [];
//      if (widget.customFieldModel != null) {
//        for (int i = 0; i < widget.customFieldModel.data.length; i++) {
//          TextEditingController tec = new TextEditingController();
//          cftec.add(tec);
//        }
//      }
//    }
//  }

  setInitialData() {
    if (widget.isFromDetail) {
      _projectC.text = "${widget.projectName ?? ''}";
      projectId = int.parse("${widget.projectID}");
      _getPoListChallan();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _titleC.dispose();
//    _scrollController.dispose();
    _projectC.dispose();
    _amountC.dispose();
    _poDescriptionController.dispose();
    //  disposeCustomField();
    super.dispose();
  }

//  disposeCustomField() {
//    if (widget.customFieldDikha) {
//      for (int i = 0; i < cftec.length; i++) {
//        cftec[i].dispose();
//      }
//    }
//  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;

    letfPad = 0.00944;
    topPad = 0.00470;

    return SafeArea(
      bottom: true,
      child: Scaffold(
          key: _scaffoldKeyCreateChallan,
          backgroundColor: Colors.white,
          appBar: CustomAppBar(
            title: "Credit Note",

          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                flex: 90,
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      child: Column(
                        //physics: BouncingScrollPhysics(),
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB((letfPad*4*w), (topPad*4*h), (letfPad*4*w), 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    FocusScopeNode currentFocus = FocusScope.of(context);

                                    if (!currentFocus.hasPrimaryFocus) {
                                      currentFocus.unfocus();
                                    }
                                    if (imgRes != null && imgRes.length != 0) {
                                      navWalk();
                                    } else {
                                      _showmodalsheet(
                                        w,
                                        h,
                                        context,
                                      );
                                    }
                                  },
                                  child: Card(
                                    color: Color(0xffeeeeee),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(Radius.circular( h*0.0094)),
                                        side: BorderSide(color: borderColor)),
                                    elevation: 2,
                                    child: Container(
                                        width: w*0.28,
                                        height: h*0.11,
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.all(Radius.circular( h*0.0094)),
                                          child: (imgRes != null && imgRes.length > 0)
                                              ? Stack(
                                            alignment: Alignment.topRight,
                                            fit: StackFit.expand,
                                            children: <Widget>[
                                              FadeInImage(
                                                placeholder: AssetImage(
                                                    'assets/images/loading.gif'),
                                                image: NetworkImage(
                                                    (imgRes[0].contains('pdf') || imgRes[0].contains('xlsx') || imgRes[0].contains("xls")
                                                        ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
                                                        : imgRes[0].contains('png') || imgRes[0].contains('jpeg') || imgRes[0].contains("jpg") ? "${imgRes[0]}?token=${_singleton.tempToken}" : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW" ) ),
                                                fit: BoxFit.cover,
                                              ),
                                              /*  Image(
                                          image: NetworkImage(imgRes[0]),
                                          fit: BoxFit.cover,
                                        ),*/

                                              imgRes.length > 1
                                                  ? Container(
                                                padding: EdgeInsets.only(left: (letfPad*8*w), right: (letfPad*8*w), top: (topPad*8*h), bottom: (topPad*8*h)),
                                                child: SvgPicture.asset(
                                                    'assets/images/multiple-uploads.svg',
                                                    color: Color(0xffd0d3d8),
                                                    semanticsLabel:
                                                    'document logo'),
                                              )
                                                  : Container()
                                            ],
                                          )
                                              : Padding(
                                            padding: EdgeInsets.only(left: (letfPad*2*w), right: (letfPad*2*w), top: (topPad*2*h), bottom: (topPad*2*h)),
                                            child: SvgPicture.asset(
                                                'assets/images/file.svg',
                                                color: Color(0xffd0d3d8),
                                                semanticsLabel: 'document logo'),
                                          ),
                                        )),
                                  ),
                                ),
                                Container(
                                  height: h*0.1270,
                                  width: w * .60 - w*0.0377,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                          padding: EdgeInsets.only(left: (letfPad*2*w), top: (topPad*h), right: (letfPad*2*w), bottom: (topPad*h*2)),
                                          width: w * .60 - w*0.0377,
                                          height: h*0.06351,
                                          child: RaisedButton(
                                            color: appColor,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.all(Radius.circular( h*0.0094)),
                                                side: BorderSide(color: borderColor)),
                                            onPressed: () {
                                              FocusScopeNode currentFocus = FocusScope.of(context);

                                              if (!currentFocus.hasPrimaryFocus) {
                                                currentFocus.unfocus();
                                              }
                                              _showmodalsheet(
                                                w,
                                                h,
                                                context,
                                              );
                                            },
                                            child: FittedBox(
                                              fit: BoxFit.fitWidth,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Icon(
                                                    CupertinoIcons.share_up,
                                                    color: Colors.white,
                                                    size: h*0.01881,
                                                  ),
                                                  SizedBox(
                                                    width: w*0.01416,
                                                  ),
                                                  Text(
                                                    "Upload from Gallery",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: h*0.014,
                                                        fontFamily: 'SanMedium'),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )),
                                      Container(
                                          padding: EdgeInsets.only(left: letfPad*2*w, bottom: topPad*2*h, right: letfPad*2*w, top: topPad*h),
                                          width: w * .60 - w*0.0377,
                                          height: h*0.06,
                                          child: RaisedButton(
                                            color: appColor,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.all(Radius.circular( h*0.0094)),
                                                side: BorderSide(color: borderColor)),
                                            onPressed: () {
                                              FocusScopeNode currentFocus = FocusScope.of(context);

                                              if (!currentFocus.hasPrimaryFocus) {
                                                currentFocus.unfocus();
                                              }
                                              onCamera(context, 2);
                                            },
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Icon(
                                                  Icons.camera_alt_outlined,
                                                  size: h*0.01881,
                                                  color: Colors.white,
                                                ),
                                                SizedBox(
                                                  width: w*0.01416,
                                                ),
                                                Text(
                                                  "Use the Camera",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: h*0.014,
                                                      fontFamily: 'SanMedium'),
                                                )
                                              ],
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.028,
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB((letfPad*4*w), 0, (letfPad*4*w), 0),
                            child: Column(
                              children: <Widget>[

                                Container(
                                  height: h*0.05,
                                  child: RaisedButton(
                                    color: lightGrey,
                                    padding: EdgeInsets.only(left: 8.0),
                                    onPressed: () {
                                      FocusScope.of(context).requestFocus(FocusNode());
                                      selectedIndex = 0;
                                      tabIndex = 1;
                                      _filter1 == 1
                                          ? _openBottomSheetCategory(context)
                                          : null;

                                      if (_filter1 != 1) {
                                        final snackBar = SnackBar(
                                            duration: Duration(seconds: 1),
                                            content: Text('Please wait loading data...'));
                                        _scaffoldKey.currentState.showSnackBar(snackBar);
                                      }
                                    },
                                    child: TextField(
                                      readOnly: true,
                                      decoration: new InputDecoration(
                                          hintText: "Select Project",
                                          hintStyle: TextStyle(fontFamily: 'SanRegular'),
                                          border: InputBorder.none,
                                          enabled: false,
                                          suffixIcon: Icon(
                                            Icons.keyboard_arrow_down,
                                            color: Colors.black,
                                            size: h*0.035,
                                          )),
                                      style: Styles.formInputTextStyle,
                                      controller: _projectC,
                                      obscureText: false,
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(h*0.0094)),
                                        side: BorderSide(color: borderColor)),
                                  ),
                                ),
                                SizedBox(
                                  height: h*0.0182,
                                ),
                                Container(
                                  height: h*0.05,
                                  child: RaisedButton(
                                    color: lightGrey,
                                    padding: EdgeInsets.only(left: 8.0),
                                    onPressed: () {
                                      FocusScopeNode currentFocus = FocusScope.of(context);

                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }
                                      if (_projectC.text.isEmpty) {
                                        final snackBar = SnackBar(
                                            duration: Duration(seconds: 1),
                                            content: Text(
                                                'Please select Project first'));
                                        _scaffoldKeyCreateChallan.currentState
                                            .showSnackBar(snackBar);
                                      } else {
                                        selectedIndex = 0;
                                        tabIndex = 2;

                                        _filter2 == 1
                                            ? _openBottomSheetCategory(context)
                                            : null;

                                        if (_filter2 != 1) {
                                          final snackBar = SnackBar(
                                              duration: Duration(seconds: 1),
                                              content: Text(
                                                  'Please wait loading data...'));
                                          _scaffoldKeyCreateChallan.currentState
                                              .showSnackBar(snackBar);
                                        }
                                      }
                                    },
                                    child: TextField(
                                      readOnly: true,
                                      enabled: false,
                                      decoration: new InputDecoration(
                                          hintText: "Select Expense",
                                          hintStyle:
                                          TextStyle(fontFamily: 'SanRegular'),
                                          border: InputBorder.none,
                                          suffixIcon: Icon(
                                            Icons.keyboard_arrow_down,
                                            color: Colors.black,
                                            size: h*0.035,
                                          )),
                                      style: Styles.formInputTextStyle,
                                      controller: _poC,
                                      obscureText: false,
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.all(Radius.circular(h*0.0094)),
                                        side:
                                        BorderSide(color: borderColor)),
                                  ),
                                ),

                                SizedBox(
                                  height: h*0.018,
                                ),

                                Container(
                                  width: w,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 45,
                                        child: Container(
                                          height: h*0.05,
                                          color: lightGrey,
                                          padding: EdgeInsets.only(left: 8.0),
                                          child: TextField(
                                            controller: invoiceNumber,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'Enter Invoice Number',
                                              hintStyle: TextStyle(fontFamily: 'SanRegular'),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 5,
                                        child: Container(),
                                      ),
                                      Expanded(
                                        flex: 45,
                                        child: Container(
                                        height: h*0.05,
                                        color: lightGrey,
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: TextField(
                                          onTap: () {
                                            _selectDateInvoiceDate();
                                          },
                                          readOnly: true,
                                          controller: invoiceDate,
                                          decoration: InputDecoration(
                                            hintText: 'Enter Invoice Date',
                                            border: InputBorder.none,
                                            hintStyle: TextStyle(fontFamily: 'SanRegular'),

                                          ),
                                        ),
                                      ),)
                                    ],
                                  ),
                                ),


                                SizedBox(
                                  height: h*0.018,
                                ),


                                // Container(
                                //   height: h*0.05,
                                //   child: TextField(
                                //     onTap: () {
                                //       _selectDateInvoiceDate();
                                //     },
                                //     readOnly: true,
                                //     controller: invoiceDate,
                                //     decoration: InputDecoration(
                                //       hintText: 'Enter Invoice Date',
                                //       hintStyle: TextStyle(fontFamily: 'SanRegular'),
                                //       prefixIcon: Container(
                                //         height: h*0.02,
                                //         child: IconButton(
                                //           padding: EdgeInsets.all(0),
                                //           icon: SvgPicture.asset('assets/images/birthday.svg',
                                //               fit: BoxFit.cover,
                                //               width:  h*0.0211,
                                //               height: h*0.02,
                                //               semanticsLabel: 'popup close'),
                                //           onPressed: null,
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),

                                Container(
                                  height: h*0.075,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 42,
                                        child: PopupMenuButton<String>(
                                          itemBuilder: (BuildContext context) {
                                            return currNameList.map((String s) {
                                              return PopupMenuItem<String>(
                                                value: s,
                                                child: Text(s),
                                              );
                                            }).toList();
                                          },
                                          onSelected: (j) {
                                            FocusScope.of(context).requestFocus(FocusNode());
                                            currencyS = j;
                                            setState(() {});
                                          },
                                          child: Container(
                                              padding: EdgeInsets.only(left: letfPad*1*w, ),
                                              width: (w) / 3 - letfPad*2*w,
                                              height: h*0.05,
                                              decoration: BoxDecoration(
                                                  color: lightGrey,
                                                  borderRadius:
                                                  BorderRadius.all(Radius.circular(letfPad*w))),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  RichText(
                                                      textAlign: TextAlign.start,
                                                      text: TextSpan(
                                                        children: [
                                                          TextSpan(
                                                              text: 'Currency: ',
                                                              style: TextStyle(
                                                                  fontFamily: 'SanRegular',
                                                                  color: Colors.black,
                                                                  fontSize: h*0.016)),
                                                          TextSpan(
                                                              text: currencyS ?? '',
                                                              style: TextStyle(
                                                                  fontFamily: 'SanSemiBold',
                                                                  color: Colors.black,
                                                                  fontSize: h*0.016)),
                                                        ],
                                                      )),
                                                  Icon(Icons.arrow_drop_down, size: h*0.03,)
                                                ],
                                              )),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(),
                                      ),
                                      Expanded(
                                        flex: 54,
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                height: h*0.05,
                                                color: lightGrey,
                                                padding: EdgeInsets.only(left: 8.0),
                                                child: TextField(
                                                  decoration: new InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText: "Enter Expense Amount",
                                                    hintStyle: TextStyle(
                                                        fontFamily: 'SanRegular'),
                                                  ),
                                                  inputFormatters: [
                                                    new BlacklistingTextInputFormatter(
                                                        new RegExp('[\\,|\\-|\\.|\\ ]')),
                                                  ],
                                                  style: Styles.formInputTextStyle,
                                                  controller: _amountC ?? 0,
                                                  keyboardType: TextInputType.number,
                                                  obscureText: false,
                                                ),
                                              ),
                                              Text("Taxes Excluded", style: TextStyle(
                                                  fontSize: h*0.01, fontFamily: 'SanRegular'),)
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: h*0.018,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 42,
                                      child: PopupMenuButton(
                                        itemBuilder: (BuildContext context) {
                                          return gstName.map((String s) {
                                            return PopupMenuItem<String>(
                                              value: s,
                                              child: Text("$s"),
                                            );
                                          }).toList();
                                        },

                                        onSelected: (j) {
                                          FocusScope.of(context).requestFocus(FocusNode());

                                          gstAmount.text="";
                                          if(_amountC.text == "") {
                                            final snackBar = SnackBar(
                                                content: Text("Please enter amount"));
                                            _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
                                          } else {
                                            totalExpenseValue = double.parse(_amountC.text);
                                            gstValueName = j;
                                            double val = double.parse(_amountC.text);

                                            for(int i=0;i<gstObj.length; i++) {
                                              if(gstValueName == gstObj[i]['name']) {
                                                print(gstObj[i]['percent']);
                                                mainGstId =  gstObj[i]['id'];
                                                if (gstObj[i]['percent'] == 0.0 || gstObj[i]['percent']==0) {
                                                  manualChange = false;
                                                  break;
                                                } else {
                                                  totalExpenseValue = double.parse(_amountC.text) + (gstObj[i]['percent'] / 100 * val);
                                                  double gstValue = (gstObj[i]['percent'] / 100 * val);
                                                  gstAmount.text = gstValue.toStringAsFixed(2).toString();
                                                  manualChange = false;
                                                  break;
                                                }
                                              }
                                            }
                                            setState(() {});
                                          }
                                        },
                                        child: Container(
                                            padding: EdgeInsets.only(right: letfPad*w, left: letfPad*w),
                                            height: h*0.05,
                                            decoration: BoxDecoration(
                                                color: lightGrey,
                                                borderRadius:
                                                BorderRadius.all(Radius.circular(h*0.0094))),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                RichText(
                                                    textAlign: TextAlign.start,
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    text: TextSpan(
                                                      children: [
                                                        TextSpan(
                                                            text: 'Tax Type : ',
                                                            style: TextStyle(
                                                                fontFamily: 'SanRegular',
                                                                color: Colors.black,
                                                                fontSize: h*0.014)),
                                                        TextSpan(
                                                            text: gstValueName,
                                                            style: TextStyle(
                                                                fontFamily: 'SanSemiBold',
                                                                color: Colors.black,
                                                                fontSize: h*0.012)),
                                                      ],
                                                    )),
                                                Icon(Icons.arrow_drop_down)
                                              ],
                                            )),
                                      ),
                                    ),

                                    Expanded(
                                      flex: 4,
                                      child: Container(),
                                    ),

                                    Expanded(
                                      flex: 54,
                                      child: Container(
                                        color: lightGrey,
                                        padding: EdgeInsets.only(left: 8.0),
                                        height: h*0.05,
                                        child: TextField(
                                          readOnly: manualChange,
                                          onChanged: (text) {
                                            totalExpenseValue = double.parse(_amountC.text) + double.parse(gstAmount.text);
                                            setState(() {});
                                          },
                                          decoration: new InputDecoration(
                                            border: InputBorder.none,
                                            hintText: 'Enter Tax Value'
                                          ),
                                          inputFormatters: [
                                            new BlacklistingTextInputFormatter(
                                                new RegExp('[\\,|\\-|\\ ]')),
                                          ],
                                          style: Styles.formInputTextStyle,
                                          controller: gstAmount,
                                          keyboardType: TextInputType.number,
                                          obscureText: false,
                                          onSubmitted: (text) {
                                            totalExpenseValue = double.parse(_amountC.text) + double.parse(gstAmount.text);
                                            setState(() {});
                                          },
                                        ),
                                      ),
                                    ),

                                  ],
                                ),
                                SizedBox(height: h*0.018,),
                                Container(
                                  width: w,
                                  child: Text("Total Amount", style: TextStyle(fontSize: h*0.021, color: appColor),),
                                ),
                                SizedBox(height: 8.0,),
                                Container(
                                    width: w,
                                    child: Text("${totalExpenseValue.toStringAsFixed(2) ?? 0}", style: TextStyle(fontSize: h*0.021, color: appColor, fontWeight: FontWeight.bold), maxLines: 1,)),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: h*0.028,
                          ),

                          Padding(
                            padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                            child: new Card(
                              color: lightGrey,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(h*0.01)),
                              child: new Container(
                                  padding: EdgeInsets.all(0),
                                  margin: EdgeInsets.only(left: (w*letfPad*4), right: (w*letfPad*4)),
                                  height: h*0.117,
                                  child: TextField(
                                    textInputAction: TextInputAction.go,
                                    maxLines: 4,
                                    controller: _poDescriptionController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Description...',
                                        hintStyle: TextStyle(fontFamily: 'SanRegular')),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: h*0.0423,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Flexible(
                flex: 10,
                child: CustomBtn(
                  title: "Submit",
                  onTap: () async {
                    ConnectionHelper mCH;
                    mCH = ConnectionHelper.getInstance();

                    bool con = await mCH.checkConnection();

                    if (con) {
                      if (_check_for_err()) {
                        String fImage = imgRes.join(',');
                        var body = json.encode({
                          "gst_id" : mainGstId,
                          "gst_amount" : gstAmount.text,
                          "credit_image": fImage ?? '',
                          "total_amount" : totalExpenseValue,
                          "credit_invoice_no" : invoiceNumber.text,
                          "credit_date" : invoiceDate.text,
                          "currency_id" : currencyS,
                          "credit_reason" : _poDescriptionController.text,
                          "credit_amount" : _amountC.text,
                          "purchase_id" : mainPurchaseId,

                        });

                        print(body);

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        String accessToken = prefs.getString('access_token');

                        var Headers = {
                          "Content-type" : "application/json",
                          "Accept": "application/json",
                          "authorization": accessToken ?? ""
                        };

                        final res = await http.post("${BASE_URL}creditNote?organization_id=${prefs.getInt('orgId')}", headers: Headers, body: body);

                        if(res.statusCode == 200 || res.statusCode == 201) {
                          _bookEventDialog(context, "Your Credit Note has been submitted");
                        } else {
                          final snackBar = SnackBar(
                              duration: Duration(seconds: 1),
                              content: Text("Something went wrong"));
                          _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
                        }

                        //submitExpense();
                      }
                    } else {
                      final snackBar = SnackBar(
                          duration: Duration(seconds: 1),
                          content:
                          Text("Please check your internet connection"));
                      _scaffoldKeyCreateChallan.currentState
                          .showSnackBar(snackBar);
                    }
                  },
                ),
              ),
            ],
          )),
    );
  }

  void _bookEventDialog(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CustomPopUp(
          msg: message,
        );
      },
    );
  }

  void _showmodalsheet(double w, double h, BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(h*0.03), topRight: Radius.circular(h*0.03)),
        ),
        context: context,
        builder: (buider) {
          return getBottomSheet(w, h, context);
        });
  }

  bool _check_for_err() {
    if (imgRes == null) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select an Expense image.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_projectC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select Project'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poC.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please select Expense'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (invoiceNumber.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Invoice Number.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (invoiceDate.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Invoice Date.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (gstAmount.text.isEmpty) {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter GST Amount'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    }  else if (_amountC.text.isEmpty) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Expense amount.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    } else if (_poDescriptionController.text.isEmpty) {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please write Expense Description.'));
      _scaffoldKeyCreateChallan.currentState.showSnackBar(snackBar);
      return false;
    }
//    else if (widget.customFieldDikha) {
//      for(int i=0; i<cftec.length;i++) {
//        if(cftec[i].text.isEmpty) {
//          final snackBar = SnackBar(content: Text('Please fill all the fields.'));
//          _scaffoldKey.currentState.showSnackBar(snackBar);
//          return false;
//        }
//      }
//      return true;
//    }
    else {
      return true;
    }
  }

  Widget getBottomSheet(double w, double h, BuildContext context) {
    return Container(
        height: h*0.1411,
        width: double.infinity,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 2);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/document.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'document logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Document")
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                onGallary(context, 1);
                //onCamera(context, 1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SvgPicture.asset('assets/images/gallery.svg',
                      fit: BoxFit.cover,
                      height: h*0.0541,
                      width: h*0.0541,
                      semanticsLabel: 'gallery logo'),
                  SizedBox(
                    height: h*0.00470,
                  ),
                  Text("Image")
                ],
              ),
            ),
          ],
        ));
  }

  void _openBottomSheetCategory(BuildContext context) {
    getBottomSheetCategory(context);
  }

  void getBottomSheetCategory(BuildContext context) async {
    var results;

    if (tabIndex == 1) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return ChallanSheet(
              projectList: projectList,
              tabIndex: 1,
            );
          });
    } else if (tabIndex == 2) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return CreditSheet(
              dynamicList: obj,
              tabIndex: 2,
            );
          });
    }

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);

        if (tabIndex == 1) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _projectC.text = results['approver_list'][j].project_name;
              projectId = results['approver_list'][j].project_id;
              results['approver_list'][j].isSelected = false;
              poList = [];
              poNameList.clear();
              _filter2 = 0;
              // _getPoListChallan();
              getExpenseData();
            }
          }

          _poC.clear();

          setState(() {});
        } else if (tabIndex == 2) {

          for(int i=0; i<obj.length; i++) {
            if(obj[i]['id'] == results['approver_list']) {
              _poC.text = obj[i]['name'];
              break;
            }
          }
          mainPurchaseId = results['approver_list'];
        }
        setState(() {});
      }
    }
  }
  Future _selectDateInvoiceDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(DateTime.now().year - 2),
      lastDate: new DateTime(DateTime.now().year + 2),
    );


    String formattedDate = DateFormat('dd-MM-yyyy – kk:mm').format(picked);

    print(formattedDate);
    String date;

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        invoiceDate.text = date;
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }
  void navWalk() async {
    var results = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => CarouselDemo(
              imgUrl: imgRes,
              fromPage: 3,
            )));

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);
        imgRes = results['approver_list'];
        setState(() {});
      }
    }
  }
  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }
}


