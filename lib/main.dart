import 'dart:async';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/splash/splash.dart';
import 'package:flutter/services.dart';

void main() {
//  Crashlytics.instance.enableInDevMode = true;
//
//  // Pass all uncaught errors to Crashlytics.
//  FlutterError.onError = Crashlytics.instance.recordFlutterError;
//
//  runZoned(() {
//
//  }, onError: Crashlytics.instance.recordError);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Map<int, Color> dColor = {
    50: Color.fromRGBO(237, 28, 36, 1),
    100: Color.fromRGBO(237, 28, 36, 1),
    200: Color.fromRGBO(237, 28, 36, 1),
    300: Color.fromRGBO(237, 28, 36, 1),
    400: Color.fromRGBO(237, 28, 36, 1),
    500: Color.fromRGBO(237, 28, 36, 1),
    600: Color.fromRGBO(237, 28, 36, 1),
    700: Color.fromRGBO(237, 28, 36, 1),
    800: Color.fromRGBO(237, 28, 36, 1),
    900: Color.fromRGBO(237, 28, 36, 1),
  };

  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xFFED1C24, dColor);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Yash Raj Films',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: colorCustom, cursorColor: appColor),
      home: Splash(),
    );
  }
}