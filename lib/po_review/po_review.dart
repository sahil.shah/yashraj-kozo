import 'dart:convert';
import 'dart:io';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:intl/intl.dart';
import 'package:yashrajkozo/Home/home_api.dart';
import 'package:yashrajkozo/createAllVM/create_all_vm.dart';
import 'package:yashrajkozo/create_po/category_sheet.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:yashrajkozo/utils/autoselect.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:yashrajkozo/po_review/po_review_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:yashrajkozo/utils/custom_button.dart';
import 'package:yashrajkozo/utils/custom_popup.dart';
import 'package:yashrajkozo/utils/text_style.dart';
import 'package:yashrajkozo/utils/err.dart';
import 'package:yashrajkozo/utils/loader.dart';
import 'package:yashrajkozo/utils/walk.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui' as ui;
const directoryName = 'Signature';
/*
* normal user
user_lvl  != 0
map_active =1
mod =0

moderator
user_lvl  == 0
map_active =4
mod =1

admin manager
user_lvl  == 0
map_active =6
mod =2

admin
user_lvl  == 0
mod =0
map_active =1 */

class PoReview extends StatefulWidget {
  int pId;
  int mId;
  String approverId;
  int pMapId;
  bool isFromUserPending = false;
  bool isFromUser = false;

  PoReview(
      {this.pId,
      this.mId,
      this.approverId,
      this.pMapId,
      this.isFromUserPending = false,
      this.isFromUser = false});

  //mId = moderatorId
  //pId = purchase ID

  // approverId = 0 is for Expense.

  //i == 3 is for accepting over budget PO/Expenses.

  @override
  _PoReviewState createState() => _PoReviewState();
}

class _PoReviewState extends State<PoReview> {
  final _userC = TextEditingController();
  final _projC = TextEditingController();
  final _vchC = TextEditingController();
  final _shootLocation = TextEditingController();
  final _cateC = TextEditingController();
  final _descC = TextEditingController();
  final _counC = TextEditingController();
  final _cityC = TextEditingController();
  final _payC = TextEditingController();
  final _prioC = TextEditingController();
  final _subCatC = TextEditingController();
  final _expeC = TextEditingController();
  final _totalAmount = TextEditingController();
  final _balanC = TextEditingController();
  final invoiceNumber = TextEditingController();
  final invoiceDate = TextEditingController();
  final paymentDueDate = TextEditingController();
  final gstAmount = TextEditingController();

  AppSingleton _singleton = AppSingleton();

  Future<GetReviewModel> mF;
  PoReviewVM rVm;

  List<String> imgUrl = [];

  bool isModerator = false;
  bool isLvl1Edit = false;

  bool editOn = false;
//  bool adminEditOn = false;

  int _filter1 = 0;
  int _filter2 = 0;
  int _filter3 = 0;
  int _filter4 = 0;
  int _filter7 = 0;
  int _filter9 = 0;
  int _filter10 = 0;
  int tabIndex = 0;

  int userId;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _scaffoldKey1 = GlobalKey<ScaffoldState>();
  final signatureKey = GlobalKey();

  //for all project
  GetProjectModel projectData;
  List<ProjectModelData> projectList = [];
  List<String> pNameList = [];

  // for all category
  SelectCategoryModel categoryData;
  List<SelectCategoryModelData> categoryList = [];
  List<String> cNameList = [];

  //for all country
  SelectCountryModel countryData;
  List<SelectCountryModelData> countryList = [];
  List<String> countNameList = [];

  //shoot location
  SelectShootlocationModelData shootLocationData;
  List<ShootLocationData> shootLocationList = [];

  //for all city
  SelectCityModel cityData;
  List<SelectCityModelData> cityList = [];
  List<String> cityNameList = [];

  //for all approver
  SelectApproverModel approverData;
  List<SelectApproverModelData> approverList = [];
  List<String> approNameList = [];

  //for all currency
  SelectCurrencyModel currencyData;
  List<SelectCurrencyModelData> currencyList = [];
  List<String> currNameList = [];

  int projectId = 0;
  int catId = 0;
  int location_id = 0;
  int countryId = 0;
  int cityId = 0;
  int subCatId = 0;

  CreateAllVM cVM;
  HomeApi homeApiObj;

  bool isBudgetFirstTime = true;

  bool showBudgetReason = false;

  List<GetReviewTreeModelData> topList = [];
  List<GetReviewTreeModelData> bottomList = [];

  ImageUploadRes imgres;
  ConnectionHelper mCH;
  
  double h,w;
  double letfPad, topPad;
  bool callRendorBox = true;
  Color signatureColor = Colors.black;
  double signStrokeWidth = 2.0;
  DateTime mainDate;

  var gstObj = [];
  List<String> payModeData = [];
  List<int> paymentIdList = [];
  List<String> priorityData = [];
  List<int> priorityId = [];
  int mainPayId = 0;
  int mainPriorityId = 0;

  var gstData;
  List<double> gstPercentage = [];
  List<String> gstName = [];
  String gstValueName = "";
  double totalExpenseValue = 0.0;
  bool manualChange = true;
  int mainGstId = 0;

  List<Offset> _points =  <Offset>[];
  final GlobalKey paintKey = GlobalKey();
  Size containerSize;
  Offset containerOffset;
  String SignatureImage = "";
  Future<ui.Image> get rendered {
    // [CustomPainter] has its own @canvas to pass our
    // [ui.PictureRecorder] object must be passed to [Canvas]#contructor
    // to capture the Image. This way we can pass @recorder to [Canvas]#contructor
    // using @painter[SignaturePainter] we can call [SignaturePainter]#paint
    // with the our newly created @canvas
    ui.PictureRecorder recorder = ui.PictureRecorder();
    Canvas canvas = Canvas(recorder,);
    DrawingPaint painter = DrawingPaint(points: _points, totalWidth: w*0.8, totalHeight: h*0.175, signColor: signatureColor, signStrokeWidth: signStrokeWidth);
    var size = Size(w*0.8, h*0.175);
    painter.paint(canvas, size);
    return recorder.endRecording()
        .toImage(size.width.floor(), size.height.floor());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mCH = ConnectionHelper.getInstance();
    getSignPadConfig();
    getReviewTree();
    rVm = PoReviewVM();
    homeApiObj = HomeApi();
    cVM = CreateAllVM();

    mF = rVm.getReviewData(widget.pId, context);
    _getAllApiData();
  }

  getSignPadConfig() {
    signStrokeWidth = _singleton.signStrokeWidth;
    signatureColor = _singleton.signColor;
  }

  // getPosition() {
  //   RenderBox paintBox = signatureKey.currentContext.findRenderObject();
  //   containerSize = paintBox.size;
  //   containerOffset = paintBox.localToGlobal(Offset(0.0, 0.0));
  // }

  @override
  void dispose() {
    // TODO: implement dispose
    _userC.dispose();
    _projC.dispose();
    _cateC.dispose();
    _descC.dispose();
    _counC.dispose();
    _subCatC.dispose();
    _cityC.dispose();
    _payC.dispose();
    _vchC.dispose();
    _balanC.dispose();
    _prioC.dispose();
    _expeC.dispose();
    _totalAmount.dispose();
    invoiceDate.dispose();
    invoiceNumber.dispose();
    super.dispose();
  }

  _getModeratorData(GetReviewModel d) async {

    print("pmap = ${widget.pMapId}");
    print("pId = ${widget.pId}");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = prefs.getInt('userId');

    d.data.moderator_array.where((k)=> k.user_id == userId);

    for(final e in  d.data.moderator_array.where((k)=> k.user_id == userId) ){
      if(widget.approverId == "0" && e.user_id == userId){
        isModerator = true;
      }
    }

   /* if (widget.mId == userId && widget.approverId == "0") {

    }*/

    setState(() {});
  }

  GetReviewTreeModel res_d;

  // List<String> paymentList = [
  //   "Cash",
  //   "Bank",
  //   "Card",
  // ];

 // List<String> priorityList = ["Low", "Normal", "High"];

  bool isLoading = false;

  Future<GetReviewTreeModel> getReviewTree() async {
    isLoading = true;
   // bool con = true;
    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    if (con) {
      try {
        //pId = purchase_id
        final res = await http.get(
            "${BASE_URL}purchase/purchaseMapDetails/${widget.pId}?organization_id=${prefs.getInt('orgId')}",
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = GetReviewTreeModel.fromJson(j_data);

            for (int k = 0; k < res_d.data.length; k++) {
              if (res_d.is_already_paid == 1 && res_d.data[k].is_admin_manager_user == 1) {
                topList = res_d.data.toList();
              } else {
                topList = res_d.data.where((p) => p.mod == 1).toList();
                bottomList =
                    res_d.data.where((p) => p.user_level != 0).toList();
                print(topList);
                print(bottomList);
                if (topList.length > 0) {
                  bottomList.remove(res_d.data.where((p) => p.mod == 1));
                }
              }
            }

            print(topList);
            print(bottomList);

            isLoading = false;
            setState(() {});

            return res_d;
          default:
            return GetReviewTreeModel.buildErr(res.statusCode);
        }
      } catch (err) {
        isLoading = false;
        return GetReviewTreeModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }

  bool isRef = false;
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    isRef = true;
    mF = rVm.getReviewData(widget.pId, context);
    setState(() {});
    return null;
  }

  @override
  Widget build(BuildContext context) {

    return RefreshIndicator(
      onRefresh: refreshList,
      key: refreshKey,
      child: FutureBuilder(
          initialData: null,
          future: mF,
          builder: (context, snap) => _checkApIdata(context, snap)),
    );
  }



  String payMode = "";
  bool isFirst = true;

  Widget _checkApIdata(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch
      GetReviewModel _d = snap.data;

      if ((_d?.Status ?? false) && isLoading == false) {


        if (!isModerator && isBudgetFirstTime) {
          if (_d.data.isPopup == 1) {
            if (widget.isFromUserPending) {
              isBudgetFirstTime = false;
              Future.delayed(
                  Duration.zero, () => showBudgetExceedingPopup(_d, context));
//            setState(() {
              // widget.isFromUser = false;
//            });
              showBudgetReason = true;
            }
          }
        }
        print("${_d.data.assignData}");
        if (_d.data.assignData) {
          _getModeratorData(_d);
          // if (_d.data.payment_mode == 1) {
          //   payMode = "Cash";
          // } else if (_d.data.payment_mode == 2) {
          //   payMode = "Bank";
          // } else if (_d.data.payment_mode == 3) {
          //   payMode = "Card";
          // } else {
          //   payMode = "Cash";
          // }

          _userC.text = _d.data.user_name ?? "";
          _projC.text = _d.data.project_name ?? "";
          _vchC.text = _d.data.VHC_code ?? "";
          _descC.text = _d.data.purchase_description ?? "";
          _counC.text = _d.data.country_name ?? "";
          _cityC.text = _d.data.city_name ?? "";
          _payC.text = _d.data.payment_mode_name ?? "";
          _prioC.text = _d.data.priority_name ?? _d.data.priority;
          _expeC.text = _d.data.total.toString() ?? "";
          _totalAmount.text = _d.data.total_amount.toString() ?? "0";
          _balanC.text = _d?.data?.chartBalanceAmount ?? "";
          projectId = _d.data.project_id;
          cityId = _d.data.city_id;
          countryId = _d.data.country_id;
          invoiceNumber.text = _d.data.invoice_number ?? "";
          invoiceDate.text = _d.data.invoice_date ?? "";
          paymentDueDate.text = _d.data.payment_due_date ?? "";
          gstAmount.text = _d?.data?.gst_amount?.toString() ?? "0";
          gstValueName = _d.data.gst_name ?? "";
          mainGstId = int.parse("${_d.data.gst_id}");
          mainPayId = int.parse("${_d.data.payment_mode ?? ""}");
          _shootLocation.text = _d?.data?.location_name ?? "";
         // mainPriorityId = int.parse("${_d.data.priority ?? 0}");

          if(_d.data.invoice_date != "" && _d.data.invoice_date != null) {
            mainDate = new DateFormat('dd-mm-yy').parse("${_d.data.invoice_date}");
          }
          print("$mainGstId is my main Gst");

          for (final e in _singleton.userLvl1List
              .where((k) => k.project_id == _d.data.project_id)) {
            isLvl1Edit = true;
          }

          print("Balance = ${_balanC.text}");

          if (_d.data.parent_id == 0) {
            // no sub category present
            _cateC.text = _d.data.category_name ?? "";
            catId = _d.data.category_id;
          } else {
            _subCatC.text = _d.data.category_name ?? "";
            subCatId = _d.data.category_id;
            catId = _d.data.parent_id;
            _cateC.text = _d.data.parent_name;
          }

          _getCategoryList();

          _d.data.assignData = false;

          // isFirst = false;
          // isRef = false;
        }

        // API true
        return _buildList(_d);
      } else if (isLoading == true || (_d?.is_loading ?? false)) {
        return Loader(title: '');
      } else {
        return Err(
            bar_visibility: true,
            p_title: 'Home',
            m: _d.Message,
            mL: () => setState(() {
                  _d.is_loading = true;
                  mF = rVm.getReviewData(widget.pId, context);
                }));
      }
    } else {
      // initial loading
      return Loader(title: '');
    }
  }

  Widget _buildList(GetReviewModel d) {
    w = MediaQuery.of(context).size.width;
    h = MediaQuery.of(context).size.height;
    letfPad = 0.00944;
    topPad = 0.00470;
    print('Height is $h/$w');
    imgUrl.add(d.data.purchase_image);

    List<String> p = [];

    if (d.data.purchase_image != null && d.data.purchase_image != "-") {
      p = d.data.purchase_image.split(',');
      print(p);
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        //title: "Review ${d.data.approver_id == "0" ? "Expense" : "PO"}",
        title: "${d.data.poReviewHeader ?? ""}",

      ),
      body: Container(
        constraints: BoxConstraints(minHeight: h),
        child: ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w, top: topPad*4*h, bottom: topPad*4*h),
          children: <Widget>[
            Card(
              child: Padding(
                padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w, top: topPad*4*h, bottom: topPad*4*h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    (p != null && p.length > 0)
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) => CarouselDemo(
                                            fromPage: 1,
                                            imgUrl: p,
                                          )));
                            },
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    constraints: BoxConstraints(
                                        minHeight: 0, maxHeight: h*0.235),
                                    child: FadeInImage(
                                      placeholder: AssetImage(
                                          'assets/images/loading.gif'),
                                      image: NetworkImage(((p[0]
                                                      .contains('png') ||
                                                  p[0].contains('jpg') ||
                                                  p[0].contains('jpeg') ||
                                                  p[0].contains('tif'))
                                              ? "${p[0]}?token=${_singleton.tempToken}"
                                              : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW") ??
                                          "https://developers.google.com/maps/documentation/streetview/images/error-image-generic.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                p.length > 1
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(h*0.01)),
                                        child: Container(
                                          color: Colors.white,
                                          padding: EdgeInsets.only(left: letfPad*2*w, right: letfPad*2*w, top: topPad*2*h, bottom: topPad*2*h),
                                          child: Text("+${p.length - 1}"),
                                        ),
                                      )
                                    : Container()
                              ],
                            ),
                          )
                        : Container() /*Padding(
                            padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                            child: SvgPicture.asset('assets/images/file.svg',
                                color: Color(0xffd0d3d8),
                                semanticsLabel: 'document logo'),
                          )*/
                    ,

                    /* p.length > 0
                        ? GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) => CarouselDemo(
                                            fromPage: 1,
                                            imgUrl: p,
                                          )));
                            },
                            child: Align(
                              alignment: Alignment.center,
                              child: Container(
                                constraints: BoxConstraints(
                                    minHeight: 0, maxHeight: 200),
                                child: FadeInImage(
                                  placeholder:
                                      AssetImage('assets/images/loading.gif'),
                                  image: NetworkImage((p[0].contains('pdf')
                                          ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
                                          : p[0]) ??
                                      "https://developers.google.com/maps/documentation/streetview/images/error-image-generic.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          )
                        : Container(),*/

                    (p != null && p.length > 0)
                        ? Divider(
                            color: Colors.black,
                          )
                        : Container(),
                    SizedBox(
                      height: h*0.014,
                    ),
                    Text(
                      d.data.po_name ?? "",
                      style: TextStyle(fontSize: h*0.0282),
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color:
                                      editOn ? appColor : Color(0xffe0e0e0))),
                          labelText: "User Name",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: _userC,
                      obscureText: false,
                      enabled: false,
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: Color(0xffe0e0e0)
//                                  color: editOn ? appColor : Color(0xffe0e0e0)
                              )),
                          labelText: "Project",
                          labelStyle: TextStyle(color: appColor)),
                      style: Styles.formInputTextStyle,
                      controller: _projC,
                      obscureText: false,
                      readOnly: true,
                      maxLines: null,
                      enabled: false,
//                      enabled: editOn ? true : false,
                      onTap: editOn
                          ? () {
                              tabIndex = 1;
                              _filter1 == 1
                                  ? _openBottomSheetCategory(context)
                                  : null;

                              if (_filter1 != 1) {
                                final snackBar = SnackBar(
                                    duration: Duration(seconds: 1),
                                    content:
                                        Text('Please wait loading data...'));
                                _scaffoldKey.currentState
                                    .showSnackBar(snackBar);
                              }
                            }
                          : null,
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: Color(0xffe0e0e0))),
                          labelText: "VCH Code",
                          labelStyle: TextStyle(color: appColor)),
                      style: Styles.formInputTextStyle,
                      controller: _vchC,
                      obscureText: false,
                      readOnly: true,
                      maxLines: null,
                      enabled: false,
                      onTap: null,
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: editOn ? appColor : Color(0xffe0e0e0),)),
                          labelText: "Category",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: _cateC,
                      readOnly: true,
                      obscureText: false,
                      enabled: (editOn ) ? true : false,
                      //enabled: false,
                      //onTap: null,
                      maxLines: null,
                     onTap: (editOn )
                         ? () {
                             if (_projC.text.isEmpty) {
                               final snackBar = SnackBar(
                                   duration: Duration(milliseconds: 500),
                                   content:
                                       Text('Please select Project first'));
                               _scaffoldKey.currentState
                                   .showSnackBar(snackBar);
                             } else {
                               tabIndex = 2;

                               _filter2 == 1
                                   ? _openBottomSheetCategory(context)
                                   : null;

                               if (_filter2 != 1) {
                                _getCategoryList();
                               }
                             }
                           }
                         : null,
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: editOn ? appColor : Color(0xffe0e0e0),)),
                          labelText: "Sub Category",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: _subCatC,
                      readOnly: true,
                      obscureText: false,
                     enabled: (editOn) ? true : false,
                     // enabled: false,
                     // onTap: null,
                      maxLines: null,
                     onTap: (editOn )
                         ? () {
                             if (_cateC.text.isEmpty) {
                               final snackBar = SnackBar(
                                   duration: Duration(milliseconds: 500),
                                   content:
                                       Text('Please select Category first'));
                               _scaffoldKey.currentState
                                   .showSnackBar(snackBar);
                             } else {
                               tabIndex = 7;

                               _filter7 == 1
                                   ? _openBottomSheetCategory(context)
                                   : null;
                               if (_filter7 != 1) {
                                 final snackBar = SnackBar(
                                     content:
                                         Text('Please Reselect Category...'));
                                 _scaffoldKey.currentState
                                     .showSnackBar(snackBar);
                               }
                             }
                           }
                         : null,
                    ),

                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                color: editOn ? appColor : Color(0xffe0e0e0),)),
                          labelText: "Set Name",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: _shootLocation,
                      readOnly: true,
                      obscureText: false,
                      enabled: (editOn ) ? true : false,
                      //enabled: false,
                      //onTap: null,
                      maxLines: null,
                      onTap: (editOn )
                          ? () {
                          tabIndex = 10;

                          _filter10 == 1
                              ? _openBottomSheetCategory(context)
                              : null;

                      }
                          : null,
                    ),

                    SizedBox(
                      height: h*0.0094,
                    ),
                    TextField(
                      readOnly: editOn ? false : true,
                      enabled: editOn ? true : false,
                      maxLines: null,
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color:
                                      editOn ? appColor : Color(0xffe0e0e0))),
                          labelText: "Description",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: _descC,
                      obscureText: false,
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          width: w / 2 - (w*0.10),
                          child: TextField(
                            readOnly: true,
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: editOn
                                            ? appColor
                                            : Color(0xffe0e0e0))),
                                labelText: "Country",
                                labelStyle: TextStyle(
                                    color: appColor, fontFamily: 'SanRegular')),
                            style: Styles.formInputTextStyle,
                            controller: _counC,
                            obscureText: false,
                            enabled: editOn ? true : false,
                            onTap: editOn
                                ? () {
                                    tabIndex = 3;

                                    _filter3 == 1
                                        ? _openBottomSheetCategory(context)
                                        : null;

                                    if (_filter3 != 1) {
                                      final snackBar = SnackBar(
                                          duration: Duration(seconds: 1),
                                          content: Text(
                                              'Please wait loading data...'));
                                      _scaffoldKey.currentState
                                          .showSnackBar(snackBar);
                                    }
                                  }
                                : null,
                          ),
                        ),
                   /*     SizedBox(
                          width: w*0.018,
                        ),
                        Container(
                          width: w / 2 - 44,
                          child: TextField(
                            decoration: new InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: editOn
                                            ? appColor
                                            : Color(0xffe0e0e0))),
                                labelText: "City",
                                labelStyle: TextStyle(
                                    color: appColor, fontFamily: 'SanRegular')),
                            style: Styles.formInputTextStyle,
                            controller: _cityC,
                            obscureText: false,
                            readOnly: true,
                            enabled: editOn ? true : false,
                            onTap: editOn
                                ? () {
                                    tabIndex = 4;

                                    _filter4 == 1
                                        ? _openBottomSheetCategory(context)
                                        : null;

                                    if (_filter4 != 1) {
                                      final snackBar = SnackBar(
                                          content: Text(
                                              'Please wait loading data...'));
                                      _scaffoldKey.currentState
                                          .showSnackBar(snackBar);
                                    }
                                  }
                                : null,
                          ),
                        )*/
                      ],
                    ),
                    SizedBox(
                      height: h*0.0094,
                    ),
                    Row(
                      children: <Widget>[
                        PopupMenuButton<String>(
                          itemBuilder: (BuildContext context) {
                            return payModeData.map((String s) {
                              return PopupMenuItem<String>(
                                value: s,
                                child: Text(s),
                              );
                            }).toList();
                          },
                          enabled: editOn ? true : false,
                          onSelected: (j) {
//                            payMode = j;
                            int index = payModeData.indexOf(j);
                            print(index);
                            mainPayId = paymentIdList[index];
                           // payMode = j;
                            _payC.text = j;
                            setState(() {});
                          },
                          child: Container(
                            width: w / 2 - (w*0.10),
                            child: TextField(
                              decoration: new InputDecoration(
                                  enabledBorder: new OutlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: editOn
                                              ? appColor
                                              : Color(0xffe0e0e0))),
                                  labelText: "Payment Mode",
                                  labelStyle: TextStyle(
                                      color: appColor,
                                      fontFamily: 'SanRegular')),
                              style: Styles.formInputTextStyle,
                              controller: _payC,
                              obscureText: false,
                              readOnly: true,
                              enabled: false,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: w*0.0188,
                        ),
                        PopupMenuButton<String>(
                            itemBuilder: (BuildContext context) {
                              return priorityData.map((String s) {
                                return PopupMenuItem<String>(
                                  value: s,
                                  child: Text(s),
                                );
                              }).toList();
                            },
                            enabled: editOn ? true : false,
                            onSelected: (j) {
//                            payMode = j;
                              int indexPrio = priorityData.indexOf(j);
                              mainPriorityId = priorityId[indexPrio];
                              _prioC.text = j;
                              setState(() {});
                            },
                            child: Container(
                              width: w / 2 - (w*0.10),
                              child: TextField(
                                readOnly: true,
                                enabled: false,
                                decoration: new InputDecoration(
                                    labelText: "Priority",
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: editOn
                                                ? appColor
                                                : Color(0xffe0e0e0))),
                                    labelStyle: TextStyle(
                                        color: appColor,
                                        fontFamily: 'SanRegular')),
                                style: Styles.formInputTextStyle,
                                controller: _prioC,
                                obscureText: false,
                              ),
                            )),
                      ],
                    ),
                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? SizedBox(
                      height: h*0.0094,
                    ) : Container(),
                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: editOn
                                      ? appColor
                                      : Color(0xffe0e0e0),)),
                          labelText: "Invoice Number",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: invoiceNumber,
                      readOnly: editOn ? false : true,
                      obscureText: false,
                      enabled: (editOn ) ? true : false,
                      //enabled: false,
                      onTap: null,
                      maxLines: null,
//                      onTap: (editOn )
//                          ? () {
//                              if (_cateC.text.isEmpty) {
//                                final snackBar = SnackBar(
//                                    duration: Duration(milliseconds: 500),
//                                    content:
//                                        Text('Please select Category first'));
//                                _scaffoldKey.currentState
//                                    .showSnackBar(snackBar);
//                              } else {
//                                tabIndex = 7;
//
//                                _filter7 == 1
//                                    ? _openBottomSheetCategory(context)
//                                    : null;
//                                if (_filter7 != 1) {
//                                  final snackBar = SnackBar(
//                                      content:
//                                          Text('Please Reselect Category...'));
//                                  _scaffoldKey.currentState
//                                      .showSnackBar(snackBar);
//                                }
//                              }
//                            }
//                          : null,
                    ) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? SizedBox(
                      height: h*0.0094,
                    ) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ?  TextField(
                      decoration: new InputDecoration(
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: editOn
                                      ? appColor
                                      : Color(0xffe0e0e0),)),
                          labelText: "Invoice Date",
                          labelStyle: TextStyle(
                              color: appColor, fontFamily: 'SanRegular')),
                      style: Styles.formInputTextStyle,
                      controller: invoiceDate,
                      readOnly: true,
                      obscureText: false,
                      enabled: (editOn ) ? true : false,
                     // enabled: false,
                      onTap: () {
                        _selectDateInvoiceDate();
                      },
                      maxLines: null,
//                      onTap: (editOn )
//                          ? () {
//                              if (_cateC.text.isEmpty) {
//                                final snackBar = SnackBar(
//                                    duration: Duration(milliseconds: 500),
//                                    content:
//                                        Text('Please select Category first'));
//                                _scaffoldKey.currentState
//                                    .showSnackBar(snackBar);
//                              } else {
//                                tabIndex = 7;
//
//                                _filter7 == 1
//                                    ? _openBottomSheetCategory(context)
//                                    : null;
//                                if (_filter7 != 1) {
//                                  final snackBar = SnackBar(
//                                      content:
//                                          Text('Please Reselect Category...'));
//                                  _scaffoldKey.currentState
//                                      .showSnackBar(snackBar);
//                                }
//                              }
//                            }
//                          : null,
                    ) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ?  SizedBox(
                      height: h*0.0094,
                    ) : Container(),

                  (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? Container(
                      width: w  - (w*0.10),
                      child: TextField(
                        readOnly: true,
                        decoration: new InputDecoration(
                            enabledBorder: new OutlineInputBorder(
                                borderSide: new BorderSide(
                                    color: editOn
                                        ? appColor
                                        : Color(0xffe0e0e0))),
                            labelText: "Payment Due Date",
                            labelStyle: TextStyle(
                                color: appColor, fontFamily: 'SanRegular')),
                        style: Styles.formInputTextStyle,
                        controller: paymentDueDate,
                        obscureText: false,
                        enabled: editOn ? true : false,
                        onTap: (){
                          if(invoiceDate.text.isEmpty) {
                          final snackBar = SnackBar(
                          duration: Duration(seconds: 1),
                          content: Text('Please select Invoice Date'));
                          _scaffoldKey.currentState.showSnackBar(snackBar);
                          } else {
                          _selectDatePaymentDueDate(invoiceDate.text);
                          }
                        },
                      ),
                    ) : Container(),

                    SizedBox(
                      height: h*0.018,
                    ),



                    TextField(
                      readOnly: editOn ? false : true,
                      enabled: editOn ? true : false,
                      onChanged: (text){

                        if(d.data.approver_id == 0 || d.data.approver_id == "0") {
                          print(_expeC.text);
                          if(mainGstId != 0) {
                            for(int k = 0; k<gstObj.length; k++) {
                              if(mainGstId == gstObj[k]['id']) {
                                //totalExpenseValue = double.parse(_expeC.text) + (gstObj[i]['percent'] / 100 * val);
                                double gstValue = (gstObj[k]['percent'] / 100 * (double.parse("${_expeC.text}")));
                                gstAmount.text = gstValue.toStringAsFixed(2).toString();
                                totalExpenseValue = double.parse("${_expeC.text}") + gstValue;
                                totalExpenseValue = double.parse(totalExpenseValue.toStringAsFixed(2));
                                _totalAmount.text = totalExpenseValue.toString();
                                break;
                              }
                            }
                          }
                        }
                      },
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        new BlacklistingTextInputFormatter(
                            new RegExp('[\\,|\\-|\\.|\\ ]')),
                      ],
                      decoration: new InputDecoration(
                          labelText: "Expense Amount",
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color:
                                      editOn ? appColor : Color(0xffe0e0e0))),
                          border: InputBorder.none,
                          labelStyle: TextStyle(color: appColor, fontSize: h*0.018)),
                      style: Styles.homecommonTextStyle,
                      controller: _expeC,
                      obscureText: false,
                    ),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? SizedBox(height: 8,) : Container(),
                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? Text("Tax", style: TextStyle(color: appColor),) : Container(),
                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? SizedBox(height: 8,) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? PopupMenuButton(
                      enabled: editOn ? true : false,
                      itemBuilder: (BuildContext context) {
                        return gstName.map((String s) {
                          return PopupMenuItem<String>(
                            value: s,
                            child: Text("$s"),
                          );
                        }).toList();
                      },

                      onSelected: (j) {
                        FocusScope.of(context).requestFocus(FocusNode());
                        gstAmount.text="";

                        if(_expeC.text == "") {
                          final snackBar = SnackBar(
                              duration: Duration(seconds: 1),
                              content: Text("Please enter amount"));
                          _scaffoldKey.currentState.showSnackBar(snackBar);
                        } else {
                          totalExpenseValue = double.parse(_expeC.text.isEmpty ? 0.0 : _expeC.text);
                          _totalAmount.text = totalExpenseValue.toString();
                          gstValueName = j;
                          double val = double.parse(_expeC.text);

                          for(int i=0;i<gstObj.length; i++) {
                            if(gstValueName == gstObj[i]['name']) {
                              print(gstObj[i]['percent']);
                              mainGstId =  gstObj[i]['id'];
                              if (gstObj[i]['percent'] == 0.0 || gstObj[i]['percent']==0) {
                                manualChange = false;
                                break;
                              } else {
                                totalExpenseValue = double.parse(_expeC.text) + (gstObj[i]['percent'] / 100 * val);
                                double gstValue = (gstObj[i]['percent'] / 100 * val);
                                gstAmount.text = gstValue.toStringAsFixed(2).toString();
                                totalExpenseValue = double.parse(totalExpenseValue.toStringAsFixed(2));
                                _totalAmount.text = totalExpenseValue.toString();
                                manualChange = true;
                                break;
                              }
                            }
                          }
                          setState(() {});
                        }
                      },
                      child: Container(
                          padding: EdgeInsets.only(right: letfPad*w, left: letfPad*w),
                          height: h*0.05,
                          decoration: BoxDecoration(
                              border: editOn ? Border.all(color: appColor) : Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.all(Radius.circular(h*0.0094))),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              RichText(
                                  textAlign: TextAlign.start,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                    children: [
                                      // TextSpan(
                                      //     text: 'Tax Type : ',
                                      //     style: TextStyle(
                                      //         fontFamily: 'SanRegular',
                                      //         color: Colors.grey,
                                      //         fontSize: h*0.012)),
                                      TextSpan(
                                          text: gstValueName == "" ? "Select Tax Type" : "$gstValueName",
                                          style: TextStyle(
                                              fontFamily: 'SanSemiBold',
                                              color: Colors.black,
                                              fontSize: h*0.012)),
                                    ],
                                  )),
                              Icon(Icons.arrow_drop_down)
                            ],
                          )),
                    ) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(

                            child: TextField(
                              readOnly: editOn ? manualChange : false,
                              decoration: new InputDecoration(
                                labelText: "Tax value",
                                labelStyle: TextStyle(fontFamily: 'SanRegular'),
                              ),
                              inputFormatters: [
                                new BlacklistingTextInputFormatter(
                                    new RegExp('[\\,|\\-|\\ ]')),
                              ],
                              style: Styles.formInputTextStyle,
                              controller: gstAmount,
                              keyboardType: TextInputType.number,
                              onChanged: (text) {
                                totalExpenseValue = double.parse(_expeC.text) + double.parse(gstAmount.text);
                                _totalAmount.text = totalExpenseValue.toString();

                              },
                              obscureText: false,
                              onSubmitted: (text) {
                                totalExpenseValue = double.parse(_expeC.text) + double.parse(gstAmount.text);
                                _totalAmount.text = totalExpenseValue.toString();
                              },
                            ),
                          ),

                        ],
                      ),
                    ) : Container(),

                    (d.data.approver_id) == "0" || (d.data.approver_id) == 0 ? TextField(
                      readOnly: true,
                      enabled: false,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        new BlacklistingTextInputFormatter(
                            new RegExp('[\\,|\\-|\\.|\\ ]')),
                      ],
                      decoration: new InputDecoration(
                          labelText: "Total Amount",
                          enabledBorder: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: Color(0xffe0e0e0))),
                          border: InputBorder.none,
                          labelStyle: TextStyle(color: appColor, fontSize: h*0.018)),
                      style: Styles.homecommonTextStyle,
                      controller: _totalAmount,
                      obscureText: false,
                    ) : Container(),


                    (isLvl1Edit && widget.isFromUserPending)
                        ? ((_balanC.text == "-" || _balanC.text == "")
                            ? Container()
                            : TextField(
                                readOnly: true,
                                enabled: false,
                                decoration: new InputDecoration(
                                    labelText: "Balance Amount",
                                    enabledBorder: new OutlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Color(0xffe0e0e0))),
                                    border: InputBorder.none,
                                    labelStyle: TextStyle(
                                        color: appColor, fontSize: h*0.018)),
                                style: TextStyle(
                                  color: double.parse(_balanC.text) < 0.0
                                      ? Colors.red
                                      : Colors.black,
                                  fontSize: h*0.03293,
                                  fontWeight: FontWeight.w400,
                                ),
                                controller: _balanC,
                                obscureText: false,
                              ))
                        : Container(),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: h*0.018,
            ),
            widget.isFromUserPending
                ? Container(
              height: h*0.054,
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                        height: h*0.042,
                        decoration: BoxDecoration(
                            color: appColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(h*0.0094))),
                        child: FlatButton(
                          onPressed: () {
                            isModerator
                                ? _showVersionDialog(context, 1, d.data, h,w)
                                : (d.data.isPopup == 1
                                ? _showVersionDialog(
                                context, 3, d.data, h, w)
                                : _showVersionDialog(
                                context, 1, d.data,h, w));
                          },
                          child: Text(
                            (isModerator || isLvl1Edit)
                                ? "Verify"
                                : "Accept",
                            style: TextStyle(
                                color: Colors.white, fontSize: h*0.01),
                          ),
                        )),
                    SizedBox(
                      width: w*0.0094,
                    ),
                    (d.data.is_editor)
                        ? Container(
                        height: h*0.042,
                        decoration: BoxDecoration(
                            color: appColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(h*0.0094))),
                        child: FlatButton(
                          onPressed: () {
                            if (!editOn && d.data.is_editor) {
                              editOn = true;
                              setState(() {});
                            } else {
                              _editPo(context, d.data);
                            }
                          },
                          child: Text(
                            (editOn ) ? "Save" : "Edit",
                            style: TextStyle(
                                color: Colors.white, fontSize: h*0.01),
                          ),
                        ))
                        : Container(),
                    SizedBox(
                      width: w*0.0094,
                    ),
                    Container(
                        height: h*0.042,
                        decoration: BoxDecoration(
                            color: appColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(h*0.0094))),
                        child: FlatButton(
                          onPressed: () {
                            _showVersionDialog(context, 2, d.data, h, w);
                          },
                          child: Text(
                            "Reject",
                            style: TextStyle(
                                color: Colors.white, fontSize: h*0.01),
                          ),
                        )),
                  ],
                ),
              ),
            )
                : Container(),
            SizedBox(
              height: h*0.03763,
            ),

            // if coming from userPending
            widget.isFromUserPending
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListView.builder(
                          shrinkWrap: true,
                          reverse: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: res_d?.data?.length ?? 0,
                          itemBuilder: (context, i) {
                            GetReviewTreeModelData fD = res_d.data[i];

                            print("Reason is:- ${fD.reason}");

                            if(isLvl1Edit == true){
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(),
                                      (fD.approver_id == "0" && fD.mod == 1)
                                          ? Text(
                                        "Moderator",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: h*0.018,
                                            fontFamily: 'SanSemiBold'),
                                        textAlign: TextAlign.center,
                                      )
                                          : (fD.approver_id == "0"
                                          ? Text(
                                        "Level ${fD.user_level.toString() ?? ""}",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: h*0.018,
                                            fontFamily: 'SanMedium'),
                                        textAlign: TextAlign.center,
                                      )
                                          : Text("")),
                                      _iconWidget(fD)
                                    ],
                                  ),
                                  SizedBox(
                                    height: h*0.0094,
                                  ),
                                  Text(
                                    treeString(fD),
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: h*0.016,
                                        fontFamily: 'SanRegular'),
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    height: fD.purchase_map_status == 2 ? h*0.0094 : 0,
                                  ),
                                  ( fD.purchase_map_status == 1 && fD.reason != null && fD.reason != "-" && fD.reason != "")
                                      ? RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                              text: 'Remark: ',
                                              style: TextStyle(
                                                  fontFamily: 'SanMedium',
                                                  color: Colors.black87,
                                                  fontSize: h*0.016)),
                                          TextSpan(
                                              text: treeSubString(fD),
                                              style: TextStyle(
                                                  fontFamily: 'SanSemiBold',
                                                  color: Colors.black54,
                                                  fontSize: h*0.016)),
                                        ],
                                      ))
                                      : Container(),

                                  /*  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: 'Remark: ',
                                            style: TextStyle(
                                                fontFamily: 'SanMedium',
                                                color: Colors.black87,
                                                fontSize: h*0.016)),
                                        TextSpan(
                                            text: treeSubString(fD),
                                            style: TextStyle(
                                                fontFamily: 'SanSemiBold',
                                                color: Colors.black54,
                                                fontSize: h*0.016)),
                                      ],
                                    )),*/
                                  SizedBox(
                                    height: h*0.018,
                                  ),
                                  res_d.data.length- 1 == i ? Container(
                                    height: 1,
                                    width: w*0.2361,
                                    color: appColor,
                                  ) : Container(),
                                  SizedBox(
                                    height: h*0.018,
                                  ),
                                ],
                              );
                            }else{
                              return Container();
                            }


                          }),

                    ],
                  )
                : Container(),
            showBudgetReason
                ? SizedBox(
                    height: h*0.03763,
                  )
                : Container(),

            // my po/expense vala tree
            widget.isFromUser != true
                ? Column(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          topList.length > 0
                              ? Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: h*0.0094,
                                    ),
                                    ListView.builder(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount: topList?.length ?? 0,
                                        itemBuilder: (context, i) {
                                          return Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Container(),
                                                  /*  topList[i].mod == 1
                                                      ? Text(
                                                    "Moderator",
                                                    style: TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: h*0.018,
                                                        fontFamily: 'SanSemiBold'),
                                                    textAlign: TextAlign.center,
                                                  )
                                                      : Text(
                                                    "Admin Manager",
                                                    style: TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: h*0.018,
                                                        fontFamily: 'SanSemiBold'),
                                                    textAlign: TextAlign.center,
                                                  ),*/

                                                  _headingWidget(topList[i]),
                                                  _iconWidget(topList[i])
                                                ],
                                              ),
                                              SizedBox(
                                                height: h*0.0094,
                                              ),
                                              Text(
                                                treeString(topList[i]),
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h*0.016,
                                                    fontFamily: 'SanRegular'),
                                                textAlign: TextAlign.center,
                                              ),
                                              (topList[i].reason != null && topList[i].reason != "-" && topList[i].reason != "")
                                                  ? RichText(
                                                      textAlign:
                                                          TextAlign.center,
                                                      text: TextSpan(
                                                        children: [
                                                          TextSpan(
                                                              text: 'Remark: ',
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'SanMedium',
                                                                  color: Colors
                                                                      .black87,
                                                                  fontSize:
                                                                  h*0.0164)),
                                                          TextSpan(
                                                              text:
                                                                  treeSubString(
                                                                      topList[
                                                                          i]),
                                                              style: TextStyle(
                                                                  fontFamily:
                                                                      'SanSemiBold',
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize:
                                                                      h*0.0164)),
                                                        ],
                                                      ))
                                                  : Container(),
                                              SizedBox(
                                                height: h*0.018,
                                              ),
                                            ],
                                          );
                                        }),
                                    SizedBox(
                                      height:
                                          topList[0].purchase_map_status == 2
                                              ? 8
                                              : 0,
                                    ),
                                    SizedBox(
                                      height: h*0.018,
                                    ),
                                    Container(
                                      height: 1,
                                      width: w*0.2361,
                                      color: appColor,
                                    ),
                                  ],
                                )
                              : Container(),
                          SizedBox(
                            height: h*0.018,
                          ),
                        ],
                      ),

                      //bottom vala tree part

                      ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: bottomList?.length ?? 0,
                          itemBuilder: (context, i) {
                            GetReviewTreeModelData fD = bottomList[i];
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(),
                                    (fD.approver_id == "0" && fD.mod == 1)
                                        ? Text(
                                            "Moderator",
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: h*0.018,
                                                fontFamily: 'SanSemiBold'),
                                            textAlign: TextAlign.center,
                                          )
                                        : (fD.approver_id == "0"
                                            ? Text(
                                                "Level ${fD.user_level.toString() ?? ""}",
                                                style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: h*0.018,
                                                    fontFamily: 'SanMedium'),
                                                textAlign: TextAlign.center,
                                              )
                                            : Text("")),
                                    _iconWidget(fD)
                                  ],
                                ),
                                SizedBox(
                                  height: h*0.0094,
                                ),
                                Text(
                                  treeString(fD),
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: h*0.016,
                                      fontFamily: 'SanRegular'),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: fD.purchase_map_status == 2 ? 8 : 0,
                                ),
                                (fD.reason != null && fD.reason != "-" && fD.reason != "") ?   RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: 'Remark: ',
                                            style: TextStyle(
                                                fontFamily: 'SanMedium',
                                                color: Colors.black87,
                                                fontSize: h*0.016)),
                                        TextSpan(
                                            text: treeSubString(fD),
                                            style: TextStyle(
                                                fontFamily: 'SanSemiBold',
                                                color: Colors.black54,
                                                fontSize: h*0.016)),
                                      ],
                                    )) :Container(),
                                SizedBox(
                                  height: h*0.018,
                                ),

                              ],
                            );
                          }),
                    ],
                  )
                : Container(),
            SizedBox(
              height: h*0.05645,
            ),
          ],
        ),
      ),
    );
  }

  void _acceptPo(BuildContext context) async {
    _showLoading(context);
    SetFavouriteModel res = await rVm.acceptPo(
        widget.pMapId, context, _poDescriptionController.text);

    if (res.Status ?? false) {
      Navigator.pop(context);
      _bookEventDialog(context, "You have Accepted!");
    } else {
      Navigator.pop(context);
      Navigator.pop(context);
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text(res.Message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _acceptOverBudgetPo(BuildContext context) async {
    if (_poDescriptionController.text.isEmpty ||
        _poDescriptionController.text == "-") {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Please enter reason"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else {
      _showLoading(context);
      SetFavouriteModel res = await rVm.acceptPo(
          widget.pMapId, context, _poDescriptionController.text);
      if (res.Status ?? false) {
        Navigator.pop(context);
        _bookEventDialog(context, "You have Accepted!");
      } else {
        Navigator.pop(context);
        Navigator.pop(context);
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(res.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }
  }

  void _editPo(BuildContext context, GetReviewModelData data) async {
    if(checkForEditData()) {
      // int payId;
      // if (payMode == "Cash") {
      //   payId = 1;
      // } else if (payMode == "Bank") {
      //   payId = 2;
      // } else if (payMode == "Card") {
      //   payId = 3;
      // } else {
      //   payId = 1;
      // }

      int sCatId;

      if (subCatId == 0) {
        sCatId = catId;
      } else {
        sCatId = subCatId;
      }

      var body = json.encode({
        "project_id": projectId,
        "po_name": data.po_name,
        "vendor_id" : data.vendor_id,
        "item_name": "-",
        "user_id": data.user_id,
        "company_name": "-",
        "category_id": sCatId,
        "company_address": "-",
        "price": _expeC.text,
        "currency": data.currency,
        "sgst": "0",
        "cgst": "0",
        "total": _expeC.text,
        "purchase_image": data.purchase_image,
        "country_id": countryId,
        "purchase_description": _descC.text,
        "purchase_status": "0",
        "approved_percentage": "0",
        "purchase_active": "1",
        "payment_mode": mainPayId,
        "payment_mode_description": "-",
        "priority": "$mainPriorityId",
        "approver_id": data.approver_id,
        "city_id": cityId,
        "signatureURL" : _singleton.SignatureImage,
        "payment_due_date" : paymentDueDate.text,
        "invoice_number" : invoiceNumber.text,
        "invoice_date" : invoiceDate.text,
        "purchase_id" : widget.pId,
        "gst_id" : mainGstId,
        "gst_amount" : gstAmount.text == "" ? "0" : gstAmount.text,
        "total_amount" : _totalAmount.text,
        "location_id" : location_id ?? 0
      });

      _showLoading(context);
      SetFavouriteModel res = await rVm.editPo(widget.pId, body, context);

      if (res.Status ?? false) {
        Navigator.pop(context);
        isFirst = true;
        editOn = false;
        refreshList();
      } else {
        Navigator.pop(context);
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(res.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }
  }

  bool checkForEditData() {
    if(_subCatC.text == "") {
      final snackBar =
      SnackBar(
          duration: Duration(seconds: 1),
          content: Text('Please enter Sub Category'));
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return false;
    } else {
      return true;
    }
  }

  void _rejectPo(BuildContext context) async {
    _showLoading(context);
    SetFavouriteModel res = await rVm.rejectPo(
        widget.pMapId, _poDescriptionController.text, context);

    if (res.Status ?? false) {
      Navigator.pop(context);
      //Navigator.pop(context);
      _bookEventDialog(context, "You have Rejected!");
    } else {
      Navigator.pop(context);
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text(res.Message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void _bookEventDialog(BuildContext context, String message) {
    showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return CustomPopUp(
          msg: message,
          fromPending: widget.isFromUserPending
        );
      },
    );
  }

  Widget _iconWidget(GetReviewTreeModelData fD) {
    if (fD.purchase_map_status == 0) {
      return Icon(
        FontAwesomeIcons.userClock,
        color: Colors.grey,
        size: 24,
      );
    } else if (fD.purchase_map_status == 1) {
      return Icon(
        FontAwesomeIcons.solidCheckCircle,
        color: Colors.green,
        size: 24,
      );
    } else if (fD.purchase_map_status == 2) {
      return Icon(
        FontAwesomeIcons.timesCircle,
        color: Colors.red,
        size: 24,
      );
    } else if (fD.purchase_map_status == 3 || fD.purchase_map_status == 4) {
      return Icon(
        FontAwesomeIcons.timesCircle,
        color: Colors.white,
        size: 24,
      );
    } else {
      return Icon(
        FontAwesomeIcons.userClock,
        color: Colors.grey,
        size: 24,
      );
    }
  }

  Future _selectDateInvoiceDate() async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(DateTime.now().year - 2),
      lastDate: new DateTime(DateTime.now().year + 2),
    );

    String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(picked);

    mainDate = picked;

    print(formattedDate);
    String date;

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        invoiceDate.text = date;
        paymentDueDate.text = "";
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }

  Widget _headingWidget(GetReviewTreeModelData topList) {
    if (topList.mod == 1) {
      return Text(
        "Moderator",
        style: TextStyle(
            color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanSemiBold'),
        textAlign: TextAlign.center,
      );
    } else if (topList.mod == 2) {
      return Text(
        "Admin Manager",
        style: TextStyle(
            color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanSemiBold'),
        textAlign: TextAlign.center,
      );
    } else {
      return Text(
        "Admin",
        style: TextStyle(
            color: Colors.black87, fontSize: h*0.018, fontFamily: 'SanSemiBold'),
        textAlign: TextAlign.center,
      );
    }
  }

  String treeString(GetReviewTreeModelData fD) {
    String dynamicText = "";

    if (fD.approver_id == "0") {
      dynamicText = "Expense";
    } else {
      dynamicText = "PO";
    }

    if(fD.user_id == fD.puserid){
      return "This $dynamicText has been created by ${fD.user_name ?? ""} ";
    } else if (fD.purchase_map_status == 0) {
      return "Your $dynamicText approval is pending from ${fD.user_name ?? ""}";
    } else if (fD.purchase_map_status == 1) {
      return (fD.approvedBy == "-" || fD.user_name == fD.approvedBy)
          ? "${fD.user_name ?? ""} has approved the $dynamicText"
          : "The $dynamicText has been approved by  ${fD.approvedBy} on behalf of ${fD.user_name} ";
    } else if (fD.purchase_map_status == 2) {
      return "${fD.user_name ?? ""} has rejected the $dynamicText";
    } else if (fD.purchase_map_status == 3) {
      return "Your $dynamicText has not yet reached to ${fD.user_name ?? ""} ";
    } else if (fD.purchase_map_status == 4) {
      return "${fD.approvedBy} for ${fD.user_name}";
    }
  }

  String treeSubString(GetReviewTreeModelData fD) {
    if (fD.reason != "-" || fD.reason != "") {
      return "${fD.reason ?? ""}";
    } else {
      return "-";
    }
  }

/*  String treeSubString(GetReviewTreeModelData fD) {
  if (fD.purchase_map_status == 2) {
      return "${fD.}";
    }
  }*/

  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              width: 200,
              child: Row(
                children: <Widget>[
                  CircularProgressIndicator(),
                  Padding(
                    padding: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w, top: topPad*4*h, bottom: topPad*4*h),
                  ),
                  Text('Loading...'),
                ],
              ),
            ),
          );
        }); //end showDialog()
  }

  _showVersionDialog(context, int i, GetReviewModelData data, double h, double w) async {
    await showDialog<String>(

      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return StatefulBuilder(
          key: signatureKey,
          builder: (context, setState) {
            return UpdatePopUp(context, i, data, setState, h, w);
          },
        );
      },
    );
  }

  final _poDescriptionController = TextEditingController();

  String textbody(int i) {
    if (i == 1) {
      return "Are you sure want to accept this!";
    } else if (i == 2) {
      return "Are you sure want to reject this!";
    } else if (i == 3) {
      return "Please provide the reason of accepting";
    } else {
      return "Are you sure want to update the changes you have made!";
    }
  }

  String texttitle(int i) {
    if (i == 1) {
      return "Accept";
    } else if (i == 2) {
      return "Reject";
    } else if (i == 3) {
      return "Budget Accept Reason";
    } else {
      return "Edit";
    }
  }

  Future<void> getImageFromPaint() async {
    ui.Image renderedImage = await rendered;
    ByteData pngBytes = await renderedImage.toByteData(format: ui.ImageByteFormat.png);
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(signatureColor != Colors.black) {
      String temp = signatureColor.toString().split("(").last.split(")").first;
      print(temp);
      int val = int.parse("$temp");
      _singleton.signColor = Color(val);
      prefs.setString("signColor", "$temp");
    }

    if(signStrokeWidth != 2.0) {
      prefs.setDouble("signStroke", signStrokeWidth);
      _singleton.signStrokeWidth = signStrokeWidth;
    }


    //Directory directory = await getExternalStorageDirectory();

    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path;
    print(path);
    await Directory('$path/$directoryName').create(recursive: true);
   File signatureFile = File('$path/$directoryName/${formattedDate()}.png');
       signatureFile.writeAsBytesSync(pngBytes.buffer.asInt8List());

    FormData formData = new FormData();

    String filePath = signatureFile.path;
    String fileName = signatureFile.path;

    Image image = Image.file(new File('$path/$directoryName/${formattedDate()}.png'));
    print(image);

    formData = new FormData.fromMap({
      'file[]': await MultipartFile.fromFile(filePath, filename: fileName)
    });

    Dio dio = new Dio()
      ..options.baseUrl = BASE_URL;
    dio.options.headers['authorization'] = prefs.getString('access_token');

    try {
      Response response = await dio.post(
        "multipleUploads?organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
        data: formData,
      );
      print(response.statusCode);
      StringBuffer sb = new StringBuffer();
      sb.write(response);

      var d = json.decode(sb.toString());
      imgres = ImageUploadRes.fromJson(d);
      print(imgres);

      if (imgres.Status ?? false) {
        _singleton.SignatureImage = imgres.data.image_path;
        await Directory('$path/$directoryName').delete(recursive: true);
        print("Image deleted");
      } else {
        final snackBar = SnackBar(
            duration: Duration(seconds: 1),
            content: Text(imgres.Message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } catch (err) {
      print("error: $err");
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Cannot load media."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

   // await Directory('$path/$directoryName').delete(recursive: true);
    print(renderedImage);
  }

  String formattedDate() {
    DateTime dateTime = DateTime.now();
    String dateTimeString = 'Signature_' +
        dateTime.year.toString() +
        dateTime.month.toString() +
        dateTime.day.toString() +
        dateTime.hour.toString() +
        ':' + dateTime.minute.toString() +
        ':' + dateTime.second.toString() +
        ':' + dateTime.millisecond.toString() +
        ':' + dateTime.microsecond.toString();
    return dateTimeString;
  }


  Widget UpdatePopUp(BuildContext context, int i, GetReviewModelData data, setState, double height, double width) {
    final w = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
          child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Material(
          color: Colors.white,
          elevation: 5.0,
          child: Container(
            width: w * .80,
//            padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
//            margin: EdgeInsets.only(left: 32,right: 32),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(
                  alignment: Alignment.centerRight,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      color: appColor,
                      height: h*0.058,
                      child: Center(
                          child: Text(
                        texttitle(i),
                        style: TextStyle(
                            fontSize: h*0.02117,
                            fontFamily: 'SanSemiBold',
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      )),
                    ),
                    Container(
                      width: w*0.1180,
                      child: FlatButton(
                        onPressed: () {
                          _points.clear();
                          setState(() {});
                          Navigator.pop(context);
                        },
                        child: SvgPicture.asset('assets/images/pop_close.svg',
                            height: h*0.016,
                            fit: BoxFit.cover,
                            color: Colors.white,
                            semanticsLabel: 'popup close'),
                      ),
                    )
                  ],
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(letfPad*4*w, topPad*2*h, letfPad*4*w, topPad*h*2),
                        child: Text(
                          textbody(i),
                          style:
                              TextStyle(fontSize: h*0.02117, fontFamily: 'SanMedium'),
                          textAlign: TextAlign.center,
                        ),
                      ),

                      SizedBox(height: h*0.0094,),

                      data.showSignaturePad ? Container(
                        width: w,
                        padding: EdgeInsets.only(left: letfPad*5*w),
                        child: Text("Add your Signature", style: TextStyle(fontSize: h*0.02117, fontWeight: FontWeight.w500),),
                      ) : Container(),

                      data.showSignaturePad ? Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(h*0.01)),
                        child: new Container(

                          width: width*0.8,
                          height: height*0.175,
                          child: GestureDetector(
                            onPanEnd: (DragEndDetails details) {
                              setState(() {
                                _points.add(null);
                              });
                            },
                            onPanUpdate: (DragUpdateDetails details) {
                              setState(() {
                                RenderBox object = context.findRenderObject();
                              //  Offset _localposition = object.localToGlobal(details.localPosition);
                                _points = new List.from(_points)..add(details.localPosition);
                              });
                            },
                            child: CustomPaint(
                              painter: new DrawingPaint(points: _points, totalWidth: width*0.8, totalHeight: height*0.175, signColor: signatureColor, signStrokeWidth : signStrokeWidth),
                            ),
                          ),
                        ),
                      ) : Container(),

                      data.showSignaturePad ? SizedBox(height: 8.0,) : Container(),

                      data.showSignaturePad ? Container(
                          key: paintKey,
                          padding: EdgeInsets.only(right: letfPad*3*w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(),
                              PopupMenuButton(
                               onSelected: (i){
                                 print(i);
                                 if(i == 3) {
                                   _points.clear();
                                   setState(() {});
                                 } else if(i == 1) {
                                   showDialog(
                                     context: context,
                                     child: AlertDialog(
                                       title: const Text('Pick a color!'),
                                       content: SingleChildScrollView(
                                         child: ColorPicker(
                                           pickerColor: signatureColor,
                                           onColorChanged: (color){
                                             print(color);
                                             signatureColor = color;
                                             setState(() {});
                                           },
                                           showLabel: true,
                                           pickerAreaHeightPercent: 0.8,
                                         ),
                                         // Use Material color picker:
                                         //
                                         // child: MaterialPicker(
                                         //   pickerColor: pickerColor,
                                         //   onColorChanged: changeColor,
                                         //   showLabel: true, // only on portrait mode
                                         // ),
                                         //
                                         // Use Block color picker:
                                         //
                                         // child: BlockPicker(
                                         //   pickerColor: currentColor,
                                         //   onColorChanged: changeColor,
                                         // ),
                                       ),
                                       actions: <Widget>[
                                         FlatButton(
                                           child: const Text('Got it'),
                                           onPressed: () {
                                             Navigator.pop(context);
                                           },
                                         ),
                                       ],
                                     ),
                                   );
                                 } else if(i == 2) {
                                   showDialog(
                                     context: context,
                                     child: AlertDialog(
                                       title: const Text('Pick size!'),
                                       content: SingleChildScrollView(
                                         child: Column(
                                           children: [3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0].map((e) {
                                             return Column(
                                               children: [
                                                 InkWell(
                                                   onTap: (){
                                                      print(e);
                                                      signStrokeWidth = e;
                                                      setState(() {});
                                                      Navigator.pop(context);
                                                   },
                                                   child: Text("$e"),
                                                 ),
                                                 Divider(),
                                               ],
                                             );
                                           }).toList(),
                                         )
                                         // Use Material color picker:
                                         //
                                         // child: MaterialPicker(
                                         //   pickerColor: pickerColor,
                                         //   onColorChanged: changeColor,
                                         //   showLabel: true, // only on portrait mode
                                         // ),
                                         //
                                         // Use Block color picker:
                                         //
                                         // child: BlockPicker(
                                         //   pickerColor: currentColor,
                                         //   onColorChanged: changeColor,
                                         // ),
                                       ),
                                       // actions: <Widget>[
                                       //   FlatButton(
                                       //     child: const Text('Apply'),
                                       //     onPressed: () {
                                       //       Navigator.pop(context);
                                       //     },
                                       //   ),
                                       // ],
                                     ),
                                   );
                                 }
                               },
                               child: Container(
                                   width: 100,
                                   height: 40,
                                   alignment: Alignment.center,
                                   decoration: BoxDecoration(
                                     color: appColor,
                                   ),
                                   child: Text("Options", style: TextStyle(color: Colors.white),)),
                               itemBuilder: (context) {
                                 var list = List<PopupMenuEntry<Object>>();
                                 list.add(
                                   PopupMenuItem(
                                     child: Text("Change Color"),
                                     value: 1,
                                   ),
                                 );
                                 list.add(
                                   PopupMenuDivider(
                                     height: 10,
                                   ),
                                 );
                                 list.add(
                                   PopupMenuItem(
                                     child: Text("Change Width"),
                                     value: 2,
                                   ),
                                 );
                                 list.add(
                                   PopupMenuDivider(
                                     height: 10,
                                   ),
                                 );
                                 list.add(
                                   PopupMenuItem(
                                     child: Text("Clear"),
                                     value: 3,
                                   ),
                                 );
                           return list;
                         },

                       ),
                              // RaisedButton(
                              //   color: appColor,
                              //   onPressed: (){
                              //     setState(() {
                              //       _points.clear();
                              //     });
                              //   },
                              //   child: Row(
                              //     children: [
                              //       Text("Options", style: TextStyle(color: Colors.white),),
                              //       SizedBox(width: w*0.037,),
                              //       Icon(Icons.refresh, color: Colors.white,)
                              //     ],
                              //   ),
                              // ),
                            ],
                          )
                      ) : Container(),
                      data.showSignaturePad ? SizedBox(height: 8.0,) : Container(),

                      i == 1
                          ? Padding(
                              padding: EdgeInsets.only(left: letfPad*3*w, right: letfPad*3*w),
                              child: new Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(h*0.01)),
                                child: new Container(
                                    padding: EdgeInsets.all(0),
                                    margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                                    height: h*0.1176,
                                    child: new Form(
//                                  key: _formKeyScreen1,
                                      child: TextField(
                                        textInputAction: TextInputAction.go,
                                        maxLines: 4,
                                        controller: _poDescriptionController,
                                        onEditingComplete: () {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        onSubmitted: (l) {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Reason for accepting...',
                                        ),
                                      ),
                                    )),
                              ),
                            )
                          : Container(),

                      i == 2
                          ? Padding(
                              padding: EdgeInsets.only(left: letfPad*3*w, right: letfPad*3*w),
                              child: new Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(h*0.01)),
                                child: new Container(
                                    padding: EdgeInsets.all(0),
                                    margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                                    height: h*0.1176,
                                    child: new Form(
//                                  key: _formKeyScreen1,
                                      child: TextField(
                                        textInputAction: TextInputAction.go,
                                        maxLines: 4,
                                        controller: _poDescriptionController,
                                        onEditingComplete: () {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        onSubmitted: (l) {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Reason for rejection...',
                                        ),
                                      ),
                                    )),
                              ),
                            )
                          : Container(),
                      i == 3
                          ? Padding(
                              padding: EdgeInsets.only(left: letfPad*3*w, right: letfPad*3*w),
                              child: new Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(h*0.01)),
                                child: new Container(
                                    padding: EdgeInsets.all(0),
                                    margin: EdgeInsets.only(left: letfPad*4*w, right: letfPad*4*w),
                                    height: h*0.1176,
                                    child: new Form(
//                                  key: _formKeyScreen1,
                                      child: TextField(
                                        textInputAction: TextInputAction.go,
                                        maxLines: 4,
                                        controller: _poDescriptionController,
                                        onEditingComplete: () {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        onSubmitted: (l) {
                                          FocusScopeNode currentFocus = FocusScope.of(context);

                                          if (!currentFocus.hasPrimaryFocus) {
                                            currentFocus.unfocus();
                                          }
                                        },
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Reason for accepting...',
                                        ),
                                      ),
                                    )),
                              ),
                            )
                          : Container(),

                      SizedBox(
                        height: h*0.0094,
                      ),

                      CustomBtn(
                        title: "submit",
                        onTap: () async {

                          bool con  = await mCH.checkConnection();

                          if(con) {
                            if (data.showSignaturePad) {
                              //_showLoading(context);
                              await getImageFromPaint();
                             // Navigator.of(context).pop();
                            }
                            if (i == 1) {
                              _acceptPo(context);
                            } else if (i == 2) {
                              _rejectPo(context);
                            } else if (i == 3) {
                              _acceptOverBudgetPo(context);
                            } else {
                              _editPo(context, data);
                            }
                          } else {
                            Navigator.pop(context);
                            final snackBar = SnackBar(
                                duration: Duration(seconds: 1),
                                content: Text("Please check your internet connection"));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          }
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      )),
    );
  }

  bool isProjectEdited = false;
  bool isCategoryEdited = false;
  bool isCountryEdited = false;
  bool isCityEdited = false;

  void _openBottomSheetCategory(BuildContext context) async {
    await getBottomSheetCategory(context);
    Autoselect.subCategory(
      tabIndex,
      categoryList,
      catId,
      _cateC,
      _subCatC,
      (value) {
        subCatId = value;
      },
      setState,
    );
  }

  Future<void> getBottomSheetCategory(BuildContext context) async {
    var results;

    if (tabIndex == 1) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              projectList: projectList,
              tabIndex: 1,
            );
          });
    } else if (tabIndex == 2) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 2,
            );
          });
    } else if (tabIndex == 3) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              countryList: countryList,
              tabIndex: 3,
            );
          });
    } else if (tabIndex == 4) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              cityList: cityList,
              tabIndex: 4,
            );
          });
    } else if (tabIndex == 7) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return CategorySheet(
              categoryList: categoryList,
              tabIndex: 7,
              catId: catId,
            );
          });
    } else if (tabIndex == 10) {
      results = await showModalBottomSheet(
          context: context,
          builder: (context) {
            return DynamicSheet(
              shootDataList: shootLocationList,
              tabIndex: 10,
            );
          });
    }

    if (results != null) {
      if (results.containsKey('approver_list')) {
        print(results['approver_list']);

        if (tabIndex == 1) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _projC.text = results['approver_list'][j].project_name;
              projectId = results['approver_list'][j].project_id;
              results['approver_list'][j].isSelected = false;
              _getCategoryList();
              _cateC.clear();
              _subCatC.clear();
              categoryList.clear();
            }
          }
        } else if (tabIndex == 2) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _cateC.text = results['approver_list'][j].category_name;
              catId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
              _filter7 = 1;
              _subCatC.clear();
              subCatId = 0;
            }
          }
          /*  _cateC.text = results['approver_list'].category_name;
          catId = results['approver_list'].category_id;

          results['approver_list'].isSelected = false;
          results['approver_list'].selectedIndex = -1;*/
        } else if (tabIndex == 3) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _counC.text = results['approver_list'][j].country_name;
              countryId = results['approver_list'][j].country_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 4) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _cityC.text = results['approver_list'][j].city_name;
              cityId = results['approver_list'][j].city_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 7) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _subCatC.text = results['approver_list'][j].category_name;
              subCatId = results['approver_list'][j].category_id;
              results['approver_list'][j].isSelected = false;
            }
          }
        } else if (tabIndex == 10) {
          for (int j = 0; j < results['approver_list'].length; j++) {
            if (results['approver_list'][j].isSelected) {
              _shootLocation.text = results['approver_list'][j].location_name;
              location_id = results['approver_list'][j].location_id;
              results['approver_list'][j].isSelected = false;
              print(location_id);
            }
          }
        }

        setState(() {});
      }
    }
  }

  Future _selectDatePaymentDueDate(String invoiceData) async {

    DateTime picked = await showDatePicker(
      context: context,
      initialDate: mainDate,
      firstDate: mainDate,
      lastDate: new DateTime(2050),
    );


    String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(picked);

    print(formattedDate);
    String date;

    if (formattedDate != null) {
      print(formattedDate);
      setState(() {
        date = formattedDate.toString().split(" ").first;
        paymentDueDate.text = date;
      });
    }
//    if(picked != null) setState(() => date = picked.toString().split(" ").first);
  }

  callProjectAPI() async {
    projectData = await homeApiObj.fetchProjectList(context);
    if (projectData.Status ?? false) {
      projectList = projectData.data;

      for (int j = 0; j < projectList.length; j++) {
        pNameList.add(projectList[j].project_name);
      }
      _filter1 = 1;
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Please wait loading data..."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callShootLocation() async {
    shootLocationData = await cVM.getAllShootlocationData();
    if(shootLocationData.Status ?? false) {
      _filter10 = 1;
      shootLocationList = shootLocationData.data;
      print("Data is");
      setState(() {

      });
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("${shootLocationData.Message}"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callCountryAPI() async {
    countryData = await cVM.getAllCountry();
    if (countryData.Status ?? false) {
      countryList = countryData.data;
      for (int j = 0; j < countryList.length; j++) {
        countNameList.add(countryList[j].country_name);
      }
      _filter3 = 1;
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Please wait loading data..."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callCurrencyAPI() async {
    currencyData = await cVM.getAllCurrency();
    if (currencyData.Status ?? false) {
      currencyList = currencyData.data;

      for (int j = 0; j < currencyList.length; j++) {
        currNameList.add(currencyList[j].currency_code);
      }
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Please wait loading data..."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  callGSTAPI() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');
    print(accessToken);
    print(prefs.getInt('orgId'));


    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    final res = await http.get("${BASE_URL}master?organization_id=${prefs.getInt('orgId')}", headers: Headers);
    if (res.statusCode == 200 || res.statusCode == 201) {
      final j_data = json.decode(res.body);
      gstData = j_data;
      print(gstData.runtimeType);
      for(int i=0; i<gstData['data']['gst'].length; i++) {
        gstName.add(gstData['data']['gst'][i]['gst_name']);

        gstObj.add({
          "name" : gstData['data']['gst'][i]['gst_name'],
          "id" : gstData['data']['gst'][i]['gst_id'],
          "percent" : double.parse("${gstData['data']['gst'][i]['gst_percentage']}")
        });
      }

      payModeData = [];
      paymentIdList = [];
      for(int j = 0; j<gstData['data']['payment_mode'].length; j++) {
        payModeData.add("${gstData['data']['payment_mode'][j]['payment_mode_name']}");
        paymentIdList.add(int.parse("${gstData['data']['payment_mode'][j]['payment_mode_id']}"));
      }

      priorityData = [];
      priorityId = [];
      for(int k = 0; k<gstData['data']['priority'].length; k++) {
        priorityData.add(gstData['data']['priority'][k]['priority_name']);
        priorityId.add(int.parse("${gstData['data']['priority'][k]['priority_id']}"));
      }
      _filter9 = 1;

      // for(int j = 0; j<gstData['data']['payment_mode'].length; j++) {
      //   payModeObj.add({
      //     "payId" : gstData['data']['payment_mode'][j]['payment_mode_id'],
      //     "payName" : gstData['data']['payment_mode'][j]['payment_mode_name']
      //   });
      // }
      // for(int k = 0; k<gstData['data']['priority'].length; k++) {
      //   prioObj.add({
      //     "prioId" : gstData['data']['priority'][k]['priority_id'],
      //     "prioName" : gstData['data']['priority'][k]['priority_name'],
      //   });
      // }
      //
      // print(payModeObj);
      // print(prioObj);
    }
    else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Something went wrong!"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  _getAllApiData() async {
    // get all project list

    callProjectAPI();

    callCountryAPI();

    callCurrencyAPI();

    callShootLocation();

    callGSTAPI();
    // get all currency list


    // get all country list


    // // get all city list
    // cityData = await cVM.getAllCity();
    // if (cityData.Status ?? false) {
    //   cityList = cityData.data;
    //   for (int j = 0; j < cityList.length; j++) {
    //     cityNameList.add(cityList[j].city_name);
    //   }
    //   _filter4 = 1;
    // } else {
    //   final snackBar = SnackBar(
    //       duration: Duration(seconds: 1),
    //       content: Text("Please wait loading data..."));
    //   _scaffoldKey.currentState.showSnackBar(snackBar);
    // }

    //GetGstData

  }

  _getCategoryList() async {
    // get all category list
    categoryData = await cVM.getAllCategory(projectId);
    if (categoryData.Status ?? false) {
      categoryList = categoryData.data;

      for (int j = 0; j < categoryList.length; j++) {
        cNameList.add(categoryList[j].category_name);
      }
      _filter2 = 1;
      _filter7 = 1;
      setState(() {});
    } else {
      final snackBar = SnackBar(
          duration: Duration(seconds: 1),
          content: Text("Please wait loading data..."));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void showBudgetExceedingPopup(GetReviewModel d, BuildContext context) {
    showDialog<void>(
      context: context, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Budget Exceeded'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('The amount is exceeding the Bugdet!'),
              ],
            ),
          ),
        );
      },
    );
  }
}


class DrawingPaint extends CustomPainter {
  List<Offset> points;
  double totalWidth;
  double totalHeight;
  Color signColor;
  double signStrokeWidth;

  DrawingPaint({this.points, this.totalWidth, this.totalHeight, this.signColor, this.signStrokeWidth});

  @override
  void paint(Canvas canvas, Size size) {

    Paint paint = new Paint();
    paint.color = signColor;
    paint.strokeCap = StrokeCap.round;
    paint.strokeWidth = signStrokeWidth;

    double x, y;

    for(int i=0; i<points.length-1; i++) {
      if(points[i] != null && points[i+1] != null) {
        if(points[i].dx > 0 && points[i].dy > 0) {
//        x = points[i].dx;
//        y = points[i].dy;
//        if(x < 0) {
//          x = x * (-1);
//        }
//        if(y < 0) {
//          y = y * (-1);
//        }
          if ( points[i].dx < totalWidth) {
            if (points[i].dy < totalHeight) {
              canvas.drawLine(points[i], points[i + 1], paint);
            }
          }
        }


      }
    }

  }

  @override
  bool shouldRepaint(DrawingPaint oldDelegate) => oldDelegate.points != points;
}


