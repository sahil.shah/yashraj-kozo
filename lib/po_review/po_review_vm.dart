import 'dart:convert';

import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PoReviewVM {
  ConnectionHelper mCH;

  PoReviewVM() {
    mCH = ConnectionHelper.getInstance();
  }

  AppSingleton _singleton = AppSingleton();

  Future<GetReviewModel> getReviewData(int pId, BuildContext context) async {
    GetReviewModel res_d;

    // bool con = true;

    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    print("Purchase Id $pId");
    print(prefs.getInt('orgId'));
    print(accessToken);

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    print("URL is:- $BASE_URL/purchase/$pId?owner=0&organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}");
    if (con) {
      try {
        final res = await http.get(
            "${BASE_URL}purchase/$pId?owner=0&organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = GetReviewModel.fromJson(j_data);
            if(res_d.data.assignData == null) {
              res_d.data.assignData = true;
            }
            print(res_d);
            return res_d;

          case 401:
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return GetReviewModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetReviewModel.buildErr(0,
            message: "${err.toString()}");
      }
    } else {
      return GetReviewModel.buildErr(1);
    }
  }

  Future<SetFavouriteModel> acceptPo(
      int pMapId, BuildContext context, String reason) async {
    SetFavouriteModel res_d;

    //bool con = true;

    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    if (con) {
      try {
        var body = json.encode({
          "reason": reason ?? "-",
          "signatureURL" : _singleton.SignatureImage,
        });

        final res = await http.post(
            "${BASE_URL}purchase/approve/$pMapId?organization_id=${prefs.getInt('orgId')}",
            body: body,
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;
            break;

          case 401:
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SetFavouriteModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }

  Future<SetFavouriteModel> editPo(
      int purchaseId, String body, BuildContext context) async {
    SetFavouriteModel res_d;

    //bool con = true;
    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    print("PID = $purchaseId");
    if (con) {
      try {
        final res = await http.put(
            "${BASE_URL}purchase/$purchaseId?organization_id=${prefs.getInt('orgId')}",
            body: body,
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;
            break;
          case 401:
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;
          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SetFavouriteModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }

  Future<SetFavouriteModel> rejectPo(
      int pMapId, String text, BuildContext context) async {
    SetFavouriteModel res_d;

    //bool con = true;
    bool con = await mCH.checkConnection();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type": "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    var body = json.encode({
      "reason": text ?? "-",
      "signatureURL" : _singleton.SignatureImage,
    });
    if (con) {
      try {
        final res = await http.post(
            "${BASE_URL}purchase/reject/$pMapId?organization_id=${prefs.getInt('orgId')}",
            body: body,
            headers: Headers);

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);
            print('Res ---> ${res.body}');
            res_d = SetFavouriteModel.fromJson(j_data);

            return res_d;
            break;

          case 401:
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return SetFavouriteModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SetFavouriteModel.buildErr(0,
            message: "Something went wrong. Please try again later.");
      }
    }
  }
}
