import 'dart:async';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Project_Details/project_details.dart';
import 'package:yashrajkozo/search_page/search_page_vm.dart';
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/loader.dart';

class DSearch extends StatefulWidget {
  @override
  _DSearchState createState() => _DSearchState();
}

class _DSearchState extends State<DSearch> {
  SearchVM sVM;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<SearchModel> mF;

  final _searchQuery = new TextEditingController();
  Timer _debounce;
  AppSingleton _singleton = AppSingleton();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sVM = SearchVM();
//    mF = sVM.initSearch("");
    _searchQuery.addListener(_onSearch);
  }

  @override
  void dispose() {
    _searchQuery.removeListener(_onSearch);
    _searchQuery.dispose();
    super.dispose();
  }

  String displayText = "";
  String s_q = "";

  SearchModel _d;

  final _s_c = TextEditingController();

  _onSearch() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      // do something with _searchQuery.text
      if (_searchQuery.text.isNotEmpty && s_q != _searchQuery.text) {
        s_q = _searchQuery.text;
        setState(() {
          _d.Status = false;
          _d.is_loading = true;
//          mF = sVM.initSearch(_searchQuery.text);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        initialData: null,
        future: mF,
        builder: (context, snap) => _checkAPIData(context, snap));
  }

  Widget _checkAPIData(BuildContext c, AsyncSnapshot snap) {
    //ConnectionState.active = snap.connectionState
    if (snap.hasData) {
      // API
      // 404
      // catch

      _d = snap.data;

      if (_d.Status ?? false) {
        // API true
        return _buildList(_d, 1);
      } else if (_d.is_loading ?? false) {
        //when clicking retry
        return Loader(title: "");
      } else {
        return _buildList(_d, 2);
      }
    } else {
      // initial loading
      return Loader(title: "");
    }
  }

  Widget _buildList(SearchModel d, int i) {
    List<SearchData> sM = d.data;

    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: TextField(
            controller: _searchQuery,
//          focusNode: widget.delegate._focusNode,
//          style: theme.textTheme.title,
            textInputAction: TextInputAction.search,
            style: TextStyle(color: Colors.white, fontSize: 20),
            onSubmitted: (String _) {
              _onSearch();
//                _initSearch(context);
            },
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Search by Project Name",
                hintStyle: TextStyle(color: Colors.white),
                fillColor: Colors.white),
          ),
          backgroundColor: Colors.pink,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                _s_c.text = "";
                _searchQuery.text = "";
              },
            )
          ],
        ),
        body: new Container(
          constraints: BoxConstraints(minHeight: h),
          padding: EdgeInsets.only(
            top: 32,
          ),
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: i == 1
                  ? ListView.builder(
                      shrinkWrap: true,
                      itemCount: sM.length,
                      itemBuilder: (context, i) {
                        SearchData fData = sM[i];
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    settings:
                                        RouteSettings(name: '/project_detail'),
                                    pageBuilder: (c, a1, a2) => ProjectDetails(
                                        imageUrl: fData.project_icon,
                                        projectID: fData.project_id.toString(),
                                        projectName: fData.project_name,
                                        projectDetails: fData.project_details),
                                    transitionsBuilder: (c, anim, a2, child) =>
                                        FadeTransition(
                                            opacity: anim, child: child),
                                    transitionDuration:
                                        Duration(milliseconds: 500),
                                  ),
                                );
                              },
                              child: Container(
                                padding: EdgeInsets.only(top: 8),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                  ),
                                  elevation: 2,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width: w,
                                        height: 160,
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(h*0.0141),
                                                topLeft: Radius.circular(h*0.0141)),
                                            child: FadeInImage(
                                              fit: BoxFit.cover,
                                              placeholder: AssetImage(
                                                  "assets/images/bg.png"),
                                              image: NetworkImage("${fData.project_icon}?token=${_singleton.tempToken}"),
                                            )),
                                      ),
                                      Container(
                                          padding: EdgeInsets.all(6.0),
                                          child: Text(
                                            fData.project_name,
                                            style: TextStyle(
                                                fontFamily: 'SanBold'),
                                          )),
                                      Container(
                                          padding: EdgeInsets.only(left: 6.0),
                                          child: Text(
                                            fData.company_name,
                                            style: TextStyle(
                                                fontFamily: 'SanMedium'),
                                          )),
                                      Container(
                                          padding: EdgeInsets.fromLTRB(
                                              6.0, 6, 6, 16),
                                          child: Text(
                                            fData.project_details,
                                            style: TextStyle(
                                                fontFamily: 'SanRegular'),
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                            )

                            /* GestureDetector(
                              onTap: () {
                               */ /* Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        settings: RouteSettings(
                                            name: '/movie_detail'),
                                        builder: (context) => MovieDetail(
                                              formatId: sM[i].value,
                                              movieId: sM[i].MovieID,
                                            )));*/ /*
                              },
                              child: Container(
                                  padding:
                                      EdgeInsets.only(top: 16, bottom: 16),
                                  */ /* color: Colors.yellow,*/ /*
                                  child: Text(
                                    fData.project_name,
                                    style: TextStyle(
                                        color: Colors.yellow,
                                        fontFamily: 'SanMedium',
                                        fontSize: 16),
                                  )),
                            ),*/
                          ],
                        );
                      })
                  : Center(
                      child: Text(
                        "No matching results found!",
                        style: TextStyle(
                            color: Colors.green,
                            fontFamily: 'SanMedium',
                            fontSize: 16),
                      ),
                    )),
        ));
  }


  void _showLoading(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
              backgroundColor: Colors.transparent,
              //contentPadding: const EdgeInsets.only(left : 16, right: 0, top: 10, bottom: 10),
              content: Center(
                child: Container(
                  width: 48,
                  height: 48,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                ),
              ));
        }); //end showDialog()
  }
}
