import 'package:yashrajkozo/Login/login.dart';
import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:yashrajkozo/utils/app_models.dart';
import 'package:yashrajkozo/utils/check_connection.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SearchVM {
  ConnectionHelper mCH;

  SearchVM() {
    mCH = ConnectionHelper.getInstance();
  }

  int languageId;
  int countryId;

  AppSingleton _singleton = AppSingleton();

  //@route    /LoginModel/
  //@desc     post LoginModel
  //@access   public


  Future<GetProjectModel> initSearch(String text, BuildContext context) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    GetProjectModel res_d;

//    bool con = true;
    bool con = await mCH.checkConnection();

    if (con) {
      try {
        print('URL ---> "${BASE_URL}project?search=$text"');
        //print('BODY ---> ${req_d}');

        final res = await http.get(
          "${BASE_URL}project?search=$text&organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
          headers: Headers,
        );

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);

            res_d = GetProjectModel.fromJson(j_data);
            print('Res ---> ${res.body}');

            print(res_d);

            return res_d;

          case 401 :
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                settings: RouteSettings(name: '/login'),
                pageBuilder: (c, a1, a2) => LoginPage(),
                transitionsBuilder: (c, anim, a2, child) =>
                    FadeTransition(opacity: anim, child: child),
                transitionDuration: Duration(milliseconds: 500),
              ),
            );
            break;

          default:
            return GetProjectModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetProjectModel.buildErr(0, message: err.toString());
      }
    } else {
      return GetProjectModel.buildErr(1);
    }
  }


  Future<GetMyPoModel> initMyPOSearch(String text) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    GetMyPoModel res_d;
    bool con = await mCH.checkConnection();
//    bool con = true;
    if (con) {
      try {
        print('URL ---> "${BASE_URL}purchase?search=$text"');
        //print('BODY ---> ${req_d}');

        final res = await http.get(
          "${BASE_URL}purchase?search=$text&organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
          headers: Headers,
        );

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);

            res_d = GetMyPoModel.fromJson(j_data);
            print('Res ---> ${res.body}');

            print(res_d);

            return res_d;
          default:
            return GetMyPoModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetMyPoModel.buildErr(0, message: err.toString());
      }
    } else {
      return GetMyPoModel.buildErr(1);
    }
  }

  Future<GetUserPoModel> initUserPOSearch(String text) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };

    GetUserPoModel res_d;
    bool con = await mCH.checkConnection();

    //bool con = true;
    if (con) {
      try {
        print('URL ---> "${BASE_URL}purchase/other?search=$text"');
        //print('BODY ---> ${req_d}');

        final res = await http.get(
          "${BASE_URL}purchase/other?search=$text&organization_id=${prefs.getInt('orgId')}&token=${prefs.getString('temp_token')}",
          headers: Headers,
        );

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);

            res_d = GetUserPoModel.fromJson(j_data);
            print('Res ---> ${res.body}');

            print(res_d);

            return res_d;
          default:
            return GetUserPoModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return GetUserPoModel.buildErr(0, message: err.toString());
      }
    } else {
      return GetUserPoModel.buildErr(1);
    }
  }



  initCategorySearch(String text, int id, int projectId) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token');

    var Headers = {
      "Content-type" : "application/json",
      "Accept": "application/json",
      "authorization": accessToken ?? ""
    };
    SelectCategoryModel res_d;
      bool con = await mCH.checkConnection();
    //bool con = true;
    if (con) {
      try {
        print('URL ---> "${BASE_URL}category/singleDataByParent/$id/$projectId?search=$text"');
        //print('BODY ---> ${req_d}');

        final res = await http.get(
          "${BASE_URL}category/singleDataByParent/$id/$projectId?search=$text&organization_id=${prefs.getInt('orgId')}",
          headers: Headers,
        );

        switch (res.statusCode) {
          case 200:
          case 201:
            final j_data = json.decode(res.body);

            res_d = SelectCategoryModel.fromJson(j_data);
            print('Res ---> ${res.body}');

            print(res_d);

            return res_d;
          default:
            return SelectCategoryModel.buildErr(res.statusCode);
        }
      } catch (err) {
        return SelectCategoryModel.buildErr(0, message: "Somthing went wrong. Please try again later.");
      }
    } else {
      return SelectCategoryModel.buildErr(1);
    }
  }

}
