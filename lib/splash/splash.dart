import 'dart:async';

import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:yashrajkozo/Home/home.dart';
import 'package:yashrajkozo/Login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash>  with SingleTickerProviderStateMixin{

  bool navigation = false;

  final String assetName = 'assets/images/newLogo.png';

  Animation<double> _animation;
  AnimationController _controller;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  AppSingleton _singleton = AppSingleton();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sortNavigationForFirstPage();
    _firebaseToken();

/*    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));

    _animation = Tween<double>(begin: 20.0, end: 200.0).animate(_controller);

    _animation.addListener(() {
      setState(() {});
    });
    _animation.addStatusListener((status) => print(status));
    _controller.forward();*/

  }

  void _firebaseToken() async {
    _firebaseMessaging.getToken().then((token) async {
      print(token);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('firebase_token', token);

    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
//    _controller.dispose();
    super.dispose();
  }

  sortNavigationForFirstPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getString("access_token") != null) {
      if(prefs.getString("access_token").isNotEmpty) {
        setState(() {
          navigation = true;
        });
      }
    }

    Future.delayed(Duration(seconds: 2), () {

      if(navigation) {

        _singleton.userName = prefs.getString('userName');
        _singleton.userId = prefs.getInt('userId');
        _singleton.access_token = prefs.getString('access_token');

        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/home'),
            pageBuilder: (c, a1, a2) => MyHomePage(
              isForNotification: 1,
            ),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );

      }else {
        Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            settings: RouteSettings(name: '/login'),
            pageBuilder: (c, a1, a2) => LoginPage(),
            transitionsBuilder: (c, anim, a2, child) =>
                FadeTransition(opacity: anim, child: child),
            transitionDuration: Duration(milliseconds: 500),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    final h = MediaQuery.of(context).size.height;
    return Container(
        color: Colors.white,
        child: Center(
          child: Image.asset(assetName, fit: BoxFit.cover,height: h*0.2,),
        )
    );
  }

}