import 'package:json_annotation/json_annotation.dart';

part 'app_models.g.dart';

@JsonSerializable()
class Base {
  bool Status;
  String Message;
  bool is_loading = false;

  Base({this.Status, this.Message, this.is_loading});

  Base buildError(int Errcode, {String message}) {
    var _h = Base()
      ..Status = false
      ..is_loading = false;
    switch (Errcode) {
      case 0:
        //Failed to connect server Error Details:
        return _h..Message = '${message}';
      case 1:
        return _h..Message = 'Check your internet connection';
      case 2:
        return _h..Message = 'Row and Columns of the seats are not equal';
      default:
        return _h..Message = 'HTTP: Status Code ${Errcode}';
    }
  }
}

@JsonSerializable()
class LoginModel extends Base {
  String Message;
  bool Status;
  LoginDataObj data;
  bool is_loading = false;

  LoginModel({this.data, this.Message, this.Status});

  factory LoginModel.fromJson(Map<String, dynamic> json) =>
      _$LoginModelFromJson(json);

  static LoginModel buildErr(int Errcode, {String message}) {
    LoginModel res_d = LoginModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class LoginDataObj {
  String access_token;
  String user_name;
  String token_type;
  String temp_token;

  LoginDataObj({this.access_token, this.token_type, this.user_name, this.temp_token});

  factory LoginDataObj.fromJson(Map<String, dynamic> json) =>
      _$LoginDataObjFromJson(json);
}

//HomePageAPI
@JsonSerializable()
class GetProjectModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<ProjectModelData> data;

  GetProjectModel({this.Status, this.Message, this.data});

  factory GetProjectModel.fromJson(Map<String, dynamic> json) =>
      _$GetProjectModelFromJson(json);

  static GetProjectModel buildErr(int Errcode, {String message}) {
    GetProjectModel res_d = GetProjectModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class ProjectModelData extends Object {
  int project_id;
  String project_name;
  String project_icon;
  String project_details;
  int company_id;
  String company_name;
  int moderation;
  int moderator_user_id;
  int finance_user_id;
  int project_active;
  int favourite;
  bool isSelected;
  bool is_loading = false;

  ProjectModelData(
      {this.project_id,
      this.project_name,
      this.project_icon,
      this.isSelected = false,
      this.project_details,
      this.company_id,
      this.company_name,
      this.moderation,
      this.moderator_user_id,
      this.finance_user_id,
      this.project_active,
      this.favourite});

  factory ProjectModelData.fromJson(Map<String, dynamic> json) =>
      _$ProjectModelDataFromJson(json);
}

//Project Detail API
@JsonSerializable()
class ProjectDetailModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<ProjectDetailModelData> data;

  ProjectDetailModel({this.Status, this.Message, this.data});

  factory ProjectDetailModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailModelFromJson(json);

  static ProjectDetailModel buildErr(int Errcode, {String message}) {
    ProjectDetailModel res_d = ProjectDetailModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class ProjectDetailModelData {
  int project_id;
  String project_name;
  String project_icon;
  String project_details;
  int company_id;
  String company_name;
  int moderation;
  int moderator_user_id;
  int finance_user_id;
  int project_active;
  int favourite;

  ProjectDetailModelData(
      {this.project_id,
      this.project_name,
      this.project_icon,
      this.project_details,
      this.company_id,
      this.company_name,
      this.moderation,
      this.moderator_user_id,
      this.finance_user_id,
      this.project_active,
      this.favourite});

  factory ProjectDetailModelData.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailModelDataFromJson(json);
}

@JsonSerializable()
class ProjectDetailTreeModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<ProjectDetailTreeModelData> data;

  ProjectDetailTreeModel({this.Status, this.Message, this.data});

  factory ProjectDetailTreeModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailTreeModelFromJson(json);

  static ProjectDetailTreeModel buildErr(int Errcode, {String message}) {
    ProjectDetailTreeModel res_d = ProjectDetailTreeModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class ProjectDetailTreeModelData {
  int map_id;
  int user_id;
  String user_name;
  String user_pic;
  String user_status;
  int projec_id;
  String project_name;
  int user_level;
  int map_active;

  ProjectDetailTreeModelData(
      {this.map_id,
      this.user_id,
      this.user_name,
      this.user_pic,
      this.user_status,
      this.projec_id,
      this.project_name,
      this.user_level,
      this.map_active});

  factory ProjectDetailTreeModelData.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailTreeModelDataFromJson(json);
}

//notification page
@JsonSerializable()
class GetNotificationModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<GetNotificationModelData> data;

  GetNotificationModel({this.Status, this.Message, this.data});

  factory GetNotificationModel.fromJson(Map<String, dynamic> json) =>
      _$GetNotificationModelFromJson(json);

  static GetNotificationModel buildErr(int Errcode, {String message}) {
    GetNotificationModel res_d = GetNotificationModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetNotificationModelData {
  int notification_id;
  int project_id;
  String project_name;
  int user_id;
  int purchase_id;
  int pmap_id;
  int approver_id;
  int moderator_user_id;
  int adminManager_user_id;
  int isGraph;
  String user_name;
  String notification_icon;
  String notification_title;
  String notification_msg;
  String created_date;
  int notification_active;
  int isBudget;
  int budgetLog_id;

  GetNotificationModelData(
      {this.notification_id,
      this.project_id,
      this.created_date,
      this.project_name,
      this.user_id,
      this.user_name,
      this.notification_icon,
      this.notification_msg,
      this.notification_active,
      this.purchase_id,
      this.notification_title,
      this.isGraph,
      this.approver_id,
      this.adminManager_user_id,
      this.moderator_user_id,
      this.budgetLog_id,
      this.isBudget,
      this.pmap_id});

  factory GetNotificationModelData.fromJson(Map<String, dynamic> json) =>
      _$GetNotificationModelDataFromJson(json);
}

// my po list
@JsonSerializable()
class GetMyPoModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<GetMyPoModelData> data;

  GetMyPoModel({this.Status, this.Message, this.data});

  factory GetMyPoModel.fromJson(Map<String, dynamic> json) =>
      _$GetMyPoModelFromJson(json);

  static GetMyPoModel buildErr(int Errcode, {String message}) {
    GetMyPoModel res_d = GetMyPoModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetMyPoModelData {
  int purchase_id;
  String po_name;
  var total;
  String purchase_image;
  String purchase_description;
  int purchase_status;
  var approver_id;
  String project_name;
  String project_icon;
  String project_details;
  String user_name;
  String created_date;

  GetMyPoModelData(
      {this.purchase_id,
      this.po_name,
      this.total,
      this.purchase_image,
      this.purchase_description,
      this.purchase_status,
      this.approver_id,
      this.project_name,
      this.project_icon,
      this.project_details,
      this.user_name,
      this.created_date});

  factory GetMyPoModelData.fromJson(Map<String, dynamic> json) =>
      _$GetMyPoModelDataFromJson(json);
}

// user po list
@JsonSerializable()
class GetUserPoModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<GetUserPoModelData> data;

  GetUserPoModel({this.Status, this.Message, this.data});

  factory GetUserPoModel.fromJson(Map<String, dynamic> json) =>
      _$GetUserPoModelFromJson(json);

  static GetUserPoModel buildErr(int Errcode, {String message}) {
    GetUserPoModel res_d = GetUserPoModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetUserPoModelData {
  int pmap_id;
  int purchase_id;
  int pmap_status;
  String po_name;
  String created_date;
  String purchase_image;
  int country_id;
  String purchase_description;
  var approver_id;
  String project_name;
  int moderator_user_id;
  String user_name;

  GetUserPoModelData({
    this.pmap_id,
    this.purchase_id,
    this.pmap_status,
    this.created_date,
    this.po_name,
    this.purchase_image,
    this.country_id,
    this.purchase_description,
    this.approver_id,
    this.project_name,
    this.moderator_user_id,
    this.user_name,
  });

  factory GetUserPoModelData.fromJson(Map<String, dynamic> json) =>
      _$GetUserPoModelDataFromJson(json);
}

// review model
@JsonSerializable()
class GetReviewModel extends Base {
  bool is_loading = false;
  bool Status;
  String Message;
  GetReviewModelData data;

  GetReviewModel({
    this.Status,
    this.Message,
    this.data,
  });

  factory GetReviewModel.fromJson(Map<String, dynamic> json) =>
      _$GetReviewModelFromJson(json);

  static GetReviewModel buildErr(int Errcode, {String message}) {
    GetReviewModel res_d = GetReviewModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetReviewModelData {
  int purchase_id;
  int project_id;
  int category_id;
  int user_id;
  String po_name;
  String VHC_code;
  String currency;
  var total;
  String purchase_image;
  int country_id;
  String purchase_description;
  int payment_mode;
  var approver_id;
  int city_id;
  String priority;
  String project_name;
  String category_name;
  int parent_id;
  String parent_name;
  String user_name;
  String country_name;
  String city_name;
  int isPopup;
  String chartBalanceAmount;
  String poReviewHeader;
  bool showSignaturePad;
  bool is_editor;
  String invoice_number;
  String invoice_date;
  String payment_due_date;
  String payment_mode_name;
  String priority_name;
  String gst_name;
  var total_amount;
  var gst_amount;
  int vendor_id;
  var gst_id;
  List<ReviewModeratorList> moderator_array;
  String location_name;
  int location_id;
  bool assignData;

  GetReviewModelData(
      {this.purchase_id,
      this.project_id,
      this.category_id,
      this.parent_id,
      this.moderator_array,
      this.parent_name,
      this.user_id,
      this.po_name,
      this.VHC_code,
      this.currency,
      this.chartBalanceAmount,
      this.total,
      this.purchase_image,
      this.country_id,
      this.purchase_description,
      this.payment_mode,
      this.approver_id,
      this.city_id,
      this.priority,
      this.project_name,
      this.category_name,
      this.user_name,
      this.country_name,
      this.city_name,
      this.is_editor,
      this.poReviewHeader,
      this.showSignaturePad,
      this.invoice_date,
      this.invoice_number,
      this.payment_due_date,
      this.payment_mode_name,
      this.total_amount,
      this.gst_amount,
      this.priority_name,
      this.vendor_id,
      this.gst_name,
      this.gst_id,
      this.location_id,
      this.location_name,
      this.assignData,
      this.isPopup});

  factory GetReviewModelData.fromJson(Map<String, dynamic> json) =>
      _$GetReviewModelDataFromJson(json);
}

@JsonSerializable()
class ReviewModeratorList {
  int user_id;
  String user_name;

  ReviewModeratorList({this.user_id, this.user_name});

  factory ReviewModeratorList.fromJson(Map<String, dynamic> json) =>
      _$ReviewModeratorListFromJson(json);
}

// review tree model
@JsonSerializable()
class GetReviewTreeModel extends Base {
  bool Status;
  bool is_loading = false;
  String Message;
  int is_already_paid;
  List<GetReviewTreeModelData> data;

  GetReviewTreeModel(
      {this.Status, this.Message, this.data, this.is_already_paid});

  factory GetReviewTreeModel.fromJson(Map<String, dynamic> json) =>
      _$GetReviewTreeModelFromJson(json);

  static GetReviewTreeModel buildErr(int Errcode, {String message}) {
    GetReviewTreeModel res_d = GetReviewTreeModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetReviewTreeModelData {
  int map_active;
  String reason;
  String approvedBy;
  int map_id;
  int user_level;
  int user_id;
  int puserid;
  String user_name;
  int department_id;
  int purchase_id;
  var approver_id;
  int approver;
  int mod;
  int is_admin_manager_user;
  int purchase_map_status;

  GetReviewTreeModelData(
      {this.map_active,
      this.map_id,
      this.user_level,
      this.approvedBy,
      this.user_id,
      this.puserid,
      this.user_name,
      this.department_id,
      this.reason,
      this.purchase_id,
      this.approver_id,
      this.approver,
      this.mod,
      this.is_admin_manager_user,
      this.purchase_map_status});

  factory GetReviewTreeModelData.fromJson(Map<String, dynamic> json) =>
      _$GetReviewTreeModelDataFromJson(json);
}

// profile
@JsonSerializable()
class GetUserProfileModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  GetUserProfileModelData data;

  GetUserProfileModel({this.Status, this.Message, this.data});

  factory GetUserProfileModel.fromJson(Map<String, dynamic> json) =>
      _$GetUserProfileModelFromJson(json);

  static GetUserProfileModel buildErr(int Errcode, {String message}) {
    GetUserProfileModel res_d = GetUserProfileModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class GetUserProfileModelData {
  int user_id;
  String user_name;
  String user_email;
  int user_mobile;
  String user_gender;
  String user_role;
  int role_id;
  String user_status;
  String user_address;
  String user_dob;
  String user_pic;
  int organization_id;
  String organizations_name;
  int project_id;
  int approved;
  int pending;
  int rejected;
  List<ProjectListModel> project_role;
  List<UserOrganizationData> organization_data;
  List<DashBoardUserData> dashboard_user;
  bool isCashMang;
  int creditNote_isActive;
  bool showBudgetAlert;
  bool isAgainstRequest;
  String againstRequestText;

  GetUserProfileModelData(
      {this.user_id,
      this.user_name,
      this.user_email,
      this.user_mobile,
      this.user_gender,
      this.user_role,
      this.role_id,
      this.dashboard_user,
      this.user_status,
      this.user_address,
      this.user_dob,
      this.user_pic,
      this.organization_id,
      this.organizations_name,
      this.project_id,
      this.approved,
      this.pending,
      this.isCashMang,
      this.creditNote_isActive,
      this.showBudgetAlert,
      this.isAgainstRequest,
      this.againstRequestText,
      this.rejected});

  factory GetUserProfileModelData.fromJson(Map<String, dynamic> json) =>
      _$GetUserProfileModelDataFromJson(json);
}

@JsonSerializable()
class ProjectListModel {
  final int project_id;
  final String project_role;

  ProjectListModel({this.project_id, this.project_role});

  factory ProjectListModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectListModelFromJson(json);
}

@JsonSerializable()
class UserOrganizationData {
  final int organization_id;
  final String organization_name;

  bool isSelected = false;

  UserOrganizationData(
      {this.organization_id, this.organization_name, this.isSelected = false});

  factory UserOrganizationData.fromJson(Map<String, dynamic> json) =>
      _$UserOrganizationDataFromJson(json);
}

@JsonSerializable()
class DashBoardUserData {
  final int project_id;
  final int user_level;
  final String project_name;

  DashBoardUserData({this.project_id, this.project_name, this.user_level});

  factory DashBoardUserData.fromJson(Map<String, dynamic> json) =>
      _$DashBoardUserDataFromJson(json);
}

//select Category
@JsonSerializable()
class SelectCategoryModel extends Base {
  bool Status;
  bool is_loading = false;
  String Message;
  List<SelectCategoryModelData> data;

  SelectCategoryModel({this.Status, this.Message, this.data});

  factory SelectCategoryModel.fromJson(Map<String, dynamic> json) =>
      _$SelectCategoryModelFromJson(json);

  static SelectCategoryModel buildErr(int Errcode, {String message}) {
    SelectCategoryModel res_d = SelectCategoryModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SelectCategoryModelData {
  int category_id;
  int organization_id;
  int parent_id;
  int category_active;
  int visibility;
  String organization_name;
  String category_name;
  bool isSelected;
  int selectedIndex;
  String category_description;
  List<SelectCategoryModelData> childList;

  SelectCategoryModelData(
      {this.category_id,
      this.organization_id,
      this.selectedIndex,
      this.childList,
      this.parent_id,
      this.category_active,
      this.visibility,
      this.organization_name,
      this.category_name,
      this.category_description,
      this.isSelected = false});

  factory SelectCategoryModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectCategoryModelDataFromJson(json);
}


@JsonSerializable()
class SelectShootlocationModelData extends Base {
  bool Status;
  bool is_loading = false;
  String Message;
  List<ShootLocationData> data;

  SelectShootlocationModelData({this.Status, this.Message, this.data});

  factory SelectShootlocationModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectShootlocationModelDataFromJson(json);

  static SelectShootlocationModelData buildErr(int Errcode, {String message}) {
    SelectShootlocationModelData res_d = SelectShootlocationModelData();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}


@JsonSerializable()
class ShootLocationData {
  int location_id;
  int organization_id;
  int location_active;
  int visibility;
  String location_name;
  bool isSelected;
  int selectedIndex;


  ShootLocationData({
      this.location_id,
      this.organization_id,
      this.location_active,
      this.visibility,
      this.location_name,
      this.isSelected,
      this.selectedIndex
  });

  factory ShootLocationData.fromJson(Map<String, dynamic> json) =>
      _$ShootLocationDataFromJson(json);
  
}

//Select Country
@JsonSerializable()
class SelectCountryModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<SelectCountryModelData> data;

  SelectCountryModel({this.Status, this.Message, this.data});

  factory SelectCountryModel.fromJson(Map<String, dynamic> json) =>
      _$SelectCountryModelFromJson(json);

  static SelectCountryModel buildErr(int Errcode, {String message}) {
    SelectCountryModel res_d = SelectCountryModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SelectCountryModelData extends Object {
  int country_id;
  int country_active;
  String country_name;
  String country_timezone;
  String country_code;
  bool isSelected;

  SelectCountryModelData(
      {this.country_id,
      this.country_active,
      this.isSelected = false,
      this.country_name,
      this.country_timezone,
      this.country_code});

  factory SelectCountryModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectCountryModelDataFromJson(json);
}

//City
@JsonSerializable()
class SelectCityModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<SelectCityModelData> data;

  SelectCityModel({this.Status, this.Message, this.data});

  factory SelectCityModel.fromJson(Map<String, dynamic> json) =>
      _$SelectCityModelFromJson(json);

  static SelectCityModel buildErr(int Errcode, {String message}) {
    SelectCityModel res_d = SelectCityModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SelectCityModelData extends Object {
  int city_id;
  int country_id;
  int city_active;
  String country_name;
  String city_name;
  bool isSelected;
  bool is_loading = false;

  SelectCityModelData(
      {this.city_id,
      this.country_id,
      this.city_active,
      this.isSelected = false,
      this.country_name,
      this.city_name});

  factory SelectCityModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectCityModelDataFromJson(json);
}

//Currency
@JsonSerializable()
class SelectCurrencyModel extends Base {
  bool Status;
  bool is_loading = false;
  String Message;
  List<SelectCurrencyModelData> data;

  SelectCurrencyModel({this.Status, this.Message, this.data});

  factory SelectCurrencyModel.fromJson(Map<String, dynamic> json) =>
      _$SelectCurrencyModelFromJson(json);

  static SelectCurrencyModel buildErr(int Errcode, {String message}) {
    SelectCurrencyModel res_d = SelectCurrencyModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SelectCurrencyModelData {
  int currency_id;
  int currency_active;
  String currency_code;
  String currency_name;

  SelectCurrencyModelData(
      {this.currency_id,
      this.currency_active,
      this.currency_code,
      this.currency_name});

  factory SelectCurrencyModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectCurrencyModelDataFromJson(json);
}

// @JsonSerializable()
// class GSTCashPriorityModel extends Base {
//   bool Status;
//   bool is_loading = false;
//   String Message;
//   GSTCashPriorityData data;
//
//   GSTCashPriorityModel({
//       this.Status, this.is_loading, this.Message, this.data});
//
//   factory GSTCashPriorityModel.fromJson(Map<String, dynamic> json) =>
//       _$GSTCashPriorityModelFromJson(json);
//
//   static GSTCashPriorityModel buildErr(int Errcode, {String message}) {
//     GSTCashPriorityModel res_d = GSTCashPriorityModel();
//     Base _b = res_d.buildError(Errcode, message: message);
//     return res_d
//       ..Message = _b.Message
//       ..Status = _b.Status
//       ..is_loading = _b.is_loading;
//   }
//
//
// }
//
// @JsonSerializable()
// class GSTCashPriorityData {
//   List<GSTData> gst;
//
//   GSTCashPriorityData({this.gst});
//
//   factory GSTCashPriorityData.fromJson(Map<String, dynamic> json) =>
//       _$GSTCashPriorityDataFromJson(json);
//
//
// }
//
// @JsonSerializable()
// class GSTData {
//   int gst_id;
//   int organization_id;
//   double gst_percentage;
//   String gst_name;
//
//   GSTData({
//       this.gst_id, this.organization_id, this.gst_percentage, this.gst_name});
//
//   factory GSTData.fromJson(Map<String, dynamic> json) =>
//       _$GSTDataFromJson(json);
// }

//Select Approver
@JsonSerializable()
class SelectApproverModel extends Base {
  bool Status;
  bool is_loading = false;
  String Message;
  List<SelectApproverModelData> data;

  SelectApproverModel({this.Status, this.Message, this.data});

  factory SelectApproverModel.fromJson(Map<String, dynamic> json) =>
      _$SelectApproverModelFromJson(json);

  static SelectApproverModel buildErr(int Errcode, {String message}) {
    SelectApproverModel res_d = SelectApproverModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SelectApproverModelData {
  int map_active;
  int map_id;
  int user_level;
  int department_id;
  int user_id;
  String user_name;
  bool isSelected;

  SelectApproverModelData(
      {this.map_active,
      this.map_id,
      this.user_level,
      this.department_id,
      this.user_id,
      this.user_name,
      this.isSelected = false});

  factory SelectApproverModelData.fromJson(Map<String, dynamic> json) =>
      _$SelectApproverModelDataFromJson(json);
}

// set favourite
@JsonSerializable()
class SetFavouriteModel extends Base {
  bool Status;
  bool is_loading = false;
  String Message;

  SetFavouriteModel({this.Status, this.Message});

  factory SetFavouriteModel.fromJson(Map<String, dynamic> json) =>
      _$SetFavouriteModelFromJson(json);

  static SetFavouriteModel buildErr(int Errcode, {String message}) {
    SetFavouriteModel res_d = SetFavouriteModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

// upload image

//image_upload

@JsonSerializable()
class ImageUploadRes extends Base {
  final String Message;
  final bool Status;
  final ImageResData data;
  bool is_loading = false;

  ImageUploadRes({this.Message, this.Status, this.data});

  factory ImageUploadRes.fromJson(Map<String, dynamic> json) =>
      _$ImageUploadResFromJson(json);

  static ImageUploadRes buildErr(int Errcode, {String message}) {
    ImageUploadRes res_d = ImageUploadRes();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class ImageResData extends Base {
  final String image_path;

  ImageResData({this.image_path});

  factory ImageResData.fromJson(Map<String, dynamic> json) =>
      _$ImageResDataFromJson(json);

  static ImageResData buildErr(int Errcode, {String message}) {
    ImageResData res_d = ImageResData();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

// edit profile model
@JsonSerializable()
class EditProfileModel extends Base {
  String statusCode;
  String Message;
  bool Status;
  bool is_loading = false;

  EditProfileModel({this.statusCode, this.Message, this.Status});

  factory EditProfileModel.fromJson(Map<String, dynamic> json) =>
      _$EditProfileModelFromJson(json);

  static EditProfileModel buildErr(int Errcode, {String message}) {
    EditProfileModel res_d = EditProfileModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

// feedback
@JsonSerializable()
class FeedbackModel extends Base {
  String Message;
  bool Status;
  String statusCode;
  bool is_loading = false;

  FeedbackModel({this.Message, this.Status, this.statusCode});

  factory FeedbackModel.fromJson(Map<String, dynamic> json) =>
      _$FeedbackModelFromJson(json);

  static FeedbackModel buildErr(int Errcode, {String message}) {
    FeedbackModel res_d = FeedbackModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

//change password model
@JsonSerializable()
class ChangePasswordModel extends Base {
  String Message;
  bool Status;
  String statusCode;
  bool is_loading = false;

  ChangePasswordModel({this.Message, this.Status, this.statusCode});

  factory ChangePasswordModel.fromJson(Map<String, dynamic> json) =>
      _$ChangePasswordModelFromJson(json);

  static ChangePasswordModel buildErr(int Errcode, {String message}) {
    ChangePasswordModel res_d = ChangePasswordModel();
  }
}

// get po list in create challan
@JsonSerializable()
class GetPoListChallanModel extends Base {
  final String Message;
  bool is_loading = false;
  final bool Status;
  final List<POListChallanData> data;

  GetPoListChallanModel({this.Message, this.Status, this.data});

  factory GetPoListChallanModel.fromJson(Map<String, dynamic> json) =>
      _$GetPoListChallanModelFromJson(json);

  static GetPoListChallanModel buildErr(int Errcode, {String message}) {
    GetPoListChallanModel res_d = GetPoListChallanModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

//forgot password model
@JsonSerializable()
class ForgetPasswordModel extends Base {
  String Message;
  bool Status;
  String statusCode;
  bool is_loading = false;

  ForgetPasswordModel({this.Message, this.Status, this.statusCode});

  factory ForgetPasswordModel.fromJson(Map<String, dynamic> json) =>
      _$ForgetPasswordModelFromJson(json);

  static ForgetPasswordModel buildErr(int Errcode, {String message}) {
    ForgetPasswordModel res_d = ForgetPasswordModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

// otp model
@JsonSerializable()
class VerifyOTPModel extends Base {
  String Message;
  bool Status;
  String statusCode;
  VerifyOTPModelData data;
  bool is_loading = false;

  VerifyOTPModel({this.Message, this.Status, this.statusCode, this.data});

  factory VerifyOTPModel.fromJson(Map<String, dynamic> json) =>
      _$VerifyOTPModelFromJson(json);

  static VerifyOTPModel buildErr(int Errcode, {String message}) {
    VerifyOTPModel res_d = VerifyOTPModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class VerifyOTPModelData {
  String token;

  VerifyOTPModelData({this.token});

  factory VerifyOTPModelData.fromJson(Map<String, dynamic> json) =>
      _$VerifyOTPModelDataFromJson(json);
}

//reset password
@JsonSerializable()
class ResetPasswordModel extends Base {
  String Message;
  bool Status;
  String statusCode;
  bool is_loading = false;

  ResetPasswordModel({this.Message, this.Status, this.statusCode});

  factory ResetPasswordModel.fromJson(Map<String, dynamic> json) =>
      _$ResetPasswordModelFromJson(json);

  static ResetPasswordModel buildErr(int Errcode, {String message}) {
    ResetPasswordModel res_d = ResetPasswordModel();
  }
}

// list of PO in create challan
@JsonSerializable()
class POListChallanData {
  final String po_name;
  final int purchase_id;
  final String user_name;
  final String expense_name;
  bool isSelected;
  bool is_loading = false;

  POListChallanData({this.user_name, this.po_name, this.purchase_id, this.expense_name});

  factory POListChallanData.fromJson(Map<String, dynamic> json) =>
      _$POListChallanDataFromJson(json);
}

// search page
@JsonSerializable()
class SearchModel extends Base {
  String Message;
  bool is_loading = false;
  bool Status;
  List<SearchData> data;

  SearchModel({this.Message, this.Status, this.data});

  factory SearchModel.fromJson(Map<String, dynamic> json) =>
      _$SearchModelFromJson(json);

  static SearchModel buildErr(int Errcode, {String message}) {
    SearchModel res_d = SearchModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class SearchData {
  final String project_name;
  final int project_id;
  final int company_id;
  final int moderation;
  final int moderator_user_id;
  final int finance_user_id;
  final int project_active;
  final int favourite;
  final String project_icon;
  final String project_details;
  final String company_name;

  SearchData(
      {this.project_name,
      this.project_id,
      this.company_id,
      this.moderation,
      this.moderator_user_id,
      this.finance_user_id,
      this.project_active,
      this.favourite,
      this.project_icon,
      this.project_details,
      this.company_name});

  factory SearchData.fromJson(Map<String, dynamic> json) =>
      _$SearchDataFromJson(json);
}

@JsonSerializable()
class GetCustomKeyModel extends Base {
  String Message;
  bool is_loading = false;
  bool Status;
  List<CustomKeyData> data;

  GetCustomKeyModel({this.Message, this.is_loading, this.Status, this.data});

  factory GetCustomKeyModel.fromJson(Map<String, dynamic> json) =>
      _$GetCustomKeyModelFromJson(json);

  static GetCustomKeyModel buildErr(int Errcode, {String message}) {
    GetCustomKeyModel res_d = GetCustomKeyModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class CustomKeyData {
  int expense_id;
  String expense_name;
  String expense_icon;
  int expense_active;

  CustomKeyData(
      {this.expense_id,
      this.expense_name,
      this.expense_active,
      this.expense_icon});

  factory CustomKeyData.fromJson(Map<String, dynamic> json) =>
      _$CustomKeyDataFromJson(json);
}

@JsonSerializable()
class GetCustomFieldModel extends Base {
  String Message;
  bool is_loading = false;
  bool Status;
  List<CustomFieldData> data;

  GetCustomFieldModel({this.Message, this.is_loading, this.Status, this.data});

  factory GetCustomFieldModel.fromJson(Map<String, dynamic> json) =>
      _$GetCustomFieldModelFromJson(json);

  static GetCustomFieldModel buildErr(int Errcode, {String message}) {
    GetCustomFieldModel res_d = GetCustomFieldModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class CustomFieldData {
  int key_id;
  String key_name;
  int field_type_id;
  String field_type;

  CustomFieldData(
      {this.key_id, this.key_name, this.field_type_id, this.field_type});

  factory CustomFieldData.fromJson(Map<String, dynamic> json) =>
      _$CustomFieldDataFromJson(json);
}

@JsonSerializable()
class GetChatGroups extends Base {
  bool Status;
  String Message;
  List<ChatGroupList> rooms;
  bool is_loading = false;

  GetChatGroups({this.Status, this.Message, this.rooms});

  factory GetChatGroups.fromJson(Map<String, dynamic> json) =>
      _$GetChatGroupsFromJson(json);

  static GetChatGroups buildErr(int Errcode, {String message}) {
    GetChatGroups res_d = GetChatGroups();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class ChatGroupList {
  int room_id;
  int user_id;
  int room_active;
  int organization_id;
  int project_id;
  int department_id;
  String room_unique_id;
  String date_time;
  String room_name;
  String display_name;
  String room_description;

  ChatGroupList(
      {this.room_id,
      this.user_id,
      this.room_active,
      this.organization_id,
      this.project_id,
      this.department_id,
      this.room_unique_id,
      this.date_time,
      this.room_name,
      this.display_name,
      this.room_description});

  factory ChatGroupList.fromJson(Map<String, dynamic> json) =>
      _$ChatGroupListFromJson(json);
}

@JsonSerializable()
class GetHistoryMsgModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<HistoryMessages> chats;

  GetHistoryMsgModel({this.Status, this.Message, this.chats});

  factory GetHistoryMsgModel.fromJson(Map<String, dynamic> json) =>
      _$GetHistoryMsgModelFromJson(json);

  static GetHistoryMsgModel buildErr(int Errcode, {String message}) {
    GetHistoryMsgModel res_d = GetHistoryMsgModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class HistoryMessages {
  String date;
  String displayDate;
  List<ChatMessageList> ChatList;

  HistoryMessages({this.date, this.ChatList, this.displayDate});

  factory HistoryMessages.fromJson(Map<String, dynamic> json) =>
      _$HistoryMessagesFromJson(json);
}

@JsonSerializable()
class ChatMessageList {
  int chat_message_id;
  int chat_room_id;
  int user_id;
  int project_id;
  String text_message;
  String user_name;
  String created_at;
  String time;

  ChatMessageList(
      {this.chat_message_id,
      this.chat_room_id,
      this.user_id,
      this.project_id,
      this.text_message,
      this.created_at,
      this.user_name,
      this.time});

  factory ChatMessageList.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageListFromJson(json);
}

@JsonSerializable()
class BehalfPeopleModel extends Base {
  bool Status;
  String Message;
  String project_id;
  bool is_loading = false;
  List<BehalfPeopleList> data;

  BehalfPeopleModel({this.Status, this.Message, this.data, this.project_id});

  factory BehalfPeopleModel.fromJson(Map<String, dynamic> json) =>
      _$BehalfPeopleModelFromJson(json);

  static BehalfPeopleModel buildErr(int Errcode, {String message}) {
    BehalfPeopleModel res_d = BehalfPeopleModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class BehalfPeopleList {
  bool isSelected;
  final int user_id;
  final String user_name;

  BehalfPeopleList({this.user_name, this.user_id, this.isSelected});

  factory BehalfPeopleList.fromJson(Map<String, dynamic> json) =>
      _$BehalfPeopleListFromJson(json);
}

// help data

@JsonSerializable()
class HelpModel extends Base {
  bool Status;
  String Message;
  bool is_loading = false;
  List<HelpDataList> data;

  HelpModel({this.Status, this.Message, this.data});

  factory HelpModel.fromJson(Map<String, dynamic> json) =>
      _$HelpModelFromJson(json);  

  static HelpModel buildErr(int Errcode, {String message}) {
    HelpModel res_d = HelpModel();

    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class HelpDataList {
  final String title;
  final String body;

  HelpDataList({this.title, this.body});

  factory HelpDataList.fromJson(Map<String, dynamic> json) =>
      _$HelpDataListFromJson(json);
}

@JsonSerializable()
class VendorAutoCompleteData {
  List<VendorData> data;
  bool status;
  String message;
  bool is_loading = false;

  VendorAutoCompleteData({this.data, this.status, this.message, this.is_loading});

  factory VendorAutoCompleteData.fromJson(Map<String, dynamic> json) =>
      _$VendorAutoCompleteDataFromJson(json);
}

@JsonSerializable()
class VendorData {
  int vendor_id;
  String vendor_name;

  VendorData({this.vendor_id, this.vendor_name});

  factory VendorData.fromJson(Map<String, dynamic> json) =>
      _$VendorDataFromJson(json);
}

@JsonSerializable()
class BudgetAlertModel extends Base {
  
  bool is_loading = false;
  bool Status;
  String Message;
  List <BudgetData> data;

  BudgetAlertModel({this.is_loading, this.Status, this.Message, this.data});

  factory BudgetAlertModel.fromJson(Map<String, dynamic> json) =>
      _$BudgetAlertModelFromJson(json);

  static BudgetAlertModel buildErr(int Errcode, {String message}) {
    BudgetAlertModel res_d = BudgetAlertModel();
    Base _b = res_d.buildError(Errcode, message: message);
    return res_d
      ..Message = _b.Message
      ..Status = _b.Status
      ..is_loading = _b.is_loading;
  }
}

@JsonSerializable()
class BudgetData {
  int log_id;
  String user_name;
  var amount;
  var budget_amount;
  String category_name;
  String sub_category_name;
  String reason;
  String budgetLog_created_date;
  BudgetData({this.user_name, this.amount, this.budget_amount,
      this.category_name, this.sub_category_name, this.reason, this.budgetLog_created_date, this.log_id});

  factory BudgetData.fromJson(Map<String, dynamic> json) =>
      _$BudgetDataFromJson(json);

}




