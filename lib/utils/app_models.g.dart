// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Base _$BaseFromJson(Map<String, dynamic> json) {
  return Base(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    is_loading: json['is_loading'] as bool,
  );
}

Map<String, dynamic> _$BaseToJson(Base instance) => <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
    };

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) {
  return LoginModel(
    data: json['data'] == null
        ? null
        : LoginDataObj.fromJson(json['data'] as Map<String, dynamic>),
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$LoginModelToJson(LoginModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'data': instance.data,
      'is_loading': instance.is_loading,
    };

LoginDataObj _$LoginDataObjFromJson(Map<String, dynamic> json) {
  return LoginDataObj(
    access_token: json['access_token'] as String,
    token_type: json['token_type'] as String,
    user_name: json['user_name'] as String,
    temp_token: json['temp_token'] as String,
  );
}

Map<String, dynamic> _$LoginDataObjToJson(LoginDataObj instance) =>
    <String, dynamic>{
      'access_token': instance.access_token,
      'user_name': instance.user_name,
      'token_type': instance.token_type,
      'temp_token': instance.temp_token,
    };

GetProjectModel _$GetProjectModelFromJson(Map<String, dynamic> json) {
  return GetProjectModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ProjectModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetProjectModelToJson(GetProjectModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

ProjectModelData _$ProjectModelDataFromJson(Map<String, dynamic> json) {
  return ProjectModelData(
    project_id: json['project_id'] as int,
    project_name: json['project_name'] as String,
    project_icon: json['project_icon'] as String,
    isSelected: json['isSelected'] as bool,
    project_details: json['project_details'] as String,
    company_id: json['company_id'] as int,
    company_name: json['company_name'] as String,
    moderation: json['moderation'] as int,
    moderator_user_id: json['moderator_user_id'] as int,
    finance_user_id: json['finance_user_id'] as int,
    project_active: json['project_active'] as int,
    favourite: json['favourite'] as int,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ProjectModelDataToJson(ProjectModelData instance) =>
    <String, dynamic>{
      'project_id': instance.project_id,
      'project_name': instance.project_name,
      'project_icon': instance.project_icon,
      'project_details': instance.project_details,
      'company_id': instance.company_id,
      'company_name': instance.company_name,
      'moderation': instance.moderation,
      'moderator_user_id': instance.moderator_user_id,
      'finance_user_id': instance.finance_user_id,
      'project_active': instance.project_active,
      'favourite': instance.favourite,
      'isSelected': instance.isSelected,
      'is_loading': instance.is_loading,
    };

ProjectDetailModel _$ProjectDetailModelFromJson(Map<String, dynamic> json) {
  return ProjectDetailModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ProjectDetailModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ProjectDetailModelToJson(ProjectDetailModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

ProjectDetailModelData _$ProjectDetailModelDataFromJson(
    Map<String, dynamic> json) {
  return ProjectDetailModelData(
    project_id: json['project_id'] as int,
    project_name: json['project_name'] as String,
    project_icon: json['project_icon'] as String,
    project_details: json['project_details'] as String,
    company_id: json['company_id'] as int,
    company_name: json['company_name'] as String,
    moderation: json['moderation'] as int,
    moderator_user_id: json['moderator_user_id'] as int,
    finance_user_id: json['finance_user_id'] as int,
    project_active: json['project_active'] as int,
    favourite: json['favourite'] as int,
  );
}

Map<String, dynamic> _$ProjectDetailModelDataToJson(
        ProjectDetailModelData instance) =>
    <String, dynamic>{
      'project_id': instance.project_id,
      'project_name': instance.project_name,
      'project_icon': instance.project_icon,
      'project_details': instance.project_details,
      'company_id': instance.company_id,
      'company_name': instance.company_name,
      'moderation': instance.moderation,
      'moderator_user_id': instance.moderator_user_id,
      'finance_user_id': instance.finance_user_id,
      'project_active': instance.project_active,
      'favourite': instance.favourite,
    };

ProjectDetailTreeModel _$ProjectDetailTreeModelFromJson(
    Map<String, dynamic> json) {
  return ProjectDetailTreeModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ProjectDetailTreeModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ProjectDetailTreeModelToJson(
        ProjectDetailTreeModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

ProjectDetailTreeModelData _$ProjectDetailTreeModelDataFromJson(
    Map<String, dynamic> json) {
  return ProjectDetailTreeModelData(
    map_id: json['map_id'] as int,
    user_id: json['user_id'] as int,
    user_name: json['user_name'] as String,
    user_pic: json['user_pic'] as String,
    user_status: json['user_status'] as String,
    projec_id: json['projec_id'] as int,
    project_name: json['project_name'] as String,
    user_level: json['user_level'] as int,
    map_active: json['map_active'] as int,
  );
}

Map<String, dynamic> _$ProjectDetailTreeModelDataToJson(
        ProjectDetailTreeModelData instance) =>
    <String, dynamic>{
      'map_id': instance.map_id,
      'user_id': instance.user_id,
      'user_name': instance.user_name,
      'user_pic': instance.user_pic,
      'user_status': instance.user_status,
      'projec_id': instance.projec_id,
      'project_name': instance.project_name,
      'user_level': instance.user_level,
      'map_active': instance.map_active,
    };

GetNotificationModel _$GetNotificationModelFromJson(Map<String, dynamic> json) {
  return GetNotificationModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : GetNotificationModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetNotificationModelToJson(
        GetNotificationModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

GetNotificationModelData _$GetNotificationModelDataFromJson(
    Map<String, dynamic> json) {
  return GetNotificationModelData(
    notification_id: json['notification_id'] as int,
    project_id: json['project_id'] as int,
    created_date: json['created_date'] as String,
    project_name: json['project_name'] as String,
    user_id: json['user_id'] as int,
    user_name: json['user_name'] as String,
    notification_icon: json['notification_icon'] as String,
    notification_msg: json['notification_msg'] as String,
    notification_active: json['notification_active'] as int,
    purchase_id: json['purchase_id'] as int,
    notification_title: json['notification_title'] as String,
    isGraph: json['isGraph'] as int,
    approver_id: json['approver_id'] as int,
    adminManager_user_id: json['adminManager_user_id'] as int,
    moderator_user_id: json['moderator_user_id'] as int,
    budgetLog_id: json['budgetLog_id'] as int,
    isBudget: json['isBudget'] as int,
    pmap_id: json['pmap_id'] as int,
  );
}

Map<String, dynamic> _$GetNotificationModelDataToJson(
        GetNotificationModelData instance) =>
    <String, dynamic>{
      'notification_id': instance.notification_id,
      'project_id': instance.project_id,
      'project_name': instance.project_name,
      'user_id': instance.user_id,
      'purchase_id': instance.purchase_id,
      'pmap_id': instance.pmap_id,
      'approver_id': instance.approver_id,
      'moderator_user_id': instance.moderator_user_id,
      'adminManager_user_id': instance.adminManager_user_id,
      'isGraph': instance.isGraph,
      'user_name': instance.user_name,
      'notification_icon': instance.notification_icon,
      'notification_title': instance.notification_title,
      'notification_msg': instance.notification_msg,
      'created_date': instance.created_date,
      'notification_active': instance.notification_active,
      'isBudget': instance.isBudget,
      'budgetLog_id': instance.budgetLog_id,
    };

GetMyPoModel _$GetMyPoModelFromJson(Map<String, dynamic> json) {
  return GetMyPoModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : GetMyPoModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetMyPoModelToJson(GetMyPoModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

GetMyPoModelData _$GetMyPoModelDataFromJson(Map<String, dynamic> json) {
  return GetMyPoModelData(
    purchase_id: json['purchase_id'] as int,
    po_name: json['po_name'] as String,
    total: json['total'],
    purchase_image: json['purchase_image'] as String,
    purchase_description: json['purchase_description'] as String,
    purchase_status: json['purchase_status'] as int,
    approver_id: json['approver_id'],
    project_name: json['project_name'] as String,
    project_icon: json['project_icon'] as String,
    project_details: json['project_details'] as String,
    user_name: json['user_name'] as String,
    created_date: json['created_date'] as String,
  );
}

Map<String, dynamic> _$GetMyPoModelDataToJson(GetMyPoModelData instance) =>
    <String, dynamic>{
      'purchase_id': instance.purchase_id,
      'po_name': instance.po_name,
      'total': instance.total,
      'purchase_image': instance.purchase_image,
      'purchase_description': instance.purchase_description,
      'purchase_status': instance.purchase_status,
      'approver_id': instance.approver_id,
      'project_name': instance.project_name,
      'project_icon': instance.project_icon,
      'project_details': instance.project_details,
      'user_name': instance.user_name,
      'created_date': instance.created_date,
    };

GetUserPoModel _$GetUserPoModelFromJson(Map<String, dynamic> json) {
  return GetUserPoModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : GetUserPoModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetUserPoModelToJson(GetUserPoModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

GetUserPoModelData _$GetUserPoModelDataFromJson(Map<String, dynamic> json) {
  return GetUserPoModelData(
    pmap_id: json['pmap_id'] as int,
    purchase_id: json['purchase_id'] as int,
    pmap_status: json['pmap_status'] as int,
    created_date: json['created_date'] as String,
    po_name: json['po_name'] as String,
    purchase_image: json['purchase_image'] as String,
    country_id: json['country_id'] as int,
    purchase_description: json['purchase_description'] as String,
    approver_id: json['approver_id'],
    project_name: json['project_name'] as String,
    moderator_user_id: json['moderator_user_id'] as int,
    user_name: json['user_name'] as String,
  );
}

Map<String, dynamic> _$GetUserPoModelDataToJson(GetUserPoModelData instance) =>
    <String, dynamic>{
      'pmap_id': instance.pmap_id,
      'purchase_id': instance.purchase_id,
      'pmap_status': instance.pmap_status,
      'po_name': instance.po_name,
      'created_date': instance.created_date,
      'purchase_image': instance.purchase_image,
      'country_id': instance.country_id,
      'purchase_description': instance.purchase_description,
      'approver_id': instance.approver_id,
      'project_name': instance.project_name,
      'moderator_user_id': instance.moderator_user_id,
      'user_name': instance.user_name,
    };

GetReviewModel _$GetReviewModelFromJson(Map<String, dynamic> json) {
  return GetReviewModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: json['data'] == null
        ? null
        : GetReviewModelData.fromJson(json['data'] as Map<String, dynamic>),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetReviewModelToJson(GetReviewModel instance) =>
    <String, dynamic>{
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'Message': instance.Message,
      'data': instance.data,
    };

GetReviewModelData _$GetReviewModelDataFromJson(Map<String, dynamic> json) {
  return GetReviewModelData(
    purchase_id: json['purchase_id'] as int,
    project_id: json['project_id'] as int,
    category_id: json['category_id'] as int,
    parent_id: json['parent_id'] as int,
    moderator_array: (json['moderator_array'] as List)
        ?.map((e) => e == null
            ? null
            : ReviewModeratorList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    parent_name: json['parent_name'] as String,
    user_id: json['user_id'] as int,
    po_name: json['po_name'] as String,
    VHC_code: json['VHC_code'] as String,
    currency: json['currency'] as String,
    chartBalanceAmount: json['chartBalanceAmount'] as String,
    total: json['total'],
    purchase_image: json['purchase_image'] as String,
    country_id: json['country_id'] as int,
    purchase_description: json['purchase_description'] as String,
    payment_mode: json['payment_mode'] as int,
    approver_id: json['approver_id'],
    city_id: json['city_id'] as int,
    priority: json['priority'] as String,
    project_name: json['project_name'] as String,
    category_name: json['category_name'] as String,
    user_name: json['user_name'] as String,
    country_name: json['country_name'] as String,
    city_name: json['city_name'] as String,
    is_editor: json['is_editor'] as bool,
    poReviewHeader: json['poReviewHeader'] as String,
    showSignaturePad: json['showSignaturePad'] as bool,
    invoice_date: json['invoice_date'] as String,
    invoice_number: json['invoice_number'] as String,
    payment_due_date: json['payment_due_date'] as String,
    payment_mode_name: json['payment_mode_name'] as String,
    total_amount: json['total_amount'],
    gst_amount: json['gst_amount'],
    priority_name: json['priority_name'] as String,
    vendor_id: json['vendor_id'] as int,
    location_id: json['location_id'] as int,
    gst_name: json['gst_name'] as String,
    location_name: json['location_name'] as String,
    gst_id: json['gst_id'],
    isPopup: json['isPopup'] as int,
  );
}

Map<String, dynamic> _$GetReviewModelDataToJson(GetReviewModelData instance) =>
    <String, dynamic>{
      'purchase_id': instance.purchase_id,
      'project_id': instance.project_id,
      'category_id': instance.category_id,
      'user_id': instance.user_id,
      'po_name': instance.po_name,
      'VHC_code': instance.VHC_code,
      'currency': instance.currency,
      'total': instance.total,
      'purchase_image': instance.purchase_image,
      'country_id': instance.country_id,
      'purchase_description': instance.purchase_description,
      'payment_mode': instance.payment_mode,
      'approver_id': instance.approver_id,
      'city_id': instance.city_id,
      'priority': instance.priority,
      'project_name': instance.project_name,
      'category_name': instance.category_name,
      'parent_id': instance.parent_id,
      'parent_name': instance.parent_name,
      'user_name': instance.user_name,
      'country_name': instance.country_name,
      'city_name': instance.city_name,
      'isPopup': instance.isPopup,
      'chartBalanceAmount': instance.chartBalanceAmount,
      'poReviewHeader': instance.poReviewHeader,
      'showSignaturePad': instance.showSignaturePad,
      'is_editor': instance.is_editor,
      'invoice_number': instance.invoice_number,
      'invoice_date': instance.invoice_date,
      'payment_due_date': instance.payment_due_date,
      'payment_mode_name': instance.payment_mode_name,
      'priority_name': instance.priority_name,
      'gst_name': instance.gst_name,
      'total_amount': instance.total_amount,
      'gst_amount': instance.gst_amount,
      'vendor_id': instance.vendor_id,
      'gst_id': instance.gst_id,
      'moderator_array': instance.moderator_array,
      'location_name': instance.location_name,
      'location_id': instance.location_id,
    };

ReviewModeratorList _$ReviewModeratorListFromJson(Map<String, dynamic> json) {
  return ReviewModeratorList(
    user_id: json['user_id'] as int,
    user_name: json['user_name'] as String,
  );
}

Map<String, dynamic> _$ReviewModeratorListToJson(
        ReviewModeratorList instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
      'user_name': instance.user_name,
    };

GetReviewTreeModel _$GetReviewTreeModelFromJson(Map<String, dynamic> json) {
  return GetReviewTreeModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : GetReviewTreeModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    is_already_paid: json['is_already_paid'] as int,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetReviewTreeModelToJson(GetReviewTreeModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
      'is_already_paid': instance.is_already_paid,
      'data': instance.data,
    };

GetReviewTreeModelData _$GetReviewTreeModelDataFromJson(
    Map<String, dynamic> json) {
  return GetReviewTreeModelData(
    map_active: json['map_active'] as int,
    map_id: json['map_id'] as int,
    user_level: json['user_level'] as int,
    approvedBy: json['approvedBy'] as String,
    user_id: json['user_id'] as int,
    puserid: json['puserid'] as int,
    user_name: json['user_name'] as String,
    department_id: json['department_id'] as int,
    reason: json['reason'] as String,
    purchase_id: json['purchase_id'] as int,
    approver_id: json['approver_id'],
    approver: json['approver'] as int,
    mod: json['mod'] as int,
    is_admin_manager_user: json['is_admin_manager_user'] as int,
    purchase_map_status: json['purchase_map_status'] as int,
  );
}

Map<String, dynamic> _$GetReviewTreeModelDataToJson(
        GetReviewTreeModelData instance) =>
    <String, dynamic>{
      'map_active': instance.map_active,
      'reason': instance.reason,
      'approvedBy': instance.approvedBy,
      'map_id': instance.map_id,
      'user_level': instance.user_level,
      'user_id': instance.user_id,
      'puserid': instance.puserid,
      'user_name': instance.user_name,
      'department_id': instance.department_id,
      'purchase_id': instance.purchase_id,
      'approver_id': instance.approver_id,
      'approver': instance.approver,
      'mod': instance.mod,
      'is_admin_manager_user': instance.is_admin_manager_user,
      'purchase_map_status': instance.purchase_map_status,
    };

GetUserProfileModel _$GetUserProfileModelFromJson(Map<String, dynamic> json) {
  return GetUserProfileModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: json['data'] == null
        ? null
        : GetUserProfileModelData.fromJson(
            json['data'] as Map<String, dynamic>),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetUserProfileModelToJson(
        GetUserProfileModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

GetUserProfileModelData _$GetUserProfileModelDataFromJson(
    Map<String, dynamic> json) {
  return GetUserProfileModelData(
    user_id: json['user_id'] as int,
    user_name: json['user_name'] as String,
    user_email: json['user_email'] as String,
    user_mobile: json['user_mobile'] as int,
    user_gender: json['user_gender'] as String,
    user_role: json['user_role'] as String,
    role_id: json['role_id'] as int,
    dashboard_user: (json['dashboard_user'] as List)
        ?.map((e) => e == null
            ? null
            : DashBoardUserData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    user_status: json['user_status'] as String,
    user_address: json['user_address'] as String,
    user_dob: json['user_dob'] as String,
    user_pic: json['user_pic'] as String,
    organization_id: json['organization_id'] as int,
    organizations_name: json['organizations_name'] as String,
    project_id: json['project_id'] as int,
    approved: json['approved'] as int,
    pending: json['pending'] as int,
    isCashMang: json['isCashMang'] as bool,
    creditNote_isActive: json['creditNote_isActive'] as int,
    showBudgetAlert: json['showBudgetAlert'] as bool,
    isAgainstRequest: json['isAgainstRequest'] as bool,
    againstRequestText: json['againstRequestText'] as String,
    rejected: json['rejected'] as int,
  )
    ..project_role = (json['project_role'] as List)
        ?.map((e) => e == null
            ? null
            : ProjectListModel.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..organization_data = (json['organization_data'] as List)
        ?.map((e) => e == null
            ? null
            : UserOrganizationData.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$GetUserProfileModelDataToJson(
        GetUserProfileModelData instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
      'user_name': instance.user_name,
      'user_email': instance.user_email,
      'user_mobile': instance.user_mobile,
      'user_gender': instance.user_gender,
      'user_role': instance.user_role,
      'role_id': instance.role_id,
      'user_status': instance.user_status,
      'user_address': instance.user_address,
      'user_dob': instance.user_dob,
      'user_pic': instance.user_pic,
      'organization_id': instance.organization_id,
      'organizations_name': instance.organizations_name,
      'project_id': instance.project_id,
      'approved': instance.approved,
      'pending': instance.pending,
      'rejected': instance.rejected,
      'project_role': instance.project_role,
      'organization_data': instance.organization_data,
      'dashboard_user': instance.dashboard_user,
      'isCashMang': instance.isCashMang,
      'creditNote_isActive': instance.creditNote_isActive,
      'showBudgetAlert': instance.showBudgetAlert,
      'isAgainstRequest': instance.isAgainstRequest,
      'againstRequestText': instance.againstRequestText,
    };

ProjectListModel _$ProjectListModelFromJson(Map<String, dynamic> json) {
  return ProjectListModel(
    project_id: json['project_id'] as int,
    project_role: json['project_role'] as String,
  );
}

Map<String, dynamic> _$ProjectListModelToJson(ProjectListModel instance) =>
    <String, dynamic>{
      'project_id': instance.project_id,
      'project_role': instance.project_role,
    };

UserOrganizationData _$UserOrganizationDataFromJson(Map<String, dynamic> json) {
  return UserOrganizationData(
    organization_id: json['organization_id'] as int,
    organization_name: json['organization_name'] as String,
    isSelected: json['isSelected'] as bool,
  );
}

Map<String, dynamic> _$UserOrganizationDataToJson(
        UserOrganizationData instance) =>
    <String, dynamic>{
      'organization_id': instance.organization_id,
      'organization_name': instance.organization_name,
      'isSelected': instance.isSelected,
    };

DashBoardUserData _$DashBoardUserDataFromJson(Map<String, dynamic> json) {
  return DashBoardUserData(
    project_id: json['project_id'] as int,
    project_name: json['project_name'] as String,
    user_level: json['user_level'] as int,
  );
}

Map<String, dynamic> _$DashBoardUserDataToJson(DashBoardUserData instance) =>
    <String, dynamic>{
      'project_id': instance.project_id,
      'user_level': instance.user_level,
      'project_name': instance.project_name,
    };

SelectCategoryModel _$SelectCategoryModelFromJson(Map<String, dynamic> json) {
  return SelectCategoryModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SelectCategoryModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectCategoryModelToJson(
        SelectCategoryModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
      'data': instance.data,
    };

SelectCategoryModelData _$SelectCategoryModelDataFromJson(
    Map<String, dynamic> json) {
  return SelectCategoryModelData(
    category_id: json['category_id'] as int,
    organization_id: json['organization_id'] as int,
    selectedIndex: json['selectedIndex'] as int,
    childList: (json['childList'] as List)
        ?.map((e) => e == null
            ? null
            : SelectCategoryModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    parent_id: json['parent_id'] as int,
    category_active: json['category_active'] as int,
    visibility: json['visibility'] as int,
    organization_name: json['organization_name'] as String,
    category_name: json['category_name'] as String,
    category_description: json['category_description'] as String,
    isSelected: json['isSelected'] as bool,
  );
}

Map<String, dynamic> _$SelectCategoryModelDataToJson(
        SelectCategoryModelData instance) =>
    <String, dynamic>{
      'category_id': instance.category_id,
      'organization_id': instance.organization_id,
      'parent_id': instance.parent_id,
      'category_active': instance.category_active,
      'visibility': instance.visibility,
      'organization_name': instance.organization_name,
      'category_name': instance.category_name,
      'isSelected': instance.isSelected,
      'selectedIndex': instance.selectedIndex,
      'category_description': instance.category_description,
      'childList': instance.childList,
    };

SelectShootlocationModelData _$SelectShootlocationModelDataFromJson(
    Map<String, dynamic> json) {
  return SelectShootlocationModelData(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ShootLocationData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectShootlocationModelDataToJson(
        SelectShootlocationModelData instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
      'data': instance.data,
    };

ShootLocationData _$ShootLocationDataFromJson(Map<String, dynamic> json) {
  return ShootLocationData(
    location_id: json['location_id'] as int,
    organization_id: json['organization_id'] as int,
    location_active: json['location_active'] as int,
    visibility: json['visibility'] as int,
    location_name: json['location_name'] as String,
    isSelected: json['isSelected'] as bool,
    selectedIndex: json['selectedIndex'] as int,
  );
}

Map<String, dynamic> _$ShootLocationDataToJson(ShootLocationData instance) =>
    <String, dynamic>{
      'location_id': instance.location_id,
      'organization_id': instance.organization_id,
      'location_active': instance.location_active,
      'visibility': instance.visibility,
      'location_name': instance.location_name,
      'isSelected': instance.isSelected,
      'selectedIndex': instance.selectedIndex,
    };

SelectCountryModel _$SelectCountryModelFromJson(Map<String, dynamic> json) {
  return SelectCountryModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SelectCountryModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectCountryModelToJson(SelectCountryModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

SelectCountryModelData _$SelectCountryModelDataFromJson(
    Map<String, dynamic> json) {
  return SelectCountryModelData(
    country_id: json['country_id'] as int,
    country_active: json['country_active'] as int,
    isSelected: json['isSelected'] as bool,
    country_name: json['country_name'] as String,
    country_timezone: json['country_timezone'] as String,
    country_code: json['country_code'] as String,
  );
}

Map<String, dynamic> _$SelectCountryModelDataToJson(
        SelectCountryModelData instance) =>
    <String, dynamic>{
      'country_id': instance.country_id,
      'country_active': instance.country_active,
      'country_name': instance.country_name,
      'country_timezone': instance.country_timezone,
      'country_code': instance.country_code,
      'isSelected': instance.isSelected,
    };

SelectCityModel _$SelectCityModelFromJson(Map<String, dynamic> json) {
  return SelectCityModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SelectCityModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectCityModelToJson(SelectCityModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

SelectCityModelData _$SelectCityModelDataFromJson(Map<String, dynamic> json) {
  return SelectCityModelData(
    city_id: json['city_id'] as int,
    country_id: json['country_id'] as int,
    city_active: json['city_active'] as int,
    isSelected: json['isSelected'] as bool,
    country_name: json['country_name'] as String,
    city_name: json['city_name'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectCityModelDataToJson(
        SelectCityModelData instance) =>
    <String, dynamic>{
      'city_id': instance.city_id,
      'country_id': instance.country_id,
      'city_active': instance.city_active,
      'country_name': instance.country_name,
      'city_name': instance.city_name,
      'isSelected': instance.isSelected,
      'is_loading': instance.is_loading,
    };

SelectCurrencyModel _$SelectCurrencyModelFromJson(Map<String, dynamic> json) {
  return SelectCurrencyModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SelectCurrencyModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectCurrencyModelToJson(
        SelectCurrencyModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
      'data': instance.data,
    };

SelectCurrencyModelData _$SelectCurrencyModelDataFromJson(
    Map<String, dynamic> json) {
  return SelectCurrencyModelData(
    currency_id: json['currency_id'] as int,
    currency_active: json['currency_active'] as int,
    currency_code: json['currency_code'] as String,
    currency_name: json['currency_name'] as String,
  );
}

Map<String, dynamic> _$SelectCurrencyModelDataToJson(
        SelectCurrencyModelData instance) =>
    <String, dynamic>{
      'currency_id': instance.currency_id,
      'currency_active': instance.currency_active,
      'currency_code': instance.currency_code,
      'currency_name': instance.currency_name,
    };

SelectApproverModel _$SelectApproverModelFromJson(Map<String, dynamic> json) {
  return SelectApproverModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : SelectApproverModelData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SelectApproverModelToJson(
        SelectApproverModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
      'data': instance.data,
    };

SelectApproverModelData _$SelectApproverModelDataFromJson(
    Map<String, dynamic> json) {
  return SelectApproverModelData(
    map_active: json['map_active'] as int,
    map_id: json['map_id'] as int,
    user_level: json['user_level'] as int,
    department_id: json['department_id'] as int,
    user_id: json['user_id'] as int,
    user_name: json['user_name'] as String,
    isSelected: json['isSelected'] as bool,
  );
}

Map<String, dynamic> _$SelectApproverModelDataToJson(
        SelectApproverModelData instance) =>
    <String, dynamic>{
      'map_active': instance.map_active,
      'map_id': instance.map_id,
      'user_level': instance.user_level,
      'department_id': instance.department_id,
      'user_id': instance.user_id,
      'user_name': instance.user_name,
      'isSelected': instance.isSelected,
    };

SetFavouriteModel _$SetFavouriteModelFromJson(Map<String, dynamic> json) {
  return SetFavouriteModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SetFavouriteModelToJson(SetFavouriteModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'is_loading': instance.is_loading,
      'Message': instance.Message,
    };

ImageUploadRes _$ImageUploadResFromJson(Map<String, dynamic> json) {
  return ImageUploadRes(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    data: json['data'] == null
        ? null
        : ImageResData.fromJson(json['data'] as Map<String, dynamic>),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ImageUploadResToJson(ImageUploadRes instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'data': instance.data,
      'is_loading': instance.is_loading,
    };

ImageResData _$ImageResDataFromJson(Map<String, dynamic> json) {
  return ImageResData(
    image_path: json['image_path'] as String,
  )
    ..Status = json['Status'] as bool
    ..Message = json['Message'] as String
    ..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ImageResDataToJson(ImageResData instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'image_path': instance.image_path,
    };

EditProfileModel _$EditProfileModelFromJson(Map<String, dynamic> json) {
  return EditProfileModel(
    statusCode: json['statusCode'] as String,
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$EditProfileModelToJson(EditProfileModel instance) =>
    <String, dynamic>{
      'statusCode': instance.statusCode,
      'Message': instance.Message,
      'Status': instance.Status,
      'is_loading': instance.is_loading,
    };

FeedbackModel _$FeedbackModelFromJson(Map<String, dynamic> json) {
  return FeedbackModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    statusCode: json['statusCode'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$FeedbackModelToJson(FeedbackModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'statusCode': instance.statusCode,
      'is_loading': instance.is_loading,
    };

ChangePasswordModel _$ChangePasswordModelFromJson(Map<String, dynamic> json) {
  return ChangePasswordModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    statusCode: json['statusCode'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ChangePasswordModelToJson(
        ChangePasswordModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'statusCode': instance.statusCode,
      'is_loading': instance.is_loading,
    };

GetPoListChallanModel _$GetPoListChallanModelFromJson(
    Map<String, dynamic> json) {
  return GetPoListChallanModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : POListChallanData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetPoListChallanModelToJson(
        GetPoListChallanModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'data': instance.data,
    };

ForgetPasswordModel _$ForgetPasswordModelFromJson(Map<String, dynamic> json) {
  return ForgetPasswordModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    statusCode: json['statusCode'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ForgetPasswordModelToJson(
        ForgetPasswordModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'statusCode': instance.statusCode,
      'is_loading': instance.is_loading,
    };

VerifyOTPModel _$VerifyOTPModelFromJson(Map<String, dynamic> json) {
  return VerifyOTPModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    statusCode: json['statusCode'] as String,
    data: json['data'] == null
        ? null
        : VerifyOTPModelData.fromJson(json['data'] as Map<String, dynamic>),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$VerifyOTPModelToJson(VerifyOTPModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'statusCode': instance.statusCode,
      'data': instance.data,
      'is_loading': instance.is_loading,
    };

VerifyOTPModelData _$VerifyOTPModelDataFromJson(Map<String, dynamic> json) {
  return VerifyOTPModelData(
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$VerifyOTPModelDataToJson(VerifyOTPModelData instance) =>
    <String, dynamic>{
      'token': instance.token,
    };

ResetPasswordModel _$ResetPasswordModelFromJson(Map<String, dynamic> json) {
  return ResetPasswordModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    statusCode: json['statusCode'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$ResetPasswordModelToJson(ResetPasswordModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'Status': instance.Status,
      'statusCode': instance.statusCode,
      'is_loading': instance.is_loading,
    };

POListChallanData _$POListChallanDataFromJson(Map<String, dynamic> json) {
  return POListChallanData(
    user_name: json['user_name'] as String,
    po_name: json['po_name'] as String,
    purchase_id: json['purchase_id'] as int,
    expense_name: json['expense_name'] as String,
  )
    ..isSelected = json['isSelected'] as bool
    ..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$POListChallanDataToJson(POListChallanData instance) =>
    <String, dynamic>{
      'po_name': instance.po_name,
      'purchase_id': instance.purchase_id,
      'user_name': instance.user_name,
      'expense_name': instance.expense_name,
      'isSelected': instance.isSelected,
      'is_loading': instance.is_loading,
    };

SearchModel _$SearchModelFromJson(Map<String, dynamic> json) {
  return SearchModel(
    Message: json['Message'] as String,
    Status: json['Status'] as bool,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : SearchData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$SearchModelToJson(SearchModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'data': instance.data,
    };

SearchData _$SearchDataFromJson(Map<String, dynamic> json) {
  return SearchData(
    project_name: json['project_name'] as String,
    project_id: json['project_id'] as int,
    company_id: json['company_id'] as int,
    moderation: json['moderation'] as int,
    moderator_user_id: json['moderator_user_id'] as int,
    finance_user_id: json['finance_user_id'] as int,
    project_active: json['project_active'] as int,
    favourite: json['favourite'] as int,
    project_icon: json['project_icon'] as String,
    project_details: json['project_details'] as String,
    company_name: json['company_name'] as String,
  );
}

Map<String, dynamic> _$SearchDataToJson(SearchData instance) =>
    <String, dynamic>{
      'project_name': instance.project_name,
      'project_id': instance.project_id,
      'company_id': instance.company_id,
      'moderation': instance.moderation,
      'moderator_user_id': instance.moderator_user_id,
      'finance_user_id': instance.finance_user_id,
      'project_active': instance.project_active,
      'favourite': instance.favourite,
      'project_icon': instance.project_icon,
      'project_details': instance.project_details,
      'company_name': instance.company_name,
    };

GetCustomKeyModel _$GetCustomKeyModelFromJson(Map<String, dynamic> json) {
  return GetCustomKeyModel(
    Message: json['Message'] as String,
    is_loading: json['is_loading'] as bool,
    Status: json['Status'] as bool,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : CustomKeyData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$GetCustomKeyModelToJson(GetCustomKeyModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'data': instance.data,
    };

CustomKeyData _$CustomKeyDataFromJson(Map<String, dynamic> json) {
  return CustomKeyData(
    expense_id: json['expense_id'] as int,
    expense_name: json['expense_name'] as String,
    expense_active: json['expense_active'] as int,
    expense_icon: json['expense_icon'] as String,
  );
}

Map<String, dynamic> _$CustomKeyDataToJson(CustomKeyData instance) =>
    <String, dynamic>{
      'expense_id': instance.expense_id,
      'expense_name': instance.expense_name,
      'expense_icon': instance.expense_icon,
      'expense_active': instance.expense_active,
    };

GetCustomFieldModel _$GetCustomFieldModelFromJson(Map<String, dynamic> json) {
  return GetCustomFieldModel(
    Message: json['Message'] as String,
    is_loading: json['is_loading'] as bool,
    Status: json['Status'] as bool,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : CustomFieldData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$GetCustomFieldModelToJson(
        GetCustomFieldModel instance) =>
    <String, dynamic>{
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'data': instance.data,
    };

CustomFieldData _$CustomFieldDataFromJson(Map<String, dynamic> json) {
  return CustomFieldData(
    key_id: json['key_id'] as int,
    key_name: json['key_name'] as String,
    field_type_id: json['field_type_id'] as int,
    field_type: json['field_type'] as String,
  );
}

Map<String, dynamic> _$CustomFieldDataToJson(CustomFieldData instance) =>
    <String, dynamic>{
      'key_id': instance.key_id,
      'key_name': instance.key_name,
      'field_type_id': instance.field_type_id,
      'field_type': instance.field_type,
    };

GetChatGroups _$GetChatGroupsFromJson(Map<String, dynamic> json) {
  return GetChatGroups(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    rooms: (json['rooms'] as List)
        ?.map((e) => e == null
            ? null
            : ChatGroupList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetChatGroupsToJson(GetChatGroups instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'rooms': instance.rooms,
      'is_loading': instance.is_loading,
    };

ChatGroupList _$ChatGroupListFromJson(Map<String, dynamic> json) {
  return ChatGroupList(
    room_id: json['room_id'] as int,
    user_id: json['user_id'] as int,
    room_active: json['room_active'] as int,
    organization_id: json['organization_id'] as int,
    project_id: json['project_id'] as int,
    department_id: json['department_id'] as int,
    room_unique_id: json['room_unique_id'] as String,
    date_time: json['date_time'] as String,
    room_name: json['room_name'] as String,
    display_name: json['display_name'] as String,
    room_description: json['room_description'] as String,
  );
}

Map<String, dynamic> _$ChatGroupListToJson(ChatGroupList instance) =>
    <String, dynamic>{
      'room_id': instance.room_id,
      'user_id': instance.user_id,
      'room_active': instance.room_active,
      'organization_id': instance.organization_id,
      'project_id': instance.project_id,
      'department_id': instance.department_id,
      'room_unique_id': instance.room_unique_id,
      'date_time': instance.date_time,
      'room_name': instance.room_name,
      'display_name': instance.display_name,
      'room_description': instance.room_description,
    };

GetHistoryMsgModel _$GetHistoryMsgModelFromJson(Map<String, dynamic> json) {
  return GetHistoryMsgModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    chats: (json['chats'] as List)
        ?.map((e) => e == null
            ? null
            : HistoryMessages.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$GetHistoryMsgModelToJson(GetHistoryMsgModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'chats': instance.chats,
    };

HistoryMessages _$HistoryMessagesFromJson(Map<String, dynamic> json) {
  return HistoryMessages(
    date: json['date'] as String,
    ChatList: (json['ChatList'] as List)
        ?.map((e) => e == null
            ? null
            : ChatMessageList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    displayDate: json['displayDate'] as String,
  );
}

Map<String, dynamic> _$HistoryMessagesToJson(HistoryMessages instance) =>
    <String, dynamic>{
      'date': instance.date,
      'displayDate': instance.displayDate,
      'ChatList': instance.ChatList,
    };

ChatMessageList _$ChatMessageListFromJson(Map<String, dynamic> json) {
  return ChatMessageList(
    chat_message_id: json['chat_message_id'] as int,
    chat_room_id: json['chat_room_id'] as int,
    user_id: json['user_id'] as int,
    project_id: json['project_id'] as int,
    text_message: json['text_message'] as String,
    created_at: json['created_at'] as String,
    user_name: json['user_name'] as String,
    time: json['time'] as String,
  );
}

Map<String, dynamic> _$ChatMessageListToJson(ChatMessageList instance) =>
    <String, dynamic>{
      'chat_message_id': instance.chat_message_id,
      'chat_room_id': instance.chat_room_id,
      'user_id': instance.user_id,
      'project_id': instance.project_id,
      'text_message': instance.text_message,
      'user_name': instance.user_name,
      'created_at': instance.created_at,
      'time': instance.time,
    };

BehalfPeopleModel _$BehalfPeopleModelFromJson(Map<String, dynamic> json) {
  return BehalfPeopleModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : BehalfPeopleList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    project_id: json['project_id'] as String,
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$BehalfPeopleModelToJson(BehalfPeopleModel instance) =>
    <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'project_id': instance.project_id,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

BehalfPeopleList _$BehalfPeopleListFromJson(Map<String, dynamic> json) {
  return BehalfPeopleList(
    user_name: json['user_name'] as String,
    user_id: json['user_id'] as int,
    isSelected: json['isSelected'] as bool,
  );
}

Map<String, dynamic> _$BehalfPeopleListToJson(BehalfPeopleList instance) =>
    <String, dynamic>{
      'isSelected': instance.isSelected,
      'user_id': instance.user_id,
      'user_name': instance.user_name,
    };

HelpModel _$HelpModelFromJson(Map<String, dynamic> json) {
  return HelpModel(
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : HelpDataList.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..is_loading = json['is_loading'] as bool;
}

Map<String, dynamic> _$HelpModelToJson(HelpModel instance) => <String, dynamic>{
      'Status': instance.Status,
      'Message': instance.Message,
      'is_loading': instance.is_loading,
      'data': instance.data,
    };

HelpDataList _$HelpDataListFromJson(Map<String, dynamic> json) {
  return HelpDataList(
    title: json['title'] as String,
    body: json['body'] as String,
  );
}

Map<String, dynamic> _$HelpDataListToJson(HelpDataList instance) =>
    <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
    };

VendorAutoCompleteData _$VendorAutoCompleteDataFromJson(
    Map<String, dynamic> json) {
  return VendorAutoCompleteData(
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : VendorData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    status: json['status'] as bool,
    message: json['message'] as String,
    is_loading: json['is_loading'] as bool,
  );
}

Map<String, dynamic> _$VendorAutoCompleteDataToJson(
        VendorAutoCompleteData instance) =>
    <String, dynamic>{
      'data': instance.data,
      'status': instance.status,
      'message': instance.message,
      'is_loading': instance.is_loading,
    };

VendorData _$VendorDataFromJson(Map<String, dynamic> json) {
  return VendorData(
    vendor_id: json['vendor_id'] as int,
    vendor_name: json['vendor_name'] as String,
  );
}

Map<String, dynamic> _$VendorDataToJson(VendorData instance) =>
    <String, dynamic>{
      'vendor_id': instance.vendor_id,
      'vendor_name': instance.vendor_name,
    };

BudgetAlertModel _$BudgetAlertModelFromJson(Map<String, dynamic> json) {
  return BudgetAlertModel(
    is_loading: json['is_loading'] as bool,
    Status: json['Status'] as bool,
    Message: json['Message'] as String,
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : BudgetData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$BudgetAlertModelToJson(BudgetAlertModel instance) =>
    <String, dynamic>{
      'is_loading': instance.is_loading,
      'Status': instance.Status,
      'Message': instance.Message,
      'data': instance.data,
    };

BudgetData _$BudgetDataFromJson(Map<String, dynamic> json) {
  return BudgetData(
    user_name: json['user_name'] as String,
    amount: json['amount'],
    budget_amount: json['budget_amount'],
    category_name: json['category_name'] as String,
    sub_category_name: json['sub_category_name'] as String,
    reason: json['reason'] as String,
    budgetLog_created_date: json['budgetLog_created_date'] as String,
    log_id: json['log_id'] as int,
  );
}

Map<String, dynamic> _$BudgetDataToJson(BudgetData instance) =>
    <String, dynamic>{
      'log_id': instance.log_id,
      'user_name': instance.user_name,
      'amount': instance.amount,
      'budget_amount': instance.budget_amount,
      'category_name': instance.category_name,
      'sub_category_name': instance.sub_category_name,
      'reason': instance.reason,
      'budgetLog_created_date': instance.budgetLog_created_date,
    };
