
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_models.dart';




class AppSingleton {
  static final AppSingleton _singletonObj = AppSingleton._internal();

  factory AppSingleton(){
    return _singletonObj;
  }


  bool isData;
  int menuCode;
  String userName;
  String currentCity;
  int userId;
  List<UserOrganizationData> userOrgList;
  List<DashBoardUserData> userLvl1List;
  List<GetMyPoModel> pageList;
  int orgId;
  String access_token;
  bool showCashRequest = true;
  String SignatureImage = "";
  String tempToken = "";
  Color signColor;
  double signStrokeWidth;
  bool isAgainstRequest = false;
  String againstRequestText = "";

  void _getUName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userName = prefs.getString('userName');
    userId = prefs.getInt('userId');
    access_token = prefs.getString('access_token');
  }

  AppSingleton._internal(){
    menuCode = 0;
    _getUName();
    currentCity = "";
    orgId = 0;
    isData = false;
    userOrgList = [];
    userLvl1List = [];
    pageList = [];
  }

}