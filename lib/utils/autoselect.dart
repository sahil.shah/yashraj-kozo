import 'package:flutter/widgets.dart';
import 'package:yashrajkozo/utils/app_models.dart';

class Autoselect {
  static void subCategory(
    int tabIndex,
    List<SelectCategoryModelData> categoryList,
    int catId,
    TextEditingController _cateC,
    TextEditingController _subCatC,
    void Function(int categoryId) setSubCategoryId,
    void Function(VoidCallback fn) setState,
  ) {
    print('[autoSelectSubCategory] started');
    if (tabIndex == 2) {
      print('[autoSelectSubCategory] Category was closed');
      // if select category sheet was closed
      if (_cateC.text.isNotEmpty) {
        // if category has selected value
        final subCategoryList = categoryList.where((k) => k.parent_id == catId);
        print('Sub categories: $subCategoryList');
        // set matching sub category

        if (subCategoryList.isEmpty) {
          return;
        }

        String cleanString(String value) {
          return value.trim().toLowerCase().replaceAll(' ', '');
        }

        final _categoryTextForComparisons = cleanString(_cateC.text);

        bool _didModify = false;

        void _select(SelectCategoryModelData subcategory) {
          _subCatC.text = subcategory.category_name;
          // subCatId = subcategory.category_id;
          setSubCategoryId(subcategory.category_id);
          _didModify = true;
        }

        for (var i = 0; i < subCategoryList.length; i++) {
          final subCategory = subCategoryList.elementAt(i);
          print(
              '[autoSelectSubCategory] Matching ${subCategory.category_name} ${_cateC.text}');

          if (subCategory.category_name == _cateC.text) {
            print('Found exact match');
            _select(subCategory);
            _didModify = true;
            // We don't need to check others, this was an exact match
            break;
          } else if (cleanString(subCategory.category_name) ==
              _categoryTextForComparisons) {
            print('Found close match');
            _select(subCategory);
            _didModify = true;
            // Not breaking as there could be some other value which might match exact
          }
        }

        if (_didModify) {
          setState(() {
            // Update UI with above changes
          });
          subCategoryList.forEach((it) {
            it.isSelected = false;
          });
        }
      }
    }
  }
}
