import 'package:flutter/material.dart';
import 'app_singleton.dart';

AppSingleton _singleton = AppSingleton();

final HEADERS = {
  "Content-type": "application/json",
  "Accept": "application/json",
};

//const BASE_URL = "https://uat-api.gokozo.com/v2/";
//const BASE_URL = "https://dev-api.gokozo.com/v2/";
//const BASE_URL = "https://api.gokozo.com/v2/";
//const BASE_URL = "https://dev1-api.gokozo.com/v2/";
//const BASE_URL = "https://yashrajgokozo.binarynumbers.io/v2/";
const BASE_URL = "https://api.yrf.in/v2/";

final Color appColor = Color(0xFFED1C24);
final Color borderColor = Colors.transparent;
final title = "Yash Raj Films";
Color lightGrey = Color(0xffF0F4F8);