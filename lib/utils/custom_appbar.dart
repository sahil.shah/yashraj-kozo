import 'package:flutter/material.dart';
import 'constants.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget{

  String title;
  List<Widget> action;
  Widget leading;

  CustomAppBar({this.title, this.action, this.leading});

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    return Container(
      child: AppBar(
        elevation: 5,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back, color: appColor,),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(title, style: TextStyle(fontFamily: 'SanSemiBold', fontSize: h*0.02, color: appColor), textAlign: TextAlign.center,),
            SizedBox(width: w*0.085,)
          ],
        ),
        centerTitle: true,
        actions: action,
        backgroundColor: Colors.white,
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
