import 'package:flutter/material.dart';
import 'constants.dart';

class CustomBtn extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final onTap;

  CustomBtn({this.title, this.onTap});

  @override
  Widget build(BuildContext context) {

    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Container(
      width: w,
      height: h*0.055,
      child: RaisedButton(
        splashColor: Colors.white,
          onPressed: onTap,
          color: appColor,
          child: Text(
            title,
            style: TextStyle(color: Colors.white, fontFamily: 'SanSemiBold', fontSize: h*0.019),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(0)))),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => null;
}
