
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomPopUp extends StatefulWidget {
  final String msg;
  bool fromPending;
  //fromPage = 1  is edit profile
  CustomPopUp({this.msg, this.fromPending = false});

  @override
  _CustomPopUpState createState() => _CustomPopUpState();
}

class _CustomPopUpState extends State<CustomPopUp> {

  var isTap = true;
  final String assetName1 = 'assets/images/pop_close.svg';
  String imgUrl = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    imgUrl = 'assets/images/check.png';
  }

  back() {
    if(widget.fromPending ?? false) {
      Navigator.pop(context);
      Navigator.pop(context);
      Navigator.of(context).pop({'is_done': true});
    } else {
      Navigator.popUntil(context, ModalRoute.withName('/home'));
    }
  }

  @override
  Widget build(BuildContext context) {
    final w = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: (){
        return back();
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
//      color: Colors.red,
          child: SingleChildScrollView(
            child: Center(
              child: Material(
//          borderRadius: BorderRadius.circular(8.0),
                color: Colors.white,
                elevation: 5.0,
                child: Container(
                  width: w * .80,
//            padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
//            margin: EdgeInsets.only(left: 32,right: 32),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Stack(
                        alignment: Alignment.centerRight,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 50,
                            child: Center(child: _titleWidget()),
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: <Color>[
                                    Color(0xffc4c5c7),
                                    Color(0xffdcdddf),
                                    Color(0xffebebeb)
                                  ],
                                )),
                          ),
                          Container(
                            width: 60,
                            child: FlatButton(onPressed: (){
                              if(widget.fromPending ?? false) {
                                Navigator.pop(context);
                                Navigator.pop(context);
                                Navigator.of(context).pop({'is_done': true});
                              } else {
                                Navigator.popUntil(context, ModalRoute.withName('/home'));
                              }
                            }, child:
                            SvgPicture.asset(assetName1,
                                height: 14,
                                fit: BoxFit.cover,
                                color:Colors.black,
                                semanticsLabel: 'popup close'),),
                          )
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.fromLTRB(24, 16, 23, 24),
                          child: Column(
                            children: <Widget>[
                              _imageWidget(),
                              SizedBox(
                                height: 8,
                              ),
                              Center(
                                child: _contentWidget(),
                              ),
                            ],
                          ))
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _titleWidget() {
    return
      Text(
        "SUCCESS!",
        style: TextStyle(
            fontFamily:'SanBold',
            fontSize: 15,
            color: Colors.black),
        textAlign: TextAlign.center,
      );
  }

//  Widget _imageWidget() {
//    return SvgPicture.asset('assets/images/warning.svg',
//        height: 28,
//        fit: BoxFit.cover,
//
//        semanticsLabel: 'popup close');
//  }
  Widget _imageWidget() {
    return Image(image: AssetImage(imgUrl), height: 48,);
  }

  Widget _contentWidget() {
    return Text(
      widget.msg ?? "You cannot like your own comments.",
      style: TextStyle(
          fontSize: 14,
          color: Colors.black,
          fontFamily:
          'SanMedium'),
      textAlign: TextAlign.center,
    );
  }
}
