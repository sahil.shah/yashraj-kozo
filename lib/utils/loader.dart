import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'constants.dart';

class Loader extends StatelessWidget {
  bool bar_visibility = true;
  String title;
  Loader({this.title, this.bar_visibility = true});
  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: bar_visibility ? AppBar(
            elevation: 5,
            title: w < 600 ? Image.asset('assets/images/logo.png',
                height: h*0.06, fit: BoxFit.cover) : Image.asset('assets/images/logo.png',
                height: h*0.048, fit: BoxFit.cover),
            centerTitle: true,
            backgroundColor: Colors.white,
            leading: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Icon(Icons.arrow_back, color: appColor,),
            ),
          ) : null,
          body: Center(child: CircularProgressIndicator())
      ),
    );
  }
}