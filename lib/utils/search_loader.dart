import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'constants.dart';

class SearchLoader extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    return Container(
      height: h*.70,
      child: Center(child: CircularProgressIndicator()),
    );
  }
}
