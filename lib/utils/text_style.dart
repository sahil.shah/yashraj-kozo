import 'package:flutter/material.dart';

import 'constants.dart';

class Styles {
  static final baseTextStyle = const TextStyle(
      fontFamily: 'Poppins'
  );
  static final smallTextStyle = commonTextStyle.copyWith(
    fontSize: 9.0,
  );
  static final commonTextStyle = baseTextStyle.copyWith(
      color: const Color(0xffb6b2df),
      fontSize: 16.0,
      fontWeight: FontWeight.w400,
  );
  static final commonTextStyle2 = baseTextStyle.copyWith(
      color:  Colors.black,
      fontSize: 14.0,
      fontWeight: FontWeight.w400
  );

  static final titleTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 18.0,
      fontWeight: FontWeight.w600
  );
  static final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 20.0,
      fontWeight: FontWeight.w400
  );

  static final homeTitleTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 22.0,
      fontWeight: FontWeight.w600
  );

  static final projectListTextStyle = baseTextStyle.copyWith(
      color: Colors.white,
      fontSize: 18.0,
      fontWeight: FontWeight.w600
  );

  static final homecommonTextStyle = baseTextStyle.copyWith(
    color: Colors.black,
    fontSize: 28.0,
    fontWeight: FontWeight.w400,
  );

  static final formInputTextStyle = baseTextStyle.copyWith(
    color: Colors.black,
    fontFamily: 'SanRegular',
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
  );

  static final notificationcommonTextStyle = baseTextStyle.copyWith(
    color: Colors.white,
    fontSize: 15.0,
    fontWeight: FontWeight.w400,
  );

  static final detailInfoDesTextStyle = baseTextStyle.copyWith(
      color: Colors.black,
      fontSize: 18.0,
      fontWeight: FontWeight.w500
  );
}

InputDecoration textDecoration(String label) {
  return InputDecoration(
    hasFloatingPlaceholder: true,
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(20))),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(20))),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(20))),
    focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(20))),
    // hintText: 'Enter your product title',
    labelStyle: TextStyle(color: appColor),
    errorStyle: TextStyle(
      color: Colors.black,
    ),

    labelText: label,
  );
}