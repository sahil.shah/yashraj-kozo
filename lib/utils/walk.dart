import 'dart:io';

import 'package:yashrajkozo/utils/app_singleton.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/rendering.dart';
import 'package:yashrajkozo/utils/constants.dart';
import 'package:yashrajkozo/utils/custom_appbar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:url_launcher/url_launcher.dart';

class CarouselDemo extends StatefulWidget {
  int fromPage;

  List<String> imgUrl;

  CarouselDemo({this.fromPage, this.imgUrl});

  @override
  CarouselDemoState createState() => CarouselDemoState();
}

class CarouselDemoState extends State<CarouselDemo> {
  //
  CarouselSlider carouselSlider;
  int _current = 0;
  String deleteUrl;

  List<String> formatList = [
    'pdf',
    'xls',
    'txt',
    'csv',
    'xlsx',
    'ods',
    'xlr',
    'doc',
    'odt',
  ];

  AppSingleton _singleton = AppSingleton();
  /* List imgList = [
    "assets/images/login.png",
    "assets/images/bg.png",

  ];*/

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  back() async {
    // 1 is from po review
    //2 is from create expense
    //3 is from create challan
    //4 is from create po

    if (widget.fromPage == 1) {
      return Navigator.popUntil(context, ModalRoute.withName('/po_review'));
    } else if (widget.fromPage == 2) {
      Navigator.of(context).pop({'approver_list': widget.imgUrl});
    }   else if (widget.fromPage == 3) {
      Navigator.of(context)
          .pop({'approver_list': widget.imgUrl});
//      return Navigator.popUntil(context, ModalRoute.withName('/create_challan'));

    }else if (widget.fromPage == 4) {
      Navigator.of(context)
          .pop({'approver_list': widget.imgUrl});
//      return Navigator.popUntil(context, ModalRoute.withName('/create_po'));

    }
  }

  @override
  Widget build(BuildContext context) {
    final h = MediaQuery.of(context).size.height;
    final w = MediaQuery.of(context).size.width;

    return WillPopScope(
      onWillPop: () {
        return back();
      },
      child: Scaffold(
        appBar: AppBar(
          title: w < 600 ? Image.asset('assets/images/logo.png',
              height: h*0.06, fit: BoxFit.cover) : Image.asset('assets/images/logo.png',
              height: h*0.048, fit: BoxFit.cover),
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back, color: appColor,),
          ),
        ),
        body: SafeArea(
          child: Container(
            child: widget.imgUrl.length > 0
                ? Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                carouselSlider = CarouselSlider(
                  height: h,
                  initialPage: 0,
                  aspectRatio: 1.0,
                  viewportFraction: 1.0,
                  enlargeCenterPage: false,
                  autoPlay: false,
                  reverse: false,
                  enableInfiniteScroll: false,
//                  autoPlayInterval: Duration(seconds: 2),
//                  autoPlayAnimationDuration: Duration(milliseconds: 3000),
//                  pauseAutoPlayOnTouch: Duration(seconds: 10),
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (index) {
                    setState(() {
                      _current = index;
                    });
                  },
                  items: widget.imgUrl.map((imgUrl) {
                    deleteUrl = imgUrl;

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              Builder(
                                builder: (BuildContext context) {
                                  return Container(
                                    width: w,
                                    height: Platform.isIOS ? (h-(h*0.21)) : (h - (h*0.153)),
                                    child: Center(
                                      child: PhotoView(
                                        imageProvider: networkWidget(imgUrl),
                                      ),

                                      /*Image(
                                         fit: BoxFit.contain,
                                          image: AssetImage(
                                        imgUrl,
                                      )),*/
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: h*0.01),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: map<Widget>(widget.imgUrl, (index, url) {
                      return Container(
                        width: h*0.01,
                        height: h*0.01,
                        margin: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 2.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _current == index
                              ? Colors.redAccent
                              : Colors.black,
                        ),
                      );
                    }),
                  ),
                ),
                widget.fromPage == 1
                    ?  Positioned(
                  right: w*0.0377,
                  top: h*0.0188,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.black),
                        borderRadius:
                        BorderRadius.all(Radius.circular(h*0.0094))),
                    child: FlatButton(onPressed: (){
                      _launchURL("${widget.imgUrl[_current]}?token=${_singleton.tempToken}");
                    }, child: Text("PREVIEW")),
                  ),
                )
                    : Positioned(
                  right: w*0.0377,
                  top: h*0.0188,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                        BorderRadius.all(Radius.circular(h*0.0094))),
                    width: h*0.0541,
                    height: h*0.0541,
                    child: IconButton(
                      icon: Icon(FontAwesomeIcons.trash),
                      onPressed: () {

                        setState(() {
                          widget.imgUrl.removeAt(_current);
                          if (widget.imgUrl.length == 0) {
                            Navigator.of(context).pop(
                                {'approver_list': widget.imgUrl});
                          } else {
                            _current = widget.imgUrl.length - 1;
                          }
                        });
                      },
                    ),
                  ),
                ),
                Positioned(
                    left: w*0.056,
                    child: FlatButton(
                        onPressed: () {
                          _navFun(1);
                        },
                        child: Text(
                          "PREV",
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'SanBold',
                              fontSize: h*0.018),
                        ))),
                Positioned(
                    right: w*0.056,
                    child: FlatButton(
                        onPressed: () {
                          _navFun(2);
                        },
                        child: Text(
                          customText(),
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'SanBold',
                              fontSize: 15),
                        )))
              ],
            )
                : Container(
              width: w,
              height: h - (h*0.153),
              child: Center(
                child: widget.imgUrl.length > 0
                    ? PhotoView(
                  imageProvider: NetworkImage("${widget.imgUrl[0]}?token=${_singleton.tempToken}"),
                )
                    : Container(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  networkWidget(String imgUrl){
    return NetworkImage((imgUrl.contains('png') || imgUrl.contains('jpeg') || imgUrl.contains("jpg") || imgUrl.contains("tif")) ? "$imgUrl?token=${_singleton.tempToken}" : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
    );

    /*  for(int k =0; k < formatList.length; k++){
      return NetworkImage(imgUrl.contains(formatList[k]) ? "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuSKs7ZZ7FbLAT1shDmB8lraUnzY0wuhVFyo7BaQHOHyOZ63gW"
          : imgUrl);
    }*/

    /* setState(() {
    });*/
  }

  String customText() {
    return "NEXT";
  }

  _navFun(int i) async {
    if (i == 1) {
      carouselSlider.previousPage(
          duration: Duration(milliseconds: 300), curve: Curves.linear);
    } else {
      carouselSlider.nextPage(
          duration: Duration(milliseconds: 300), curve: Curves.linear);
    }
  }

  _launchURL(String d) async {

    if (await canLaunch(d)) {
      await launch(d);
    } else {
      throw 'Could not launch $d';
    }
  }
}